//用户名不为空的情况下验证只能为  中文，英文，数字并且长度不超过10
function loginIdCheck(textId){
	var loginId = $("#"+textId).val();
	if(loginId.length == 0){
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 数字、字母、或中文组合,长度不超过10</span>');
		return false;
	}else{
        reg = /^[\u4e00-\u9fa5_a-zA-Z0-9]{1,10}$/;
        if(!reg.test(loginId)){
        	$("#"+textId+"_Html").html('');
    		$("#"+textId+"_Html").html('<span style="color: red">* 数字、字母、或中文组合,长度不超过10</span>');
    		return false;
        }else{
        	$("#"+textId+"_Html").html('<span style="color: green">*</span>');
        	return true;
        }
    }
}
//判断密码
function passwordCheck(textId){
	var password = $("#"+textId).val();
	reg = /^[a-zA-Z0-9]{1,10}$/;
	if(password.length !=0){
		if(reg.test(password)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">* 字母、数字,长度不超过10</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 字母、数字,长度不超过10</span>');
		return false;
	}
}
//判断英文名，必须有由英文字符组成
function userEnameCheck(textId){
	var userEname = $("#"+textId).val();
	if(userEname.length != 0){
		reg = /^[a-zA-Z]{1,10}$/;
		if(reg.test(userEname)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">* 字母,长度不超过10</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 字母,长度不超过10</span>');
		return false;
	}
}
//判断中文名，必须由中文字符组成
function userCnameCheck(textId){
	var userCname = $("#"+textId).val();
	if(userCname.length != 0){
		reg = /^[\u4e00-\u9fa5]{1,10}$/;
		if(reg.test(userCname)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">* 中文,长度不超过10</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 中文,长度不超过10</span>');
		return false;
	}
}

//判断是否选择了部门
function departmentCheck(textId){
	var department = $("#"+textId).combobox('getValue');
	if(department.length != 0){
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: green">*</span>');
		return true;
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">*请选择部门</span>');
		return false;
	}
}

//是否输入ip
function ipCheck(textId){
	var ip = $("#"+textId).val();
	if(ip.length != 0){
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: green">*</span>');
		return true;
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">*请输入ip地址</span>');
		return false;
	}
}

//判断联系电话，座机   例如：0575-85627232
function teleCheck(textId){
	var tele = $("#"+textId).val();
	if(tele.length != 0){
		reg = /^\d{3,4}-\d{7,8}$/;
		if(reg.test(tele)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">* 数字</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 数字</span>');
		return false;
	}
}

//判断移动电话
function phoneCheck(textId){
	var phone = $("#"+textId).val();
	if(phone.length != 0){
		reg = /^1[3|4|5|8][0-9]\d{8}$/;
		if(reg.test(phone)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">* 数字</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">* 数字</span>');
		return false;
	}
}
//判断邮箱
function emailCheck(textId){
	var email = $("#"+textId).val();
	if(email.length != 0){
		reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;
		if(reg.test(email)){
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: green">*</span>');
			return true;
		}else{
			$("#"+textId+"_Html").html('');
			$("#"+textId+"_Html").html('<span style="color: red">*</span>');
			return false;
		}
	}else{
		$("#"+textId+"_Html").html('');
		$("#"+textId+"_Html").html('<span style="color: red">*</span>');
		return false;
	}
}

//判断中国居民身份证号码
function idCardCheck(textId){
     var idCard = $("#"+textId).val();
     //var city={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
     var city = ['11','12','13','14','15','21','22','23','31','32','33','34','35','36','37','41','42','43','44','45','46','50','51','52','53','54','61','62','63','64','65','71','81','82','91'];
     var areaCode = idCard.substring(0,2);
     var idCardResult = false;
     if(idCard.length == 15){
    	 for(var i = 0;i<city.length;i++){
    		 if(areaCode == city[i]){
    			 idCardResult = true;
    			 break;
    		 }
    	 }
    	 
    	 if(idCardResult){
    		 reg = /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)?$/;
        	 if(reg.test(idCard)){
        		 $("#"+textId+"_Html").html('');
         		 $("#"+textId+"_Html").html('<span style="color: green">*</span>');
        		return true;
        	 }else{
        		 $("#"+textId+"_Html").html('');
         		 $("#"+textId+"_Html").html('<span style="color: red">*</span>');
        		 return false;
        	 }
    	 }else{
    		 return false;
    	 }
     }else if(idCard.length == 18){
    	 for(var i = 0;i<city.length;i++){
    		 if(areaCode == city[i]){
    			 idCardResult = true;
    			 break;
    		 }
    	 }
    	 
    	 if(idCardResult){
    		 reg = /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)?$/;
        	 if(reg.test(idCard)){
        		 $("#"+textId+"_Html").html('');
         		 $("#"+textId+"_Html").html('<span style="color: green">*</span>');
        		return true;
        	 }else{
        		 $("#"+textId+"_Html").html('');
         		 $("#"+textId+"_Html").html('<span style="color: red">*</span>');
        		 return false;
        	 }    
    	 }else{
    		 return false;
    	 }
     }else{
    	 $("#"+textId+"_Html").html('');
 		 $("#"+textId+"_Html").html('<span style="color: red">*</span>');
 		 return false;
     }
}

//编码
function encode(str){
	   return encodeURI(str);
}


//获取当前时间
function getNowFormatDate(){
	var myDate = new Date();
	var year = myDate.getFullYear(); //获取完整的年份(4位,1970-????)
	var month = myDate.getMonth() + 1; //获取当前月份(0-11,0代表1月)
	var day = myDate.getDate(); //获取当前日(1-31)
	var hour = myDate.getHours(); //获取当前小时数(0-23)
	var minute = myDate.getMinutes(); //获取当前分钟数(0-59)
	var second = myDate.getSeconds(); //获取当前秒数(0-59)
	return year+'-'+(month < 10 ? '0'+month : month)+'-'+ (day <10 ? '0'+day : day) +' '+(hour<10 ? '0'+hour : hour)+':'+(minute<10 ?'0'+minute : minute)+':'+(second<10 ?'0'+second : second);
}
