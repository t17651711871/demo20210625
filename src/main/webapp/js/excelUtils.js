/**
 * 打开Excel 文件选择框
 */
function openExcelSelectDialog() {
    document.getElementById("importbtn").disabled = "disabled";
    $('#excelfile').val('');
    $('#importExcel').dialog('open').dialog('center');
}


/**
 * excel 导入文件选择
 */
function selectExcelOnchange() {
    var file = document.getElementById("excelfile").files[0];
    if (file == null) {
        document.getElementById("importbtn").disabled = "disabled";
    } else {
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx') {
            if (file) {
                document.getElementById("importbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！", "error");
            document.getElementById("importbtn").disabled = "disabled";
        }
    }
}


/**
 * 导入 excel
 */
function excelImport(entityName) {
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'importExcel' + entityName,
        fileElementId: 'excelfile',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
            } else {
                $.messager.alert('提示', "导入失败", 'error');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
        }
    });
}

function downloadExcelTemplate(entityName){
    $.messager.confirm('操作提示', "确认下载?", function (r) {
        if (r) {
            window.location.href = "downloadExcelTemplate"+entityName;
        }
    });
}