<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.common.util.VariableUtils"%>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
	Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>人员信息</title>
<%@include file="head.jsp"%>
</head>
<body>
	<table class="easyui-datagrid" title="人员信息" id="dg1"
		data-options="singleSelect:true,rownumbers:true,toolbar:'#tb',autoRowHeight:false,pagination:true,pageSize:25,pageList:[15,25,35]"
		style="height: 460px">
		<thead>
			<tr>
				<th data-options="field:'id',width:80,align:'center',hidden:true">ID</th>
				<th data-options="field:'handlename',width:100,align:'center',hidden:true">操作名</th>
				<th data-options="field:'loginId',width:80,align:'center'">登录账号</th>
				<th data-options="field:'userEname',width:80,align:'center'">用户英文名</th>
				<th data-options="field:'userCname',width:80,align:'center'">用户中文名</th>
				<th data-options="field:'password',width:100,align:'center',hidden:true">密码</th>
				<th data-options="field:'orgId',width:200,align:'center'">机构编号</th>
				<th data-options="field:'orgName',width:160,align:'center'">机构名称</th>
				<th data-options="field:'departId',width:80,align:'center',hidden:true">部门ID</th>
				<th data-options="field:'departmentName',width:80,align:'center'">部门名称</th>
				<th data-options="field:'tel',width:80,align:'center',hidden:true">联系电话</th>
				<th data-options="field:'mobile',width:150,align:'center'">移动电话</th>
				<th data-options="field:'address',width:100,align:'center'">地址</th>
				<th data-options="field:'email',width:160,align:'center'">邮箱</th>
				<th data-options="field:'isLocked',width:160,align:'center'">是否锁定用户</th>
				<th data-options="field:'userLockedReson',width:160,align:'center'">锁定原因</th>
				<th data-options="field:'startDate',width:80,align:'center',hidden:true">开始时间</th>
				<th data-options="field:'endDate',width:80,align:'center',hidden:true">结束时间</th>
				<th data-options="field:'createTime',width:140,align:'center'">创建时间</th>
				<th data-options="field:'description',width:160,align:'center'">修改明细</th>
				<th data-options="field:'enabled',width:100,align:'center',hidden:true">启用标识</th>
				<th data-options="field:'isDelete',width:80,align:'center',hidden:true">是否删除</th>
				<th data-options="field:'lastLoginDate',width:140,align:'center'">最后登录时间</th>
				<th data-options="field:'lastModifyDate',width:140,align:'center'">最后修改密码日期</th>
				<th data-options="field:'isFirstLogin',width:120,align:'center'">是否为首次登录</th>
				<th data-options="field:'handleperson',width:100,align:'center'">操作人</th>
				<th data-options="field:'handledate',width:140,align:'center'">操作时间</th>
				<th data-options="field:'ip',width:140,align:'center',hidden:true">用户使用的ip地址</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${list}" var="li">
				<tr>
					<td>${li.id}</td>
					<td>${li.handlename}</td>
					<td>${li.loginid}</td>
					<td>${li.userename}</td>
					<td>${li.usercname}</td>
					<td>${li.password}</td>
					<td>${li.orgid}</td>
					<td>${li.orgname}</td>
					<td>${li.departid}</td>
					<td>${li.departmentname}</td>
					<td>${li.tel}</td>
					<td>${li.mobile}</td>
					<td>${li.address}</td>
					<td>${li.email}</td>
					<td><c:if test="${li.islocked == 'N'}">
							<a href="javascript:void(0);" style="text-decoration: none"
								onclick="doLocked('${li.handlename}','${li.id}','${li.islocked}')">未锁定</a>
						</c:if> <c:if test="${li.islocked == 'Y'}">
							<a href="javascript:void(0);" style="text-decoration: none"
								onclick="doLocked('${li.handlename}','${li.id}','${li.islocked}')"><span
								style="color: red">已锁定</span></a>
						</c:if></td>
					<td>${li.userlockedreson}</td>
					<td>${li.startdate}</td>
					<td>${li.enddate}</td>
					<td>${li.createtime}</td>
					<td>${li.description}</td>
					<td>${li.enabled}</td>
					<td>
					  <c:if test="${li.isdelete == 'N'}">
                                                                         未删除
                      </c:if> 
                      <c:if test="${li.isdelete == 'Y'}">
                                                                         已删除
                      </c:if>
                    </td>
					<td>${li.lastlogindate}</td>
					<td>${li.lastmodifydate}</td>
					<td>
					   <c:if test="${li.isfirstlogin == 'Y'}">
                                                                           首次登录
                       </c:if> 
                       <c:if test="${li.isfirstlogin == 'N'}">
                                                                          多次登录
                       </c:if>
                    </td>
                    <td>${li.handleperson}</td>
                    <td>${li.handledate}</td>
                    <td>${li.ip}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<div id="tb" style="padding: 5px; height: auto;">
		用户中文名: <input type="text" style="width:173px; height: 21px" id="userCname" name="userCname" value="${userCname}">&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByName()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-add" onclick="addUser()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="editUser()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="editPasswordUser()">重置用户密码</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		
		<!-- <a class="easyui-linkbutton" iconCls="icon-remove" onclick="remove()">删除用户</a> -->
		<!-- <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel()">导出</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a> -->
	</div>





	<!-- 新增用户dialog -->
	<div style="visibility: hidden;">
		<div id="addDialog" class="easyui-dialog" title="新增用户" data-options="iconCls:'icon-save',toolbar:'#tbForAddDialog'" style="width: 600px; height: 400px; align-items: center;">
			<!--   新增对话框的工具栏  -->
			<div id="tbForAddDialog" style="padding-left: 30px; padding-top: 10px">
				<a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForAddDialog()">保存</a>&nbsp;&nbsp; 
				<a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForAdd()">重置</a>
			</div>

			<!-- 信息录入 -->
			<form id="formForAddUser" method="post">
				<div style="padding-top: 20px">
					<table style="padding-left: 30px;font-size: 8px;">
						<tr>
							<td align="right">登录账号:</td>
							<td><input type="text" name="loginId_add" id="loginId_add" onblur="loginIdValidate('loginId_add')" style="height: 21px; width: 173px" /></td>
							<td style="text-align: left; font-size: small;" id="loginId_add_Html"><span style="color: red">*数字、字母、或中文组合,长度不超过10</span></td>
						</tr>

						<tr>
							<td align="right">用户英文名:</td>
							<td><input type="text" name="userEname_add" id="userEname_add" onblur="userEnameValidate('userEname_add')" style="height: 21px; width: 173px" /></td>
							<td style="text-align: left; font-size: small;" id="userEname_add_Html"><span style="color: red">*字母,长度不超过50</span></td>
						</tr>

						<tr>
							<td align="right">用户中文名:</td>
							<td><input type="text" name="userCname_add" id="userCname_add" onblur="userCnameValidate('userCname_add')" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">机构代码:</td>
							<td>
							  <select class="easyui-combobox" name="orgId_add" id="orgId_add" editable=false style="height: 21px; width: 173px">
									<!-- <option value="" selected="selected">---请选择机构代码---</option> -->
									<c:forEach items="${orgList}" var="ol">
										<option value="${ol.orgId}">${ol.orgId}-${ol.orgName}</option>
									</c:forEach>
							  </select></td>
						</tr>

						<tr>
							<td align="right">部门名称:</td>
							<td>
							  <select class="easyui-combobox" name="departId_add"
								id="departId_add" editable=false
								style="height: 21px; width: 173px" data-options="onSelect:function(record){departmentValidate('departId_add');}">
									<option value="" selected="selected">---请选择部门---</option>
									<c:forEach items="${departmentList}" var="dl">
										<option value="${dl.departmentId}">${dl.departmentName}</option>
									</c:forEach>
							  </select></td>
							<td style="text-align: left; font-size: small;" id="departId_add_Html"><span style="color: red">*请选择部门</span></td>
						</tr>

						<tr>
							<td align="right">联系电话:</td>
							<td><input type="text" name="tel_add" id="tel_add" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">移动电话:</td>
							<td><input type="text" name="mobile_add" id="mobile_add" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">地址:</td>
							<td><input type="text" name="address_add" id="address_add" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">邮箱:</td>
							<td><input type="text" name="email_add" id="email_add" style="height: 21px; width: 173px" /></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>



	<!-- 修改用户dialog -->
	<div style="visibility: hidden;">
		<div id="editDialog" class="easyui-dialog" title="修改用户"
			data-options="iconCls:'icon-save',toolbar:'tbForEditDialog'"
			style="width: 600px; height: 400px; align-items: center;">
			<!--   新增对话框的工具栏  -->
			<div id="tbForEditDialog"
				style="padding-left: 30px; padding-top: 10px">
				<a class="easyui-linkbutton" data-options="iconCls:'icon-save'"
					onclick="saveForEditDialog()">保存</a>&nbsp;&nbsp; <a
					class="easyui-linkbutton" data-options="iconCls:'icon-undo'"
					onclick="resetForEdit()">重置</a>
			</div>

			<!-- 信息录入 -->
			<form id="formForEditUser" method="post">
				<div style="padding-top: 20px">
					<table style="padding-left: 30px;font-size: 8px;">
					    <input type="hidden" name="editId" id="editId" />
						<input type="hidden" name="isEditLoginId" id="isEditLoginId" />
						
						<tr>
							<td align="right">登录账号:</td>
							<td><input type="text" name="loginId_edit"
								id="loginId_edit" onblur="loginIdValidate('loginId_edit')"
								style="height: 21px; width: 173px" /></td>
							<td style="text-align: left; font-size: small;"
								id="loginId_edit_Html"><span style="color: red">*
									数字、字母、或中文组合,长度不超过10</span></td>
						</tr>


						<tr>
							<td align="right">用户英文名:</td>
							<td><input type="text" name="userEname_edit"
								id="userEname_edit" onblur="userEnameValidate('userEname_edit')"
								style="height: 21px; width: 173px" /></td>
							<td style="text-align: left; font-size: small;"
								id="userEname_edit_Html"><span style="color: red">*
									字母,长度不超过50</span></td>
						</tr>

						<tr>
							<td align="right">用户中文名:</td>
							<td><input type="text" name="userCname_edit"
								id="userCname_edit" onblur="userCnameValidate('userCname_edit')"
								style="height: 21px; width: 173px" /></td>
						</tr>

						<!-- <tr>
				     <td align="right">密码:</td>
				     <td><input class="easyui-textbox" name="password_add" id="password_add" missingMessage="密码不能为空" data-options="events:{blur:function(){passwordValidate('password_add')}}" style="height: 32px;width: 173px"/></td>
				     <td style="text-align: left;font-size: small;" id="password_add_Html"><span style="color: red">* 字母、数字,长度不超过10</span></td>
				   </tr> -->

						<tr>
							<td align="right">机构代码:</td>
							<td><select class="easyui-combobox" name="orgId_edit"
								id="orgId_edit" editable=false style="height: 21px; width: 173px">
									<!-- <option value="" selected="selected">---请选择机构代码---</option> -->
									<c:forEach items="${orgList}" var="ol">
										<option value="${ol.orgId}">${ol.orgId}-${ol.orgName}</option>
									</c:forEach>
							</select></td>
						</tr>

						<tr>
							<td align="right">部门名称:</td>
							<td><select class="easyui-combobox" name="departId_edit"
								id="departId_edit" editable=false
								style="height: 21px; width: 173px" data-options="onSelect:function(record){departmentValidate('departId_edit');}">
									<option value="" selected="selected">---请选择部门---</option>
									<c:forEach items="${departmentList}" var="dl">
										<option value="${dl.departmentId}">${dl.departmentName}</option>
									</c:forEach>
							</select></td>
							<td style="text-align: left; font-size: small;" id="departId_edit_Html"><span style="color: red">*请选择部门</span></td>
						</tr>

						<tr>
							<td align="right">联系电话:</td>
							<td><input type="text" name="tel_edit"
								id="tel_edit" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">移动电话:</td>
							<td><input type="text" name="mobile_edit"
								id="mobile_edit" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">地址:</td>
							<td><input type="text" name="address_edit"
								id="address_edit" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">邮箱:</td>
							<td><input type="text" name="email_edit"
								id="email_edit" style="height: 21px; width: 173px" /></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>

	<!-- 导入文件dialog -->
	<div style="visibility: hidden;">
		<div id="importFileDialog" class="easyui-dialog"
			style="width: 500px; height: 220px; padding-left: 30px" title="导入文件">
			<h4>附件上传：</h4>
			<form enctype="multipart/form-data" id="importform">
				<table id=test border="0">
					<tr>
						<td><input type="file" name="file_info" id="file_info" size="60"  accept="xsl">&nbsp;</td>
					</tr>
				</table>
				<button onclick="addline();">单击此处添加更多附件</button>
				<br> <input type="button" value="提交" onClick="uploadFile()">
			</form>
		</div>
	</div>

	<!-- 导入excel文档后显示错误信息 -->

	<script type="text/javascript">
		var loginIdResult;
		var userEnameResult;
		var departmentResult;
		var passwordReslt;

		function loginIdValidate(textId) {
			loginIdResult = loginIdCheck(textId);
		}

		function userEnameValidate(textId) {
			userEnameResult = userEnameCheck(textId);
		}
		
		function departmentValidate(textId) {
			
			departmentResult = departmentCheck(textId);
		}

		/* function passwordValidate(textId){
			passwordReslt = passwordCheck(textId);
		} */

		var loginIdBackUp;//保存要修改的用户的登录账号  

		$(function() {
			$('#addDialog').dialog('close');
			$('#editDialog').dialog('close');
			$('#importFileDialog').dialog('close');

			$('#dg1').datagrid({loadFilter : pagerFilter});//.datagrid('loadData', getData());
			$('.datagrid-header-check').find('input').attr('style','display:none');
		});

		function pagerFilter(data) {
			if (typeof data.length == 'number'
					&& typeof data.splice == 'function') { // is array
				data = {
					total : data.length,
					rows : data
				}
			}
			var dg = $(this);
			var opts = dg.datagrid('options');
			var pager = dg.datagrid('getPager');
			pager.pagination({
				onSelectPage : function(pageNum, pageSize) {
					opts.pageNumber = pageNum;
					opts.pageSize = pageSize;
					pager.pagination('refresh', {
						pageNumber : pageNum,
						pageSize : pageSize
					});
					dg.datagrid('loadData', data);
				}
			});
			if (!data.originalRows) {
				data.originalRows = (data.rows);
			}
			var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
			var end = start + parseInt(opts.pageSize);
			data.rows = (data.originalRows.slice(start, end));
			return data;
		}
		
		//同意申请
		function agreenApply(){
			var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
			if(row != null){
				var applyname = row.handlename;
				if(applyname.substring(0,2) == '申请'){
					$.ajax({
						url : 'agreenApply',
						type : 'POST', //GET
						async : true, //或false,是否异步
						contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
						data : {"id":row.id,"applyname":applyname},
						dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
						success : function(data) {
							if(applyname == '申请新增'){
								if (data == '1') {
									$.messager.alert('','同意新增成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','同意新增失败','error');
								}
							}else if(applyname == '申请修改'){
								if (data == '1') {
									$.messager.alert('','同意修改成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','同意修改失败','error');
								}
							}else if(applyname == '申请重置密码'){
								if (data == '1') {
									$.messager.alert('','同意重置密码成功,该用户密码已重置为初始密码','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','同意重置密码失败','error');
								}
							}else if(applyname == '申请锁定'){
								if (data == '1') {
									$.messager.alert('','同意锁定成功,该用户已被锁定','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','同意锁定失败','error');
								}
							}else if(applyname == '申请解锁'){
								if (data == '1') {
									$.messager.alert('','同意解锁成功,该用户已被解锁','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','同意解锁失败','error');
								}
							}else{
								
							}
						}
					});
				}else{
					$.messager.alert('操作提示', '请选择一条申请的数据','info');
				}
			}else{
				$.messager.alert('操作提示', '请选择一条申请的数据','info');
			}
		}
		
		
		//拒绝申请
		function refuseApply(){
			var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
			if(row != null){
				var applyname = row.handlename;
				if(applyname.substring(0,2) == '申请'){
					$.ajax({
						url : 'refuseApply',
						type : 'POST', //GET
						async : true, //或false,是否异步
						contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
						data : {"id":row.id,"applyname":applyname},
						dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
						success : function(data) {
							if(applyname == '申请新增'){
								if (data == '1') {
									$.messager.alert('','拒绝新增成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','拒绝新增失败','error');
								}
							}else if(applyname == '申请修改'){
								if (data == '1') {
									$.messager.alert('','拒绝修改成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','拒绝修改失败','error');
								}
							}else if(applyname == '申请重置密码'){
								if (data == '1') {
									$.messager.alert('','拒绝重置密码成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','拒绝重置密码失败','error');
								}
							}else if(applyname == '申请锁定'){
								if (data == '1') {
									$.messager.alert('','拒绝锁定成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','拒绝锁定失败','error');
								}
							}else if(applyname == '申请解锁'){
								if (data == '1') {
									$.messager.alert('','拒绝解锁成功','info',function(r) {
										window.location.href = "sys_user";
								    });
								} else {
									$.messager.alert('操作提示','拒绝解锁失败','error');
								}
							}else{
								
							}
						}
					});
				}else{
					$.messager.alert('操作提示', '请选择一条申请的数据','info');
				}
			}else{
				$.messager.alert('操作提示', '请选择一条申请的数据','info');
			}
		}

		//点击查询按钮
		function selectRecordByName() {
			var userCname = $("#userCname").val();
			window.location.href = 'getSys_user?userCname=' + encode(userCname);
		}

		//点击新增用户按钮
		function addUser() {
			$('#addDialog').dialog('open'); //打开对话框
			$('#addDialog').dialog({
				modal : true
			});
		}

		//点击修改用户按钮
		function editUser() {
			var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
			if (row != null) { //如果选中
				//为修改dialog框中的文本框赋值
				document.getElementById("editId").value = row.id;
				loginIdBackUp = row.loginId;
				$("#loginId_edit").val(row.loginId);
				$("#userEname_edit").val(row.userEname);
				$("#userCname_edit").val(row.userCname);
				$("#orgId_edit").combobox('setValue', row.orgId);
				$("#departId_edit").combobox('setValue', row.departId);
				$("#tel_edit").val(row.tel);
				$("#mobile_edit").val(row.mobile);
				$("#address_edit").val(row.address);
				$("#email_edit").val(row.email);

				//验证文本框
				loginIdValidate('loginId_edit');
				userEnameValidate('userEname_edit');
				departmentValidate('departId_edit');
				
				$('#editDialog').dialog('open');
				$('#editDialog').dialog({
					modal : true
				});
			} else {
				$.messager.alert('操作提示', '请选择一条数据','info');
			}
		}

		//点击重置用户密码按钮
		function editPasswordUser() {
			var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
			if (row != null) { //如果选中
				if(true){
					$.messager.confirm('操作提示','确定要重置密码吗？',function(r) {
						if (r) {
							$.ajax({
									    url : 'applyReaptPwd',
										type : 'GET', //GET
										async : true, //或false,是否异步
										contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
										data : {"id" : row.id,"pwd" : "12345678"},
										dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
										success : function(data) {
											if (data == '1') {
												$.messager.alert('','重置密码成功','info',function(r) {
														window.location.href = "sys_user";
												});
											} else {
												$.messager.alert('操作提示','重置密码失败','error');
											}
										}
							})
						}
	               });
				}else{
					$.messager.alert('操作提示', '请选择一条操作名为空的数据','info');
				}
				
				/* $.messager.confirm('操作提示','确定要重置该用户的密码吗？',function(r) {
									if (r) {
										$.ajax({
													url : 'editPasswordSys_User',
													type : 'GET', //GET
													async : true, //或false,是否异步
													contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
													data : {"editPasswordId" : row.id,"password_edit" : "123456"},
													dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
													success : function(data) {
														if (data == '1') {
															$.messager.alert('','重置密码成功','info',function(r) {
																				window.location.href = "sys_user";
															});
														} else {
															$.messager.alert('操作提示','哎呦，重置密码失败','error');
														}
													}
												})
									}
				}); */
			} else {
				$.messager.alert('操作提示', '请选择一条操作名为空的数据','info');
			}
		}

		//点击删除用户按钮
		/* function remove() {
			var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
		    if (row != null) { //如果选中
		    	if(row.loginId == "admin"){
					$.messager.alert('操作提示','不能删除管理员账号','error');
				}else{
					$.messager.alert('操作提示','是否要将此用户删除,如果删除，此用户将不能再使用','warning',function(r) {
						$.ajax({
							url : 'deleteSys_User',
							type : 'POST', //GET
							async : true, //或false,是否异步
							contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
							data : {"id" : row.id},
							dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
							success : function(data) {
							    if (data.result == '1') { //如果返回1，删除成功
								  	$.messager.alert('','删除用户成功','info',function(r) {
										window.location.href = "sys_user";
									});
							    } else { //否则删除失败
									$.messager.alert('操作提示','哎呦，此用户删除失败','error');
								}
							}
						})
					});
				}
			} else {
				$.messager.alert('操作提示', '请选择一条数据删除', 'info');
			}
		} */

		//点击导出按钮
		function exportExcel() {
			$.messager.confirm('操作提示', '确认将数据导出到Excel表格吗？', function(r) {
				if (r) {
					window.location.href = "exportExcelSys_User";
				}
			});
		}

		//===============与导入相关的js============================	
		function importExcel() {
			$('#importFileDialog').dialog('open');
			$('#importFileDialog').dialog({
				modal : true
			});
		}

		function addline() {
			newline = document.all.test.insertRow();
			newline.insertCell().innerHTML = "<input type='file' name='file_info' size='60'>&nbsp;"
					+ "<button onclick='javascript:removeline(this)'>移除</button>";
		}

		function removeline(obj) {
			var objSourceRow = obj.parentNode.parentNode;
			var objTable = obj.parentNode.parentNode.parentNode.parentNode;
			objTable.lastChild.removeChild(objSourceRow);
		}

		function uploadFile() {
			$.messager
					.confirm(
							'操作提示',
							'确认要导入Excel表格吗？',
							function(r) {
								if (r) {
									var fd = new FormData();
									fd
											.append(
													"fileToUpload",
													document
															.getElementById('file_info').files[0]);
									var filesize = document
											.getElementById('file_info').files[0].size;
									var filetype = document
											.getElementById('file_info').files[0].type;
									if (filetype != "application/vnd.ms-excel") {
										$('#importFileDialog').dialog('close');
										$.messager.alert({
											title : "错误",
											msg : "您上传的文件类型有误，请重新上传!",
											icon : "error"
										});

										return false;
									}
									if (filesize > 1048576) {
										$('#importFileDialog').dialog('close');
										$.messager.alert('错误',
												'您上传的文件大小超出2MB请重新上传!',
												"warning");
										return false;
									}
									var xhr = new XMLHttpRequest();
									xhr.open("POST", "importExcelSys_User");
									console.info(fd.polyfill);
									//由于IE8不兼容FromDate对象所以使用ployfill来判断
									if (fd.polyfill) {
										xhr.setRequestHeader("Content-Type",
												"multipart/form-data; boundary="
														+ fd.boundary)
										// formData.toString() returns Promise
										fd.toString().then(function(data) {
											xhr.send(data);
										})
									} else {
										// normal way
										xhr.send(fd);
									}
									xhr.onreadystatechange = function() {

										if (xhr.readyState == 4) {
											if (xhr.status == 200) {
												var info = xhr.responseText;
												$('#importFileDialog').dialog(
														'close');
												$.messager
														.alert({
															title : "提示",
															msg : info
																	.substring(
																			1,
																			info.length - 1),
															icon : "info",
															width : 500,
															fn : function() {
																window.location.href = "sys_user";
															}
														});

											} else {
												$('#importFileDialog').dialog(
														'close');
												var info = xhr.responseText;
												$.messager
														.alert(
																"提示",
																info
																		.substring(
																				1,
																				info.length - 1),
																"info");
											}
										}
									};
								} else {
									$('#importFileDialog').dialog('close');
								}
							})
		}

		/************************************************************************************************************************************/
		/************************************************************************************************************************************/
		/************************************************************************************************************************************/

		function resetForAdd() {
			$("#loginId_add").val('');
			$("#userEname_add").val('');
			$("#userCname_add").val('');
			$("#orgId_add").combobox('setValue', '');
			$("#departId_add").combobox('setValue', '');
			$("#tel_add").val('');
			$("#mobile_add").val('');
			$("#address_add").val('');
			$("#email_add").val('');
		}

		function resetForEdit() {
			$("#loginId_edit").val('');
			$("#userEname_edit").val('');
			$("#userCname_edit").val('');
			$("#orgId_edit").combobox('setValue', '');
			$("#departId_edit").combobox('setValue', '');
			$("#tel_edit").val('');
			$("#mobile_edit").val('');
			$("#address_edit").val('');
			$("#email_edit").val('');
		}

		//验证密码
		function checkPassword() {
			var password = $("#password_edit").val(); //获取密码框输入的密码
			if (password.length == 0 || password == null || password == '') {
				$.messager.alert('操作提示', '请输入密码', 'info');
			}
		}

		//验证重复密码
		function checkRepeatPassword() {
			var password = $("#password_edit").val(); //获取密码框输入的密码
			var repeatPassword = $("#passwordRepeat_edit").val(); //获取重复密码框输入的密码
			if (repeatPassword.length == 0 || repeatPassword == null
					|| repeatPassword == '') {
				$.messager.alert('操作提示', '请输入重复密码', 'info');
			} else if (password != repeatPassword) {
				$.messager.alert('', '两次输入的密码不一致', 'info', function(r) {
					$("#passwordRepeat_edit").val('');
				});
			}
		}

		//点击新增dialog上的保存按钮
		function saveForAddDialog() {
			$("#formForAddUser").form('submit',{
								url : 'addSys_User',
								onSubmit : function() {
									if (loginIdResult == true && userEnameResult == true && departmentResult == true) {
										return true;
									} else {
										$.messager.alert('操作提示','请填写正确的用户信息！','info');
										return false;
									}
								},
								success : function(res) {
									if (res == '1') {
										$.messager.alert('','新增用户成功！','info',function(r) {
											window.location.href = "sys_user";
										});
									} else if (res == '0') {
										$.messager.alert('操作提示', '新增用户失败了','error');
									} else {
										$.messager.alert('操作提示', '用户中文名已存在','error');
									}
								}
			});
		}

		//点击修改dialog上的保存按钮
		function saveForEditDialog() {
			if (loginIdBackUp != $("#loginId_edit").val()) { //如果修改前的登录账号和修改后的登录账号不一致，则表示用户修改了登录账号
				document.getElementById("isEditLoginId").value = '1';
			} else {
				document.getElementById("isEditLoginId").value = '0';
			}

			$("#formForEditUser").form('submit', {
				url : 'editSys_User',
				onSubmit : function() {
					if (loginIdResult == true && userEnameResult == true && departmentResult == true) {
						return true;
					} else {
						$.messager.alert('操作提示','请填写正确的用户信息！','info');
						return false;
					}
				},
				success : function(res) {
					if (res == '1') {
						$.messager.alert('', '修改成功！', 'info', function(r) {
							window.location.href = "sys_user";
						});
					} else if (res == '0') {
						$.messager.alert('操作提示', '哎呦，修改用户失败了', 'error');
					} else {
						$.messager.alert('操作提示', '哎呦，登录账号已存在', 'error');
					}
				}
			});
		}

		//锁定用户与解锁用户
		function doLocked(handlename,id, isLocked) {
			if (isLocked == 'Y') { //如果该用户已锁定，则申请解锁
				if(true){
					$.messager.alert('操作提示','是否要解锁该用户','question',function(r) {
						$.ajax({
							url : 'applyNoLock',
							type : 'POST', //GET
							async : true, //或false,是否异步
							contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
							data : {"id" : id},
							dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
							success : function(data) {
								if (data == '1') { 
									$.messager.alert('', '解锁成功', 'info', function(r) {
										window.location.href = "sys_user";
									});
								} else { 
									$.messager.alert('操作提示','解锁失败','error');
								}
							}
						});
                    });
				}else{
					$.messager.alert('操作提示','请选择一条操作名为空的数据','info');
				}
			} else if (isLocked == 'N') { //如果该用户未锁定，则加锁(即锁定用户)
				if(true){
					$.messager.prompt('操作提示', '请填写锁定用户原因', function(r){
						if(r){
							$.ajax({
								url : 'applyDoLock',
								type : 'POST', //GET
								async : true, //或false,是否异步
								contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
								data : {"id" : id,"reason":r},
								dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
								success : function(data) {
									if (data == '1') { 
										$.messager.alert('', '锁定成功', 'info', function(r) {
											window.location.href = "sys_user";
										});
									} else { 
										$.messager.alert('操作提示','锁定失败','error');
									}
								}
							});
						}
                    });
				}else{
					$.messager.alert('操作提示','请选择一条操作名为空的数据','info');
				}
			}
		}
	</script>
</body>
</html>
