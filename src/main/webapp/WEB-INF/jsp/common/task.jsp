<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>My JSP 'home.jsp' starting page</title>
    <script type="text/javascript">
        $(function(){
        	$("#dg1").datagrid({
	    		checkOnSelect:true,
	    		autoRowHeight:false,
	    		singleSelect:true,
	    		fitColumns:true,
	    		toolbar:'#tb',
	    		columns:[[
	    			{field:'jobs',title:'待处理任务',width:80,align:'center',
	    				formatter: function(value,row,index){
	    					var before = value.substring(0,2);
	    					var after = value.substring(value.indexOf('条'));
	    					var number = value.substring(2,value.indexOf('条'));
	    					return before+'<span style="color: red;">'+number+'</span>'+after;
	    				}
	    			}
	    	    ]]
	    	});
        	
        	refresh();
       })
       
       function refresh(){
        	var jsonstr = '[';
        	jsonstr += '{"jobs":"您有5条抵(质)押物数据待处理"},';
        	jsonstr += '{"jobs":"您有9条企业财务报表数据待处理"},';
        	jsonstr += '{"jobs":"您有8条企业担保交易数据待处理"},';
        	jsonstr += '{"jobs":"您有7条企业基本数据待处理"},';
        	jsonstr += '{"jobs":"您有6条借贷交易数据待处理"},';
        	jsonstr += '{"jobs":"您有11条授信协议数据待处理"}]';
        	var data = eval('(' + jsonstr + ')');
        	$('#dg1').datagrid('loadData',data);
        	/* $.ajax({
                url:'countInit',
                type:'POST', //GET
                async:true,    //或false,是否异步
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){
                	var jsonstr = '[';
                	jsonstr += '{"jobs":"您有'+data.count1+'条放款待提交数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count2+'条还款待提交数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count3+'条放还款可疑交易待复核数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count4+'条可疑交易补足数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count5+'条可疑交易复核数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count6+'条初评高分险客户数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count7+'条年审高分险客户数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count8+'条总黑名单匹配客户数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count9+'条风险等级复核数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count10+'条风险等级-尽职调查数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count11+'条可疑交易审定数据待处理"},';
                	jsonstr += '{"jobs":"您有'+data.count12+'条数据证件已过期"}]';
                	
                	var data = eval('(' + jsonstr + ')');
                	$('#dg1').datagrid('loadData',data);
                }
         }) */
       }
    </script>
  </head>
  
  <body>
    <div style="background-repeat: no-repeat;height:100%;width:100%; background-size: 100% 100%;">
	    <div style="height:50px;text-align: center;"><h1>企业征信报文系统(二代)</h1></div>
	    <div style="height: 300px;width: 480px;padding-top: 10px;padding-left: 10px;">
	        <div id="tb" style="padding:5px;height:auto;">
			    <a class="easyui-linkbutton" iconCls="icon-reload" onclick="refresh()">刷新</a>
	        </div>
	        
	        <table title="" id="dg1" style="height: 250px;width: 380px;"></table>
	    </div>
    </div>
  </body>
</html>
