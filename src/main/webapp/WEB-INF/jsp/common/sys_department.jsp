<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>部门信息</title>
    <%@include file="head.jsp"%>
</head>
<body>
	<table class="easyui-datagrid" fitColumns="true" title="部门信息" id="dg1"
			data-options="singleSelect:true,rownumbers:true,toolbar:'#tb',autoRowHeight:false,pagination:true,pageSize:25,pageList:[15,25,35]" style="height: 460px">
		<thead>
			<tr>
			    <th data-options="field:'id',width:80,align:'center',hidden:true">ID</th>
				<th data-options="field:'departmentId',width:80,align:'center',hidden:true">部门代码</th>
				<th data-options="field:'departmentName',width:80,align:'center'">部门名称</th>
				<th data-options="field:'description',width:80,align:'center'">描述</th>
			</tr>
		</thead>
		
		<tbody>
		   <c:forEach items="${list}" var="li">
			<tr>
			    <td>${li.id}</td>
                <td>${li.departmentId}</td>
                <td>${li.departmentName}</td>
                <td>${li.description}</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<div id="tb" style="padding:5px;height:auto;">
			    	部门名称: 
					<input type="text" style="width:173px;height:21px" id="departmentName" name="departmentName" value="${departmentName}">&nbsp;&nbsp;
				    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByName()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" iconCls="icon-add"  onclick="addDepartment()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" iconCls="icon-edit"  onclick="editDepartment()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" iconCls="icon-remove" onclick="remove()">删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="openExcelTemplet()">导出</a>
	</div>
	
	
<!-- 新增部门dialog -->
<div style="visibility: hidden;">
   <div id="addDialog" class="easyui-dialog" title="新增部门" data-options="iconCls:'icon-save',toolbar:'#tbForAddDialog'" style="width:500px;height:260px;top: 266px">
          <!--   新增对话框的工具栏  -->
          <div id="tbForAddDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForAddDialog()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForAdd()">重置</a>
          </div>
          
         <!-- 信息录入 -->
         <form id="formForAddDepartment" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px;font-size: 8px;">
               <tr>
                 <td align="right">部门名称:</td>
			     <td><input class="easyui-validatebox" name="departmentName_add" id="departmentName_add" data-options="required:true,missingMessage:'部门名称不能为空'" style="width:173px;height: 21px"/></td>
			   </tr>
				 
               <tr>
                 <td align="right">描述:</td>
				 <td><input type="text" name="description_add" id="description_add" style="height: 100px;width: 300px"/></td>
			   </tr> 
              </table>
           </div>
         </form>
	</div>
</div>





<!-- 修改部门dialog -->
<div style="visibility: hidden;">
   <div id="editDialog" class="easyui-dialog" title="修改部门" data-options="iconCls:'icon-save',toolbar:'#tbForEditDialog'" style="width:500px;height:260px;top: 266px">
          <!--   新增对话框的工具栏  -->
          <div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForEditDialog()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">重置</a>
          </div>
          
         <!-- 信息录入 -->
         <form id="formForEditDepartment" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px;font-size: 8px;">
              <input type="hidden" name="editId" id="editId"/>
              <input type="hidden" name="isEditDepartmentName" id="isEditDepartmentName"/>
               <tr>
                 <td align="right">部门名称:</td>
			     <td><input class="easyui-validatebox" name="departmentName_edit" id="departmentName_edit" data-options="required:true,missingMessage:'部门名称不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
				 
               <tr>
                 <td align="right">描述:</td>
				 <td><input type="text" name="description_edit" id="description_edit" style="height: 100px;width: 300px"/></td>
			   </tr> 
              </table>
           </div>
         </form>
	</div>
</div>



<!-- 导出模板选择 -->
<div style="visibility: hidden;">
   <div id="selectExcelTemplet" class="easyui-dialog" title="选择导出Excel模板" style="width:300px;height:120px;align-items: center;text-align: center;padding: 20px 5px">
        <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel('xls')">xls(2003版)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel('xlsx')">xlsx(2007版)</a>
	</div>
</div>
<!--------------------------------------------------js ------------------------------------------------------------------------------->	
<script type="text/javascript">
   
   var departmentNameBackUp;
  
   $(function(){
	  $('#addDialog').dialog('close');
	  $('#editDialog').dialog('close');
	  $('#selectExcelTemplet').dialog('close');
	  $('#dg1').datagrid({loadFilter:pagerFilter});//.datagrid('loadData', getData());
	  $('.datagrid-header-check').find('input').attr('style','display:none');
});

   
   function pagerFilter(data){
		if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
			data = {
				total: data.length,
				rows: data
			}
		}
		var dg = $(this);
		var opts = dg.datagrid('options');
		var pager = dg.datagrid('getPager');
		pager.pagination({
			onSelectPage:function(pageNum, pageSize){
				opts.pageNumber = pageNum;
				opts.pageSize = pageSize;
				pager.pagination('refresh',{
					pageNumber:pageNum,
					pageSize:pageSize
				});
				dg.datagrid('loadData',data);
			}
		});
		if (!data.originalRows){
			data.originalRows = (data.rows);
		}
		var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
		var end = start + parseInt(opts.pageSize);
		data.rows = (data.originalRows.slice(start, end));
		return data;
}
   
   
      
   //点击查询按钮
   function selectRecordByName(){
	   var departmentName = $("#departmentName").val();
	   window.location.href='getSys_DepartmentByLikeDepartmentName?departmentName='+encode(departmentName);
}

   //点击添加部门信息按钮
   function addDepartment(){
	   $('#addDialog').dialog('open');  //打开对话框
	   $('#addDialog').dialog({modal:true});
}


   function editDepartment(){
	   var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
       if(row != null){ //如果选中
       	 //为修改dialog框中的文本框赋值
       	  document.getElementById("editId").value = row.id;
       	  departmentNameBackUp = row.departmentName;
          $("#departmentName_edit").val(row.departmentName);
          $("#description_edit").val(row.description);
       	 
       	  $('#editDialog').dialog('open');
   		  $('#editDialog').dialog({modal:true});
       }else{
       	$.messager.alert('操作提示','请选择一条数据修改','info');
       }
}
   
   
   
    function remove(){
    	var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
		if(row != null){ //如果选中
			$.messager.alert('操作提示','是否要将此部门删除？','warning',function(r){
				$.ajax({
                    url:'deleteSys_Department',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{"id":row.id},
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){
                   	if(data.result == '1'){  //如果返回1，删除成功
                   		$.messager.alert('','删除部门成功','info',function(r){
                   			window.location.href="sys_department";
                   		});
                   	}else{  //否则删除失败
                   		$.messager.alert('操作提示','此部门删除失败','error');
                   	}
                    }
                })
			});
       }else{
       	$.messager.alert('操作提示','请选择一条数据删除','info');
       }
}
   
    
   function openExcelTemplet(){
	   $('#selectExcelTemplet').dialog('open');
	   $('#selectExcelTemplet').dialog({modal:true});
}
   
   
   
   function exportExcel(excelType){
	   var departmentName = $("#departmentName").val();
	   $.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
		if(r){
			window.location.href="exportSys_Department?excelType="+excelType+"&departmentName="+encode(departmentName);
			$('#selectExcelTemplet').dialog('close');
		}
	   });
   }

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/************************************************************************************************************************************/



    function resetForAdd(){
    	$("#departmentName_add").val('');
        $("#description_add").val('');
}

    function resetForEdit(){
    	$("#departmentName_edit").val('');
        $("#description_edit").val('');
}
    
	
    //点击新增dialog上的保存按钮
    function saveForAddDialog(){
    	$("#formForAddDepartment").form('submit',{
			 url:'addSys_Department',
			 onSubmit:function(){
				 return $("#formForAddDepartment").form('validate');
			 },
			 success:function(res){
				  if(res == '1'){
					  $.messager.alert('','新增成功！','info',function(r){
						  window.location.href="sys_department";
					  });
				  }else if(res == '0'){
					  $.messager.alert('操作提示','新增部门失败了','error');
				  }else{
					  $.messager.alert('操作提示','部门名称已存在','error');
				  }
			 }
		});
}

    //点击修改dialog上的保存按钮
    function saveForEditDialog(){
    	if(departmentNameBackUp != $("#departmentName_edit").val()){
			document.getElementById("isEditDepartmentName").value = '1';
		}else{
			document.getElementById("isEditDepartmentName").value = '0';
		}
		
		$("#formForEditDepartment").form('submit',{
			 url:'editSys_Department',
			 onSubmit:function(){
				 return $("#formForEditDepartment").form('validate');
			 },
			 success:function(res){
				  if(res == '1'){
					  $.messager.alert('','修改成功！','info',function(r){
						  window.location.href="sys_department";
					  });
				  }else if(res == '0'){
					  $.messager.alert('操作提示','修改部门失败了','error');
				  }else{
					  $.messager.alert('操作提示','部门名称已存在','error');
				  }
			 }
		});
}
    

</script>		
</body>
</html >
