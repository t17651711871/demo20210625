<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>单位贷款担保物信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="单位贷款担保物信息"></table>
	<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
		<form id="dateform" action="">
			请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
		</form>
	</div>

	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		担保合同编码 ：<input type="text" id="gteecontractcodeParam" name="gteecontractcodeParam" style="height: 23px; width:200px"/>&nbsp;&nbsp;
		贷款合同编码 ：<input type="text" id="loancontractcodeParam" name="loancontractcodeParam" style="height: 23px; width:200px"/>&nbsp;&nbsp;
		<label>数据日期：</label><input id="operationtimeParam"  class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
	</div>
	
	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importFileDialog" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table>
        </div>
    </div>	
<!-- 导出进度条 -->
<div style="visibility: hidden;">
  <div id="exportFileDialog" class="easyui-dialog" style="width:550px;height:100px;padding-left: 10px;top:200px;" title="导出数据"> 
	<div align="center"><span id="daorutishi">请您稍等,数据正在导出中......</span></div>
	<div id="exportFile" class="easyui-progressbar" style="width:500px;heigth:50px;"></div>
  </div>
</div>
	
<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="单位贷款担保物信息" data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构代码 :</td>
                 <td><input class="easyui-validatebox" id="financeorgcode_m" name="financeorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                 
               <tr>
                 <td>内部机构号:</td>
                 <td><input class="easyui-validatebox" id="financeorginnum_m" name="financeorginnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode_m" name="gteecontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>被担保合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode_m" name="loancontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode_m" name="gteegoodscode" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select id="gteegoodscategory_m" name="gteegoodscategory" style="height: 23px; width:230px" disabled="disabled">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
						<option value="B01">B01:交易类应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
					    <option value="D01">D01:存货、仓单和提单</option>
					    <option value="D02">D02:机器设备</option>
					    <option value="D03">D03:交通运输设备</option>
					    <option value="D04">D04:采（探）矿权</option>
					    <option value="D05">D05:林权</option>
					    <option value="D06">D06:其他资源资产</option>
					    <option value="D07">D07:知识产权</option>
					    <option value="D08">D08:农村承包土地的经营权</option>
					    <option value="D09">D09:农民住房财产权</option>
					    <option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode_m" name="warrantcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select id="isfirst_m" name="isfirst" style="height: 23px; width:230px" disabled="disabled">
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select id="assessmode_m" name="assessmode" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select id="assessmethod_m" name="assessmethod" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:组合法</option>
					    <option value="09">09:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue_m" name="assessvalue" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input id="assessdate_m" name="assessdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物票面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt_m" name="gteegoodsamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt_m" name="firstrightamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>估值周期 :</td>
			     <td>
			        <select id="gzzq_m" name="gzzq" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:年</option>
					    <option value="02">02:半年</option>
					    <option value="03">03:季度</option>
					    <option value="04">04:月</option>
					    <option value="05">05:旬</option>
					    <option value="06">06:周</option>
					    <option value="07">07:日</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>
			   <!-- <tr>
			     <td>担保物状态 :</td>
			     <td>
			        <select id="gteegoodsstataus_m" name="gteegoodsstataus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常</option>
					    <option value="02">02:已变现处置</option>
					    <option value="03">03:待处理抵债资产</option>
					    <option value="04">04:灭失损毁</option>
					    <option value="05">05:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>抵质押率 :</td>
			     <td>
			        <input class="easyui-validatebox" id="mortgagepgerate_m" name="mortgagepgerate" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr> -->
			   
              </table>
           </div>
         </form>
	</div>
</div>

<script type="text/javascript">
var operationtimeParam = '<%=operationtimeParam%>';
$(function(){
    $('#datedialog').window('close');
	$('#dialog_m').dialog('close');
	$("#importFileDialog").dialog("close");
	$('#exportFileDialog').dialog('close');
	$("#dg1").datagrid({
		method:'post',
		url:'XGetHistoryXdkdbwxH?operationtimeParam='+operationtimeParam,
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			    {field:'ck',checkbox:true},
			    {field:'financeorgcode',title:'金融机构代码',width:100,align:'center'},
			    {field:'financeorginnum',title:'内部机构号',width:100,align:'center'},
			    {field:'gteecontractcode',title:'担保合同编码',width:100,align:'center'},
			    {field:'loancontractcode',title:'贷款合同编码',width:100,align:'center'},
			    {field:'gteegoodscode',title:'担保物编码',width:100,align:'center'},
			    {field:'gteegoodscategory',title:'担保物类别',width:100,align:'center'},
			    {field:'warrantcode',title:'权证编号',width:100,align:'center'},
			    {field:'isfirst',title:'是否第一顺位',width:100,align:'center'},
			    {field:'assessmode',title:'评估方式',width:100,align:'center'},
			    {field:'assessmethod',title:'评估方法',width:100,align:'center'},
			    {field:'assessvalue',title:'评估价值',width:100,align:'center'},
			    {field:'assessdate',title:'评估基准日',width:100,align:'center'},
			    {field:'gteegoodsamt',title:'担保物票面价值',width:100,align:'center'},
			    {field:'firstrightamt',title:'优先受偿权数额',width:100,align:'center'},
			    {field:'gzzq',title:'估值周期',width:100,align:'center'},
                {field:'sjrq',title:'数据日期',width:100,align:'center'},
			    // {field:'gteegoodsstataus',title:'担保物状态',width:100,align:'center'},
			    // {field:'mortgagepgerate',title:'抵质押率',width:100,align:'center'},
			    {field:'operator',title:'操作人',width:100,align:'center'},
			    {field:'operationtime',title:'操作时间',width:100,align:'center'}
	    ]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
	    	$("#formForMore").form('load',rowData);
	    	$('#dialog_m').dialog({modal : true});	
	    	//$("#gteegoodscategory_m").combobox({disabled: true});
        	//$("#isfirst_m").combobox({disabled: true});
        	//$("#assessmode_m").combobox({disabled: true});
        	//$("#assessmethod_m").combobox({disabled: true});
        	//$("#assessdate_m").datebox({disabled: true});
        	//$("#gteegoodsstataus_m").combobox({disabled: true});
	    	//关闭弹窗默认隐藏div
			/* $(dia).window({
		    	onBeforeClose: function () {
		    		//初始化表单的元素的状态
		    		initForm();
		    	}
			}); */
	    }
	});
	$('#operationtimeParam').datebox('setValue',operationtimeParam);

});

//查询
function chaxun(){
	var gteecontractcodeParam = $("#gteecontractcodeParam").val();
    var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
    var operationtimeParam = $("#operationtimeParam").datebox("getValue");
	$("#dg1").datagrid("load",{"gteecontractcodeParam":gteecontractcodeParam,"loancontractcodeParam":loancontractcodeParam,"sjrqParam":operationtimeParam});
}

function initForm(){
	$("#financeorgcode_m").val('');
	$("#financeorginnum_m").val('');
	$("#gteecontractcode_m").val('');
	$("#loancontractcode_m").val('');
	$("#gteegoodscode_m").val('');
	$("#gteegoodscategory_m").val('');
	$("#warrantcode_m").val('');
	$("#isfirst_m").val('');
	$("#assessmode_m").val('');
	$("#assessmethod_m").val('');
	$("#assessvalue_m").val('');
	$("#assessdate_m").val('');
	$("#gteegoodsamt_m").val('');
	$("#firstrightamt_m").val('');
	$("#gzzq_m").val('');
    $("#sjrq_m").val('');
	// $("#gteegoodsstataus_m").val('');
	// $("#mortgagepgerate_m").val('');
}

//导出
function daochu(){
	var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochudkdbwxH?selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochudkdbwxH?selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			var gteecontractcode = $('#gteecontractcodeParam').val();
			var loancontractcode = $('#loancontractcodeParam').val();
			var operationtime = $('#operationtimeParam').datebox('getValue');
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochudkdbwxH?gteecontractcode="+gteecontractcode+"&loancontractcode="+loancontractcode+"&operationtime="+operationtime;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochudkdbwxH?gteecontractcode="+gteecontractcode+"&loancontractcode="+loancontractcode+"&operationtime="+operationtime;
			}
		});	
	}
}
/**
 * 导出excel（带进度条）
 * @param exportExcelUrl
 * @param scanTime 检测是否导出完毕请求间隔 单位毫秒
 * @param interval 进度条更新间隔（每次更新进度10%）  单位毫秒  导出时间越长 请设置越大 200 对应2秒导出时间
 */
function exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval){
    window.location.href = exportExcelUrl;
     var timer = setInterval(function(){
         $.ajax({
                url: isExportUrl,
                type:'POST', //GET
        	    async:true,    //或false,是否异步
                success: function(data){
                    if(data == "success"){
                        $('#exportFile').progressbar('setValue','100');
                        clearInterval(timer);
                        $('#exportFileDialog').dialog('close');
                        $('#exportFile').progressbar('setValue','0');
                    }else{
                    	var value = $('#exportFile').progressbar('getValue');
                    	if(value <= 100){
                    		value += Math.floor(Math.random() * 10);
                        	$('#exportFile').progressbar('setValue', value);
                    	}else{
                        	$('#exportFile').progressbar('setValue', '0');
                    	}
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            }); 
          }, scanTime);
}

//生成报文
function shengchengbaowen(){
	var rows = $('#dg1').datagrid('getSelections');
	/*if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}*/
	$.messager.confirm('提示','是否生成报文？',function (r){
		if(r){
			$.messager.progress({
				title: '请稍等',
				msg: '数据处理中......'
			});
			$.ajax({
				url: "XCreateReportFour",
				type: "POST",
				contentType: 'application/json;charset=UTF-8',
				async: true,
				success: function (data) {
					$.messager.progress('close');
					$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
					$("#dg1").datagrid("reload");
				},
				error: function (error) {
					$.messager.progress('close');
					$.messager.alert('操作提示', '生成报文失败', 'error');
				}
			})
		}
	});
}

//数据打回
function dataBack(){
    var v = $('#dateform').form('validate');
    if(v){
        var value = $('#dateselect').datebox('getValue');
        $('#datedialog').window('close');
        $.messager.confirm('提示','是否打回数据？',function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据处理中......'
                });
                $.ajax({
                    url: "XdatabackxdkdbwxH",
                    type: "POST",
                    contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                    async: true,
                    data:{"date":value},
                    dataType:'json',
                    success: function (data) {
                        if(data != 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回成功！', 'info');
                            $("#dg1").datagrid("reload");
                        }else{
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '没有数据可打回！', 'error');
                            $("#dg1").datagrid("reload");
                        }

                    },
                    error: function (error) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示', '数据打回失败！', 'error');
                    }
                })
            }
        });
    }

}
</script>
</body>
</html>
