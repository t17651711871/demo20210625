<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="../../common/head.jsp"%>
<html>
<head>
    <title>历史数据</title>
</head>
<script type="text/javascript">
    $(function (){
        $("#dg").datagrid({
            loadMsg:'数据加载中，请稍后...',
            url:'XHistoryView',
            cache:false,
            rownumbers:true,            //开启行号
            columns:[[
                {field:'name',title:'报文名称',align:'center',width:800},
                {field:'noMessageName',title:'报文条数 ',align:'center',width:800,formatter:function (value,row){
                        return '<a href = '+row.historyDataUrl+' style="color: blue">'+ value +'</a>';
                    /*if (row.messagename == '存量单位贷款基础数据信息'){
                        return '<a href = "XHistoryDataXcldkxxH" style="color: blue">'+ value +'</a>';
                    }else if (row.messagename == '单位贷款发生额信息') {
                        return '<a href = "XHistoryDataXdkfsxxH" style="color: blue">'+ value +'</a>';
                    }else if (row.messagename == '单位贷款担保合同信息') {
                        return '<a href = "XHistoryDataXdkdbhtH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '单位贷款担保物信息'){
                    	return '<a href = "XHistoryDataXdkdbwxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '金融机构（分支机构）基础信息'){
                    	return '<a href = "XHistoryDataXjrjgfzjcxxH" style="color: blue">'+ value +'</a>';
                    }else if (row.messagename == '非同业单位客户基础信息') {
                        return '<a href = "XHistoryDataXftykhxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '金融机构（法人）基础信息'){
                    	return '<a href = "XHistoryDataXjrjgfrjcxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '存量个人贷款基础数据信息'){
                        return '<a href = "XHistoryDataXclgrdkxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '个人客户基础信息'){
                        return '<a href = "XHistoryDataXgrkhxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '个人贷款发生额信息'){
                        return '<a href = "XHistoryDataXgrdkfsxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '存量委托贷款基础数据信息'){
                        return '<a href = "XHistoryDataXclwtdkxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '委托贷款发生额信息'){
                        return '<a href = "XHistoryDataXwtdkfseH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '存量同业借贷信息'){
                        return '<a href = "XHistoryDataXcltyjdxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '同业借贷发生额信息'){
                        return '<a href = "XHistoryDataXtyjdfsexxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '同业客户基础信息'){
                        return '<a href = "XHistoryDataXtykhjcxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '存量同业存款信息'){
                        return '<a href = "XHistoryDataXcltyckxxH" style="color: blue">'+ value +'</a>';
                    }else if(row.messagename == '同业存款发生额信息'){
                        return '<a href = "XHistoryDataXtyckfsexxH" style="color: blue">'+ value +'</a>';
                    }*/
                }}
            ]],
            onClickRow: function () {
                $('#dg').datagrid('clearSelections');
            },
        });

    });

    //生成报文
    function generateMessage(){
        var rows = $("#dg").datagrid("getRows");
        if (rows[0].count == '0' && rows[1].count == '0') {
            $.messager.alert("提示","当前没有报文可生成!","info");
        }else {
            $.messager.confirm('提示',"确定要生成报文？",function (r){
                if(r){
                    $.messager.progress({
                        title:'请稍等',
                        msg:'数据正在生成报文中......'
                    });
                    $.ajax({
                        sync: true,
                        type: "POST",
                        dataType: "json",
                        url: "MCreateAccountInfo",
                        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                        success: function (data) {
                            if (data != null && data !=''){
                                $.messager.alert('操作提示','生成报文成功','info');
                            }else {
                                $.messager.alert('操作提示','生成报文失败','info');
                            }
                            $.messager.progress('close');
                            $("#dg").datagrid("reload");
                        },
                        error: function (err) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','生成报文失败','error');
                            $("#dg").datagrid("reload");
                        }
                    });
                }
            });
        }
    }
</script>
<body>
报文日期 ：<input type="easyui-datetimebox" id="operationtime" name="operationtime" data-options="formatter:myformatter,parser:myParser" style="height: 23px; width:173px"/>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a>
<table id="dg" style="height: 480px;">
</table>
<script type="text/javascript">
$(function(){
	var curr_time = new Date()	
	getMonth('operationtime',dateformatter(curr_time,1));
});
function chaxun(){
	var operationtime = $('#operationtime').datebox('getValue');
	$("#dg").datagrid("load",{"operationtime":operationtime});
}
//日期控件只显示到月份
function getMonth(text,date){
	$('#'+text).datebox({
	       //显示日趋选择对象后再触发弹出月份层的事件，初始化时没有生成月份层
	       onShowPanel: function () {
	          //触发click事件弹出月份层
	          span.trigger('click'); 
	          if (!tds)
	            //延时触发获取月份对象，因为上面的事件触发和对象生成有时间间隔
	            setTimeout(function() { 
	                tds = p.find('div.calendar-menu-month-inner td');
	                tds.click(function(e) {
	                   //禁止冒泡执行easyui给月份绑定的事件
	                   e.stopPropagation(); 
	                   //得到年份
	                   var year = /\d{4}/.exec(span.html())[0] ,
	                   //月份
	                   //之前是这样的month = parseInt($(this).attr('abbr'), 10) + 1; 
	                   month = parseInt($(this).attr('abbr'), 10);  

	         //隐藏日期对象                     
	         $('#'+text).datebox('hidePanel') 
	           //设置日期的值
	           .datebox('setValue', year + '-' + month); 
	                        });
	                    }, 0);
	            },
	            //配置parser，返回选择的日期
	            parser: function (s) {
	                if (!s) return new Date();
	                var arr = s.split('-');
	                return new Date(parseInt(arr[0], 10), parseInt(arr[1], 10) - 1, 1);
	            },
	            //配置formatter，只返回年月 之前是这样的d.getFullYear() + '-' +(d.getMonth()); 
	            formatter: function (d) { 
	                var currentMonth = (d.getMonth()+1);
	                var currentMonthStr = currentMonth < 10 ? ('0' + currentMonth) : (currentMonth + '');
	                return d.getFullYear() + '-' + currentMonthStr; 
	            }
	        });

	        //日期选择对象
	        var p = $('#'+text).datebox('panel'), 
	        //日期选择对象中月份
	        tds = false, 
	        //显示月份层的触发控件
	        span = p.find('span.calendar-text'); 
	        //var curr_time = new Date();
	        //设置默认显示月份
	        //$("#"+text).datebox("setValue", date);
}
//格式化日期
function myformatter(date){
    var y = date.getFullYear();
    var m = date.getMonth()+1;

    return y+'-'+(m<10?('0'+m):m);
}
//格式化日期
function dateformatter(date,field){
    var y = date.getFullYear();
    if(field==""){
    	var m = date.getMonth()+1;
    }else{
    	var m = date.getMonth()+field;
    }
    return y+'-'+(m<10?('0'+m):m);
}
function myParser(s){
   if(!s){
       return new Date();
   }
   var ss = s.split('-');
   var y = parseInt(ss[0],10);
   var m = parseInt(ss[1],10);
   if (!isNaN(y) && !isNaN(m)){
       return new Date(y,m-1);
   }
}
</script>
</body>
</html>
