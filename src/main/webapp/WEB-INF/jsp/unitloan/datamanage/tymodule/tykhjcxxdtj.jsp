<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
    Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
    String status = request.getParameter("status");
    String shifoushenheziji = (String) session.getAttribute("shifoushenheziji");
    String role = vu.getRole();
    Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String handlename = sys_User.getLoginid();
    String isusedelete = (String) request.getSession().getAttribute("isusedelete");
    String isuseupdate = (String) request.getSession().getAttribute("isuseupdate");
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");

    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
        baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);



    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>同业客户基础信息</title>
    <%@include file="../../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="同业客户基础信息列表"></table>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <%if("dtj".equals(status) || "dsh".equals(status)){%>
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
    <%}else if ("dsb".equals(status)){%>
    <a class="easyui-linkbutton" href="VGenerateMessageUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;
    <%}%>&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>客户名称：</label><input id="khnameParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>客户类别：</label>
    <select class='easyui-combobox'  id="khtypeParam" name="khtypeParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='B0311'>B0311-中资大型银行</option>
		<option value='B0312'>B0312-中资中型银行</option>
		<option value='B03131'>B03131-小型城市商业银行</option>
		<option value='B03132'>B03132-农村商业银行</option>
		<option value='B03133'>B03133-农村合作银行</option>
		<option value='B03134'>B03134-村镇银行</option>
		<option value='B032'>B032-城市信用合作社</option>
		<option value='B033'>B033-农村信用社</option>
		<option value='B034'>B034-农村资金互助社</option>
		<option value='B035'>B035-财务公司</option>
		<option value='B036'>B036-外资银行</option>
		<option value='B041'>B041-信托公司</option>
		<option value='B042'>B042-金融资产管理公司</option>
		<option value='B043'>B043-金融租赁公司</option>
		<option value='B044'>B044-汽车金融公司</option>
		<option value='B045'>B045-贷款公司</option>
		<option value='B046'>B046-货币经纪公司</option>
		<option value='B047'>B047-消费金融公司</option>
		<option value='B051'>B051-证券公司</option>
		<option value='B052'>B052-证券投资基金公司</option>
		<option value='B053'>B053-期货公司</option>
		<option value='B054'>B054-投资咨询公司</option>
		<option value='B055'>B055-银行理财子公司</option>
		<option value='B056'>B056-证券子公司</option>
		<option value='B057'>B057-基金子公司</option>
		<option value='B058'>B058-期货子公司</option>
		<option value='B060'>B060-财产保险公司</option>
		<option value='B061'>B061-人身保险公司</option>
		<option value='B062'>B062-再保险公司</option>
		<option value='B063'>B063-保险资产管理公司</option>
		<option value='B064'>B064-保险经纪公司</option>
		<option value='B065'>B065-保险代理公司</option>
		<option value='B066'>B066-保险公估公司</option>
		<option value='B067'>B067-保险集团（控股）公司</option>
		<option value='B068'>B068-企业（职业）年金</option>
		<option value='B07'>B07-交易及结算类金融机构</option>
		<option value='B08'>B08-金融控股公司</option>
		<option value='B091'>B091-银行理财产品</option>
		<option value='B092'>B092-信托公司资管产品</option>
		<option value='B093'>B093-证券公司及其子公司资管产品</option>
		<option value='B094'>B094-基金公司及其子公司专户</option>
		<option value='B095'>B095-期货公司及其子公司资管产品</option>
		<option value='B096'>B096-公募基金</option>
		<option value='B097'>B097-私募机构私募基金</option>
		<option value='B098'>B098-保险资管产品</option>
		<option value='B099'>B099-金融资产投资公司资管产品</option>
		<option value='B10'>B10-其他金融机构</option>
		<option value='E012'>E012-境外金融机构</option>
    </select>&nbsp;
    <label>客户代码：</label><input id="khcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox'  id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <%if("dtj".equals(status)){%>
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>
    <%}else if ("dsh".equals(status)){%>
    <label>操作名：</label>
    <select class='easyui-combobox' id='operationnameParam' name='operationnameParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='申请删除'>申请删除</option>
    </select>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dsh')">查询</a>&nbsp;&nbsp;<br>
    <%}%>
    <%if("dtj".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("ADD")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EDIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("APPLYDELETE")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete()">申请删除</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("IMPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(true)">导出模板</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECKOUT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check()">校验</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("SUBMIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>
    <%
            }
        }
    }else if("dsh".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("EDIT") && "yes".equals(isuseupdate)){
    %>
    <%--<a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyEdit()" >申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("DELETE") && "yes".equals(isusedelete)){
    %>
    <%--<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="applyDelete()" >申请删除</a>&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("CHECK")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()" >同意申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()" >拒绝申请删除</a>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()" >审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()" >审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <%
                }
            }
        }%>
</div>

<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
    </div>
</div>

<!--新增修改窗口-->
<div id="dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialog" style="padding-left: 100px;padding-top: 10px">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
    </div>

    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">
        </form>
    </div>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
            <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户名称:
                    </td>
                    <td>
                        <input id='info_khname' name='khname' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户代码:</td>
                    <td>
                        <input id='info_khcode' name='khcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户金融机构编码:
                    </td>
                    <td>
                        <input id='info_khjrjgbm' name='khjrjgbm' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户内部编码:</td>
                    <td>
                        <input id='info_khnbbm' name='khnbbm' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        基本存款账号:
                    </td>
                    <td>
                        <input id='info_jbckzh' name='jbckzh' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    基本账户开户行名称:</td>
                    <td>
                        <input id='info_jbzhkhhmc' name='jbzhkhhmc' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        注册地址:
                    </td>
                    <td>
                        <input id='info_zcdz' name='zcdz' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    地区代码:</td>
                    <td>
                        <input id='info_dqdm' name='dqdm' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户类别:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_khtype' name='khtype' disabled="disabled" style='width:173px;'>
                       		<option value='B0311'>B0311-中资大型银行</option>
							<option value='B0312'>B0312-中资中型银行</option>
							<option value='B03131'>B03131-小型城市商业银行</option>
							<option value='B03132'>B03132-农村商业银行</option>
							<option value='B03133'>B03133-农村合作银行</option>
							<option value='B03134'>B03134-村镇银行</option>
							<option value='B032'>B032-城市信用合作社</option>
							<option value='B033'>B033-农村信用社</option>
							<option value='B034'>B034-农村资金互助社</option>
							<option value='B035'>B035-财务公司</option>
							<option value='B036'>B036-外资银行</option>
							<option value='B041'>B041-信托公司</option>
							<option value='B042'>B042-金融资产管理公司</option>
							<option value='B043'>B043-金融租赁公司</option>
							<option value='B044'>B044-汽车金融公司</option>
							<option value='B045'>B045-贷款公司</option>
							<option value='B046'>B046-货币经纪公司</option>
							<option value='B047'>B047-消费金融公司</option>
							<option value='B051'>B051-证券公司</option>
							<option value='B052'>B052-证券投资基金公司</option>
							<option value='B053'>B053-期货公司</option>
							<option value='B054'>B054-投资咨询公司</option>
							<option value='B055'>B055-银行理财子公司</option>
							<option value='B056'>B056-证券子公司</option>
							<option value='B057'>B057-基金子公司</option>
							<option value='B058'>B058-期货子公司</option>
							<option value='B060'>B060-财产保险公司</option>
							<option value='B061'>B061-人身保险公司</option>
							<option value='B062'>B062-再保险公司</option>
							<option value='B063'>B063-保险资产管理公司</option>
							<option value='B064'>B064-保险经纪公司</option>
							<option value='B065'>B065-保险代理公司</option>
							<option value='B066'>B066-保险公估公司</option>
							<option value='B067'>B067-保险集团（控股）公司</option>
							<option value='B068'>B068-企业（职业）年金</option>
							<option value='B07'>B07-交易及结算类金融机构</option>
							<option value='B08'>B08-金融控股公司</option>
							<option value='B091'>B091-银行理财产品</option>
							<option value='B092'>B092-信托公司资管产品</option>
							<option value='B093'>B093-证券公司及其子公司资管产品</option>
							<option value='B094'>B094-基金公司及其子公司专户</option>
							<option value='B095'>B095-期货公司及其子公司资管产品</option>
							<option value='B096'>B096-公募基金</option>
							<option value='B097'>B097-私募机构私募基金</option>
							<option value='B098'>B098-保险资管产品</option>
							<option value='B099'>B099-金融资产投资公司资管产品</option>
							<option value='B10'>B10-其他金融机构</option>
							<option value='E012'>E012-境外金融机构</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    成立日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_setupdate' name='setupdate' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        是否关联方:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_sfglf' name='sfglf' disabled="disabled" style='width:173px;'>
                        	<option value='1'>1-是</option>
                        	<option value='0'>0-否</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户经济成分:</td>
                    <td>
                        <select class='easyui-combobox' id='info_khjjcf' name='khjjcf' disabled="disabled" style='width:173px;'>
                        	<option value='A'>A-公有控股经济</option>
							<option value='A01'>A01-国有控股</option>
							<option value='A0101'>A0101-国有相对控股</option>
							<option value='A0102'>A0102-国有绝对控股</option>
							<option value='A02'>A02-集体控股</option>
							<option value='A0201'>A0201-集体相对控股</option>
							<option value='A0202'>A0202-集体绝对控股</option>
							<option value='B'>B-非公有控股经济</option>
							<option value='B01'>B01-私人控股</option>
							<option value='B0101'>B0101-私人相对控股</option>
							<option value='B0102'>B0102-私人绝对控股</option>
							<option value='B02'>B02-港澳台控股</option>
							<option value='B0201'>B0201-港澳台相对控股</option>
							<option value='B0202'>B0202-港澳台绝对控股</option>
							<option value='B03'>B03-外商控股</option>
							<option value='B0301'>B0301-外商相对控股</option>
							<option value='B0302'>B0302-外商绝对控股</option>
                        </select>
                    </td>
     <td align='right'>
                        客户国民经济部门:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_khgmjjbm' name='khgmjjbm' disabled="disabled" style='width:173px;'>
                        	<option value='A'>A-广义政府</option>
							<option value='A01'>A01-中央政府</option>
							<option value='A02'>A02-地方政府</option>
							<option value='A03'>A03-社会保障基金</option>
							<option value='A04'>A04-机关团体</option>
							<option value='A05'>A05-部队</option>
							<option value='A06'>A06-住房公积金</option>
							<option value='A99'>A99-其他</option>
							<option value='B'>B-金融机构部门</option>
							<option value='B01'>B01-货币当局</option>
							<option value='B02'>B02-监管当局</option>
							<option value='B03'>B03-银行业存款类金融机构</option>
							<option value='B04'>B04-银行业非存款类金融机构</option>
							<option value='B05'>B05-证券业金融机构</option>
							<option value='B06'>B06-保险业金融机构</option>
							<option value='B07'>B07-交易及结算类金融机构</option>
							<option value='B08'>B08-金融控股公司</option>
							<option value='B09'>B09-特定目的载体</option>
							<option value='B99'>B99-其他</option>
							<option value='C'>C-非金融企业部门</option>
							<option value='C01'>C01-公司</option>
							<option value='C02'>C02-非公司企业</option>
							<option value='C99'>C99-其他非金融企业部门</option>
							<option value='D'>D-住户部门</option>
							<option value='D01'>D01-住户</option>
							<option value='D02'>D02-为住户服务的非营利机构</option>
							<option value='E'>E-非居民部门</option>
							<option value='E01'>E01-国际组织</option>
							<option value='E02'>E02-外国政府</option>
							<option value='E03'>E03-境外金融机构</option>
							<option value='E04'>E04-境外非金融企业</option>
							<option value='E05'>E05-外国居民</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户信用级别总等级数:</td>
                    <td>
                        <input id='info_khxyjbzdjs' name='khxyjbzdjs' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户信用评级:
                    </td>
                    <td>
                        <input id='info_khxypj' name='khxypj' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    数据日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
</tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var tablelazyload = "<table class='enterTable' id='enterTable'>"
    	+"<tr> <td align='right'>"
    	+"                    金融机构代码:</td>"
    	+"                    <td>"
    	+"                        <input id='financeorgcode' name='financeorgcode'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        客户名称:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <input id='khname' name='khname'  style='width:173px;'/>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    客户代码:</td>"
    	+"                    <td>"
    	+"                        <input id='khcode' name='khcode'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        客户金融机构编码:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <input id='khjrjgbm' name='khjrjgbm'  style='width:173px;'/>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    客户内部编码:</td>"
    	+"                    <td>"
    	+"                        <input id='khnbbm' name='khnbbm'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        基本存款账号:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <input id='jbckzh' name='jbckzh'  style='width:173px;'/>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    基本账户开户行名称:</td>"
    	+"                    <td>"
    	+"                        <input id='jbzhkhhmc' name='jbzhkhhmc'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        注册地址:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <input id='zcdz' name='zcdz'  style='width:173px;'/>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    地区代码:</td>"
    	+"                    <td>"
    	+"            <select class='easyui-combotree' id='dqdm' name='dqdm' style='height: 23px; width:173px'  data-options=\"url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:false,value:''\">          "
        +"            </select>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        客户类别:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <select class='easyui-combobox' id='khtype' name='khtype'  style='width:173px;'>"
    	+"                       		<option value='B0311'>B0311-中资大型银行</option>"
    	+"							<option value='B0312'>B0312-中资中型银行</option>"
    	+"							<option value='B03131'>B03131-小型城市商业银行</option>"
    	+"							<option value='B03132'>B03132-农村商业银行</option>"
    	+"							<option value='B03133'>B03133-农村合作银行</option>"
    	+"							<option value='B03134'>B03134-村镇银行</option>"
    	+"							<option value='B032'>B032-城市信用合作社</option>"
    	+"							<option value='B033'>B033-农村信用社</option>"
    	+"							<option value='B034'>B034-农村资金互助社</option>"
    	+"							<option value='B035'>B035-财务公司</option>"
    	+"							<option value='B036'>B036-外资银行</option>"
    	+"							<option value='B041'>B041-信托公司</option>"
    	+"							<option value='B042'>B042-金融资产管理公司</option>"
    	+"							<option value='B043'>B043-金融租赁公司</option>"
    	+"							<option value='B044'>B044-汽车金融公司</option>"
    	+"							<option value='B045'>B045-贷款公司</option>"
    	+"							<option value='B046'>B046-货币经纪公司</option>"
    	+"							<option value='B047'>B047-消费金融公司</option>"
    	+"							<option value='B051'>B051-证券公司</option>"
    	+"							<option value='B052'>B052-证券投资基金公司</option>"
    	+"							<option value='B053'>B053-期货公司</option>"
    	+"							<option value='B054'>B054-投资咨询公司</option>"
    	+"							<option value='B055'>B055-银行理财子公司</option>"
    	+"							<option value='B056'>B056-证券子公司</option>"
    	+"							<option value='B057'>B057-基金子公司</option>"
    	+"							<option value='B058'>B058-期货子公司</option>"
    	+"							<option value='B060'>B060-财产保险公司</option>"
    	+"							<option value='B061'>B061-人身保险公司</option>"
    	+"							<option value='B062'>B062-再保险公司</option>"
    	+"							<option value='B063'>B063-保险资产管理公司</option>"
    	+"							<option value='B064'>B064-保险经纪公司</option>"
    	+"							<option value='B065'>B065-保险代理公司</option>"
    	+"							<option value='B066'>B066-保险公估公司</option>"
    	+"							<option value='B067'>B067-保险集团（控股）公司</option>"
    	+"							<option value='B068'>B068-企业（职业）年金</option>"
    	+"							<option value='B07'>B07-交易及结算类金融机构</option>"
    	+"							<option value='B08'>B08-金融控股公司</option>"
    	+"							<option value='B091'>B091-银行理财产品</option>"
    	+"							<option value='B092'>B092-信托公司资管产品</option>"
    	+"							<option value='B093'>B093-证券公司及其子公司资管产品</option>"
    	+"							<option value='B094'>B094-基金公司及其子公司专户</option>"
    	+"							<option value='B095'>B095-期货公司及其子公司资管产品</option>"
    	+"							<option value='B096'>B096-公募基金</option>"
    	+"							<option value='B097'>B097-私募机构私募基金</option>"
    	+"							<option value='B098'>B098-保险资管产品</option>"
    	+"							<option value='B099'>B099-金融资产投资公司资管产品</option>"
    	+"							<option value='B10'>B10-其他金融机构</option>"
    	+"							<option value='E012'>E012-境外金融机构</option>"
    	+"                        </select>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    成立日期:</td>"
    	+"                    <td>"
    	+"                        <input class='easyui-datebox' id='setupdate' name='setupdate'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        是否关联方:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <select class='easyui-combobox' id='sfglf' name='sfglf'  style='width:173px;'>"
    	+"                        	<option value='1'>1-是</option>"
    	+"                        	<option value='0'>0-否</option>"
    	+"                        </select>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    客户经济成分:</td>"
    	+"                    <td>"
    	+"                        <select class='easyui-combobox' id='khjjcf' name='khjjcf'  style='width:173px;'>"
    	+"                        	<option value='A'>A-公有控股经济</option>"
    	+"							<option value='A01'>A01-国有控股</option>"
    	+"							<option value='A0101'>A0101-国有相对控股</option>"
    	+"							<option value='A0102'>A0102-国有绝对控股</option>"
    	+"							<option value='A02'>A02-集体控股</option>"
    	+"							<option value='A0201'>A0201-集体相对控股</option>"
    	+"							<option value='A0202'>A0202-集体绝对控股</option>"
    	+"							<option value='B'>B-非公有控股经济</option>"
    	+"							<option value='B01'>B01-私人控股</option>"
    	+"							<option value='B0101'>B0101-私人相对控股</option>"
    	+"							<option value='B0102'>B0102-私人绝对控股</option>"
    	+"							<option value='B02'>B02-港澳台控股</option>"
    	+"							<option value='B0201'>B0201-港澳台相对控股</option>"
    	+"							<option value='B0202'>B0202-港澳台绝对控股</option>"
    	+"							<option value='B03'>B03-外商控股</option>"
    	+"							<option value='B0301'>B0301-外商相对控股</option>"
    	+"							<option value='B0302'>B0302-外商绝对控股</option>"
    	+"                        </select>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        客户国民经济部门:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <select class='easyui-combobox' id='khgmjjbm' name='khgmjjbm'  style='width:173px;'>"
    	+"                        	<option value='A'>A-广义政府</option>"
    	+"							<option value='A01'>A01-中央政府</option>"
    	+"							<option value='A02'>A02-地方政府</option>"
    	+"							<option value='A03'>A03-社会保障基金</option>"
    	+"							<option value='A04'>A04-机关团体</option>"
    	+"							<option value='A05'>A05-部队</option>"
    	+"							<option value='A06'>A06-住房公积金</option>"
    	+"							<option value='A99'>A99-其他</option>"
    	+"							<option value='B'>B-金融机构部门</option>"
    	+"							<option value='B01'>B01-货币当局</option>"
    	+"							<option value='B02'>B02-监管当局</option>"
    	+"							<option value='B03'>B03-银行业存款类金融机构</option>"
    	+"							<option value='B04'>B04-银行业非存款类金融机构</option>"
    	+"							<option value='B05'>B05-证券业金融机构</option>"
    	+"							<option value='B06'>B06-保险业金融机构</option>"
    	+"							<option value='B07'>B07-交易及结算类金融机构</option>"
    	+"							<option value='B08'>B08-金融控股公司</option>"
    	+"							<option value='B09'>B09-特定目的载体</option>"
    	+"							<option value='B99'>B99-其他</option>"
    	+"							<option value='C'>C-非金融企业部门</option>"
    	+"							<option value='C01'>C01-公司</option>"
    	+"							<option value='C02'>C02-非公司企业</option>"
    	+"							<option value='C99'>C99-其他非金融企业部门</option>"
    	+"							<option value='D'>D-住户部门</option>"
    	+"							<option value='D01'>D01-住户</option>"
    	+"							<option value='D02'>D02-为住户服务的非营利机构</option>"
    	+"							<option value='E'>E-非居民部门</option>"
    	+"							<option value='E01'>E01-国际组织</option>"
    	+"							<option value='E02'>E02-外国政府</option>"
    	+"							<option value='E03'>E03-境外金融机构</option>"
    	+"							<option value='E04'>E04-境外非金融企业</option>"
    	+"							<option value='E05'>E05-外国居民</option>"
    	+"                        </select>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    客户信用级别总等级数:</td>"
    	+"                    <td>"
    	+"                        <input id='khxyjbzdjs' name='khxyjbzdjs'  style='width:173px;'/>"
    	+"                    </td>"
    	+"     <td align='right'>"
    	+"                        客户信用评级:"
    	+"                    </td>"
    	+"                    <td>"
    	+"                        <input id='khxypj' name='khxypj'  style='width:173px;'/>"
    	+"                    </td>"
    	+"                </tr>"
    	+"<tr> <td align='right'>"
    	+"                    数据日期:</td>"
    	+"                    <td>"
    	+"                        <input class='easyui-datebox' id='sjrq' name='sjrq'  style='width:173px;'/>"
    	+"                    </td>"
    	+"</tr>"
        +"</table>"
    var index = 5;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var handlename = '<%= handlename%>';
    var role = '<%= role%>';
    var shifoushenheziji = '<%= shifoushenheziji%>';
    $(function(){
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetXtykhjcxxData?datastatus='+'${datastatus}',
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
                {field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'operationname',title:'操作名',width:150,align:'center'},
                {field:'nopassreason',title:'审核不通过原因',width:150,align:'center'},
                {field:'checkstatus',title:'校验结果',width:150,align:'center',formatter:function (value) {
                        if (value=='0'){
                            return "未校验";
                        }else if(value=='1'){
                            return "校验成功";
                        }else if (value=='2'){
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }},
                    {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                    {field:'khname',title:'客户名称',width:150,align:'center'},
                    {field:'khcode',title:'客户代码',width:150,align:'center'},
                    {field:'khjrjgbm',title:'客户金融机构编码',width:150,align:'center'},
                    {field:'khnbbm',title:'客户内部编码',width:150,align:'center'},
                    {field:'jbckzh',title:'基本存款账号',width:150,align:'center'},
                    {field:'jbzhkhhmc',title:'基本账户开户行名称',width:150,align:'center'},
                    {field:'zcdz',title:'注册地址',width:150,align:'center'},
                    {field:'dqdm',title:'地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                    {field:'khtype',title:'客户类别',width:150,align:'center',formatter:function(value){
                    	if (value=='B0311'){
                    		  return "B0311-中资大型银行";
                    		}else if (value=='B0312'){
                    		  return "B0312-中资中型银行";
                    		}else if (value=='B03131'){
                    		  return "B03131-小型城市商业银行";
                    		}else if (value=='B03132'){
                    		  return "B03132-农村商业银行";
                    		}else if (value=='B03133'){
                    		  return "B03133-农村合作银行";
                    		}else if (value=='B03134'){
                    		  return "B03134-村镇银行";
                    		}else if (value=='B032'){
                    		  return "B032-城市信用合作社";
                    		}else if (value=='B033'){
                    		  return "B033-农村信用社";
                    		}else if (value=='B034'){
                    		  return "B034-农村资金互助社";
                    		}else if (value=='B035'){
                    		  return "B035-财务公司";
                    		}else if (value=='B036'){
                    		  return "B036-外资银行";
                    		}else if (value=='B041'){
                    		  return "B041-信托公司";
                    		}else if (value=='B042'){
                    		  return "B042-金融资产管理公司";
                    		}else if (value=='B043'){
                    		  return "B043-金融租赁公司";
                    		}else if (value=='B044'){
                    		  return "B044-汽车金融公司";
                    		}else if (value=='B045'){
                    		  return "B045-贷款公司";
                    		}else if (value=='B046'){
                    		  return "B046-货币经纪公司";
                    		}else if (value=='B047'){
                    		  return "B047-消费金融公司";
                    		}else if (value=='B051'){
                    		  return "B051-证券公司";
                    		}else if (value=='B052'){
                    		  return "B052-证券投资基金公司";
                    		}else if (value=='B053'){
                    		  return "B053-期货公司";
                    		}else if (value=='B054'){
                    		  return "B054-投资咨询公司";
                    		}else if (value=='B055'){
                    		  return "B055-银行理财子公司";
                    		}else if (value=='B056'){
                    		  return "B056-证券子公司";
                    		}else if (value=='B057'){
                    		  return "B057-基金子公司";
                    		}else if (value=='B058'){
                    		  return "B058-期货子公司";
                    		}else if (value=='B060'){
                    		  return "B060-财产保险公司";
                    		}else if (value=='B061'){
                    		  return "B061-人身保险公司";
                    		}else if (value=='B062'){
                    		  return "B062-再保险公司";
                    		}else if (value=='B063'){
                    		  return "B063-保险资产管理公司";
                    		}else if (value=='B064'){
                    		  return "B064-保险经纪公司";
                    		}else if (value=='B065'){
                    		  return "B065-保险代理公司";
                    		}else if (value=='B066'){
                    		  return "B066-保险公估公司";
                    		}else if (value=='B067'){
                    		  return "B067-保险集团（控股）公司";
                    		}else if (value=='B068'){
                    		  return "B068-企业（职业）年金";
                    		}else if (value=='B07'){
                    		  return "B07-交易及结算类金融机构";
                    		}else if (value=='B08'){
                    		  return "B08-金融控股公司";
                    		}else if (value=='B091'){
                    		  return "B091-银行理财产品";
                    		}else if (value=='B092'){
                    		  return "B092-信托公司资管产品";
                    		}else if (value=='B093'){
                    		  return "B093-证券公司及其子公司资管产品";
                    		}else if (value=='B094'){
                    		  return "B094-基金公司及其子公司专户";
                    		}else if (value=='B095'){
                    		  return "B095-期货公司及其子公司资管产品";
                    		}else if (value=='B096'){
                    		  return "B096-公募基金";
                    		}else if (value=='B097'){
                    		  return "B097-私募机构私募基金";
                    		}else if (value=='B098'){
                    		  return "B098-保险资管产品";
                    		}else if (value=='B099'){
                    		  return "B099-金融资产投资公司资管产品";
                    		}else if (value=='B10'){
                    		  return "B10-其他金融机构";
                    		}else if (value=='E012'){
                    		  return "E012-境外金融机构";
                    		}
                    }},
                    {field:'setupdate',title:'成立日期',width:150,align:'center'},
                    {field:'sfglf',title:'是否关联方',width:150,align:'center',formatter:function (value) {
                        if (value=='1'){
                            return "1-是";
                        }else if(value=='0'){
                            return "0-否";
                        }
                        return value;
                    }},
                    {field:'khjjcf',title:'客户经济成分',width:150,align:'center',formatter:function (value) {
                    	if (value=='A'){
                    		  return "A-公有控股经济";
                    		}else if (value=='A01'){
                    		  return "A01-国有控股";
                    		}else if (value=='A0101'){
                    		  return "A0101-国有相对控股";
                    		}else if (value=='A0102'){
                    		  return "A0102-国有绝对控股";
                    		}else if (value=='A02'){
                    		  return "A02-集体控股";
                    		}else if (value=='A0201'){
                    		  return "A0201-集体相对控股";
                    		}else if (value=='A0202'){
                    		  return "A0202-集体绝对控股";
                    		}else if (value=='B'){
                    		  return "B-非公有控股经济";
                    		}else if (value=='B01'){
                    		  return "B01-私人控股";
                    		}else if (value=='B0101'){
                    		  return "B0101-私人相对控股";
                    		}else if (value=='B0102'){
                    		  return "B0102-私人绝对控股";
                    		}else if (value=='B02'){
                    		  return "B02-港澳台控股";
                    		}else if (value=='B0201'){
                    		  return "B0201-港澳台相对控股";
                    		}else if (value=='B0202'){
                    		  return "B0202-港澳台绝对控股";
                    		}else if (value=='B03'){
                    		  return "B03-外商控股";
                    		}else if (value=='B0301'){
                    		  return "B0301-外商相对控股";
                    		}else if (value=='B0302'){
                    		  return "B0302-外商绝对控股";
                        }
                        return value;
                    }},
                    {field:'khgmjjbm',title:'客户国民经济部门',width:150,align:'center',formatter:function (value) {
                    	if (value=='A'){
                    		  return "A-广义政府";
                    		}else if (value=='A01'){
                    		  return "A01-中央政府";
                    		}else if (value=='A02'){
                    		  return "A02-地方政府";
                    		}else if (value=='A03'){
                    		  return "A03-社会保障基金";
                    		}else if (value=='A04'){
                    		  return "A04-机关团体";
                    		}else if (value=='A05'){
                    		  return "A05-部队";
                    		}else if (value=='A06'){
                    		  return "A06-住房公积金";
                    		}else if (value=='A99'){
                    		  return "A99-其他";
                    		}else if (value=='B'){
                    		  return "B-金融机构部门";
                    		}else if (value=='B01'){
                    		  return "B01-货币当局";
                    		}else if (value=='B02'){
                    		  return "B02-监管当局";
                    		}else if (value=='B03'){
                    		  return "B03-银行业存款类金融机构";
                    		}else if (value=='B04'){
                    		  return "B04-银行业非存款类金融机构";
                    		}else if (value=='B05'){
                    		  return "B05-证券业金融机构";
                    		}else if (value=='B06'){
                    		  return "B06-保险业金融机构";
                    		}else if (value=='B07'){
                    		  return "B07-交易及结算类金融机构";
                    		}else if (value=='B08'){
                    		  return "B08-金融控股公司";
                    		}else if (value=='B09'){
                    		  return "B09-特定目的载体";
                    		}else if (value=='B99'){
                    		  return "B99-其他";
                    		}else if (value=='C'){
                    		  return "C-非金融企业部门";
                    		}else if (value=='C01'){
                    		  return "C01-公司";
                    		}else if (value=='C02'){
                    		  return "C02-非公司企业";
                    		}else if (value=='C99'){
                    		  return "C99-其他非金融企业部门";
                    		}else if (value=='D'){
                    		  return "D-住户部门";
                    		}else if (value=='D01'){
                    		  return "D01-住户";
                    		}else if (value=='D02'){
                    		  return "D02-为住户服务的非营利机构";
                    		}else if (value=='E'){
                    		  return "E-非居民部门";
                    		}else if (value=='E01'){
                    		  return "E01-国际组织";
                    		}else if (value=='E02'){
                    		  return "E02-外国政府";
                    		}else if (value=='E03'){
                    		  return "E03-境外金融机构";
                    		}else if (value=='E04'){
                    		  return "E04-境外非金融企业";
                    		}else if (value=='E05'){
                    		  return "E05-外国居民";
                        }
                        return value;
                    }},
                    {field:'khxyjbzdjs',title:'客户信用级别总等级数',width:150,align:'center'},
                    {field:'khxypj',title:'客户信用评级',width:150,align:'center'},
                    {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex,rowData) {
                //$("#tbForAddDialog").hide();//隐藏div
                //disableOcx();
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','同业客户基础信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $("#formForAdd").append(tablelazyload);
        $.parser.parse($("#enterTable").parent());
        $(".combo").click(function (e) {
            if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
                if (!$(this).prev().prop("disabled")){
                    if ($(this).prev().combobox("panel").is(":visible")) {
                        $(this).prev().combobox("hidePanel");
                    } else {
                        $(this).prev().combobox("showPanel");
                    }
                }
            }
        });

       
    })

    function disableOcx() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = "true";
        }
        /*$("#financeorgcode").attr("readOnly",true);
        $("#financeorginnum").attr("readOnly",true);
        $("#financeorgareacode").combobox({disabled: true});
        $("#brroweridnum").attr("readOnly",true);
        $("#brrowerindustry").combobox({disabled: true});
        $("#brrowerareacode").combobox({disabled: true});
        $("#inverstoreconomy").combobox({disabled: true});
        $("#enterprisescale").combobox({disabled: true});
        $("#productcetegory").combobox({disabled: true});
        $("#loanactualdirection").combobox({disabled: true});
        $("#loanstartdate").combobox({disabled: true});
        $("#loanenddate").combobox({disabled: true});
        $("#extensiondate").combobox({disabled: true});
        $("#currency").combobox({disabled: true});
        $("#receiptbalance").combobox({disabled: true});*/
    }

    function noDisable() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = false;
        }
    }

    // 查询
    function searchOnSelected(value){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");

        var operationnameParam = '';
        if (value == 'dsh'){
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam});
    }

    // 新增
    function add(){
        resetForAdd();

        $('#dialog1').dialog('open').dialog('center').dialog('setTitle','新增同业客户基础信息');
        keydownsearch();
    }

    // 修改
    function edit(){
        var rows = $('#dg1').datagrid('getSelections');
        if(rows.length == 1){
            //$("#formForAdd").form('reset');
            resetForAdd();
            $("#formForAdd").form("load", rows[0]);
            $('#dialog1').dialog('open').dialog('center').dialog('setTitle','修改同业客户基础信息');
            keydownsearch();
        }else{
            $.messager.alert("提示", "请选择一条数据进行修改!", "info");
        }
    }

    // 保存
    function save() {
        if($("#formForAdd").form('validate')){
            $.ajax({
                type: "POST",//为post请求
                url: "XsaveXtykhjcxx",//这是我在后台接受数据的文件名
                data: $('#formForAdd').serialize(),//将该表单序列化
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.status == "1") {
                        $.messager.alert('提示', '提交成功!', 'info');
                        $("#dialog1").dialog("close");
                        $("#dg1").datagrid("reload");
                    } else {
                        $.messager.alert('提示', '提交失败!', 'info');
                    }
                },
                error: function (err) {//请求失败之后的操作

                }
            });
        }
    }

    // 重置
    function resetForAdd(){
        $('#formForAdd').form('clear');
    }

    // 取消
    function cancel(){
        $('#dialog1').dialog('close');
    }

    // 删除
    function deleteit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        if(rows.length > 0){
            info = '是否删除所选中的数据？';
        }else {
            info = '您没有选择数据，是否删除所有数据？';
        }
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");

        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XDeleteXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','删除失败','error');
                    }
                });
                $("#dg1").datagrid("reload");
            }
        });
    }

    // 导入Excel相关
    function importExcel(){
        document.getElementById("importbtn").disabled = "disabled";
        $('#excelfile').val('');
        $('#importExcel').dialog('open').dialog('center');
    }

    function inexcel(){
        var file = document.getElementById("excelfile").files[0];
        if(file == null){
            document.getElementById("importbtn").disabled = "disabled";
        }else{
            var fileName = file.name;
            var fileType = fileName.substring(fileName.lastIndexOf('.'),
                fileName.length);
            if (fileType == '.xls' || fileType == '.xlsx'){
                if (file) {
                    document.getElementById("importbtn").disabled = "";
                }
            } else {
                $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
                document.getElementById("importbtn").disabled = "disabled";
            }
        }
    }

    function excel(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在导入中......'
        });
        $.ajaxFileUpload({
            type: "post",
            url: 'XimmportExceltykhjcxx',
            fileElementId: 'excelfile',
            secureuri: false,
            dataType: 'json',
            success: function (data) {
            	const that = data.msg;
                $.messager.progress('close');
                if (that == "导入成功") {
                    $.messager.alert('提示', "导入成功", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                } else if (that.startsWith("导入模板不正确")) {
                    $.messager.alert('提示', that, 'error');
                } else {
                    $.messager.show({
                        title: '导入反馈',
                        msg: "<div style='overflow-y:scroll;height:100%'>" + escape2Html(that) + "</div>",
                        timeout: 0,
                        showType: 'show',
                        width: 600,
                        height: 700,
                        style: {
                            right: '100',
                            top: document.body.scrollTop + document.documentElement.scrollTop
                        }
                    });
                }
            },
            error: function () {
                $.messager.progress('close');
                $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
            }
        });
    }

    // 导出
    function showOut(model){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        if (model){
            id = '00000000';
            info = '是否导出模板？';
        }else {
            for( var dataIndex in row){
                id = id + row[dataIndex].id + ",";
            }
            if(id != ''){
                info = '是否将选中的数据导出到Excel表中？';
            }else {
                info = '是否将全部数据导出到Excel表中？'
            }
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXtykhjcxx?financeorgcodeParam="+financeorgcodeParam+"&khnameParam="+khnameParam+"&khcodeParam="+khcodeParam+"&khtypeParam="+khtypeParam+"&checkstatusParam="+checkstatusParam +"&id="+id + "&datastatus="+'${datastatus}';
            }
        });
    }

    // 校验
  function check(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
            if (row[dataIndex].checkstatus != 0){
                $.messager.alert("提示", "只能校验未校验的数据", "info");
                return;
            }
        }
        var info = '';
        if(id != ''){
            info = '是否校验选中的数据？';
        }else {
            info = '是否校验所有数据？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");

        $.messager.confirm('提示',info,function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在校验数据中......',
                   /* timeout: 100000,*/
                });

                $.ajax({
                    url: "XCheckDataXtykhjcxx",
                    dataType: "json",
                    async: true,
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam},
                    type: "POST",
                    success: function (data) {
                        $("#dg1").datagrid("reload");
                        $.messager.progress('close');
                        if (data.msg == "1") {
                            $.messager.alert('提示', '校验成功！', 'info');
                        }else if (data.msg == "-1") {
                            $.messager.alert('提示', '没有可校验的数据！', 'info');
                        } else {
                            $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                                window.location.href = "XDownLoadCheckXtykhjcxx";
                            });
                        }
                    },
                    error: function () {
                        $.messager.progress('close');
                        $.messager.alert("提示", "校验出错，请重新校验！", "error");
                    }
                });
            }
        })
    }





    // 提交
    function submitit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            for(var i = 0;i < rows.length;i++){
                if(rows[i].checkstatus != '1'){
                    $.messager.alert("提示", "只能提交校验成功的数据", "info");
                    return;
                }
                id = id + rows[i].id + ',';
            }
            info = '是否提交所选中的数据？';
        }else {
            info = '您没有选择数据，是否提交所有数据？';
        }
        $.messager.confirm('提示',info,function (r){
            if(r){
                var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
                var khnameParam = $("#khnameParam").val().trim();
                var checkstatusParam = $("#checkstatusParam").combobox("getValue");
                var khcodeParam = $("#khcodeParam").val().trim();
                var khtypeParam = $("#khtypeParam").combobox("getValue");
                $.messager.confirm('提示',info,function (r){
                    if(r){
                        $.ajax({
                            sync: true,
                            type: "POST",
                            dataType: "json",
                            url: "XsubmitXtykhjcxx",
                            data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                            success: function (data) {
                                if (data == 0){
                                    $.messager.alert('操作提示','提交成功','info');
                                    $("#dg1").datagrid("reload");
                                }else {
                                    $.messager.alert('操作提示','提交失败','info');
                                }
                            },
                            error: function (err) {
                                $.messager.alert('操作提示','提交失败','error');
                            }
                        });
                        $("#dg1").datagrid("reload");
                    }
                });
            }
        });
    }

    //申请删除dsh
    function applyDelete() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请删除所选中的数据？';
        }else {
            info = '是否申请删除所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyDeleteXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请删除失败','error');
                    }
                });
            }
        });
    }

    //申请修改
    function applyEdit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请修改所选中的数据？';
        }else {
            info = '是否申请修改所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyEditXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请修改成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请修改失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请修改失败','error');
                    }
                });
            }
        });
    }

    /*同意申请*/
    function agreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否同意所选中的申请？';
        }else {
            info = '是否同意所有的申请？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeApplyXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','同意成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','同意失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','同意失败','error');
                    }
                });
            }
        });
    }

    /*拒绝申请*/
    function noAgreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否拒绝所选中的申请？';
        }else {
            info = '是否拒绝所有的申请？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeApplyXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','拒绝成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','拒绝失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','拒绝意失败','error');
                    }
                });
            }
        });
    }

    /*审核通过*/
    function agreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核通过所选中的数据？';
        }else {
            info = '是否审核通过所有数据？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeAuditXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核通过失败','error');
                    }
                });
            }
        });
    }

    /*审核不通过*/
    function noAgreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核不通过所选中的数据？';
        }else {
            info = '是否审核不通过所有数据？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.prompt('提示',info,function (r){
            if(r){
                console.info("12")
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeAuditXtykhjcxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam":khcodeParam,"khtypeParam":khtypeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"reason":r},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核不通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核不通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核不通过失败','error');
                    }
                });
            }else {
                $.messager.alert('操作提示','审核不通过原因必填','info');
            }
        });
    }

    //前台校验
    $.extend($.fn.datebox.defaults.rules, {
        normalDate : {
            validator : function(value) {
                var startTmp = new Date('1800-01-01');
                var endTmp = new Date('2100-12-31');
                var nowTmp = new Date(value);
                return nowTmp > startTmp && nowTmp < endTmp;
            },
            message : '日期早于1800-01-01且晚于2100-12-31'
        },
        dateFormat : {
            validator : function(value) {
                var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
                return reg.test(value);
            },
            message : '日期格式必须满足:YYYY-MM-DD。'
        },



        compareToExten : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                var LS04 = $('#loanstatus').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                if(LS04 != "LS04"){
                    var startTmp = new Date(extensiondate);
                    var nowTmp = new Date(value);
                    return nowTmp <= startTmp;
                }
                return true;
            },
            message : '贷款到期日期应小于等于贷款展期到期日期'
        },
        compareToExtenLS : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                var LS04 = $('#loanstatus').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                if(LS04 == "LS04"){
                    var startTmp = new Date(extensiondate);
                    var nowTmp = new Date(value);
                    return nowTmp >startTmp;
                }
                return true;
            },
            message : '贷款状态为LS04-缩期时，贷款到期日期应大于贷款展期到期日期'
        },

        startcompareToExten : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                var startTmp = new Date(extensiondate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '贷款发放日期应小于等于贷款展期到期日期'
        },
        compareToEnd : {
            validator : function(value) {
                var loanenddate = $('#loanenddate').combobox('getValue');
                if (loanenddate == null || loanenddate ==''){
                    return true;
                }
                var startTmp = new Date(loanenddate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '贷款发放日期应小于等于贷款到期日期'
        },
        teshu:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },
        teshua:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },

        amount : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,18}(\.\d{2})?|0\.\d{2})$/;
                return reg.test(value);
            },
            message : '总长度不超过20位的，精度保留小数点后两位。'
        },


        baserate : {
            validator : function(value) {
                if (value.indexOf("%") == -1 && value.indexOf("‰") == -1){
                    return true
                }
                return false;
            },
            message : '基准利率不能包含‰或%'
        },
        ratelevel : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,3}(\.\d{5})?|0\.\d{5})$/;
                return reg.test(value);
            },
            message : '总长度不能超过10位，小数位应保留5位'
        },


    });
    function keydownsearch(){
        //combobox可编辑，自定义模糊查询
        $.fn.combobox.defaults.editable = true;
        $.fn.combobox.defaults.filter = function(q, row){
            var opts = $(this).combobox('options');
            return row[opts.textField].indexOf(q) >= 0;
        };
    }
</script>
</body>
</html>
