<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>金融机构（分支机构）基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="金融机构（分支机构）基础信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		金融机构名称 ：<input type="text" id="selectjrjgmc" name="selectjrjgmc" style="height: 23px; width:130px"/>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<a class="easyui-linkbutton" iconCls="icon-add" onclick="xinzeng()">新增</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="xiugai()">修改</a>&nbsp;&nbsp;
		<!-- <a class="easyui-linkbutton" iconCls="icon-remove" onclick="shanchu()">删除</a>&nbsp;&nbsp; -->
		<a class="easyui-linkbutton" iconCls="icon-remove" onclick="shenqingshanchu()">申请删除</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-accept" onclick="jiaoyan()">校验</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="tijiao()">提交</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru()">导入</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochumoban()">导出模版</a>
	</div>
	
	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <!-- <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table> -->
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
        </div>
    </div>	
<!-- 导出进度条 -->
<div style="visibility: hidden;">
  <div id="exportFileDialog" class="easyui-dialog" style="width:550px;height:100px;padding-left: 10px;top:200px;" title="导出数据"> 
	<div align="center"><span id="daorutishi">请您稍等,数据正在导出中......</span></div>
	<div id="exportFile" class="easyui-progressbar" style="width:500px;heigth:50px;"></div>
  </div>
</div>	
<div style="visibility: hidden;">
    <div id="dialog1" class="easyui-dialog" title=""  data-options="iconCls:'icon-save',toolbar:'#tbForEditDialog'" style="width:520px;height:520px;">
          <div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding" onclick="saveForEditDialog()">确定</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">取消</a>
          </div>
          
         <form id="formForEditDepartment" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname" name="finorgname" style="height: 23px; width:230px" data-options="required:true,validType:['length[1,200]']" missingMessage="必填项"/></td>
                </tr> 
                 
               <tr>
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode" name="finorgcode" style="height: 23px; width:230px" maxlength="18"  data-options="required:true,validType:['teshu']"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum" name="finorgnum" style="height: 23px; width:230px" maxlength="14" data-options="required:true,validType:['teshu1']" /></td>
			   </tr>
			   
			   <tr>  
                 <td>内部机构号 :</td>
                 <td><input class="easyui-validatebox" id="inorgnum" name="inorgnum" style="height: 23px; width:230px"  maxlength="30" data-options="required:true,validType:['teshu1']"/></td>
               </tr> 
			   
			   <tr> 
                 <td>许可证号 :</td>
                 <td><input class="easyui-validatebox" id="xkzh" name="xkzh" style="height: 23px; width:230px" maxlength="15" data-options="required:true,validType:['teshu1']"/></td>
			   </tr>
			   
			   <tr>  
                 <td>支付行号 :</td>
                 <td><input class="easyui-validatebox" id="zfhh" name="zfhh" style="height: 23px; width:230px" maxlength="14" data-options="required:true,validType:['teshu1']"/></td>
               </tr>
			   
			   <tr>
			     <td>机构级别:</td>
                 <td>
                 <select class="easyui-combobox" id="orglevel" name="orglevel" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="01">01:总行</option>
						<option value="02">02:分行</option>
						<option value="03">03:支行</option>
						<option value="04">04:网点（分理处、储蓄所等）</option>
						<option value="05">05:事业部</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>直属上级管理机构名称:</td>
                 <td><input class="easyui-validatebox" id="highlevelorgname" name="highlevelorgname" style="height: 23px; width:230px" maxlength="200" data-options="required:true,validType:['teshu1']" /></td>
               </tr>
			   
			   <tr>  
                 <td>直属上级管理机构金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="highlevelfinorgcode" name="highlevelfinorgcode" style="height: 23px; width:230px" maxlength="14" data-options="required:true,validType:['teshu1']"/></td>
               </tr>
			   
			   <tr>
			     <td>直属上级管理机构内部机构号 :</td>
			     <td><input class="easyui-validatebox" id="highlevelinorgnum" name="highlevelinorgnum" style="height: 23px; width:230px" maxlength="30" data-options="required:true,validType:['teshu1']"/></td>
			   </tr>
			   
			   <tr>
			     <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regarea" name="regarea" style="height: 23px; width:230px" data-options="required:true,validType:['length[1,400]']" missingMessage="必填项"/></td>
			   </tr>
			   
			   <tr>
			     <td>地区代码 :</td>
			     <td>
			        <input class="easyui-combotree" id="regareacode" name="regareacode" style="height: 23px; width:230px" editable=true data-options="required:true,url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:true,value:''">
			     </td>
			   </tr>

			   <!-- <tr>
			     <td>办公地址 :</td>
			     <td><input class="easyui-validatebox" id="workarea" name="workarea" style="height: 23px; width:230px" data-options="required:true,validType:['length[1,400]']" missingMessage="必填项"/></td>
			   </tr>

			   <tr>
			     <td>办公地行政区划代码 :</td>
			     <td>
			        <input class="easyui-combobox" id="workareacode" name="workareacode" style="height: 23px; width:230px" editable=false data-options="valueField:'id',textField:'text',required:true" missingMessage="必填项"/>
			     </td>
			   </tr> -->

			   <tr>
			     <td>成立时间 :</td>
			     <td><input class="easyui-datebox" id="setupdate" name="setupdate" style="height: 23px; width:230px" editable=false data-options="required:true,validType:['dateFormat','normalDate']"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业状态 :</td>
			     <td>
			        <select class="easyui-combobox" id="mngmestus" name="mngmestus" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="01">01:正常运营</option>
					    <option value="02">02:停业（歇业）</option>
					    <option value="03">03:筹建</option>
					    <option value="04">04:当年关闭</option>
					    <option value="05">05:当年破产</option>
					    <option value="06">06:当年注销</option>
					    <option value="07">07:当年吊销</option>
					    <option value="99">99:其他</option>
	                </select>
			        <input type="hidden" id="id" name="id">
			        <input type="hidden" id="orgid" name="orgid">
			        <input type="hidden" id="departid" name="departid">
			        <input type="hidden" id="checkstatus" name="checkstatus">
			        <input type="hidden" id="datastatus" name="datastatus">
			        <input type="hidden" id="operator" name="operator">
			        <input type="hidden" id="operationname" name="operationname">
			        <input type="hidden" id="operationtime" name="operationtime">
			        <input type="hidden" id="nopassreason" name="nopassreason">
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input class="easyui-datebox" id="sjrq" name="sjrq" style="height: 23px; width:230px" editable=false data-options="required:true,validType:['dateFormat','normalDate']"/></td>
				 </tr>

			 </table>
           </div>
         </form>
	</div>
</div>

<!-- 详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="金融机构（分支机构）基础信息"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname_m" name="finorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode_m" name="finorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum_m" name="finorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>内部机构号 :</td>
                 <td><input class="easyui-validatebox" id="inorgnum_m" name="inorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
			   <tr> 
                 <td>许可证号 :</td>
                 <td><input class="easyui-validatebox" id="xkzh_m" name="xkzh" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>支付行号 :</td>
                 <td><input class="easyui-validatebox" id="zfhh_m" name="zfhh" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>机构级别:</td>
                 <td>
                 <select id="orglevel_m" name="orglevel" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:总行</option>
						<option value="02">02:分行</option>
						<option value="03">03:支行</option>
						<option value="04">04:网点（分理处、储蓄所等）</option>
						<option value="05">05:事业部</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>直属上级管理机构名称:</td>
                 <td><input class="easyui-validatebox" id="highlevelorgname_m" name="highlevelorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>直属上级管理机构金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="highlevelfinorgcode_m" name="highlevelfinorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>直属上级管理机构内部机构号 :</td>
			     <td><input class="easyui-validatebox" id="highlevelinorgnum_m" name="highlevelinorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regarea_m" name="regarea" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>地区代码 :</td>
			     <td>
			        <input id="regareacode_m" name="regareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <!-- <tr>
			     <td>办公地址 :</td>
			     <td>
			        <input class="easyui-validatebox" id="workarea_m" name="workarea" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>办公地行政区划代码 :</td>
			     <td>
			        <input id="workareacode_m" name="workareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr> -->
			   
			   <tr>
			     <td>成立时间 :</td>
			     <td><input id="setupdate_m" name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业状态 :</td>
			     <td>
			        <select id="mngmestus_m" name="mngmestus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常运营</option>
					    <option value="02">02:停业（歇业）</option>
					    <option value="03">03:筹建</option>
					    <option value="04">04:当年关闭</option>
					    <option value="05">05:当年破产</option>
					    <option value="06">06:当年注销</option>
					    <option value="07">07:当年吊销</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

			 </table>
           </div>
         </form>
	</div>
</div>

<script type="text/javascript">
$(function(){
	$('#dialog1').dialog('close');
	$('#dialog_m').dialog('close');
	$("#importExcel").dialog("close");
	$('#exportFileDialog').dialog('close');
	getbasecode();
	
	$("#dg1").datagrid({
		method:'post',
		url:'XfindJRJGFZJCXXdtj',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
			{field:'operationname',title:'操作名',width:100,align:'center'},
			{field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
			{field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
				if(value == '2'){
					return '<span style="color:red;">' + '校验失败' + '</span>';
				}else if(value == '1'){
					return '校验成功';
				}else if(value == '0'){
					return '未校验';
				}else{
					return value;
				}
			}},
			{field:'finorgname',title:'金融机构名称',width:100,align:'center'},
			{field:'finorgcode',title:'金融机构代码',width:100,align:'center'},
			{field:'finorgnum',title:'金融机构编码',width:100,align:'center'},
			{field:'inorgnum',title:'内部机构号',width:100,align:'center'},
			{field:'xkzh',title:'许可证号',width:100,align:'center'},
		    {field:'zfhh',title:'支付行号',width:100,align:'center'},
		    {field:'orglevel',title:'机构级别',width:100,align:'center'},
			{field:'highlevelorgname',title:'直属上级管理机构名称',width:100,align:'center'},
			{field:'highlevelfinorgcode',title:'直属上级管理机构金融机构编码',width:100,align:'center'},
			{field:'highlevelinorgnum',title:'直属上级管理机构内部机构号',width:100,align:'center'},
			{field:'regarea',title:'注册地址',width:100,align:'center'},
			{field:'regareacode',title:'地区代码',width:100,align:'center'
			},
			{field:'workarea',title:'办公地址',width:100,align:'center',hidden:true},
			{field:'workareacode',title:'办公地行政区划代码',width:100,align:'center',hidden:true},
			{field:'setupdate',title:'成立时间',width:100,align:'center'},
			{field:'mngmestus',title:'营业状态',width:100,align:'center'},
            {field:'sjrq',title:'数据日期',width:100,align:'center'},
			{field:'operator',title:'操作人',width:100,align:'center'},
		    {field:'operationtime',title:'操作时间',width:100,align:'center'}
	    ]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
        	$("#formForMore").form('load',rowData);
        	$('#dialog_m').dialog({modal : true});	
        	//$("#orglevel_m").combobox({disabled: true});
        	//$("#regareacode_m").combobox({disabled: true});
        	//$("#workareacode_m").combobox({disabled: true});
        	//$("#setupdate_m").datebox({disabled: true});
        	//$("#mngmestus_m").combobox({disabled: true});
        	//关闭弹窗默认隐藏div
    		/* $(dia).window({
    	    	onBeforeClose: function () {
    	    		//初始化表单的元素的状态
    	    		initForm();
    	    	}
    		}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//获取基础代码
function getbasecode(){
	//getAindustry("CURRENCY"); //一级行业代码
	// getArea(); //行政区划代码
	//getBindustry("customerIdType"); //二级行业代码
	//getCountry("customerIdType2"); //国家代码
	//getCurrency("artificialPersonType"); //币种代码
}

//获取行政区划代码
// function getArea(){
// 	$.ajax({
// 	    url:'getAreaCode',
// 	    type:'POST', //GET
// 	    async:true,    //或false,是否异步
// 	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
// 	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
// 	    success:function(data){
// 	    	var areaList = data.list;
// 		    if(areaList != null && areaList != ""){
// 		    	var jsonStr='[';
// 		    	for(var i = 0;i<areaList.length;i++){
// 		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].areacode+'",'+'"val":"'+areaList[i].areaname+'",'+'"text":"'+areaList[i].areacode+'-'+areaList[i].areaname+'"},'
// 		        }
// 		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
// 		        jsonStr = jsonStr + ']';
//
// 		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
// 		 	   $("#regareacode").combobox("loadData",jsonObj);
// 		 	   $("#regareacode").combobox({
// 		        	filter: function(q, row){
// 		        		var opts = $(this).combobox('options');
// 		        		return match(row[opts.textField],q)!=-1;
// 		        	}
// 		        });
// 		 	   //$("#regareacode_m").combobox("loadData",jsonObj);
// 		 	   $("#workareacode").combobox("loadData",jsonObj);
// 		 	   $("#workareacode").combobox({
// 		        	filter: function(q, row){
// 		        		var opts = $(this).combobox('options');
// 		        		return match(row[opts.textField],q)!=-1;
// 		        	}
// 		        });
// 		 	  //$("#workareacode_m").combobox("loadData",jsonObj);
// 		    }
// 	    }
// 	});
// }


//查询
function chaxun(){
	var selectjrjgmc = $("#selectjrjgmc").val();
	$("#dg1").datagrid("load",{selectjrjgmc:selectjrjgmc});
}
function initForm(){
    $('#formForEditDepartment').form('clear');
	//document.getElementById("formForEditDepartment").reset(); 
	//document.getElementById("formForMore").reset();
	// $("#finorgname").val('');
	// $("#finorgname_m").val('');
	// $("#finorgcode").val('');
	// $("#finorgcode_m").val('');
	// $("#finorgnum").val('');
	// $("#finorgnum_m").val('');
	// $("#inorgnum").val('');
	// $("#inorgnum_m").val('');
	// $("#xkzh").val('');
	// $("#xkzh_m").val('');
	// $("#zfhh").val('');
	// $("#zfhh_m").val('');
	// $("#orglevel").combobox('setValue','');
	// $("#orglevel_m").val('setValue','');
	// $("#highlevelorgname").val('');
	// $("#highlevelorgname_m").val('');
	// $("#highlevelfinorgcode").val('');
	// $("#highlevelfinorgcode_m").val('');
	// $("#highlevelinorgnum").val('');
	// $("#highlevelinorgnum_m").val('');
	// $("#regarea").val('');
	// $("#regarea_m").val('');
	// $("#regareacode").val('');
	// $("#regareacode_m").val('setValue','');
	//$("#workarea").val('');
	//$("#workarea_m").val('');
	//$("#workareacode").combobox('setValue','');
	//$("#workareacode_m").val('setValue','');
	// $("#setupdate").datebox('setValue','');
    // $("#sjrq").datebox('setValue','');
	// $("#setupdate_m").val('setValue','');
    // $("#sjrq_m").val('setValue','');
	// $("#mngmestus").combobox('setValue','');
	// $("#mngmestus_m").val('setValue','');
	// $("#id").val('');
	// $("#orgid").val('');
	// $("#departid").val('');
	// $("#checkstatus").val('');
	// $("#datastatus").val('');
	// $("#operator").val('');
	// $("#operationname").val('');
	// $("#operationtime").val('');
	// $("#nopassreason").val('');
}
//新增
function xinzeng(){
	$('#queding').linkbutton('enable');
	$('#dialog1').dialog('open');
	$('#dialog1').dialog({modal:true});
	$('#dialog1').dialog({title:"新增金融机构（分支机构）基础信息"});
	initForm();

}

//修改
function xiugai(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length != 1){
		$.messager.alert('操作提示','请选择一条数据修改','info');
	}else{
		initForm();
		$("#formForEditDepartment").form('load',rows[0]);
		$('#queding').linkbutton('enable');
		$('#dialog1').dialog('open');
		$('#dialog1').dialog({modal:true});
		$('#dialog1').dialog({title:"修改金融机构（分支机构）基础信息"});
		//关闭弹窗默认隐藏div
		/* $(dia).window({
	    	onBeforeClose: function () {
	    		//初始化表单的元素的状态
	    		initForm();
	    	}
		}); */
	}
}

//新增或修改点击确定保存
function saveForEditDialog(){
	//$('#queding').linkbutton('disable');
	$("#formForEditDepartment").form('submit',{
		url:'XsaveOrUpdatejrjgfz',
        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
        dataType:'json',
       	onSubmit:function(){
       		var isValidate = $("#formForEditDepartment").form('validate');
       		if(!isValidate){
       			return isValidate;
       		}else{
       			$.messager.progress({
					title:'请稍等',
					msg:'处理中......',
					timeout:10000,
				});  		
       		}
       	},
       	success:function(res){
       		$.messager.progress('close');
       		if(res == '1'){
   				$.messager.alert('','保存成功','info',function(r){
   					//$('#formForEditDepartment').form('clear');  
   					$('#dialog1').dialog('close');
   					$('#dg1').datagrid('reload');
   	            });
   			}else if(res == '0'){
   				$.messager.alert('操作提示','保存失败','error');
   			} 	
       	},
       	error : function(){
			$.messager.progress('close');
			$('#dialog1').dialog('close');
			$.messager.alert('操作提示','网络异常请稍后再试','error');
		}
	});
}

//取消，关闭新增或修改弹窗
function resetForEdit(){
	$('#dialog1').dialog('close');
}

//删除
function shanchu(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xdeletejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xdeletejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//申请删除
function shenqingshanchu(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要申请删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xshenqingdeletejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要申请删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xshenqingdeletejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//校验
// function jiaoyan(){
// 	var rows = $('#dg1').datagrid('getSelections');
// 	if(rows.length > 0){
// 		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
// 			if(r){
// 				var pass = true;
// 				for (var i = 0; i < rows.length; i++){
// 					if(rows[i].checkstatus=='0'){
//
// 					}else{
// 						pass = false;
// 						break;
// 					}
// 				}
// 				if(pass){
// 					$.ajax({
// 			            url:'Xcheckjrjgfz',
// 			            type:'POST', //GET
// 			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
// 			            data:{"checktype":"some","rows":JSON.stringify(rows)},
// 			            //data:{"checktype":"some","checkid":checkid},
// 			            dataType:'text',
// 			            success:function(res){
// 				           	if(res == '1'){
// 				           		$.messager.alert('','校验成功','info',function(r){
// 				           			$('#dg1').datagrid('reload');
// 				           		});
// 				           	}else{
// 				           		//否则删除失败
// 				           		/* $.messager.alert('','校验失败'+res,'error',function(r){
// 				           			$('#dg1').datagrid('reload');
// 				           		}); */
// 				           		$('#dg1').datagrid('reload');
// 								$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
// 								window.location.href = "XdownFileCheckjrjgfz";
// 				           	}
// 			            },
// 			           	error : function(){
// 			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
// 			    		}
// 			        });
// 				}else{
// 					$.messager.alert('操作提示','只能校验未校验的数据','info');
// 				}
// 			}
// 		});
// 	}else{
// 		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
// 			if(r){
// 				$.ajax({
// 		            url:'Xcheckjrjgfz',
// 		            type:'POST', //GET
// 		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
// 		            //data:{"rows":JSON.stringify(rows)},
// 		            data:{"checktype":"all"},
// 		            dataType:'text',
// 		            success:function(res){
// 			           	if(res == '1'){
// 			           		$.messager.alert('','校验成功','info',function(r){
// 			           			$('#dg1').datagrid('reload');
// 			           		});
// 			           	}else if (res =='13'){
//                             $.messager.alert('操作提示','无可校验数据','error');
//                         }else{
// 			           		//否则删除失败
// 			           		//$.messager.alert('操作提示','校验失败','error');
// 			           		$('#dg1').datagrid('reload');
// 							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
// 								window.location.href = "XdownFileCheckjrjgfz";
// 							});
// 			           	}
// 		            },
// 		           	error : function(){
// 		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
// 		    		}
// 		        });
// 			}
// 		});
// 	}
// }
function jiaoyan(){
    var row = $("#dg1").datagrid("getSelections");
    var id = '';
    for( var dataIndex in row){
        id = id + row[dataIndex].id + ",";
        if (row[dataIndex].checkstatus != 0){
            $.messager.alert("提示", "只能校验未校验的数据", "info");
            return;
        }
    }
    var info = '';
    if(id != ''){
        info = '是否校验选中的数据？';
    }else {
        info = '是否校验所有数据？'
    }
    var finorgnameParam = $("#selectjrjgmc").val().trim();

    // var checkstatusParam = $("#selectjylx").combobox("getValue");
    $.messager.confirm('提示',info,function (r) {
        if (r) {
            $.messager.progress({
                title: '请稍等',
                msg: '正在校验数据中......',
                timeout: 100000,
            });
            $.ajax({
                url: "Xcheckjrjgfz",
                dataType: "json",
                async: true,
                data:{"id" : id,"finorgnameParam" : finorgnameParam},
                type: "POST",
                success: function (data) {
                    $("#dg1").datagrid("reload");
                    $.messager.progress('close');
                    if (data.msg == "1") {
                        $.messager.alert('提示', '校验成功！', 'info');
                    } else if (data.msg == "-1") {
                        $.messager.alert('提示', '没有可校验的数据！', 'info');
                    } else {
                        $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                            window.location.href = "XDownLoadCheckJRFZ";
                        });
                    }
                },
                error: function () {
                    $.messager.progress('close');
					$.messager.alert("提示", "校验出错，请重新校验！", "error");
                }
            });
        }
    })
}
//提交
function tijiao(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要提交选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=="1"){
						deleteid = deleteid+rows[i].id+"-";
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'Xtijiaojrjgfz',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            //data:{"rows":JSON.stringify(rows)},
			            data:{"deletetype":"some","deleteid":deleteid},
			            dataType:'json',    
			            success:function(data){
				           	if(data == '1'){ 
				           		$.messager.alert('','提交成功','info',function(r){
				           			$('#dg1').datagrid('reload');
				           		});
				           	}else{  //否则删除失败
				           		$.messager.alert('操作提示','提交失败','error');
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能提交校验成功的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要提交所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xtijiaojrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','提交成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','提交失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

// 导入Excel相关
function daoru(){
    document.getElementById("importbtn").disabled = "disabled";
    $('#excelfile').val('');
    $('#importExcel').dialog('open').dialog('center');
    $('#importExcel').dialog({modal:true});
}
//导入选择文件按钮
function inexcel(){
    var file = document.getElementById("excelfile").files[0];
    if(file == null){
        document.getElementById("importbtn").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'Xdaorujrjgfz',
        fileElementId: 'excelfile',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
        }
    });
}

//导出
function daochu(){
	var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "Xdaochujrjgfz?zuhagntai=dtj&selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "Xdaochujrjgfz?zuhagntai=dtj&selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "Xdaochujrjgfz";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "Xdaochujrjgfz?zuhagntai=dtj";
			}
		});	
	}
}
/**
 * 导出excel（带进度条）
 * @param exportExcelUrl
 * @param scanTime 检测是否导出完毕请求间隔 单位毫秒
 * @param interval 进度条更新间隔（每次更新进度10%）  单位毫秒  导出时间越长 请设置越大 200 对应2秒导出时间
 */
function exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval){
    window.location.href = exportExcelUrl;
     var timer = setInterval(function(){
         $.ajax({
                url: isExportUrl,
                type:'POST', //GET
        	    async:true,    //或false,是否异步
                success: function(data){
                    if(data == "success"){
                        $('#exportFile').progressbar('setValue','100');
                        clearInterval(timer);
                        $('#exportFileDialog').dialog('close');
                        $('#exportFile').progressbar('setValue','0');
                    }else{
                    	var value = $('#exportFile').progressbar('getValue');
                    	if(value <= 100){
                    		value += Math.floor(Math.random() * 10);
                        	$('#exportFile').progressbar('setValue', value);
                    	}else{
                        	$('#exportFile').progressbar('setValue', '0');
                    	}
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            }); 
          }, scanTime);
}
//导出模版
function daochumoban(){
	$.messager.confirm('操作提示','确认要导出Excel模版文件吗？',function(r){
		if(r){
			/* $('#exportFileDialog').dialog('open');
			$("#exportFileDialog").dialog({
				   closable: false
			});
			$('#exportFileDialog').dialog({modal:true});
			var scanTime = 2000; //请求间隔毫秒
			var interval = 2000;
			var isExportUrl = "exportJinDuTiao";
			var exportExcelUrl = "Xdaochumobanjrjgfz";
			exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
			window.location.href = "Xdaochumobanjrjgfz";
		}
	});
}
//前台校验
$.extend($.fn.datebox.defaults.rules, {
    dateFormat : {
        validator : function(value) {
            var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
            return reg.test(value);
        },
        message : '日期格式必须满足:YYYY-MM-DD。'
    },
    normalDate : {
        validator : function(value) {
            var startTmp = new Date('1800-01-01');
            var endTmp = new Date('2100-12-31');
            var nowTmp = new Date(value);
            return nowTmp > startTmp && nowTmp < endTmp;
        },
        message : '成立时间日期早于1800-01-01且晚于2100-12-31'
    },
    teshu:{
        validator: function (value) {
            var patrn = /[？?！!^]/;
            return !patrn.test(value);
        },
        message: '不能包含特殊符号和空格'
    },
    teshu1:{
        validator: function (value) {
            var patrn = /[？?！!^]/;
            return !patrn.test(value);
        },
        message: '不能包含特殊符号和空格'
    },
    amount : {
        validator : function(value) {
            var reg = /^([0-9]\d{0,18}(\.\d{2})|0\.\d{2})$/;
            return reg.test(value);
        },
        message : '总长度不超过20位的，精度保留小数点后两位。'
    },
});
</script>
</body>
</html>
