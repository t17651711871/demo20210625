<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String datamanege = (String) request.getAttribute("datamanege");
%>
	
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>单位贷款担保物信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="单位贷款担保物信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <%if (!"datamanege".equals(datamanege)){%>
		<a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		担保合同编码 ：<input type="text" id="selectdbhtbm" name="selectdbhtbm" style="height: 23px; width:200px"/>&nbsp;&nbsp;
		贷款合同编码 ：<input type="text" id="selectdkhtbm" name="selectdkhtbm" style="height: 23px; width:160px"/>&nbsp;&nbsp;
		担保物编码 ：<input type="text" id="selectdkdbwbm" name="selectdkhtbm" style="height: 23px; width:160px"/>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<%--<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="shengchengbaowen()">生成报文</a>&nbsp;&nbsp;--%>
		<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="showDate()">生成报文</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="shujudahui()">数据打回</a>
		<%}else {%>
		<a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		<%}%>
	</div>
	
	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importFileDialog" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table>
        </div>
    </div>


<div style="visibility: hidden;">
	<div id="dialog_d" class="easyui-dialog" title="生成报文日期选择" data-options="iconCls:'icon-more'" style="width:400px;height:200px;padding-left:30px;padding-top:50px">		
		<form id="formForDate" method="post">
		<table>
			<tr>
				<td>请选择报文日期:</td>
				<td>
					<input class="easyui-datebox" id="createdate" name="date"style="height: 23px; width:150px" data-options="required:true" missingMessage="必填项"/>
				</td>
			</tr>
		</table>
		</form>
		<div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding" onclick="shengchengbaowen()">确定</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">取消</a>
		</div>
	</div>
</div>

<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="单位贷款担保物信息"  data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构代码 :</td>
                 <td><input class="easyui-validatebox" id="financeorgcode_m" name="financeorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                 
               <tr>
                 <td>内部机构号:</td>
                 <td><input class="easyui-validatebox" id="financeorginnum_m" name="financeorginnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode_m" name="gteecontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>被担保合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode_m" name="loancontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode_m" name="gteegoodscode" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select id="gteegoodscategory_m" name="gteegoodscategory" style="height: 23px; width:230px" disabled="disabled">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
						<option value="B01">B01:交易类应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
                        <option value="D01">D01:存货、仓单和提单</option>
                        <option value="D02">D02:机器设备</option>
                        <option value="D03">D03:交通运输设备</option>
                        <option value="D04">D04:采（探）矿权</option>
                        <option value="D05">D05:林权</option>
                        <option value="D06">D06:其他资源资产</option>
                        <option value="D07">D07:知识产权</option>
                        <option value="D08">D08:农村承包土地的经营权</option>
                        <option value="D09">D09:农民住房财产权</option>
                        <option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode_m" name="warrantcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select id="isfirst_m" name="isfirst" style="height: 23px; width:230px" disabled="disabled">
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select id="assessmode_m" name="assessmode" style="height: 23px; width:230px"disabled="disabled">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select id="assessmethod_m" name="assessmethod" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:组合法</option>
					    <option value="09">09:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue_m" name="assessvalue" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input id="assessdate_m" name="assessdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物票面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt_m" name="gteegoodsamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt_m" name="firstrightamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>估值周期 :</td>
			     <td>
			        <select id="gzzq_m" name="gzzq" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:年</option>
					    <option value="02">02:半年</option>
					    <option value="03">03:季度</option>
					    <option value="04">04:月</option>
					    <option value="05">05:旬</option>
					    <option value="06">06:周</option>
					    <option value="07">07:日</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

				 <!-- <tr>
                   <td>担保物状态 :</td>
                   <td>
                      <select id="gteegoodsstataus_m" name="gteegoodsstataus" style="height: 23px; width:230px" disabled="disabled">
                          <option value="01">01:正常</option>
                          <option value="02">02:已变现处置</option>
                          <option value="03">03:待处理抵债资产</option>
                          <option value="04">04:灭失损毁</option>
                          <option value="05">05:其他</option>
                      </select>
                   </td>
                 </tr>

                 <tr>
                   <td>抵质押率 :</td>
                   <td>
                      <input class="easyui-validatebox" id="mortgagepgerate_m" name="mortgagepgerate" style="height: 23px; width:230px" disabled="disabled"/>
                   </td>
                 </tr> -->
			   
              </table>
           </div>
         </form>
	</div>
</div>

<script type="text/javascript">
$(function(){
	$('#dialog_m').dialog('close');
	$("#importFileDialog").dialog("close");
    $('#dialog_d').dialog('close');

	$("#dg1").datagrid({
		method:'post',
		url:'XfindDWDKDBWXXscbw',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			    {field:'ck',checkbox:true},
			    {field:'financeorgcode',title:'金融机构代码',width:100,align:'center'},
			    {field:'financeorginnum',title:'内部机构号',width:100,align:'center'},
			    {field:'gteecontractcode',title:'担保合同编码',width:100,align:'center'},
			    {field:'loancontractcode',title:'被担保合同编码',width:100,align:'center'},
			    {field:'gteegoodscode',title:'担保物编码',width:100,align:'center'},
			    {field:'gteegoodscategory',title:'担保物类别',width:100,align:'center'},
			    {field:'warrantcode',title:'权证编号',width:100,align:'center'},
			    {field:'isfirst',title:'是否第一顺位',width:100,align:'center'},
			    {field:'assessmode',title:'评估方式',width:100,align:'center'},
			    {field:'assessmethod',title:'评估方法',width:100,align:'center'},
			    {field:'assessvalue',title:'评估价值',width:100,align:'center'},
			    {field:'assessdate',title:'评估基准日',width:100,align:'center'},
			    {field:'gteegoodsamt',title:'担保物票面价值',width:100,align:'center'},
			    {field:'firstrightamt',title:'优先受偿权数额',width:100,align:'center'},
			    {field:'gzzq',title:'估值周期',width:100,align:'center'},
                {field:'sjrq',title:'数据日期',width:100,align:'center'},
			    // {field:'gteegoodsstataus',title:'担保物状态',width:100,align:'center',hidden:true},
			    // {field:'mortgagepgerate',title:'抵质押率',width:100,align:'center',hidden:true},
			    {field:'operator',title:'操作人',width:100,align:'center'},
			    {field:'operationtime',title:'操作时间',width:100,align:'center'}
	    ]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
        	$("#formForMore").form('load',rowData);
        	$('#dialog_m').dialog({modal : true});
        	//$("#gteegoodscategory_m").combobox({disabled: true});
        	//$("#isfirst_m").combobox({disabled: true});
        	//$("#assessmode_m").combobox({disabled: true});
        	//$("#assessmethod_m").combobox({disabled: true});
        	//$("#assessdate_m").datebox({disabled: true});
        	//$("#gteegoodsstataus_m").combobox({disabled: true});
        	//关闭弹窗默认隐藏div
			/* $(dia).window({
		    	onBeforeClose: function () {
		    		//初始化表单的元素的状态
		    		initForm();
		    	}
			}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//查询
function chaxun(){
	var selectdbhtbm = $("#selectdbhtbm").val();
	var selectdkhtbm = $("#selectdkhtbm").val();
    var selectdkdbwbm = $("#selectdkdbwbm").val();
	$("#dg1").datagrid("load",{selectdbhtbm:selectdbhtbm,selectdkhtbm:selectdkhtbm,selectdkdbwbm:selectdkdbwbm});
}

function initForm(){
	//$('#info_formForAdd').form('clear');
	$("#financeorgcode_m").val('');
	$("#financeorginnum_m").val('');
	$("#gteecontractcode_m").val('');
	$("#loancontractcode_m").val('');
	$("#gteegoodscode_m").val('');
	$("#gteegoodscategory_m").val('setValue','');
	$("#warrantcode_m").val('');
	$("#isfirst_m").val('setValue','');
	$("#assessmode_m").val('setValue','');
	$("#assessmethod_m").val('setValue','');
	$("#assessvalue_m").val('');
	$("#assessdate_m").val('setValue','');
	$("#gteegoodsamt_m").val('');
	$("#firstrightamt_m").val('');
	$("#gzzq_m").val('');
    $("#sjrq_m").val('');
	//$("#gteegoodsstataus_m").val('setValue','');
	//$("#mortgagepgerate_m").val('');
}

//数据打回
function shujudahui(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据打回吗',function(r){
			if(r){
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xdahuidkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"checktype":"some","rows":JSON.stringify(rows)},
		            data:{"checktype":"some","selectid":selectid},
		            dataType:'json',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','数据打回成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','数据打回失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据打回吗',function(r){
			if(r){
				$.ajax({
		            url:'Xdahuidkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"checktype":"some","rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'json',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','数据打回成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','数据打回失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//生成报文
function shengchengbaowen(){
	var rows = $('#dg1').datagrid('getSelections');
	/*if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}*/
    $.messager.confirm('提示','是否生成报文？',function (r){
        if(r) {
            $("#formForDate").form('submit', {
                url: 'XCreateReportFour',
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json',
                async: true,
                onSubmit: function () {
                    var isValidate = $("#formForDate").form('validate');
                    if (!isValidate) {
                        return isValidate;
                    } else {
                        $.messager.progress({
                            title: '请稍等',
                            msg: '处理中......',
                            timeout: 10000,
                        });
                    }
                },
                success: function (data) {
                    $.messager.progress('close');
                    $.messager.alert('操作提示', '生成报文成功' + data + '条', 'info');
					$('#dialog_d').dialog('close');
                    $("#createdate").datebox('setValue','');
                    $("#dg1").datagrid("reload");
                },
                error: function (error) {
                    $.messager.progress('close');
                    $.messager.alert('操作提示', '生成报文失败', 'error');
                    $("#createdate").datebox('setValue','');
                    $('#dialog_d').dialog('close');
                }
            });
        }
    });
	// $.messager.confirm('提示','是否生成报文？',function (r){
	// 	if(r){
	// 		$.messager.progress({
	// 			title: '请稍等',
	// 			msg: '数据处理中......'
	// 		});
	// 		$.ajax({
	// 			url: "XCreateReportFour",
	// 			type: "POST",
	// 			contentType: 'application/json;charset=UTF-8',
	// 			async: true,
	// 			success: function (data) {
	// 				$.messager.progress('close');
	// 				$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
	// 				$("#dg1").datagrid("reload");
	// 			},
	// 			error: function (error) {
	// 				$.messager.progress('close');
	// 				$.messager.alert('操作提示', '生成报文失败', 'error');
	// 			}
	// 		})
	// 	}
	// });
}

function showDate(){
    $("#createdate").datebox('setValue','');
    $('#dialog_d').dialog('open');
}
//取消
function resetForEdit() {
    $("#createdate").datebox('setValue','');
    $('#dialog_d').dialog('close');
}

//校验
function jiaoyan(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				/* var checkid = "";
				for (var i = 0; i < rows.length; i++){
					checkid = checkid+rows[i].id+"-";
				} */
				$.ajax({
		            url:'checkXdkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		/* $.messager.alert('','校验失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		}); */
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'checkXdkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}


</script>
</body>
</html>
