<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String datamanege = (String) request.getAttribute("datamanege");
	List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
	List<String> baseAreas = new ArrayList<>();
	for (BaseArea baseArea : baseAreaList) {
		baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
	}
	List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
	List<String> baseAindustrys = new ArrayList<>();
	for (BaseAindustry baseAindustry : baseAindustryList) {
		baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
	}
	List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
	List<String> baseCurrencys = new ArrayList<>();
	for (BaseCurrency baseCurrency : baseCurrencyList) {
		baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>非同业单位客户基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>

	<table id="dg1" style="height: 480px;" title="非同业单位客户基础信息列表"></table>
	<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px"
    data-options="iconCls:'icon-save',resizable:true,modal:true">
    	<form id="dateform" action="">
    	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required" editable=false><br/><br/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="genMessage()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    	</form>
	</div>
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}else {%>
		<a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		金融机构代码：<input type="text" id="finorgcodeParam" style="height: 23px; width:160px"/>&nbsp;&nbsp;&nbsp;&nbsp;
		客户名称：<input id="customernameParam" type="text" style="height: 23px; width:160px"/>&nbsp;
		<label>客户证件代码：</label><input id="customercodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    	<label>客户证件类型：</label><select class='easyui-combobox' id='regamtcrenyParam' style='height: 23px; width:173px'>
    						<option value=''>=请选择=</option>
                            <option value='A01'>A01-统一社会信用代码</option>
                            <option value='A02'>A02-组织机构代码</option>
                            <option value='A03'>A03-其他</option>
                        </select>
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="javascript:$('#datedialog').window('open')">生成报文</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="goback()">数据打回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
	</div>

	<!--详情窗口-->
	<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
		<div align="center" style="padding-top: 30px">
			<form id="info_formForAdd" method="post">
				<input id="info_id" class="backId" name="id" type="hidden">
				<table class='enterTable' id='info_enterTable'>
					<tr>
						<td align='right'>
							金融机构代码:
						</td>
						<td>
							<input id='info_finorgcode' name='finorgcode' disabled="disabled" style='width:173px;'/>
						</td>
						<td align='right'>
							客户名称:
						</td>
						<td>
							<input id='info_customername' name='customername' style='width:173px;' maxlength='200' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							客户证件代码:
						</td>
						<td>
							<input id='info_customercode' name='customercode' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							基本存款账号:
						</td>
						<td>
							<input id='info_depositnum' name='depositnum' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							基本账户开户行名称:
						</td>
						<td>
							<input id='info_countopenbankname' name='countopenbankname' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							注册资本:
						</td>
						<td>
							<input id='info_regamt' name='regamt' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							客户证件类型:
						</td>
						<td>
							<select id='info_regamtcreny' name='regamtcreny' style='height: 23px; width:173px' disabled="disabled">
								<option value='A01'>A01-统一社会信用代码</option>
								<option value='A02'>A02-组织机构代码</option>
								<option value='A03'>A03-其他</option>
							</select>
						</td>
						<td align='right'>
							实收资本:
						</td>
						<td>
							<input id='info_actamt' name='actamt' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							客户国民经济部门:
						</td>
						<td>
							<select id='info_actamtcreny' name='actamtcreny' style='height: 23px; width:173px' disabled="disabled">
								<option value='A'>A-广义政府</option>
								<option value='A01'>A01-中央政府</option>
								<option value='A02'>A02-地方政府</option>
								<option value='A03'>A03-社会保障基金</option>
								<option value='A04'>A04-机关团体</option>
								<option value='A05'>A05-部队</option>
								<option value='A06'>A06-住房公积金</option>
								<option value='A99'>A99-其他</option>
								<option value='B'>B-金融机构部门</option>
								<option value='B01'>B01-货币当局</option>
								<option value='B02'>B02-监管当局</option>
								<option value='B03'>B03-银行业存款类金融机构</option>
								<option value='B04'>B04-银行业非存款类金融机构</option>
								<option value='B05'>B05-证券业金融机构</option>
								<option value='B06'>B06-保险业金融机构</option>
								<option value='B07'>B07-交易及结算类金融机构</option>
								<option value='B08'>B08-金融控股公司</option>
								<option value='B09'>B09-特定目的载体</option>
								<option value='B99'>B99-其他</option>
								<option value='C'>C-非金融企业部门</option>
								<option value='C01'>C01-公司</option>
								<option value='C02'>C02-非公司企业</option>
								<option value='C99'>C99-其他非金融企业部门</option>
								<option value='D'>D-住户部门</option>
								<option value='D01'>D01-住户</option>
								<option value='D02'>D02-为住户服务的非营力机构</option>
								<option value='E'>E-非居民部门</option>
								<option value='E01'>E01-国际组织</option>
								<option value='E02'>E02-外国政府</option>
								<option value='E03'>E03-境外金融机构</option>
								<option value='E04'>E04-境外非金融企业</option>
								<option value='E05'>E05-外国居民</option>
							</select>
						</td>
						<td align='right'>
							总资产:
						</td>
						<td>
							<input id='info_totalamt' name='totalamt' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							经营范围:
						</td>
						<td>
							<input id='info_netamt' name='netamt' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							是否上市公司:
						</td>
						<td>
							<select id='info_islistcmpy' name='islistcmpy' disabled="disabled" style='height: 23px; width:173px' >
								<option value='1'>1-是</option>
								<option value='0'>0-否</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align='right'>
							首次建立信贷关系日期:
						</td>
						<td>
							<input id='info_fistdate' name='fistdate' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							从业人员数:
						</td>
						<td>
							<input id='info_jobpeoplenum' name='jobpeoplenum' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							注册地:
						</td>
						<td>
							<input id='info_regarea' name='regarea' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							地区代码:
						</td>
						<td>
							<input id='info_regareacode' name='regareacode' style='height: 23px; width:173px' disabled="disabled">
								<!--<c:forEach items='${baseAreaList}' var='aa'>
									<option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
								</c:forEach>-->
							</input>
						</td>
					</tr>
					<tr>
						<td align='right'>
							营业收入:
						</td>
						<td>
							<input id='info_workarea' name='workarea' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							客户信用级别总等级数:
						</td>
						<td>
							<input id='info_workareacode' name='workareacode' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							经营状态:
						</td>
						<td>
							<select id='info_mngmestus' name='mngmestus' disabled="disabled" style='height: 23px; width:173px' >
								<option value='01'>01-正常运营</option>
								<option value='02'>02-停业（歇业）</option>
								<option value='03'>03-筹建</option>
								<option value='04'>04-当年关闭</option>
								<option value='05'>05-当年破产</option>
								<option value='06'>06-当年注销</option>
								<option value='07'>07-当年吊销</option>
								<option value='08'>08-其他</option>
							</select>
						</td>
						<td align='right'>
							成立日期:
						</td>
						<td>
							<input id='info_setupdate' name='setupdate' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							所属行业:
						</td>
						<td>
							<select id='info_industry' name='industry' disabled="disabled" style='height: 23px; width:173px' disabled="disabled">
								<c:forEach items='${baseAindustryList}' var='aa'>
									<option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
								</c:forEach>
							</select>
						</td>
						<td align='right'>
							企业规模:
						</td>
						<td>
							<select id='info_entpmode' name='entpmode' disabled="disabled"style='height: 23px; width:173px' >
								<option value='CS01'>CS01-大型</option>
								<option value='CS02'>CS02-中型</option>
								<option value='CS03'>CS03-小型</option>
								<option value='CS04'>CS04-微型</option>
								<option value='CS05'>CS05-其他（非企业类单位）</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align='right'>
							客户经济成分:
						</td>
						<td>
							<select id='info_entpczjjcf' name='entpczjjcf' disabled="disabled" style='height: 23px; width:173px' >
								<option value='A01'>A01:国有控股</option>
								<option value='A0101'>A0101:国有相对控股</option>
								<option value='A0102'>A0102:国有绝对控股</option>
								<option value='A02'>A02:集体控股</option>
								<option value='A0201'>A0201:集体相对控股</option>
								<option value='A0202'>A0202:集体绝对控股</option>
								<option value='B01'>B01:私人控股</option>
								<option value='B0101'>B0101:私人相对控股</option>
								<option value='B0102'>B0102:私人绝对控股</option>
								<option value='B02'>B02:港澳台控股</option>
								<option value='B0201'>B0201:港澳台相对控股</option>
								<option value='B0202'>B0202:港澳台绝对控股</option>
								<option value='B03'>B03:外商控股</option>
								<option value='B0301'>B0301:外商相对控股</option>
								<option value='B0302'>B0302:外商绝对控股</option>
							</select>
						<td align='right'>
							授信额度:
						</td>
						<td>
							<input id='info_creditamt' name='creditamt' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td align='right'>
							已用额度:
						</td>
						<td>
							<input id='info_usedamt' name='usedamt' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							是否关联方:
						</td>
						<td>
							<select id='info_isrelation' name='isrelation' disabled="disabled" style='height: 23px; width:173px' >
								<option value='1'>1-是</option>
								<option value='0'>0-否</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align='right'>
							客户信用评级:
						</td>
						<td>
							<input id='info_actctrltype' name='actctrltype' style='width:173px;' disabled="disabled"/>
						</td>
						<td align='right'>
							实际控制人证件类型:
						</td>
						<td>
							<select id='info_actctrlidtype' name='actctrlidtype' disabled="disabled" style='height: 23px; width:173px' >
								<option selected value='A01'>A01-统一社会信用代码</option>
								<option value='A02'>A02-组织机构代码</option>
								<option value='A03'>A03-其他</option>
								<option value='B01'>B01-身份证</option>
								<option value='B02'>B02-户口簿</option>
								<option value='B03'>B03-护照</option>
								<option value='B04'>B04-军官证</option>
								<option value='B05'>B05-士兵证</option>
								<option value='B06'>B06-港澳居民来往内地通行证</option>
								<option value='B07'>B07-台湾同胞来往内地通行证</option>
								<option value='B08'>B08-临时身份证</option>
								<option value='B09'>B09-外国人居留证</option>
								<option value='B10'>B10-警官证</option>
								<option value='B11'>B11-个体工商户营业执照</option>
								<option value='B12'>B12-外国人永久拘留省份证</option>
								<option value='B13'>B13-港澳台居民居住证</option>
								<option value='B99'>B99-其他证件</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align='right'>
							实际控制人证件代码:
						</td>
						<td>
							<input id='info_actctrlidcode' name='actctrlidcode' style='width:173px;' disabled="disabled"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	
<script type="text/javascript">
	var baseAreas = <%=baseAreas%>;
	var baseAindustrys = <%=baseAindustrys%>;
	var baseCurrencys = <%=baseCurrencys%>;
$(function(){
	$('#datedialog').window('close');
	$("#dg1").datagrid({
		loadMsg:'数据加载中,请稍后...',
		method:'post',
		url:'XGetXftykhxData?datastatus='+'${datastatus}',
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:false,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
        	{field:'id', title: '编号', width: 80, align: 'center', hidden: true},
			{field: 'finorgcode', title: '金融机构代码', width: 100, align: 'center'},
			{field: 'customername', title: '客户名称', width: 100, align: 'center'},
			{field: 'customercode', title: '客户证件代码', width: 100, align: 'center'},
			{field: 'depositnum', title: '基本存款账号', width: 100, align: 'center'},
			{field: 'countopenbankname', title: '基本账户开户行名称', width: 100, align: 'center'},
			{field: 'regamt', title: '注册资本', width: 100, align: 'center'},
			{field: 'regamtcreny', title: '客户证件类型', width: 100, align: 'center',formatter:function (value) {
					if (value == 'A01') {
						return "A01-统一社会信用代码";
					} else if (value == 'A02') {
						return "A02-组织机构代码";
					} else if (value == 'A03') {
						return "A03-其他";
					}
					return value;
				}
			},
			{field: 'actamt', title: '实收资本', width: 100, align: 'center'},
			{field: 'actamtcreny', title: '客户国民经济部门', width: 100, align: 'center',formatter:function (value) {
					if (value=='A'){
						return "A-广义政府";
					}else if (value=='A01'){
						return "A01-中央政府";
					}else if(value=='A02'){
						return "A02-地方政府";
					}else if (value=='A03'){
						return "A03-社会保障基金";
					}else if (value=='A04'){
						return "A04-机关团体";
					}else if(value=='A05'){
						return "A05-部队";
					}else if (value=='A06'){
						return "A06-住房公积金";
					}else if (value=='A99'){
						return "A99-其他";
					}else if(value=='B'){
						return "B-金融机构部门";
					}else if(value=='B01'){
						return "B01-货币当局";
					}else if (value=='B02'){
						return "B02-监管当局";
					}else if (value=='B03'){
						return "B03-银行业存款类金融机构";
					}else if(value=='B04'){
						return "B04-银行业非存款类金融机构";
					}else if (value=='B05'){
						return "B05-证券业金融机构";
					}else if (value=='B06'){
						return "B06-保险业金融机构";
					}else if(value=='B07'){
						return "B07-交易及结算类金融机构";
					}else if (value=='B08'){
						return "B08-金融控股公司";
					}else if(value=='B09'){
						return "B09-特定目的载体";
					}else if (value=='B99'){
						return "B99-其他";
					}else if(value=='C'){
						return "C-非金融企业部门";
					}else if(value=='C01'){
						return "C01-公司";
					}else if (value=='C02'){
						return "C02-非公司企业";
					}else if (value=='C99'){
						return "C99-其他非金融企业部门";
					}else if(value=='D'){
						return "D-住户部门";
					}else if(value=='D01'){
						return "D01-住户";
					}else if (value=='D02'){
						return "D02-为住户服务的非营力机构";
					}else if(value=='E'){
						return "E-非居民部门";
					}else if(value=='E01'){
						return "E01-国际组织";
					}else if (value=='E02'){
						return "E02-外国政府";
					}else if (value=='E03'){
						return "E03-境外金融机构";
					}else if(value=='E04'){
						return "E04-境外非金融企业";
					}else if (value=='E05'){
						return "E05-外国居民";
					}
					return value;
				}},
			{field: 'totalamt', title: '总资产', width: 100, align: 'center'},
			{field: 'netamt', title: '经营范围', width: 100, align: 'center'},
			{field: 'islistcmpy', title: '是否上市公司', width: 100, align: 'center',formatter:function (value) {
					if (value=='1'){
						return "1-是";
					}else if(value=='0'){
						return "0-否";
					}
					return value;
				}},
			{field: 'fistdate', title: '首次建立信贷关系日期', width: 100, align: 'center'},
			{field: 'jobpeoplenum', title: '从业人员数', width: 100, align: 'center'},
			{field: 'regarea', title: '注册地', width: 100, align: 'center'},
			{field: 'regareacode', title: '地区代码', width: 100, align: 'center',formatter:function (value) {
					for (var i = 0;i <  baseAreas.length;i++){
						if (baseAreas[i].substring(0,6)==value){
							return baseAreas[i];
						}
					}
				}},
			{field: 'workarea', title: '营业收入', width: 100, align: 'center'},
			{field: 'workareacode', title: '客户信用级别总等级数', width: 100, align: 'center'},
			{field: 'mngmestus', title: '经营状态', width: 100, align: 'center',formatter:function (value) {
					if (value=='01'){
						return "01-正常运营";
					}else if(value=='02'){
						return "02-停业（歇业）";
					}else if (value=='03'){
						return "03-筹建";
					}else if(value=='04'){
						return "C04-当年关闭";
					}else if (value=='C05'){
						return "05-当年破产";
					}else if (value=='06'){
						return "06-当年注销";
					}else if(value=='07'){
						return "C07-当年吊销";
					}else if (value=='08'){
						return "08其他";
					}
					return value;
				}},
			{field: 'setupdate', title: '成立日期', width: 100, align: 'center'},
			{field: 'industry', title: '所属行业', width: 100, align: 'center',formatter:function (value) {
					for (var i = 0;i <  baseAindustrys.length;i++){
						if (baseAindustrys[i].substring(0,3)==value){
							return baseAindustrys[i];
						}
					}
				}},
			{field: 'entpmode', title: '企业规模', width: 100, align: 'center',formatter:function (value) {
					if (value=='CS01'){
						return "CS01-大型";
					}else if(value=='CS02'){
						return "CS02-中型";
					}else if (value=='CS03'){
						return "CS03-小型";
					}else if(value=='CS04'){
						return "CS04-微型";
					}else if (value=='CS05'){
						return "CS05-其他（非企业类单位）";
					}
					return value;
				}},
			{field: 'entpczjjcf', title: '客户经济成分', width: 100, align: 'center',formatter:function (value) {
					if (value=='A01'){
						return "A01:国有控股";
					}else if(value=='A0101'){
						return "A0101:国有相对控股";
					}else if (value=='A0102'){
						return "A0102:国有绝对控股";
					}else if(value=='A02'){
						return "A02:集体控股";
					}else if (value=='A0201'){
						return "A0201:集体相对控股";
					}else if (value=='A0202'){
						return "A0202:集体绝对控股";
					}else if(value=='B01'){
						return "B01:私人控股";
					}else if (value=='B0101'){
						return "B0101:私人相对控股";
					}else if (value=='B0102'){
						return "B0102:私人绝对控股";
					}else if(value=='B02'){
						return "B02:港澳台控股";
					}else if (value=='B0201'){
						return "B0201:港澳台相对控股";
					}else if (value=='B0202'){
						return "B0202:港澳台绝对控股";
					}else if(value=='B03'){
						return "B03:外商控股";
					}else if (value=='B0301'){
						return "B0301:外商相对控股";
					}else if (value=='B0302'){
						return "B0302:外商绝对控股";
					}
					return value;
				}},
			{field: 'creditamt', title: '授信额度', width: 100, align: 'center'},
			{field: 'usedamt', title: '已用额度', width: 100, align: 'center'},
			{field: 'isrelation', title: '是否关联方', width: 100, align: 'center',formatter:function (value) {
					if (value=='1'){
						return "1-是";
					}else if(value=='0'){
						return "0-否";
					}
					return value;
				}},
			{field: 'actctrltype', title: '客户信用评级', width: 100, align: 'center'},
			{field: 'actctrlidtype', title: '实际控制人证件类型', width: 100, align: 'center',formatter:function (value) {
					if (value=='A01'){
						return "A01-统一社会信用代码";
					}else if(value=='A02'){
						return "A02-组织机构代码";
					}else if (value=='A03'){
						return "A03-其他";
					}else if(value=='B01'){
						return "B01-身份证";
					}else if (value=='B02'){
						return "B02-户口簿";
					}else if (value=='B03'){
						return "B03-护照";
					}else if(value=='B04'){
						return "B04-军官证";
					}else if (value=='B05'){
						return "B05-士兵证";
					}else if (value=='B06'){
						return "B06-港澳居民来往内地通行证";
					}else if(value=='B07'){
						return "B07-台湾同胞来往内地通行证";
					}else if (value=='B08'){
						return "B08-临时身份证";
					}else if(value=='B09'){
						return "B09-外国人居留证";
					}else if (value=='B10'){
						return "B10-警官证";
					}else if (value=='B11'){
						return "B11-个体工商户营业执照";
					}else if(value=='B12'){
						return "B12-外国人永久拘留省份证";
					}else if (value=='B13'){
						return "B13-港澳台居民居住证";
					}else if (value=='B99'){
						return "B99-其他证件";
					}
					return value;
				}},
			{field: 'actctrlidcode', title: '实际控制人证件代码', width: 100, align: 'center'},
			{field: 'operator', title: '操作人',  align: 'center'},
			{field: 'operationtime', title: '操作时间',  align: 'center'}
    ]],
		onDblClickRow: function (rowIndex,rowData) {
			$('#info_formForAdd').form('clear');
			$('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','非同业单位客户基础信息详情');
			$('#info_formForAdd').form('load',rowData);
		}
	});
})

// 查询
function searchOnSelected(){
	var finorgcodeParam = $("#finorgcodeParam").val().trim();
	var customernameParam = $("#customernameParam").val().trim();
	var customercodeParam = $("#customercodeParam").val().trim();
    var regamtcrenyParam = $("#regamtcrenyParam").combobox("getValue");
	$("#dg1").datagrid("load", {"finorgcodeParam" : finorgcodeParam,"customernameParam" : customernameParam,"customercodeParam":customercodeParam,"regamtcrenyParam":regamtcrenyParam});
}

// 生成报文
function genMessage(){
	var v = $('#dateform').form('validate');
	if(v){
		var value = $('#dateselect').datebox('getValue');
		$('#datedialog').window('close');
		$.messager.confirm('提示','是否生成报文？',function (r){
	        if(r){
				$.messager.progress({
					title: '请稍等',
					msg: '数据处理中......'
				});
				$.ajax({
					url: "XCreateReportSix",
					type: "POST",
					contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
					async: true,
					data:{"date":value},
			        dataType:'json',
					success: function (data) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
						$("#dg1").datagrid("reload");
					},
					error: function (error) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文失败', 'error');
					}
				})
	        }
	    });
	}
    
}
	// 打回按钮
	function goback() {
		var rows = $('#dg1').datagrid('getSelections');
		var info = '';
		var id = '';
		for (var i = 0;i < rows.length;i++){
			id = id + rows[i].id + ',';
		}
		if(rows.length > 0){
			info = '是否打回选中的数据？';
		}else {
			info = '是否打回所有的数据？';
		}
		var finorgcodeParam = $("#finorgcodeParam").val().trim();
		var customernameParam = $("#customernameParam").val().trim();
		$.messager.confirm('提示',info,function (r){
			if(r){
				$.ajax({
					sync: true,
					type: "POST",
					dataType: "json",
					url: "XgobackXftykhx",
					data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"customernameParam" : customernameParam},
					contentType:'application/x-www-form-urlencoded; charset=UTF-8',
					success: function (data) {
						if (data == 0){
							$.messager.alert('操作提示','打回成功','info');
							$("#dg1").datagrid("reload");
						}else {
							$.messager.alert('操作提示','打回失败','info');
						}
					},
					error: function (err) {
						$.messager.alert('操作提示','打回失败','error');
					}
				});
			}
		});
	}
	
	//返回
    function fanhui(){
    	$.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });	
    }
</script>
</body>
</html>
