<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.common.util.VariableUtils"%>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource"%>
<%@ page import="com.geping.etl.common.entity.Report_Info"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
String loginId = vu.getLoginId();
Set<Sys_Auth_Role_Resource> operateReportSet = vu.getOperateReportSet();
Set<Sys_Auth_Role_Resource> reportSet = vu.getReportSet();
List<Report_Info> allReportList = (List<Report_Info>)request.getAttribute("reportlist");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>报文管理</title>
    <%@include file="../../common/head.jsp"%>
<style type="text/css">
a{
   text-decoration:none;
}

</style>
</head>
<body>
		<table class="easyui-datagrid" fitColumns="true" title="报文管理" id="dg1" fit="true"
			data-options="autoRowHeight:false,pagination:true,rownumbers:true,toolbar:'#tb',singleSelect:true,pageSize:10,pageList:[10]" style="height: 550px">
		</table>
		<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    	<form id="dateform" action="">
    	<input id="name" type="hidden" value="">
			<input id="code" type="hidden" value="">
			<input id="enName" type="hidden" value="">
    	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required" editable=false><br/><br/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="genMessage()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    	</form>
	</div>
		<div id="tb" style="padding:5px;height:auto;">
		
	<%
		    
		    if(operateReportSet != null){
		      if(!operateReportSet.isEmpty()){
		    	 for(Sys_Auth_Role_Resource sarr : operateReportSet){
				       String operateReportName = sarr.getResValue();
	%>	
		          <%
	    		    if(operateReportName.equals("SHOW_REPORT")){
	              %>
						表单名称: 
							<input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
						    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	             <%
	    		    }
	             %>
	    
	             <%
	    		    if(operateReportName.equals("REPORT_DATA")){
	    		    	//if(operateReportName.equals("REPORT_DATA")){
	             %>
	                    <a class="easyui-linkbutton" iconCls="icon-more" onclick="more()">报表数据</a>
	             <%
	    		    }
	             %>
	<%
		        }
		    }  
		 }else if(loginId.equals("admin")){
	%>	
	                            表单名称: 
					<input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
				    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    <a class="easyui-linkbutton" iconCls="icon-more" onclick="more()">报表数据</a>
	<%		 
		}
	%>
	 </div>
<script type="text/javascript">
var tableNameBlank = "";//空报文计数表名
var countBlank = 0;//空报文计数
$(function(){
	$('#datedialog').window('close'); 
	$("#dg1").datagrid({
         method: 'post',
         url:'XCreateReportManagerList',
         loadMsg: '数据加载中,请稍后...',
         checkOnSelect:true,
         autoRowHeight:false,
         pagination:true,
         rownumbers:true,
         toolbar:'#tb1',
         fitColumns:true,
         pageSize: 20,
         pageList: [10,20,30],
         columns: [[
             {field: 'ck', checkbox: true,hidden: true},
             {field: 'name', title:'表单名称',width:180,align:'center'},
             {field: 'checkSuccess', title:'未生成条数',width:100,align:'center',formatter:function(value,row,index){
            	 return '<a href="'+row.generateMessageUrl+'">'+value+'</a>';
             	}
             },
             {field:'operate',title:'操作',width:150,align:'center',formatter:function(value,row,index){
					return '<a href="javascript:void(0)" onclick="generateMessage(\''+row.name+'\',\''+row.code+'\',\''+row.enName+'\')">生成报文</a>';
				}}
             ]],
	 });
	$('#dg1').datagrid({loadFilter:pagerFilter});//.datagrid('loadData', getData());
	
	$(".datagrid-cell-group").attr('style','color:red;font-size:large');
})

function pagerFilter(data){
	if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
		data = {
			total: data.length,
			rows: data
		}
	}
	var dg = $(this);
	var opts = dg.datagrid('options');
	var pager = dg.datagrid('getPager');
	pager.pagination({
		onSelectPage:function(pageNum, pageSize){
			opts.pageNumber = pageNum;
			opts.pageSize = pageSize;
			pager.pagination('refresh',{
				pageNumber:pageNum,
				pageSize:pageSize
			});
			dg.datagrid('loadData',data);
		}
	});
	if (!data.originalRows){
		data.originalRows = (data.rows);
	}
	var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
	var end = start + parseInt(opts.pageSize);
	data.rows = (data.originalRows.slice(start, end));
	return data;
}



function more(){
	var row = $("#dg1").datagrid('getSelected');
	if (row != null){
		 window.location.href = row.cursorUrl+"?status=all";
	}else{
		$.messager.alert('操作提示', '请选择一条数据查看报表数据', 'info');
	}
}


function selectRecord(){
	var reportName = $("#reportName").val();
	window.location.href = "Dreport_info?reportName="+encode(reportName);
}

function generateMessage(name,code,enName){
	if(name == tableNameBlank && countBlank == 1){
		$.messager.confirm('','是否生成空报文覆盖上一次报文',function(f){
			if(!f){
				return;
			}else{
				$("#name").val(name);
				$("#code").val(code);
				$("#enName").val(enName);
				$('#datedialog').window('open');
			}
			});
	}else{
		$("#name").val(name);
		$("#code").val(code);
		$("#enName").val(enName);
		$('#datedialog').window('open');
	}
	
}


//生成报文
function genMessage(){
	var v = $('#dateform').form('validate');
	if(v){
		var name = $("#name").val();
		var code = $("#code").val();
		var enName = $("#enName").val();
		var value = $('#dateselect').datebox('getValue');
		var url = '';
		if(typeof enName == "undefined" || enName == null || enName == ""||enName=="undefined") {
			if (name == '存量单位贷款基础数据信息') {
				url = 'XCreateReportOne';
			} else if (name == '单位贷款发生额信息') {
				url = 'XCreateReportTwo';
			} else if (name == '单位贷款担保合同信息') {
				url = 'XCreateReportThree';
			} else if (name == '单位贷款担保物信息') {
				url = 'XCreateReportFour';
			} else if (name == '金融机构（分支机构）基础信息') {
				url = 'XCreateReportFive';
			} else if (name == '非同业单位客户基础信息') {
				url = 'XCreateReportSix';
			} else if (name == '金融机构（法人）基础信息') {
				url = 'Xcreatemessagejrjgfr';
			} else if (name == '存量个人贷款信息') {
				url = 'XCreateReportXclgrdkxx';
			} else if (name == '个人客户基础信息') {
				url = 'XCreateReportXgrkhxx';
			} else if (name == '个人贷款发生额信息') {
				url = 'XCreateReportXgrdkfsxx';
			} else if (name == '委托贷款发生额信息') {
				url = 'XCreateReportwtdkfse';
			} else if (name == '存量委托贷款信息') {
				url = 'XCreateReportTen';
			} else if (name == '存量同业借贷信息') {
				url = 'XCreateReportXcltyjdxx';
			} else if (name == '同业借贷发生额信息') {
				url = 'XCreateReportXtyjdfsexx';
			} else if (name == '同业客户基础信息') {
				url = 'XCreateReportXtykhjcxx';
			} else if (name == '存量同业存款信息') {
				url = 'XCreateReportXcltyckxx';
			} else if (name == '同业存款发生额信息') {
				url = 'XCreateReportXtyckfsexx';
			}
		}else{
			url="createReportByCode"+code;
		}
		$('#datedialog').window('close');
	    $.messager.confirm('提示','是否生成报文？',function (r){
	        if(r){
				$.messager.progress({
					title: '请稍等',
					msg: '数据处理中......'
				});
				$.ajax({
					url: url,
					type: "POST",
					contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
					async: true,
					data:{"date":value},
			        dataType:'json',
					success: function (data) {
						tableNameBlank = name;
						countBlank = 1;
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文成功!', 'info');
						$("#dg1").datagrid("reload");
					},
					error: function (error) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文失败!', 'error');
					}
				})
	        }
	    });
	}
	
}
</script>		
</body>
</html >
