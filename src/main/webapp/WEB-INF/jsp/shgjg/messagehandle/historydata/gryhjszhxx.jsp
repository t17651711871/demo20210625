<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>数据报送</title>
<%@include file="../../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="个人银行结算账户信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="SHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;
	   
	        账户名称:<input type="text" id="accname" style="height: 23px; width:190px"/>&nbsp;&nbsp;
	       账户账号:<input type="text" id="accno" style="height: 23px; width:190px"/>&nbsp;&nbsp;
	       
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="button1()">查询</a>&nbsp;&nbsp;
		
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="button2()">查看详情</a>
	</div>
    
	<div style="visibility: hidden;">
		<div id="addDialog" class="easyui-dialog" data-options="iconCls:'icon-save',toolbar:'#tbForAddDialog'" style="width: 600px; height: 460px; align-items: center;">
			<!-- 信息录入 -->
			<form id="formForAddUser" method="post">
				<div style="padding-top: 20px">
					<table style="padding-left: 30px">
						<tr>
							<td align="right">开户银行金融机构代码(法人):</td>
							<td><input class="easyui-validatebox" name="org_code" id="org_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">开户机构法人机构识别编码(LEI):</td>
							<td><input class="easyui-validatebox" name="org_lei" id="org_lei" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">开户银行金融机构代码(网点):</td>
							<td><input class="easyui-validatebox" name="open_code" id="open_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">客户编号:</td>
							<td><input class="easyui-validatebox" name="cst_code" id="cst_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户名称:</td>
							<td><input class="easyui-validatebox" name="acc_name" id="acc_name" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户账号:</td>
							<td><input class="easyui-validatebox" name="acc_no" id="acc_no" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户类型:</td>
							<td>
							   <select class="easyui-combobox" name="acc_type" id="acc_type" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-I类账户</option>
							        <option value="11">11-II类账户</option>
							        <option value="12">12-III类账户</option>
							        <option value="13">13-贷记账户</option>
							        <option value="19">19-其他类型账户</option>
							   </select>
							</td>
						</tr>

						<tr>
							<td align="right">开户日期:</td>
							<td><input class="easyui-datebox" name="open_date" id="open_date" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">开户时间:</td>
							<td><input type="text" name="open_time" id="open_time" style="height: 21px; width: 173px" /></td>
						</tr>

						<tr>
							<td align="right">销户日期:</td>
							<td><input class="easyui-datebox" name="close_date" id="close_date" style="height: 21px; width: 173px"/></td>
						</tr>
						
						<tr>
							<td align="right">销户时间:</td>
							<td><input type="text" name="close_time" id="close_time" style="height: 21px; width: 173px"/></td>
						</tr>
						
						<tr>
							<td align="right">账户状态:</td>
							<td>
							   <select class="easyui-combobox" name="acc_state" id="acc_state" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-正常</option>
							        <option value="11">11-未激活</option>
							        <option value="12">12-只收不付</option>
							        <option value="13">13-不收不付</option>
							        <option value="14">14-已注销</option>
							        <option value="15">15-已冻结</option>
							        <option value="19">19-其他</option>
							   </select>
						    </td>
						</tr>
						
						<tr>
							<td align="right">证件种类:</td>
							<td>
							<select class="easyui-combobox" name="id_type" id="id_type" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-居民身份证</option>
							        <option value="11">11-临时身份证</option>
							        <option value="12">12-户口簿</option>
							        <option value="13">13-军人或武警身份证</option>
							        <option value="14">14-港澳居民来往内地通行证,台湾居民来往大陆通行证</option>
							        <option value="15">15-外国公民护照</option>
							        <option value="16">16-外国人永久居留证</option>
							        <option value="17">17-港澳台居民居住证</option>
							        <option value="18">18-中国护照</option>
							        <option value="19">19-边民出入境通行证</option>
							        <option value="20">20-其他类个人身份有效证件</option>
							   </select>
							</td>
						</tr>
						
						<tr>
							<td align="right">身份证件号码:</td>
							<td><input type="text" name="id_no" id="id_no" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">身份证件有效期开始日:</td>
							<td><input class="easyui-datebox" name="id_startline" id="id_startline" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">身份证件有效期截止日:</td>
							<td><input class="easyui-datebox" name="id_deadline" id="id_deadline" style="height: 21px; width: 173px"/></td>
						</tr>
						
						<tr>
							<td align="right">身份证件发证机关所在地的地区代码:</td>
							<td><input type="text" name="id_region" id="id_region" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">客户所属国家(地区):</td>
							<td><input type="text" name="nation" id="nation" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">客户性别:</td>
							<td>
							<select class="easyui-combobox" name="cst_sex" id="cst_sex" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-男</option>
							        <option value="11">11-女</option>
							   </select></td>
						</tr>
						
						<tr>
							<td align="right">联系地址:</td>
							<td><input type="text" name="address" id="address" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">身份核验方式:</td>
							<td>
							<select class="easyui-combobox" name="id_ver" id="id_ver" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-面对面</option>
							        <option value="11">11-非面对面</option>
							   </select></td>
						</tr>
						
						<tr>
							<td align="right">绑定的手机号码:</td>
							<td><input type="text" name="bind_mob" id="bind_mob" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">绑定的银行账户账号:</td>
							<td><input type="text" name="bind_acc" id="bind_acc" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">绑定银行账号日期:</td>
							<td><input class="easyui-datebox" name="bind_date" id="bind_date" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">绑定银行账号时间:</td>
							<td><input type="text" name="bind_time" id="bind_time" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">被绑定的银行账户开户行(法人)代码:</td>
							<td><input type="text" name="bind_bank" id="bind_bank" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人姓名:</td>
							<td><input type="text" name="id_name2" id="id_name2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件种类:</td>
							<td>
							<select class="easyui-combobox" name="id_type2" id="id_type2" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-居民身份证</option>
							        <option value="11">11-临时身份证</option>
							        <option value="12">12-户口簿</option>
							        <option value="13">13-军人或武警身份证</option>
							        <option value="14">14-港澳居民来往内地通行证,台湾居民来往大陆通行证</option>
							        <option value="15">15-外国公民护照</option>
							        <option value="16">16-外国人永久居留证</option>
							        <option value="17">17-港澳台居民居住证</option>
							        <option value="18">18-中国护照</option>
							        <option value="19">19-边民出入境通行证</option>
							        <option value="20">20-其他类个人身份有效证件</option>
							   </select>
							</td>
						</tr>
						
						<tr>
							<td align="right">代理人证件号码:</td>
							<td><input type="text" name="id_no2" id="id_no2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件有效期开始日:</td>
							<td><input class="easyui-datebox" name="id_startline2" id="id_startline2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件有效期截止日:</td>
							<td><input class="easyui-datebox" name="id_deadline2" id="id_deadline2" style="height: 21px; width: 173px"/></td>
						</tr>
						
						<tr>
							<td align="right">代理人联系电话:</td>
							<td><input type="text" name="contact2" id="contact2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">开户地地区代码:</td>
							<td><input type="text" name="acc_region" id="acc_region" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质号:</td>
							<td><input type="text" name="media_id" id="media_id" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质类型:</td>
							<td><input type="text" name="media_type" id="media_type" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质状态:</td>
							<td><input type="text" name="media_state" id="media_state" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">用户开立银行账户时的IP地址:</td>
							<td><input type="text" name="ip_addr" id="ip_addr" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">用户开立银行账户时的MAC地址:</td>
							<td><input type="text" name="mac_addr" id="mac_addr" style="height: 21px; width: 173px" /></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>

<script type="text/javascript">
$(function(){
	$("#addDialog").dialog("close");
	
	$("#dg1").datagrid({
		method:'post',
		url:'SGRYHJSZHXXHistory',
		loadMsg:'数据加载中,请稍后...',
		singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'id',title:'id',width:150,align:'center',hidden:true},
			{field:'org_code',title:'开户银行金融机构代码(法人)',width:200,align:'center'},
			{field:'org_lei',title:'开户机构法人机构识别编码(LEI)',width:200,align:'center'},
			{field:'open_code',title:'开户银行金融机构代码(网点)',width:200,align:'center'},
			{field:'cst_code',title:'客户编号',width:150,align:'center'},
			{field:'acc_name',title:'账户名称',width:150,align:'center'},
			{field:'acc_no',title:'账户账号',width:150,align:'center'},
			{field:'acc_type',title:'账户类型',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-I类账户';
				}else if(value == '11'){
					return '11-II类账户';
				}else if(value == '12'){
					return '12-III类账户';
				}else if(value == '13'){
					return '13-贷记账户';
				}else if(value == '19'){
					return '19-其他类型账户';
				}
			}},
			{field:'open_date',title:'开户日期',width:150,align:'center'},
			{field:'open_time',title:'开户时间',width:150,align:'center'},
			{field:'close_date',title:'销户日期',width:150,align:'center'},
			{field:'close_time',title:'销户时间',width:150,align:'center'},
			{field:'acc_state',title:'账户状态',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-正常';
				}else if(value == '11'){
					return '11-未激活';
				}else if(value == '12'){
					return '12-只收不付';
				}else if(value == '13'){
					return '13-不收不付';
				}else if(value == '14'){
					return '14-已注销';
				}else if(value == '15'){
					return '15-已冻结';
				}else if(value == '19'){
					return '19-其他';
				}
			}},
			{field:'id_type',title:'证件种类',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-居民身份证';
				}else if(value == '11'){
					return '11-临时身份证';
				}else if(value == '12'){
					return '12-户口簿';
				}else if(value == '13'){
					return '13-军人或武警身份证';
				}else if(value == '14'){
					return '14-港澳居民来往内地通行证,台湾居民来往大陆通行证';
				}else if(value == '15'){
					return '15-外国公民护照';
				}else if(value == '16'){
					return '16-外国人永久居留证';
				}else if(value == '17'){
					return '17-港澳台居民居住证';
				}else if(value == '18'){
					return '18-中国护照';
				}else if(value == '19'){
					return '19-边民出入境通行证';
				}else if(value == '20'){
					return '20-其他类个人身份有效证件';
				}
			}},
			{field:'id_no',title:'身份证件号码',width:150,align:'center'},
			{field:'id_startline',title:'身份证件有效期开始日',width:150,align:'center'},
			{field:'id_deadline',title:'身份证件有效期截止日',width:150,align:'center'},
			{field:'id_region',title:'身份证件发证机关所在地的地区代码',width:150,align:'center'},
			{field:'nation',title:'客户所属国家(地区)',width:150,align:'center'},
			{field:'cst_sex',title:'客户性别',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-男';
				}else if(value == '11'){
					return '11-女';
				}
			}},
			{field:'address',title:'联系地址',width:150,align:'center'},
			{field:'id_ver',title:'身份核验方式',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-面对面';
				}else if(value == '11'){
					return '11-非面对面';
				}
			}},
			{field:'bind_mob',title:'绑定的手机号码',width:150,align:'center'},
			{field:'bind_acc',title:'绑定的银行账户账号',width:150,align:'center'},
			{field:'bind_date',title:'绑定银行账号日期',width:150,align:'center'},
			{field:'bind_time',title:'绑定银行账号时间',width:150,align:'center'},
			{field:'bind_bank',title:'被绑定的银行账户开户行(法人)代码',width:150,align:'center'},
			{field:'id_name2',title:'代理人姓名',width:150,align:'center'},
			{field:'id_type2',title:'代理人证件种类',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-居民身份证';
				}else if(value == '11'){
					return '11-临时身份证';
				}else if(value == '12'){
					return '12-户口簿';
				}else if(value == '13'){
					return '13-军人或武警身份证';
				}else if(value == '14'){
					return '14-港澳居民来往内地通行证,台湾居民来往大陆通行证';
				}else if(value == '15'){
					return '15-外国公民护照';
				}else if(value == '16'){
					return '16-外国人永久居留证';
				}else if(value == '17'){
					return '17-港澳台居民居住证';
				}else if(value == '18'){
					return '18-中国护照';
				}else if(value == '19'){
					return '19-边民出入境通行证';
				}else if(value == '20'){
					return '20-其他类个人身份有效证件';
				}
			}},
			{field:'id_no2',title:'代理人证件号码',width:150,align:'center'},
			{field:'id_startline2',title:'代理人证件有效期开始日',width:150,align:'center'},
			{field:'id_deadline2',title:'代理人证件有效期截止日',width:150,align:'center'},
			{field:'contact2',title:'代理人联系电话',width:150,align:'center'},
			{field:'acc_region',title:'开户地地区代码',width:150,align:'center'},
			{field:'media_id',title:'介质号',width:150,align:'center'},
			{field:'media_type',title:'介质类型',width:150,align:'center'},
			{field:'media_state',title:'介质状态',width:150,align:'center'},
			{field:'ip_addr',title:'用户开立银行账户时的IP地址',width:150,align:'center'},
			{field:'mac_addr',title:'用户开立银行账户时的MAC地址',width:150,align:'center'},
			{field:'czr',title:'操作人',width:150,align:'center'},
			{field:'czsj',title:'操作时间',width:150,align:'center'},
			{field:'shr',title:'审核人',width:150,align:'center'},
			{field:'shsj',title:'审核时间',width:150,align:'center'}
	    ]]
	});
})
//动态查询
function button1(){
	var accname = $('#accname').val();
	var accno = $('#accno').val();
	$("#dg1").datagrid("reload", {
		accname: accname,
		accno: accno
	});
}

function button2(){
	var row = $('#dg1').datagrid('getSelected');
	if(row == null){
		$.messager.alert('操作提示','请选择一条数据','info');
	}else{
		$('#id').val(row.id);
		$('#org_code').val(row.org_code);
		$('#org_lei').val(row.org_lei);
		$('#open_code').val(row.open_code);
		$('#cst_code').val(row.cst_code);
		$('#acc_name').val(row.acc_name);
		$('#acc_no').val(row.acc_no);
		$('#acc_type').combobox('setValue',row.acc_type);
		$('#open_date').datebox('setValue',row.open_date);
		$('#open_time').val(row.open_time);
		$('#close_date').datebox('setValue',row.close_date);
		$('#close_time').val(row.close_time);
		$('#acc_state').combobox('setValue',row.acc_state);
		$('#id_type').combobox('setValue',row.id_type);
		$('#id_no').val(row.id_no);
		$('#id_startline').datebox('setValue',row.id_startline);
		$('#id_deadline').datebox('setValue',row.id_deadline);
		$('#id_region').val(row.id_region);
		$('#nation').val(row.nation);
		$('#cst_sex').combobox('setValue',row.cst_sex);
		$('#address').val(row.address);
		$('#id_ver').combobox('setValue',row.id_ver);
		$('#bind_mob').val(row.bind_mob);
		$('#bind_acc').val(row.bind_acc);
		$('#bind_date').datebox('setValue',row.bind_date);
		$('#bind_time').val(row.bind_time);
		$('#bind_bank').val(row.bind_bank);
		$('#id_name2').val(row.id_name2);
		$('#id_type2').combobox('setValue',row.id_type2);
		$('#id_no2').val(row.id_no2);
		$('#id_startline2').datebox('setValue',row.id_startline2);
		$('#id_deadline2').datebox('setValue',row.id_deadline2);
		$('#contact2').val(row.contact2);
		$('#acc_region').val(row.acc_region);
		$('#media_id').val(row.media_id);
		$('#media_type').val(row.media_type);
		$('#media_state').val(row.media_state);
		$('#ip_addr').val(row.ip_addr);
		$('#mac_addr').val(row.mac_addr);
		
		$("#addDialog").dialog({title:"查看详情"});
		$("#addDialog").dialog("open");
		$("#addDialog").dialog({modal:true});
	}
}
</script>
</body>
</html>
