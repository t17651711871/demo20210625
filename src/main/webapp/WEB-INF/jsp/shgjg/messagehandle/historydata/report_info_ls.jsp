<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.common.util.VariableUtils"%>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource"%>
<%@ page import="com.geping.etl.common.entity.Report_Info"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
String loginId = vu.getLoginId();
Set<Sys_Auth_Role_Resource> operateReportSet = vu.getOperateReportSet();
Set<Sys_Auth_Role_Resource> reportSet = vu.getReportSet();
List<Report_Info> allReportList = (List<Report_Info>)request.getAttribute("reportlist");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>数据管理</title>
    <%@include file="../../../common/head.jsp"%>
<style type="text/css">
a{
   text-decoration:none;
}

</style>
</head>
<body>
		<table class="easyui-datagrid" fitColumns="true" title="数据管理" id="dg1"
			data-options="autoRowHeight:false,pagination:true,rownumbers:true,toolbar:'#tb',singleSelect:true,pageSize:20,pageList:[20]" style="height: 550px">
		<thead>
				                   <tr>
									    <th data-options="field:'name',width:180,align:'center'">表单名称</th>
			                       </tr>
				
		</thead>
		<tbody>
		   
		    
			<%
		    	  for(Report_Info li : allReportList){
			    	    for(Sys_Auth_Role_Resource report : reportSet){
			    		     if(report.getResValue().equals(li.getCode())){
			%>
			                      <tr>
								     <td>
								      <a href ="<%=li.getHistoryDataUrl()%>" style="color: blue;"><%=li.getName()%></a>
								      </td> 
			                      </tr>
			<%
			                    break;
			                 }
			             }
		      }
			%>
		</tbody>
		</table>
		<div id="tb" style="padding:5px;height:auto;">
		
	<%
		    
		    if(operateReportSet != null){
		      if(!operateReportSet.isEmpty()){
		    	 for(Sys_Auth_Role_Resource sarr : operateReportSet){
				       String operateReportName = sarr.getResValue();
	%>	
		          <%
	    		    if(operateReportName.equals("SHOW_REPORT")){
	              %>
						表单名称: 
							<input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
						    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	             <%
	    		    }
	             %>
	    
	             <%
	    		    if(operateReportName.equals("REPORT_DATA")){
	    		    	//if(operateReportName.equals("REPORT_DATA")){
	             %>
	                    <a class="easyui-linkbutton" iconCls="icon-more" onclick="more()">报表数据</a>
	             <%
	    		    }
	             %>
	<%
		        }
		    }  
		 }else if(loginId.equals("admin")){
	%>	
	                            表单名称: 
					<input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
				    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    <a class="easyui-linkbutton" iconCls="icon-more" onclick="more()">报表数据</a>
	<%		 
		}
	%>
	 </div>
<script type="text/javascript">
$(function(){
	
	$('#dg1').datagrid({loadFilter:pagerFilter});//.datagrid('loadData', getData());
	
	$(".datagrid-cell-group").attr('style','color:red;font-size:large');
})

function pagerFilter(data){
	if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
		data = {
			total: data.length,
			rows: data
		}
	}
	var dg = $(this);
	var opts = dg.datagrid('options');
	var pager = dg.datagrid('getPager');
	pager.pagination({
		onSelectPage:function(pageNum, pageSize){
			opts.pageNumber = pageNum;
			opts.pageSize = pageSize;
			pager.pagination('refresh',{
				pageNumber:pageNum,
				pageSize:pageSize
			});
			dg.datagrid('loadData',data);
		}
	});
	if (!data.originalRows){
		data.originalRows = (data.rows);
	}
	var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
	var end = start + parseInt(opts.pageSize);
	data.rows = (data.originalRows.slice(start, end));
	return data;
}



function more(){
	var row = $("#dg1").datagrid('getSelected');
	if (row != null){
		 window.location.href = row.cursorUrl+"?status=all";
	}else{
		$.messager.alert('操作提示', '请选择一条数据查看报表数据', 'info');
	}
}


function selectRecord(){
	var reportName = $("#reportName").val();
	window.location.href = "Dreport_info?reportName="+encode(reportName);
}
</script>		
</body>
</html >
