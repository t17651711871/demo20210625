package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 *	角色信息表
 */
@Entity(name="Sys_Auth_Role")
@Table(name="SYS_AUTH_ROLE")
public class Sys_Auth_Role {
	@Id
	private String id;      //角色编号
	private String subjectId;//角色所属的项目编号
	private String roleName;//角色名称
	private String description;//描述信息
	private String enabled;//是否可用
	private String isDelete;//是否停用
	
	public Sys_Auth_Role(){
		this.isDelete = "N";
	}
	
	public String getId() {
		return id;
	}
	
	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	
	
}
