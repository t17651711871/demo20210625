package com.geping.etl.common.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;


/**
 * 
 * @author Administrator
 *   查询用户、机构、部门
 */
@Entity(name="Sys_UserAndOrgDepartment")
public class Sys_UserAndOrgDepartment {
	@Id
	private String id;
	private String loginid;          //登录ID                                                                     
	private String userename;        //用户英文名                                                                   
	private String usercname;        //用户中文名                                                                   
	private String password;         //密码                                                                         
	private String orgid;            //机构编号    
	private String orgname;          //机构名称    
	private String orginsidecode;    //内部机构号
	private String licensenumber;    //金融许可证号
	private String departid;         //部门ID 
	private String departmentname;   //部门名称
	private String tel;              //联系电话                                                                     
	private String mobile;           //移动电话                                                                     
	private String address;          //地址                                                                         
	private String email;            //邮箱                                                                         
	private String islocked;         //是否锁定用户                                                                 
	private String userlockedreson;  //锁定原因：00正常；01密码输错$var$次；02被管理员锁定；03用户$var$天未登录     
	private String startdate;        //开始时间                                                                     
	private String enddate;          //结束时间                                                                     
	private String createtime;       //创建时间                                                                     
	private String description;      //描述                                                                         
	private String enabled;          //启用标识                                                                     
	private String isdelete;         //是否删除1-是，逻辑删除                                         
	private String lastlogindate;    //最后登录时间                                                                 
	private String lastmodifydate;   //最后修改密码日期                                                        
	private String isfirstlogin;     //是否为首次登录
	private String handlename;       //操作名
	private String handleperson;     //操作人
	private String handledate;       //操作时间
	private String ip;               //用户使用的ip地址


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLoginid() {
		return loginid;
	}


	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}


	public String getUserename() {
		return userename;
	}


	public void setUserename(String userename) {
		this.userename = userename;
	}


	public String getUsercname() {
		return usercname;
	}


	public void setUsercname(String usercname) {
		this.usercname = usercname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getOrgid() {
		return orgid;
	}


	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}


	public String getDepartid() {
		return departid;
	}


	public void setDepartid(String departid) {
		this.departid = departid;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getIslocked() {
		return islocked;
	}


	public void setIslocked(String islocked) {
		this.islocked = islocked;
	}


	public String getUserlockedreson() {
		return userlockedreson;
	}


	public void setUserlockedreson(String userlockedreson) {
		this.userlockedreson = userlockedreson;
	}


	public String getStartdate() {
		return startdate;
	}


	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}


	public String getEnddate() {
		return enddate;
	}


	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}


	public String getCreatetime() {
		return createtime;
	}


	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEnabled() {
		return enabled;
	}


	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}


	public String getIsdelete() {
		return isdelete;
	}


	public void setIsdelete(String isdelete) {
		this.isdelete = isdelete;
	}


	public String getLastlogindate() {
		return lastlogindate;
	}


	public void setLastlogindate(String lastlogindate) {
		this.lastlogindate = lastlogindate;
	}


	public String getLastmodifydate() {
		return lastmodifydate;
	}


	public void setLastmodifydate(String lastmodifydate) {
		this.lastmodifydate = lastmodifydate;
	}


	public String getIsfirstlogin() {
		return isfirstlogin;
	}


	public void setIsfirstlogin(String isfirstlogin) {
		this.isfirstlogin = isfirstlogin;
	}


	public String getDepartmentname() {
		return departmentname;
	}


	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}


	public String getOrgname() {
		return orgname;
	}


	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}


	public String getOrginsidecode() {
		return orginsidecode;
	}


	public void setOrginsidecode(String orginsidecode) {
		this.orginsidecode = orginsidecode;
	}


	public String getLicensenumber() {
		return licensenumber;
	}


	public void setLicensenumber(String licensenumber) {
		this.licensenumber = licensenumber;
	}


	public String getHandlename() {
		return handlename;
	}


	public void setHandlename(String handlename) {
		this.handlename = handlename;
	}


	public String getHandleperson() {
		return handleperson;
	}


	public void setHandleperson(String handleperson) {
		this.handleperson = handleperson;
	}


	public String getHandledate() {
		return handledate;
	}


	public void setHandledate(String handledate) {
		this.handledate = handledate;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}



	
}
