package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Sys_User")
@Table(name="SYS_USER")
public class Sys_User {
	@Id
	private String id;
	private String loginId;          //登录账户                                                                      
	private String userEname;        //用户英文名                                                                   
	private String userCname;        //用户中文名                                                                   
	private String password;         //密码                                                                         
	private String orgId;            //所属机构编号                                                                      
	private String departId;         //所属部门编号                                                              
	private String tel;              //联系电话                                                                     
	private String mobile;           //移动电话                                                                     
	private String address;          //地址                                                                         
	private String email;            //邮箱                                                                         
	private String isLocked;         //是否锁定用户                                                                 
	private String userLockedReson;  //锁定原因：00正常；01密码输错$var$次；02被管理员锁定；03用户$var$天未登录     
	private String startDate;        //开始时间                                                                     
	private String endDate;          //结束时间                                                                     
	private String createTime;       //创建时间                                                                     
	private String description;      //描述                                                                         
	private String enabled;          //启用标识                                                                     
	private String isDelete;         //是否删除1-是，逻辑删除                                                       
	private String lastLoginDate;    //最后登录时间                                                                 
	private String lastModifyDate;   //最后修改密码日期                                                             
	private String isFirstLogin;     //是否为首次登录
	private String handlename;       //操作名
	private String handleperson;     //操作人
	private String handledate;       //操作时间
	private String pwdstr;           //记录使用过的密码
	private String ip;               //该用户绑定的电脑ip地址
	
	public Sys_User(){
		this.isLocked = "Y";
		this.enabled = "Y";
		this.isDelete = "N";
		this.isFirstLogin = "Y";
		this.handlename = " ";   //操作名默认值为一个空格" "
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLoginId() {
		return loginId;
	}


	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


	public String getUserEname() {
		return userEname;
	}


	public void setUserEname(String userEname) {
		this.userEname = userEname;
	}


	public String getUserCname() {
		return userCname;
	}


	public void setUserCname(String userCname) {
		this.userCname = userCname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getOrgId() {
		return orgId;
	}


	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getIsLocked() {
		return isLocked;
	}


	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}


	public String getUserLockedReson() {
		return userLockedReson;
	}


	public void setUserLockedReson(String userLockedReson) {
		this.userLockedReson = userLockedReson;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEnabled() {
		return enabled;
	}


	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}


	public String getIsDelete() {
		return isDelete;
	}


	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}


	public String getLastLoginDate() {
		return lastLoginDate;
	}


	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}


	public String getLastModifyDate() {
		return lastModifyDate;
	}


	public void setLastModifyDate(String lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	public String getIsFirstLogin() {
		return isFirstLogin;
	}


	public void setIsFirstLogin(String isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}


	public String getDepartId() {
		return departId;
	}


	public void setDepartId(String departId) {
		this.departId = departId;
	}


	public String getHandlename() {
		return handlename;
	}


	public void setHandlename(String handlename) {
		this.handlename = handlename;
	}


	public String getHandleperson() {
		return handleperson;
	}


	public void setHandleperson(String handleperson) {
		this.handleperson = handleperson;
	}


	public String getHandledate() {
		return handledate;
	}


	public void setHandledate(String handledate) {
		this.handledate = handledate;
	}


	public String getPwdstr() {
		return pwdstr;
	}


	public void setPwdstr(String pwdstr) {
		this.pwdstr = pwdstr;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	
}
