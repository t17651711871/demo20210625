package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
/**
 * 
 * @author Mr.chen
 *	系统菜单信息表
 */
@Entity(name="Sys_Menu")
@Table(name="SYS_MENU")
public class Sys_Menu {
	@Id
	private String id;      //菜单编号
	private String subjectId;//菜单所属项目编号
	private String ename;//菜单英文名称
	private String name;//菜单中文名称
	private String pmenuid;//父级菜单编号
	private String target;//Iframe的ID
	private String url;//连接地址
	private String icon;//ICon名称
	private String orderNum;//序号
	private String enabled;//是否使用
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getPmenuid() {
		return pmenuid;
	}
	public void setPmenuid(String pmenuid) {
		this.pmenuid = pmenuid;
	}
	
}
