package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 部门
 * @author Administrator
 *
 */
@Entity(name="Sys_Department")
@Table(name="SYS_DEPARTMENT")
public class Sys_Department {
	@Id
	private String id;
	private String departmentId;   //部门编号
	private String departmentName; //部门名称
	private String description;    //部门描述
	private String isDelete;       //是否删除
	
	
	public Sys_Department(){
		this.isDelete = "N";
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
   
	
}
