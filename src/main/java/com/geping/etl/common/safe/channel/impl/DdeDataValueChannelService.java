package com.geping.etl.common.safe.channel.impl;

import com.geping.etl.common.safe.channel.DataChannelService;
import com.geping.etl.utils.DdelUtils;

/**
 * Dde数据通道service
 * @author  liang.xu
 * @date 2021年5月10日
 */
public class DdeDataValueChannelService implements DataChannelService {


    @Override
    public String channel(String source) {
        return DdelUtils.filterDde(source);
    }
}