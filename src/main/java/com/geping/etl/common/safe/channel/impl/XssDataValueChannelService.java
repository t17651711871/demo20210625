package com.geping.etl.common.safe.channel.impl;

import com.geping.etl.common.safe.channel.DataChannelService;
import com.geping.etl.utils.HtmlUtils;

/**
 * Xss数据通道service
 * @author  liang.xu
 * @date 2021年5月10日
 */
public class XssDataValueChannelService implements DataChannelService {


    @Override
    public String channel(String source) {
        return HtmlUtils.htmlEncode(source);
    }
}