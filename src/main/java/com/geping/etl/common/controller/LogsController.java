package com.geping.etl.common.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.geping.etl.common.entity.Logs;
import com.geping.etl.common.entity.Sys_Org;
import com.geping.etl.common.service.LogsService;
import com.geping.etl.common.service.Sys_OrgService;

import net.sf.json.JSONObject;

@RestController
public class LogsController {
	
	@Autowired
	private LogsService ls;
	
	@Autowired
	private Sys_OrgService sys_OrgService;
	
    //访问查看日志页面
	@RequestMapping(value="/showLogsUi",method=RequestMethod.GET)
	public ModelAndView showLogsUi(){
		ModelAndView model = new ModelAndView();
		List<Sys_Org> list = sys_OrgService.findAllSysOrg();
		model.addObject("list", list);
		model.setViewName("common/logs");
		return model;
	}
	
	
	//查询日志信息
	@RequestMapping(value="/showLogs",method=RequestMethod.POST)
	public void showLogs(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
		    
		    String startDate = request.getParameter("startDate");       //开始时间
			String endDate = request.getParameter("endDate");           //结束时间
		    String logOrgCode = request.getParameter("logOrgCode");
		    
		    String currentPageNumber = request.getParameter("page");  //获取查询页数
		    String currentPageSize = request.getParameter("rows");   //获取每页条数
		    
		    int page = Integer.valueOf(currentPageNumber) - 1;
		    int size = Integer.valueOf(currentPageSize);
		    
		    Specification<Logs> querySpecifi = new Specification<Logs>() {
				@Override
				public Predicate toPredicate(Root<Logs> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if(startDate != null && !startDate.equals("")){
			            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("logDate"),startDate + " 00:00:00"));
			        }
			        
			        if(endDate != null && !endDate.equals("")){
			            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("logDate"),endDate + " 23:59:59"));
			        }
					
			        if(logOrgCode != null) {
			        	if(!logOrgCode.equals("all")) {
				        	predicates.add(criteriaBuilder.equal(root.get("logOrgCode"),logOrgCode));
				        }
			        }
			        
	                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
	        };
	        
	        /**
	         * 多条件排序
	        List<Order> orders=new ArrayList<Order>();
	        orders.add(new Order(Direction.DESC,"a"));
	        orders.add(new Order(Direction.DESC,"b"));
	        */
	        
	        /**
	         * 单一条件排序
	         */
	        Sort sort = new Sort(Sort.Direction.DESC,"logDate");
	        
	        //Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
			PageRequest pr = new PageRequest(page,size,sort);     //intPage从0开始
	        Page<Logs> gcPage =  ls.findAll(querySpecifi,pr);
		    JSONObject json = new JSONObject();
			out = response.getWriter();
			json.put("rows",gcPage.getContent());
			json.put("total",gcPage.getTotalElements());   // 将总页数传到页面上显示
			out.write(json.toString());
		} catch (IOException e){
			e.printStackTrace();
		}finally{
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
}
