package com.geping.etl.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Logs;
import com.geping.etl.common.repository.LogsRepository;
import com.geping.etl.common.service.LogsService;

@Service
@Transactional
public class LogsServiceImpl implements LogsService {

	@Autowired
	private LogsRepository lr;
	
	//新增日志信息
	public Logs save(Logs logs){
		
		return  lr.save(logs);
	}

	@Override
	public Page<Logs> findAll(Specification<Logs> spec, Pageable pageable) {
		
		return lr.findAll(spec, pageable);
	}

}
