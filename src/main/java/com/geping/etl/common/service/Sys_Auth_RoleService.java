package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_Auth_Role;



public interface Sys_Auth_RoleService {
	//得到子系统名称
	//public List<Sys_Org> getSSName();
	
	
	public List<Sys_Auth_Role> getAllSys_Auth_Role();
	
	//获取最大ID数
	public String findMaxId();
	
	//得到角色管理的所有数据，从中提出角色名称和角色用来显示
	public Iterable<Sys_Auth_Role> findAll();
	
	//根据角色名称查询，避免新增角色时系统中的角色名称中文名重复
	public Sys_Auth_Role getRoleName(String roleName);
	//新增角色管理
	public Sys_Auth_Role save(Sys_Auth_Role auth_Role);
	
	//根据角色名模糊查询角色
	public List<Sys_Auth_Role> getSys_Auth_RoleByLikeRoleName(String subjectId,String roleName);
	
	//修改角色名称和角色描述
	public Integer updateRoleNameAndRoleDesc(Sys_Auth_Role setCanshu);
	
	//删除角色管理（逻辑删除，数据库中并未删除）
	public Integer deleteSys_Auth_Role(String id);
	
	public List<Sys_Auth_Role> getSysAuthRoleNotStop();
	
	public List<Sys_Auth_Role> findRoleBySubjectId(String subjectId);
	
}
