package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_Auth_Role_User;

/**
 * 
 * @author Mr.chen
 *	用户角色对照表
 *	表：sys_auth_role_user
 */
public interface Sys_Auth_Role_UserService {
	//点击角色分配中的保存按钮，将之前分配的角色全部清空，本次实现真实删除，并非逻辑上删除
	public Integer deleteSysAuthRoleUser(String userId);
	
	//角色分配保存按钮	
	public Sys_Auth_Role_User save(Sys_Auth_Role_User sys_Auth_Role_User);
	
	//通过userId查询，是否已经存在角色分配，如果已经存在的话进行回显	
	public List<Sys_Auth_Role_User> findSysAuthRoleUserByRoleId(String userId);
	
	public List<Sys_Auth_Role_User> findRoleUserByRoleId(String roleId);
}
