package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Auth_Role;
import com.geping.etl.common.repository.Sys_Auth_RoleRepository;
import com.geping.etl.common.service.Sys_Auth_RoleService;

@Service
@Transactional
public class Sys_Auth_RoleServiceImpl implements Sys_Auth_RoleService{

	@Autowired
	private Sys_Auth_RoleRepository sysauthrolepository;

	@Override
	public Iterable<Sys_Auth_Role> findAll() {
		
		return sysauthrolepository.findAll();
	}

	@Override
	public Sys_Auth_Role getRoleName(String roleName) {
		
		return sysauthrolepository.getRoleName(roleName);
	}

	@Override
	public Sys_Auth_Role save(Sys_Auth_Role auth_Role) {
		
		return sysauthrolepository.save(auth_Role);
	}

	@Override
	public String findMaxId() {
		
		return sysauthrolepository.findMaxId();
	}

	@Override
	public List<Sys_Auth_Role> getSys_Auth_RoleByLikeRoleName(String subjectId,String roleName) {
	
		return sysauthrolepository.getSys_Auth_RoleByLikeRoleName(subjectId,roleName);
	}

	@Override
	public Integer updateRoleNameAndRoleDesc(Sys_Auth_Role setCanshu) {
		
		return sysauthrolepository.updateRoleNameAndRoleDesc(setCanshu);
	}
	
	@Override
	public List<Sys_Auth_Role> getAllSys_Auth_Role() {
		
		return sysauthrolepository.getAllSys_Auth_Role();
	}

	//删除角色管理（逻辑删除，数据库中并未删除）
	@Override
	public Integer deleteSys_Auth_Role(String id) {
		
		return sysauthrolepository.deleteSys_Auth_Role(id);
	}

	@Override
	public List<Sys_Auth_Role> getSysAuthRoleNotStop() {
		
		return sysauthrolepository.getSysAuthRoleNotStop();
	}

	@Override
	public List<Sys_Auth_Role> findRoleBySubjectId(String subjectId) {
		
		return sysauthrolepository.findRoleBySubjectId(subjectId);
	}


	//显示子系统名称
	/*@Override
	public List<Sys_Org> getSSName() {
		// TODO Auto-generated method stub
		return sysauthrolepository.getSSName();
	}*/

}
