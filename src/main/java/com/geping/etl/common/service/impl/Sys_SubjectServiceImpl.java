package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Subject;
import com.geping.etl.common.repository.Sys_SubjectRepository;
import com.geping.etl.common.service.Sys_SubjectService;

@Service
@Transactional
public class Sys_SubjectServiceImpl implements Sys_SubjectService {

	@Autowired
	private Sys_SubjectRepository ssr;
	
	@Override
	public List<Sys_Subject> findAllSubject() {
		
		return ssr.findAllSubject();
	}

	@Override
	public List<Sys_Subject> findSubjectByUserId(String userId) {
		
		return ssr.findSubjectByUserId(userId);
	}
}
