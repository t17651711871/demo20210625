package com.geping.etl.common.service;


import java.util.List;

import com.geping.etl.common.entity.Sys_Department;


public interface Sys_DepartmentService {
	
	//查询所有未删除的部门
	public List<Sys_Department> findAllSys_Department();
	
	//根据用户名模糊查询部门信息
	public List<Sys_Department> getSys_DepartmentByLikeDepartmentName(String departmentName);
	
	//插入
	public Sys_Department save(Sys_Department sys_Department);
	
	//获取最大ID数
	public String findMaxId();
	
	//修改部门信息
    public int updateSys_Department(String id_edit,String departmentName,String description);
    
    //逻辑删除部门
    public int deleteSys_Department(String id);
}
