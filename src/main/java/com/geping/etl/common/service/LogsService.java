package com.geping.etl.common.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.common.entity.Logs;

public interface LogsService {
	

	//新增日志信息
	public Logs save(Logs logs);
	
	//日志查询
	Page<Logs> findAll(Specification<Logs> spec, Pageable pageable);
}
