package com.geping.etl.common.util;

import java.util.Set;

import com.geping.etl.common.entity.Sys_Auth_Role_Resource;

public class VariableUtils {
	
	private Set<Sys_Auth_Role_Resource> reportSet;  //报表集合
	private Set<Sys_Auth_Role_Resource> operateReportSet; //按钮集合
	private String role;//角色  (maker,checker,super)
	private String loginId;
	private String roleStyle;   //角色类型：admin  maker checker
	private String status;      //数据状态
	private String orgId;//机构编号   （银行机构代码）
	private String orgName;//机构名称（银行机构名称）
	private String orgInsideCode; //内部机构号
	private String licenseNumber;  //金融许可证号
	
	public Set<Sys_Auth_Role_Resource> getReportSet() {
		return reportSet;
	}
	public void setReportSet(Set<Sys_Auth_Role_Resource> reportSet) {
		this.reportSet = reportSet;
	}
	public Set<Sys_Auth_Role_Resource> getOperateReportSet() {
		return operateReportSet;
	}
	public void setOperateReportSet(Set<Sys_Auth_Role_Resource> operateReportSet) {
		this.operateReportSet = operateReportSet;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getRoleStyle() {
		return roleStyle;
	}
	public void setRoleStyle(String roleStyle) {
		this.roleStyle = roleStyle;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgInsideCode() {
		return orgInsideCode;
	}
	public void setOrgInsideCode(String orgInsideCode) {
		this.orgInsideCode = orgInsideCode;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
}
