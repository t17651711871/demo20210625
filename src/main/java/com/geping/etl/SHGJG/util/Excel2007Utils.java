/*package com.geping.etl.SHGJG.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
*//**
 * excel2007百万级数据导出工具
 * @author WuZengWen
 * @date 2019年4月12日 下午6:23:29
 *//*
public class Excel2007Utils {

	private static final int DEFAULT_COLUMN_SIZE = 30;

	*//**
	 *      * 断言Excel文件写入之前的条件      *      * @param directory 目录      * @param
	 * fileName  文件名      * @return file      * @throws IOException      
	 *//*
	private static File assertFile(String directory, String fileName) throws IOException {
		File tmpFile = new File(directory + File.separator + fileName + ".xlsx");
		if (tmpFile.exists()) {
			if (tmpFile.isDirectory()) {
				throw new IOException("File '" + tmpFile + "' exists but is a directory");
			}
			if (!tmpFile.canWrite()) {
				throw new IOException("File '" + tmpFile + "' cannot be written to");
			}
		} else {
			File parent = tmpFile.getParentFile();
			if (parent != null) {
				if (!parent.mkdirs() && !parent.isDirectory()) {
					throw new IOException("Directory '" + parent + "' could not be created");
				}
			}
		}
		return tmpFile;
	}

	*//**
	 *      * 日期转化为字符串,格式为yyyy-MM-dd HH:mm:ss      
	 *//*
	private static String getCnDate(Date date) {
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	*//**
	 * 写Excel标题
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param columnNames 列头 非null
	 * @param sheetTitle 标题 可null
	 * @param flag 追加写入 非null
	 * @return Excel
	 * @throws IOException
	 *//*
	public static File writeExcelTitle(String filePath, String excelName, String sheetName, List<String> columnNames,
			String sheetTitle, boolean flag) throws IOException {
		File tmpFile = assertFile(filePath, excelName);
		return exportExcelTitle(tmpFile, sheetName, columnNames, sheetTitle, flag);
	}

	*//**
	 *      * 导出字符串数据      *      * @param file        文件名      * @param columnNames
	 * 表头      * @param sheetTitle  sheet页Title      * @param append      是否追加写文件  
	 *    * @return file      * @throws ReportInternalException      
	 *//*
	private static File exportExcelTitle(File file, String sheetName, List<String> columnNames, String sheetTitle,
			boolean append) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists() && append) {
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		//不为空就创建标题
		if(StringUtils.isNotBlank(sheetTitle) && sheetTitle != null) {
			// 合并单元格
			sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, columnNames.size() - 1));
			// 产生表格标题行
			Row rowMerged = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			Cell mergedCell = rowMerged.createCell(0);
			mergedCell.setCellStyle(headStyle);
			mergedCell.setCellValue(new XSSFRichTextString(sheetTitle));
		}
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	*//**
	 * 写入excel
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param objects 写入数据 非null
	 * @return excel
	 * @throws IOException
	 *//*
	public static File writeExcelData(String filePath, String excelName, String sheetName, List<List<Object>> objects)
			throws IOException {
		File tmpFile = assertFile(filePath, excelName);
		return exportExcelData(tmpFile, sheetName, objects);
	}

	*//**
	 *      * 导出字符串数据      *      * @param file    文件名      * @param objects 目标数据  
	 *    * @return      * @throws ReportInternalException      
	 *//*
	private static File exportExcelData(File file, String sheetName, List<List<Object>> objects) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists()) {
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}

		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式
		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		int onlyrow = sheet.getPhysicalNumberOfRows();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}else {
			if(onlyrow > 0) {
				lastRowIndex = onlyrow;
			}
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			Row row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	*//**
	 * 写入excel
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param columnNames 列标题 非null
	 * @param sheetTitle sheet标题 可null
	 * @param objects 写入数据 非null
	 * @param flag 是否追加写入 非null
	 * @return excel
	 * @throws IOException
	 *//*
	public static File writeExcel(String filePath, String excelName, String sheetName, List<String> columnNames,
			String sheetTitle, List<List<Object>> objects, boolean flag) throws IOException {
		File tmpFile;
		tmpFile = assertFile(filePath, excelName);
		return exportExcel(tmpFile, sheetName, columnNames, sheetTitle, objects, flag);
	}

	*//**
	 *      * 导出字符串数据      *      * @param file        文件名      * @param columnNames
	 * 表头      * @param sheetTitle  sheet页Title      * @param objects     目标数据    
	 *  * @param append      是否追加写文件      * @return      * @throws
	 * ReportInternalException      
	 *//*

	private static File exportExcel(File file, String sheetName, List<String> columnNames, String sheetTitle,
			List<List<Object>> objects, boolean append) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists() && append) {
			// 声明一个工作薄
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		//不为空就创建标题
		if(StringUtils.isNotBlank(sheetTitle) && sheetTitle != null) {
			// 合并单元格
			sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, columnNames.size() - 1));
			// 产生表格标题行
			Row rowMerged = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			Cell mergedCell = rowMerged.createCell(0);
			mergedCell.setCellStyle(headStyle);
			mergedCell.setCellValue(new XSSFRichTextString(sheetTitle));
		}
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		lastRowIndex++;
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	*//**
	 *      * 单元格样式列表      
	 *//*
	private static Map<String, CellStyle> styleMap(Workbook workbook) {
		Map<String, CellStyle> styleMap = new LinkedHashMap<>();
		styleMap.put("head", createCellHeadStyle(workbook));
		styleMap.put("content", createCellContentStyle(workbook));
		styleMap.put("integer", createCellContent4IntegerStyle(workbook));
		styleMap.put("double", createCellContent4DoubleStyle(workbook));
		return styleMap;
	}

	*//**
	 *      * 创建单元格表头样式      *      * @param workbook 工作薄      
	 *//*
	private static CellStyle createCellHeadStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 表头样式
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 把字体应用到当前的样式
		style.setFont(font);
		return style;
	}

	*//**
	 * 创建单元格正文样式
	 *
	 * @param workbook
	 *            工作薄
	 *//*
	private static CellStyle createCellContentStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		return style;
	}

	*//**
	 * 单元格样式(Integer)列表
	 *//*
	private static CellStyle createCellContent4IntegerStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));// 数据格式只显示整数
		return style;
	}

	*//**
	 * 单元格样式(Double)列表
	 *//*
	private static CellStyle createCellContent4DoubleStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));// 保留两位小数点
		return style;
	}

	*//**
	 * 网页下载默认路径
	 * @param response 结果集
	 * @param columnNames 列标题 非null
	 * @param objects 写入数据
	 * @throws IOException 
	 *//*
	public static void writeExcelResponse(HttpServletResponse response, List<String> columnNames, List<List<Object>> objects) throws IOException {
		exportExcelResponse(response, columnNames, objects);
	}
	
	*//**
	 * 网页导出数据
	 * @param response 结果集
	 * @param columnNames 列头名
	 * @param objects 写入数据
	 * @throws IOException
	 *//*
	private static void exportExcelResponse(HttpServletResponse response, List<String> columnNames,List<List<Object>> objects) throws IOException {
		OutputStream ops = null;
		// 声明一个工作薄
		Workbook workBook = new XSSFWorkbook();
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = workBook.createSheet("sheet1");
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		lastRowIndex++;
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			ops = new BufferedOutputStream(response.getOutputStream());// 输出流
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				if (ops != null) {
					ops.flush();
					ops.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
*/