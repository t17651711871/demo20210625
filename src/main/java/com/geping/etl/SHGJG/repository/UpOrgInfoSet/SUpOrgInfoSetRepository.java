package com.geping.etl.SHGJG.repository.UpOrgInfoSet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;

public interface SUpOrgInfoSetRepository extends JpaRepository<SUpOrgInfoSet,String>,JpaSpecificationExecutor<SUpOrgInfoSet>{

}
