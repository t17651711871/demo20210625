package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:07 2020/6/9
 */
public interface XcldkxxService {

    Xcldkxx save(Xcldkxx xcldkxx);

    List<Xcldkxx> saveAll(List<Xcldkxx> list);

    Page<Xcldkxx> findAll(Specification specification, PageRequest pageRequest);


}
