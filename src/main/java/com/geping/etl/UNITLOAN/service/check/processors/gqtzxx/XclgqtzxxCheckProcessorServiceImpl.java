package com.geping.etl.UNITLOAN.service.check.processors.gqtzxx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.enums.DataTypeEnum;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.EnhanceDataDictionary;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXclgqtzxxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 存量股权投资信息
 */

@Service
public class XclgqtzxxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Autowired
    private CheckXclgqtzxxRowData checkXclgqtzxxRowData;
    @Autowired
    private EnhanceDataDictionary enhanceDataDictionary;
    @Autowired
    private CustomSqlUtil customSqlUtil;
    /**
     * 获取实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xclgqtzxx.class.getSimpleName();
    }

    /**
     *获取准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xclgqtzxx a"+whereStr;
        String sqlcount="select count(*) from xclgqtzxx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 获取错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"凭证编码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xclgqtzxx xclgqtzxx= (Xclgqtzxx) t;
        checkXclgqtzxxRowData.checkRow(getHeaderTpl(),checkParamContext,xclgqtzxx);
    }

    /**
     * 表间校验
     * @param checkParamContext
     */
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+凭证编码
        tableCalibrationJS2321(checkParamContext);
        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
        tableCalibrationJS1751(checkParamContext);
        //机构证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
        tableCalibrationJS1916(checkParamContext);
    }

    /**
     * 机构证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
     * @param checkParamContext
     */
    private void tableCalibrationJS1916(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1916")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql=(String) SpringContextUtil.getBean("Xclgqtzxx.Ftkfora");
            }else{
                existsSql=(String) SpringContextUtil.getBean("Xclgqtzxx.Ftkf");
            }
            String format = String.format(existsSql,checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"机构证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在");
        }
    }

    /**
     * 金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1751(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1751")) {
            String existsSql = (String) SpringContextUtil.getBean("Xclgqtzxx.Jj");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致");
        }
    }

    /**
     *  数据日期+金融机构代码+凭证编码唯一
     * @param checkParamContext
     */
    private void tableCalibrationJS2321(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2321")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xclgqtzxx.Sjpora");
            }else {
                existsSql= (String) SpringContextUtil.getBean("Xclgqtzxx.Sjp");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+凭证编码应唯一");
        }
    }

    @Override
    protected  void enhanceCheckParamContext(CheckParamContext checkParamContext){
        //股权信息
        List<String> gqlxList = enhanceDataDictionary.findCode(DataTypeEnum.GQLX.getType());
        //机构类型
        List<String> jglxList = enhanceDataDictionary.findCode(DataTypeEnum.JGLX.getType());
        //交易方向
        List<String> jyfxList = enhanceDataDictionary.findCode(DataTypeEnum.JYFX.getType());
        //币种
        List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
        //国家地区代码C0013
        List baseCodeGj = customSqlUtil.getBaseCode(BaseCountry.class);
        //行政区划代码C0009
        List baseCodeXz = customSqlUtil.getBaseCode(BaseArea.class);
        Map<String,List> map = new HashMap<>();
        map.put("gqlxList",gqlxList);
        map.put("jglxList",jglxList);
        map.put("jyfxList",jyfxList);
        map.put("baseCode",baseCode);
        map.put("baseCodeGj",baseCodeGj);
        map.put("baseCodeXz",baseCodeXz);
        checkParamContext.setDataDictionarymap(map);
    }
}