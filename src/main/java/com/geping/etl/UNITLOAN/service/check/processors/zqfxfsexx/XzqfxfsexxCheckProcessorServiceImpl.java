package com.geping.etl.UNITLOAN.service.check.processors.zqfxfsexx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXzqfxfsexxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/***
 *   存量债券发行发生额信息 报表校验处理器
 * @author liang.xu
 * @date 2021.4.26
 */
@Service
public class XzqfxfsexxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private CheckXzqfxfsexxRowData checkXzqfxfsexxRowData;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    /**
     * 1.设置相应报表的实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xzqfxfsexx.class.getSimpleName();
    }

    /**
     * 2.准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xzqfxfsexx a"+whereStr;
        String sqlcount="select count(*) from xzqfxfsexx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 3.准备错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"债券代码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xzqfxfsexx xclzqfxxx= (Xzqfxfsexx) t;
        checkXzqfxfsexxRowData.checkRow(getHeaderTpl(),checkParamContext,xclzqfxxx);
    }

    //5.表间校验
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+债券代码+交易日期+发行、兑付标识应唯一
        tableCalibrationJS2320(checkParamContext);
        //当付息方式为利随本清式、附息式固定利率、附息式浮动利率时，票面利率不能为空
        tableCalibrationJS1411(checkParamContext);
        //债券品种与存量债券发行信息的债券品种应该一致
        tableCalibrationJS1748(checkParamContext);
    }

    /**
     * 债券品种与存量债券发行信息的债券品种应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1748(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1748")){
            String existsSql= (String) SpringContextUtil.getBean("Xzqfxfsexx.zqpz");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"债券品种与存量债券发行信息的债券品种应该一致");
        }
    }

    /**
     * 当付息方式为利随本清式、附息式固定利率、附息式浮动利率时，票面利率不能为空
     * @param checkParamContext
     */
    private void tableCalibrationJS1411(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1411")){
            String existsSql= (String) SpringContextUtil.getBean("Xzqfxfsexx.fffp");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"当付息方式为利随本清式、附息式固定利率、附息式浮动利率时，票面利率不能为空");
        }
    }

    /**
     * 数据日期+金融机构代码+债券代码+交易日期+发行、兑付标识应唯一
     * @param checkParamContext
     */
    private void tableCalibrationJS2320(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2320")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xzqfxfsexx.Sjzora");
            }else {
                existsSql= (String) SpringContextUtil.getBean("Xzqfxfsexx.Sjz");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+债券代码+交易日期+发行/兑付标识应唯一");
        }
    }
}