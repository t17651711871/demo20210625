package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbht;
import com.geping.etl.UNITLOAN.repository.report.XdkdbhtRepository;
import com.geping.etl.UNITLOAN.service.report.XdkdbhtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XdkdbhtServiceImpl implements XdkdbhtService {

    @Autowired
    private XdkdbhtRepository xdkdbhtRepository;

    @Override
    public Xdkdbht save(Xdkdbht xdkdbht) {
        return xdkdbhtRepository.save(xdkdbht);
    }

    @Override
    public List<Xdkdbht> saveAll(List<Xdkdbht> list) {
        return xdkdbhtRepository.save(list);
    }

    @Override
    public Page<Xdkdbht> findAll(Specification specification,PageRequest pageRequest){
        return xdkdbhtRepository.findAll(specification,pageRequest);
    }
}
