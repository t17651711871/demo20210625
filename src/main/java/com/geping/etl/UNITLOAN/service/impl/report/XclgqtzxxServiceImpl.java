package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import com.geping.etl.UNITLOAN.repository.report.XclgqtzxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XzqfxfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XclgqtzxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 15:58
 * @描述:
 */
@Transactional
@Service
public class XclgqtzxxServiceImpl implements XclgqtzxxService {
    @Autowired
    private XclgqtzxxRepository xclgqtzxxRepository;

    @Override
    public Page<Xclgqtzxx> findAll(Specification specification, PageRequest pageRequest) {
        return xclgqtzxxRepository.findAll(specification,pageRequest);
    }
}
