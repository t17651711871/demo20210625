package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
import com.geping.etl.UNITLOAN.repository.report.XjrjgfrProfitRepository;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrProfitService;
@Service
@Transactional
public class XjrjgfrProfitServiceImpl implements XjrjgfrProfitService{

	@Autowired
	private XjrjgfrProfitRepository xpr;
	
	@Override
	public XjrjgfrProfit findOne(String id) {
		return xpr.findOne(id);
	}

	@Override
	public List<XjrjgfrProfit> findAll() {
		return xpr.findAll();
	}

	@Override
	public List<XjrjgfrProfit> findAll(Specification<XjrjgfrProfit> spec) {
		return xpr.findAll(spec);
	}

	@Override
	public Page<XjrjgfrProfit> findAll(Specification<XjrjgfrProfit> spec, Pageable page) {
		return xpr.findAll(spec, page);
	}

	@Override
	public Integer findTotal() {
		return xpr.findTotal();
	}
	
	@Override
	public XjrjgfrProfit Save(XjrjgfrProfit XjrjgfrProfit) {
		return xpr.save(XjrjgfrProfit);
	}

	@Override
	public Integer Save(List<XjrjgfrProfit> list) {
		if(list!=null&&list.size()>0) {
			xpr.save(list);
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public XjrjgfrProfit SaveOrUpdate(XjrjgfrProfit XjrjgfrProfit) {
		return xpr.save(XjrjgfrProfit);
	}

	@Override
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus) {
		return xpr.updateXjrjgfrProfitOnCheckstatus(checkStatus);
	}

	@Override
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus, List<String> idList) {
		return xpr.updateXjrjgfrProfitOnCheckstatus(checkStatus,idList);
	}
	
	@Override
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,String departId) {
		return xpr.updateXjrjgfrProfitOnDatastatus(dataStatus,user,departId);
	}

	@Override
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,List<String> idList) {
		return xpr.updateXjrjgfrProfitOnDatastatus(dataStatus,user,idList);
	}
	
	@Override
	public Integer delete() {
		xpr.deleteAll();
		return 1;
	}

	@Override
	public Integer delete(String id) {
		xpr.delete(id);
		return 1;
	}

	@Override
	public Integer delete(List<String> list) {
		return null;
	}

}
