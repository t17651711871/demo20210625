package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xwtdkfse;
import com.geping.etl.UNITLOAN.repository.report.XwtdkfseRepository;
import com.geping.etl.UNITLOAN.service.report.XwtdkfseService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XwtdkfseServiceImpl implements XwtdkfseService {
	@Autowired
	private XwtdkfseRepository xwtdkfseRepository;

	@Override
	public Xwtdkfse save(Xwtdkfse wtdkfse) {
		return xwtdkfseRepository.save(wtdkfse);
	}

	@Override
	public List<Xwtdkfse> saveAll(List<Xwtdkfse> list) {
		return xwtdkfseRepository.save(list);
	}

	@Override
	public Page<Xwtdkfse> findAll(Specification specification, PageRequest pageRequest) {
		return xwtdkfseRepository.findAll(specification, pageRequest);
	}

}
