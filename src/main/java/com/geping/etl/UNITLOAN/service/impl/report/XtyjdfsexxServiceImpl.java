package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xtyjdfsexx;
import com.geping.etl.UNITLOAN.repository.report.XtyjdfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XtyjdfsexxService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XtyjdfsexxServiceImpl implements XtyjdfsexxService {
	@Autowired
	private XtyjdfsexxRepository xtyjdfsexxRepository;

	@Override
	public Xtyjdfsexx save(Xtyjdfsexx tyjdfsexx) {
		return xtyjdfsexxRepository.save(tyjdfsexx);
	}

	@Override
	public List<Xtyjdfsexx> saveAll(List<Xtyjdfsexx> list) {
		return xtyjdfsexxRepository.save(list);
	}

	@Override
	public Page<Xtyjdfsexx> findAll(Specification specification, PageRequest pageRequest) {
		return xtyjdfsexxRepository.findAll(specification, pageRequest);
	}

}
