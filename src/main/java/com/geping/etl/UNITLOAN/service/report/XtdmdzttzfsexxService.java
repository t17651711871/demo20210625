package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import com.geping.etl.UNITLOAN.entity.report.Xtdmdzttzfsexx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 17:36
 * @描述:
 */
public interface XtdmdzttzfsexxService {
    Page<Xtdmdzttzfsexx> findAll(Specification specification, PageRequest pageRequest);
}
