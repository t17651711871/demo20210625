package com.geping.etl.UNITLOAN.service.baseInfo;

import com.geping.etl.UNITLOAN.entity.baseInfo.DataDictionary;

import java.util.List;
import java.util.Map;

/***
 * 数据字典服务
 * @author liang.xu
 * @date 2021.4.29
 */
public interface DataDictionaryService {

    //查询所有带条件
    List<DataDictionary> findAll(Integer  type);

    /**
     * kv数据字典
     * @param type
     * @return
     */
    Map<String,String>getKv(Integer  type);
}