package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;

public interface BaseCountryService {
	
	//查询所有
	public List<BaseCountry> findAll();
	//查询所有带条件
	public List<BaseCountry> findAll(Specification<BaseCountry> spec);
	//查询所有带条件并分页
	public Page<BaseCountry> findAll(Specification<BaseCountry> spec,Pageable page);
	//导入覆盖
	public void daoRuFuGai(List<BaseCountry> list);
}
