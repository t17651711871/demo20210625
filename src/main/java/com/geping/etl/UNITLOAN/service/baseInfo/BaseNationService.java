package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:50:46  
*/
public interface BaseNationService {

	//查询所有
			public List<BaseNation> findAll();
			//查询所有带条件
			public List<BaseNation> findAll(Specification<BaseNation> spec);
			//查询所有带条件并分页
			public Page<BaseNation> findAll(Specification<BaseNation> spec,Pageable page);
			//导入覆盖
			public void daoRuFuGai(List<BaseNation> list);
}
