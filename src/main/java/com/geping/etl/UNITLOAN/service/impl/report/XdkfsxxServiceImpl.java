package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.repository.report.XdkfsxxRepository;
import com.geping.etl.UNITLOAN.service.report.XdkfsxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/28
 */
@Service
@Transactional
public class XdkfsxxServiceImpl implements XdkfsxxService {

    @Autowired
    private XdkfsxxRepository xdkfsxxRepository;

    @Override
    public Xdkfsxx save(Xdkfsxx xdkfsxx) {
        return xdkfsxxRepository.save(xdkfsxx);
    }

    @Override
    public List<Xdkfsxx> saveAll(List<Xdkfsxx> list) {
        return xdkfsxxRepository.save(list);
    }

    @Override
    public Page<Xdkfsxx> findAll(Specification specification, PageRequest pageRequest){
        return xdkfsxxRepository.findAll(specification,pageRequest);
    }
}
