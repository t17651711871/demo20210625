package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.common.report.ReportInfo;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.ReflectionUtils;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.XgetterAndSetter;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import com.geping.etl.common.service.Report_InfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class XcommonServiceImpl<T> implements XcommonService<T> {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;

    private final String subjectId="25";

    @Autowired
    private Report_InfoService riService;

    @Override
    public <T> void save(T t) {
        entityManager.merge(t);
    }

    @Override
    public void updateReportInfo(String tableName) {
        String sql = "update SYS_REPORT_INFO set check_success=(select count(ID) from "+ tableName + " where datastatus ='1'),check_fail=(select count(ID) from "+ tableName + " where datastatus ='0'),no_message_name = (select count(ID) from "+ tableName + " where datastatus ='3') where cursor_url like '%"+ tableName +"'";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void deleteDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String departId) {
        String sql = "delete from "+ className +" where datastatus = '"+ datastatus +"' and orgid = '" + orgId + "' and departid like '" + departId +"'";

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void deleteDataById(String className, String ids) {
        String sql = "delete from "+ className + " where id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public List<T> findByFielOut(Map<String, String> param, Class clazz) {
        List<T> resultList = new ArrayList<>();
        String tableName = clazz.getName().substring(clazz.getName().lastIndexOf(".")+1);
        String sql = "select * from "+tableName+" where 1 = 1";
        for (Map.Entry<String,String> entry : param.entrySet()){
            if ("id".equals(entry.getKey())){
                sql = sql + " and id in ("+entry.getValue()+")";
                continue;
            }
            sql = sql + " and "+entry.getKey() + " like '%"+entry.getValue()+"%'";
        }
        Query query = entityManager.createNativeQuery(sql,clazz);
        resultList = query.getResultList();
        return resultList;
    }

    @Override
    public void noCheckStatus(String noId, String handlename, String handledate, String tableName) {
        String sql ="update "+tableName+ " set checkstatus = '2',operator = ?1,operationtime = ?2 where id in (" + noId.substring(0,noId.length()-1)+")";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1,handlename).setParameter(2,handledate);
        query.executeUpdate();
    }

    @Override
    public void yesCheckStatus(String yesId, String handlename, String handledate, String tableName) {
        String sql ="update "+tableName+ " set checkstatus = '1',datastatus = '1',operator = ?1,operationtime = ?2 where id in (" + yesId.substring(0,yesId.length()-1)+")";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1,handlename).setParameter(2,handledate);
        query.executeUpdate();
    }

    @Override
    public <T> int batchInsert(List<T> list, String tableName, String handlename, String handletime, String orgid) {
        int result = 0;
        String sql="INSERT INTO "+tableName+"(";
        try {
            Class<?> clazz = Class.forName("com.geping.etl.UNITLOAN.entity.report."+tableName);
            Field[] fields = clazz.getDeclaredFields();
            String para = "";
            for (Field field : fields) {
                if (!"ID".equals(field.getName())){
                    sql = sql + field.getName() + ",";
                    para = para + "?,";
                }
            }
            sql = sql.substring(0,sql.length()-1);
            para = para.substring(0,para.length()-1);
            sql = sql + ") VALUES ("+para+")";
            result = jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                    int n = 0;
                    for (Field field : fields) {
                        if ("ID".equals(field.getName())){
                            continue;
                        }
                        if (n == fields.length-10){
                            break;
                        }
                        if ("java.lang.String".equals(field.getGenericType().getTypeName())){
                            preparedStatement.setString(++n, XgetterAndSetter.getter(list.get(i),"get"+field.getName()));
                        }else {
                            if (StringUtils.isNotBlank(XgetterAndSetter.getter(list.get(i),"get"+field.getName()))){
                                preparedStatement.setBigDecimal(++n, new BigDecimal(XgetterAndSetter.getter(list.get(i),"get"+field.getName())));
                            }else {
                                preparedStatement.setBigDecimal(++n, null);
                            }
                        }
                    }
                    preparedStatement.setString(++n,"0");//数据状态datastatus
                    preparedStatement.setString(++n," ");//操作名operationname
                    preparedStatement.setString(++n," ");//审核不通过原因nopassreason
                    preparedStatement.setString(++n,handlename);//操作人operator
                    preparedStatement.setString(++n,handletime);//操作时间operationtime
                    preparedStatement.setString(++n," ");//审核人Shr
                    preparedStatement.setString(++n," ");//审核时间Shsj
                    preparedStatement.setString(++n,"0");//校验checkstatus
                    preparedStatement.setString(++n,orgid);//银行机构代码orgid
                }

                @Override
                public int getBatchSize() {
                    return list.size();
                }
            }).length;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void applyDataById(String className, String ids, String handlename, String handledate, String type) {
        String sql = "update "+ className + " set datastatus = '1', operationname = '"+type+"',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void applyDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String handledate, String type, String departId) {
        String sql = "update "+ className +" set datastatus = '1', operationname = '"+type+"',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where (datastatus = '0' or datastatus = '2') and orgid = '" + orgId + "' and departid like '" + departId +"'";

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void agreeApplyDataById(String className, String ids, String handlename, String handledate) {
        String sql = "update "+ className + " set checkstatus = '0', datastatus = '0', operationname = '同意删除',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where operationname = '申请删除' and id in(" + ids + ")";
        String sql1 = "update "+ className + " set checkstatus = '0', datastatus = '0', operationname = ' ',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where operationname = '申请补足' and id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        Query query1 = entityManager.createNativeQuery(sql1);
        query.executeUpdate();
        query1.executeUpdate();
    }

    @Override
    public void agreeApplyDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji) {
        String sql = "delete from "+ className + " where operationname = '申请删除' and orgid = '" + orgId + "' and departId like '" + departId + "'";
        if (!shifoushenheziji){
            sql = sql + " and operator <> '"+ handlename+"'";
        }
        //String sql = "update "+ className + " set checkstatus = '0', datastatus = '0', operationname = '同意删除',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where operator <> '"+ handlename +"' and  operationname = '申请删除' and orgid = '" + orgId + "' and departId like '" + departId + "'";
        //String sql1 = "update "+ className + " set checkstatus = '0', datastatus = '0', operationname = ' ',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where operator <> '"+ handlename +"' and  operationname = '申请补足' and orgid = '" + orgId + "' and departId like '" + departId + "'";

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
            //sql1 = sql1 + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        //Query query1 = entityManager.createNativeQuery(sql1);
        query.executeUpdate();
        //query1.executeUpdate();
    }

    @Override
    public void noAgreeApplyDataById(String className, String ids, String handlename, String handledate) {
        String sql = "update "+ className + " set datastatus = '0', operationname = '',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where (operationname = '申请删除' or operationname = '申请补足') and id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void noAgreeApplyDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji) {
        String sql = "update "+ className + " set datastatus = '0', operationname = '',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where datastatus = '1' and (operationname = '申请删除' or operationname = '申请补足') and orgid = '" + orgId + "' and departid like '" + departId +"'";
        if (!shifoushenheziji){
            sql = sql + " and operator <> '"+ handlename+"'";
        }

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void agreeAuditDataById(String className, String ids, String handlename, String handledate) {
        String sql = "update "+ className + " set datastatus = '3', operator = '"+ handlename +"',operationtime = '"+ handledate +"' where id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void agreeAuditDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji) {
        String sql = "update "+ className + " set datastatus = '3',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where checkstatus = '1' and  (operationname = ' ' or operationname = null or operationname = '') and datastatus = '1' and orgid = '" + orgId + "' and departid like '" + departId +"'";
        if (!shifoushenheziji){
            sql = sql + " and operator <> '"+ handlename+"'";
        }

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void noAgreeAuditDataById(String className, String ids, String handlename, String handledate, String reason) {
        String sql = "update "+ className + " set operationname = '审核不通过', datastatus = '0',checkstatus = '0',nopassreason = '"+ reason +"', operator = '"+ handlename +"',operationtime = '"+ handledate +"' where id in(" + ids + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void noAgreeAuditDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String reason, String departId, boolean shifoushenheziji) {
        String sql = "update "+ className + " set operationname = '审核不通过', datastatus = '0',checkstatus = '0',nopassreason = '"+ reason +"',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where checkstatus = '1' and  (operationname = ' ' or operationname = null or operationname = '') and datastatus = '1' and orgid = '" + orgId + "' and departid like '" + departId +"'";
        if (!shifoushenheziji){
            sql = sql + " and operator <> '"+ handlename+"'";
        }

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public List<String> findByFileNamePre(String preName, String orgCode, String className) {
        String sql = "select reportname from "+ className +" where reportname like "+ preName +" and m.branchcode = "+ orgCode +" order by reportname desc ";
        Query query = entityManager.createNativeQuery(sql,String.class);
        query.executeUpdate();
        return query.getResultList();
    }

    @Override
    public List<String> findByFileNamePreWithT(String ttFilePreName, String orgCode, String className) {
        String sql = "select reportname from "+ className +" where reportnamec like '"+ ttFilePreName +"' and m.branchcode = '"+ orgCode +"' order by reportname desc ";
        Query query = entityManager.createNativeQuery(sql,String.class);
        query.executeUpdate();
        return query.getResultList();
    }

    @Override
    public void submitDataById(String className, String ids, String handlename, String handledate) {
        String sql ="update "+className+ " set datastatus = '1',operator = ?1,operationtime = ?2 where id in (" + ids+") and checkstatus = '1'";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1,handlename).setParameter(2,handledate);
        query.executeUpdate();
    }

    @Override
    public void submitDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String departId, String handledate) {
        String sql ="update "+className+ " set datastatus = '1',operator = ?1,operationtime = ?2 where orgid = ?3 and departid like ?4 and checkstatus = '1' and (datastatus = '0' or datastatus = '2')";
        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1,handlename).setParameter(2,handledate).setParameter(3,orgId).setParameter(4,departId);
        query.executeUpdate();
    }

    @Override
    public List<Map<String, String>> getHistoryInfo(String orgid, String departId,String operationtime) {
        String sql1 = "select count(1) from XcldkxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql2 = "select count(1) from XdkfsxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql3 = "select count(1) from XdkdbhtH where orgid = ?1 and departid like ?2"+operationtime;
        String sql4 = "select count(1) from XdkdbwxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql5 = "select count(1) from XjrjgfzH where orgid = ?1 and departid like ?2"+operationtime;
        String sql6 = "select count(1) from XftykhxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql7 = "select count(1) from XclgrdkxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql8 = "select count(1) from XgrdkfsxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql9 = "select count(1) from XclwtdkxxH where orgid = ?1 and departid like ?2"+operationtime;
        //String sql7 = "select count(id) from XdkdbhtH where orgid = ?1 and departid like ?2";
        String sql7_1 = "select count(1) from XjrjgfrAssetsH where orgid = ?1 and departid like ?2"+operationtime;
        String sql7_2 = "select count(1) from XjrjgfrBaseinfoH where orgid = ?1 and departid like ?2"+operationtime;
        String sql7_3 = "select count(1) from XjrjgfrProfitH where orgid = ?1 and departid like ?2"+operationtime;
        String sql10 = "select count(1) from Xgrkhxxh where orgid = ?1 and departid like ?2"+operationtime;
        String sql11 = "select count(1) from XwtdkfseH where orgid = ?1 and departid like ?2"+operationtime;
        String sql12 = "select count(1) from XcltyjdxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql13 = "select count(1) from XtyjdfsexxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql14 = "select count(1) from XtykhjcxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql15 = "select count(1) from XcltyckxxH where orgid = ?1 and departid like ?2"+operationtime;
        String sql16 = "select count(1) from XtyckfsexxH where orgid = ?1 and departid like ?2"+operationtime;
        
        Object result1 = entityManager.createQuery(sql1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result2 = entityManager.createQuery(sql2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result3 = entityManager.createQuery(sql3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result4 = entityManager.createQuery(sql4).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result5 = entityManager.createQuery(sql5).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result6 = entityManager.createQuery(sql6).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result7 = entityManager.createQuery(sql7).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result8 = entityManager.createQuery(sql8).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result9 = entityManager.createQuery(sql9).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result7_1 = entityManager.createQuery(sql7_1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result7_2 = entityManager.createQuery(sql7_2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result7_3 = entityManager.createQuery(sql7_3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result10 = entityManager.createQuery(sql10).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result11 = entityManager.createQuery(sql11).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result12 = entityManager.createQuery(sql12).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result13 = entityManager.createQuery(sql13).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result14 = entityManager.createQuery(sql14).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result15 = entityManager.createQuery(sql15).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        Object result16 = entityManager.createQuery(sql16).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
        
        List<Map<String,String>> list = new ArrayList<>();
        Map<String,String> map1 = new HashMap<>();
        map1.put("messagename","存量单位贷款基础数据信息");
        map1.put("count",result1.toString());
        list.add(map1);
        Map<String,String> map2 = new HashMap<>();
        map2.put("messagename","单位贷款发生额信息");
        map2.put("count",result2.toString());
        list.add(map2);
        Map<String,String> map3 = new HashMap<>();
        map3.put("messagename","单位贷款担保合同信息");
        map3.put("count",result3.toString());
        list.add(map3);
        Map<String,String> map4 = new HashMap<>();
        map4.put("messagename","单位贷款担保物信息");
        map4.put("count",result4.toString());
        list.add(map4);
        Map<String,String> map5 = new HashMap<>();
        map5.put("messagename","金融机构（分支机构）基础信息");
        map5.put("count",result5.toString());
        list.add(map5);
        Map<String,String> map6 = new HashMap<>();
        map6.put("messagename","非同业单位客户基础信息");
        map6.put("count",result6.toString());
        list.add(map6);
        Map<String,String> map7 = new HashMap<>();
        map7.put("messagename","金融机构（法人）基础信息");
        map7.put("count",Stringutil.panDuanShuZhiWeiLing(result7_1.toString(),result7_2.toString(),result7_3.toString()));
        list.add(map7);
        Map<String,String> map8 = new HashMap<>();
        map8.put("messagename","存量个人贷款基础数据信息");
        map8.put("count",result7.toString());
        list.add(map8);
        Map<String,String> map9 = new HashMap<>();
        map9.put("messagename","个人贷款发生额信息");
        map9.put("count",result8.toString());
        list.add(map9);
        Map<String,String> map10 = new HashMap<>();
        map10.put("messagename","个人客户基础信息");
        map10.put("count",result10.toString());
        list.add(map10);
        Map<String,String> map11 = new HashMap<>();
        map11.put("messagename","存量委托贷款基础数据信息");
        map11.put("count",result9.toString());
        list.add(map11);
        Map<String,String> map12 = new HashMap<>();
        map12.put("messagename","委托贷款发生额信息");
        map12.put("count",result11.toString());
        list.add(map12);
        Map<String,String> map13 = new HashMap<>();
        map13.put("messagename","存量同业借贷信息");
        map13.put("count",result12.toString());
        list.add(map13);
        Map<String,String> map14 = new HashMap<>();
        map14.put("messagename","同业借贷发生额信息");
        map14.put("count",result13.toString());
        list.add(map14);
        Map<String,String> map15 = new HashMap<>();
        map15.put("messagename","同业客户基础信息");
        map15.put("count",result14.toString());
        list.add(map15);
        Map<String,String> map16 = new HashMap<>();
        map16.put("messagename","存量同业存款信息");
        map16.put("count",result15.toString());
        list.add(map16);
        Map<String,String> map17 = new HashMap<>();
        map17.put("messagename","同业存款发生额信息");
        map17.put("count",result16.toString());
        list.add(map17);
        return list;
    }

    @Override
    public List<Report_Info> getReportHistoryInfo(String orgid, String departId, String operationtime) {
        List<Report_Info> reports = riService.getReportBySubjectId(subjectId);
        StringBuilder sqldtj = new StringBuilder("");
        int flag=0;
        Report_Info jrjgfrReport=null;
        for(int i=0;i<reports.size()-1;i++) {
            if(ReportInfo.getReport(reports.get(i).getCode())==null){
                flag=i;
                jrjgfrReport=reports.get(i);
                continue;
            }
            sqldtj.append("select count(1) from "+ ReflectionUtils.getTableName(ReportInfo.getReport(reports.get(i).getCode()))+"H" +" where orgid = '"+orgid+"' and departid like '"+departId+"'  union all  ");
        }
        String code=reports.get(reports.size()-1).getCode();
        sqldtj.append(" select count(1) from "+ReflectionUtils.getTableName(ReportInfo.getReport(code))+"H"+" where orgid = '"+orgid+"' and departid like '"+departId+"' ");
        List<Object> resultdtj = entityManager.createNativeQuery(sqldtj.toString()).getResultList();
        resultdtj.add(flag,0);
        for(int i=0;i<reports.size();i++) {
            reports.get(i).setNoMessageName(String.valueOf(resultdtj.get(i)));
        }
        //处理机构机构法人报表
        String sqldtj7_1 = "select count(1) from XjrjgfrAssetsH where orgid = '"+orgid+"' and departid like '"+departId+"' ";
        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfoH where orgid = '"+orgid+"' and departid like '"+departId+"'";
        String sqldtj7_3 = "select count(1) from XjrjgfrProfitH where orgid = '"+orgid+"' and departid like '"+departId+"'";

        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).getSingleResult();
        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).getSingleResult();
        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).getSingleResult();
        jrjgfrReport.setNoMessageName(Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
        return reports;
    }


//    @Override
//    public Map<String, String> getDtjDshCount(String orgid, String departId) {
//        String sqldtj1 = "select count(1) from Xcldkxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj2 = "select count(1) from Xdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj3 = "select count(1) from Xdkdbht where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj4 = "select count(1) from Xdkdbwx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj5 = "select count(1) from Xjrjgfz where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj6 = "select count(1) from Xftykhx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj7 = "select count(1) from Xclgrdkxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj8 = "select count(1) from Xgrdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj9 = "select count(1) from Xclwtdkxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj7_1 = "select count(1) from XjrjgfrAssets where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj7_3 = "select count(1) from XjrjgfrProfit where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj10 = "select count(1) from Xgrkhxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj11 = "select count(1) from Xwtdkfse where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj12 = "select count(1) from Xcltyjdxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj13 = "select count(1) from Xtyjdfsexx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj14 = "select count(1) from Xtykhjcxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj15 = "select count(1) from Xcltyckxx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        String sqldtj16 = "select count(1) from Xtyckfsexx where orgid = ?1 and departid like ?2 and datastatus = '0'";
//        
//        String sqldsh1 = "select count(1) from Xcldkxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh2 = "select count(1) from Xdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh3 = "select count(1) from Xdkdbht where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh4 = "select count(1) from Xdkdbwx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh5 = "select count(1) from Xjrjgfz where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh6 = "select count(1) from Xftykhx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh7 = "select count(1) from Xclgrdkxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh8 = "select count(1) from Xgrdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh9 = "select count(1) from Xclwtdkxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh7_1 = "select count(1) from XjrjgfrAssets where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh7_3 = "select count(1) from XjrjgfrProfit where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh10 = "select count(1) from Xgrkhxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh11 = "select count(1) from Xwtdkfse where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh12 = "select count(1) from Xcltyjdxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh13 = "select count(1) from Xtyjdfsexx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh14 = "select count(1) from Xtykhjcxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh15 = "select count(1) from Xcltyckxx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        String sqldsh16 = "select count(1) from Xtyckfsexx where orgid = ?1 and departid like ?2 and datastatus = '1'";
//        
//        Object resultdtj1 = entityManager.createQuery(sqldtj1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj2 = entityManager.createQuery(sqldtj2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj3 = entityManager.createQuery(sqldtj3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj4 = entityManager.createQuery(sqldtj4).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj5 = entityManager.createQuery(sqldtj5).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj6 = entityManager.createQuery(sqldtj6).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj7 = entityManager.createQuery(sqldtj7).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj8 = entityManager.createQuery(sqldtj8).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj9 = entityManager.createQuery(sqldtj9).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj10 = entityManager.createQuery(sqldtj10).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj11 = entityManager.createQuery(sqldtj11).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj12 = entityManager.createQuery(sqldtj12).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj13 = entityManager.createQuery(sqldtj13).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj14 = entityManager.createQuery(sqldtj14).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj15 = entityManager.createQuery(sqldtj15).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdtj16 = entityManager.createQuery(sqldtj16).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        
//        Object resultdsh1 = entityManager.createQuery(sqldsh1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh2 = entityManager.createQuery(sqldsh2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh3 = entityManager.createQuery(sqldsh3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh4 = entityManager.createQuery(sqldsh4).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh5 = entityManager.createQuery(sqldsh5).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh6 = entityManager.createQuery(sqldsh6).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh7 = entityManager.createQuery(sqldsh7).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh8 = entityManager.createQuery(sqldsh8).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh9 = entityManager.createQuery(sqldsh9).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh7_1 = entityManager.createQuery(sqldsh7_1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh7_2 = entityManager.createQuery(sqldsh7_2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh7_3 = entityManager.createQuery(sqldsh7_3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh10 = entityManager.createQuery(sqldsh10).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh11 = entityManager.createQuery(sqldsh11).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh12 = entityManager.createQuery(sqldsh12).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh13 = entityManager.createQuery(sqldsh13).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh14 = entityManager.createQuery(sqldsh14).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh15 = entityManager.createQuery(sqldsh15).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object resultdsh16 = entityManager.createQuery(sqldsh16).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        
//        Map<String,String> map = new HashMap<>();
//        map.put("Xcldkxxdtj",resultdtj1.toString());
//        map.put("Xdkfsxxdtj",resultdtj2.toString());
//        map.put("Xdkdbhtdtj",resultdtj3.toString());
//        map.put("Xdkdbwxdtj",resultdtj4.toString());
//        map.put("Xjrjgfzdtj",resultdtj5.toString());
//        map.put("Xftykhxdtj",resultdtj6.toString());
//        map.put("Xclgrdkxxdtj",resultdtj7.toString());
//        map.put("Xgrdkfsxxdtj",resultdtj8.toString());
//        map.put("Xclwtdkxxdtj",resultdtj9.toString());
//        map.put("Xjrjgfrdtj",Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
//        map.put("Xgrkhxxdtj",resultdtj10.toString());
//        map.put("Xwtdkfsedtj",resultdtj11.toString());
//        map.put("Xcltyjdxxdtj",resultdtj12.toString());
//        map.put("Xtyjdfsexxdtj",resultdtj13.toString());
//        map.put("Xtykhjcxxdtj",resultdtj14.toString());
//        map.put("Xcltyckxxdtj",resultdtj15.toString());
//        map.put("Xtyckfsexxdtj",resultdtj16.toString());
//        
//        map.put("Xcldkxxdsh",resultdsh1.toString());
//        map.put("Xdkfsxxdsh",resultdsh2.toString());
//        map.put("Xdkdbhtdsh",resultdsh3.toString());
//        map.put("Xdkdbwxdsh",resultdsh4.toString());
//        map.put("Xjrjgfzdsh",resultdsh5.toString());
//        map.put("Xftykhxdsh",resultdsh6.toString());
//        map.put("Xclgrdkxxdsh",resultdsh7.toString());
//        map.put("Xgrdkfsxxdsh",resultdsh8.toString());
//        map.put("Xclwtdkxxdsh",resultdsh9.toString());
//        map.put("Xjrjgfrdsh",Stringutil.panDuanShuZhiWeiLing(resultdsh7_1.toString(),resultdsh7_2.toString(),resultdsh7_3.toString()));
//        map.put("Xgrkhxxdsh",resultdsh10.toString());
//        map.put("Xwtdkfsedsh",resultdsh11.toString());
//        map.put("Xcltyjdxxdsh",resultdsh12.toString());
//        map.put("Xtyjdfsexxdsh",resultdsh13.toString());
//        map.put("Xtykhjcxxdsh",resultdsh14.toString());
//        map.put("Xcltyckxxdsh",resultdsh15.toString());
//        map.put("Xtyckfsexxdsh",resultdsh16.toString());
//        return map;
//    }
    
    @Override
    public Map<String, String> getDtjDshCount(String orgid, String departId) {
    	StringBuilder sqldtj = new StringBuilder("");
    	sqldtj.append("select count(1) from Xcldkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xdkdbht where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xdkdbwx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xjrjgfz where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xftykhx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xclgrdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xgrdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
    	sqldtj.append("select count(1) from Xclwtdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        String sqldtj7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";
        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";
        String sqldtj7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";
        sqldtj.append("select count(1) from Xgrkhxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xwtdkfse where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xcltyjdxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xtyjdfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xtykhjcxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xcltyckxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xtyckfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");

        sqldtj.append("select count(1) from Xclzqtzxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xzqtzfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xclzqfxxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all ");
        sqldtj.append("select count(1) from Xzqfxfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'");

        
        StringBuilder sqldsh = new StringBuilder("");
        sqldsh.append("select count(1) from Xcldkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xdkdbht where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xdkdbwx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xjrjgfz where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xftykhx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xclgrdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xgrdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xclwtdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        String sqldsh7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";
        String sqldsh7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";
        String sqldsh7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";
        sqldsh.append("select count(1) from Xgrkhxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xwtdkfse where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xcltyjdxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xtyjdfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xtykhjcxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xcltyckxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xtyckfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");

        sqldsh.append("select count(1) from Xclzqtzxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xzqtzfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xclzqfxxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all ");
        sqldsh.append("select count(1) from Xzqfxfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'");

        List<Object> resultdtj = entityManager.createNativeQuery(sqldtj.toString()).getResultList();
        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).getSingleResult();
        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).getSingleResult();
        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).getSingleResult();
        
        List<Object> resultdsh = entityManager.createNativeQuery(sqldsh.toString()).getResultList();
        Object resultdsh7_1 = entityManager.createQuery(sqldsh7_1).getSingleResult();
        Object resultdsh7_2 = entityManager.createQuery(sqldsh7_2).getSingleResult();
        Object resultdsh7_3 = entityManager.createQuery(sqldsh7_3).getSingleResult();
        
        Map<String,String> map = new HashMap<>();
        map.put("Xcldkxxdtj",resultdtj.get(0).toString());
        map.put("Xdkfsxxdtj",resultdtj.get(1).toString());
        map.put("Xdkdbhtdtj",resultdtj.get(2).toString());
        map.put("Xdkdbwxdtj",resultdtj.get(3).toString());
        map.put("Xjrjgfzdtj",resultdtj.get(4).toString());
        map.put("Xftykhxdtj",resultdtj.get(5).toString());
        map.put("Xclgrdkxxdtj",resultdtj.get(6).toString());
        map.put("Xgrdkfsxxdtj",resultdtj.get(7).toString());
        map.put("Xclwtdkxxdtj",resultdtj.get(8).toString());
        map.put("Xjrjgfrdtj",Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
        map.put("Xgrkhxxdtj",resultdtj.get(9).toString());
        map.put("Xwtdkfsedtj",resultdtj.get(10).toString());
        map.put("Xcltyjdxxdtj",resultdtj.get(11).toString());
        map.put("Xtyjdfsexxdtj",resultdtj.get(12).toString());
        map.put("Xtykhjcxxdtj",resultdtj.get(13).toString());
        map.put("Xcltyckxxdtj",resultdtj.get(14).toString());
        map.put("Xtyckfsexxdtj",resultdtj.get(15).toString());
        map.put("Xclzqtzxxdtj",resultdtj.get(16).toString());
        map.put("Xzqtzfsexxdtj",resultdtj.get(17).toString());
        map.put("Xclzqfxxxdtj",resultdtj.get(18).toString());
        map.put("Xzqfxfsexxdtj",resultdtj.get(19).toString());
        
        map.put("Xcldkxxdsh",resultdsh.get(0).toString());
        map.put("Xdkfsxxdsh",resultdsh.get(1).toString());
        map.put("Xdkdbhtdsh",resultdsh.get(2).toString());
        map.put("Xdkdbwxdsh",resultdsh.get(3).toString());
        map.put("Xjrjgfzdsh",resultdsh.get(4).toString());
        map.put("Xftykhxdsh",resultdsh.get(5).toString());
        map.put("Xclgrdkxxdsh",resultdsh.get(6).toString());
        map.put("Xgrdkfsxxdsh",resultdsh.get(7).toString());
        map.put("Xclwtdkxxdsh",resultdsh.get(8).toString());
        map.put("Xjrjgfrdsh",Stringutil.panDuanShuZhiWeiLing(resultdsh7_1.toString(),resultdsh7_2.toString(),resultdsh7_3.toString()));
        map.put("Xgrkhxxdsh",resultdsh.get(9).toString());
        map.put("Xwtdkfsedsh",resultdsh.get(10).toString());
        map.put("Xcltyjdxxdsh",resultdsh.get(11).toString());
        map.put("Xtyjdfsexxdsh",resultdsh.get(12).toString());
        map.put("Xtykhjcxxdsh",resultdsh.get(13).toString());
        map.put("Xcltyckxxdsh",resultdsh.get(14).toString());
        map.put("Xtyckfsexxdsh",resultdsh.get(15).toString());
        map.put("Xclzqtzxxdsh",resultdsh.get(16).toString());
        map.put("Xzqtzfsexxdsh",resultdsh.get(17).toString());
        map.put("Xclzqfxxxdsh",resultdsh.get(18).toString());
        map.put("Xzqfxfsexxdsh",resultdsh.get(19).toString());
        return map;
    }

//    @Override
//    public Map<String, String> getdscbwCount(String orgid, String departId) {
//        String sql1 = "select count(1) from Xcldkxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql2 = "select count(1) from Xdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql3 = "select count(1) from Xdkdbht where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql4 = "select count(1) from Xdkdbwx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql5 = "select count(1) from Xjrjgfz where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql6 = "select count(1) from Xftykhx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql7 = "select count(1) from Xclgrdkxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql8 = "select count(1) from Xgrdkfsxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql9 = "select count(1) from Xclwtdkxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql7_1 = "select count(1) from XjrjgfrAssets where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql7_3 = "select count(1) from XjrjgfrProfit where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql10 = "select count(1) from Xgrkhxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql11 = "select count(1) from Xwtdkfse where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql12 = "select count(1) from Xcltyjdxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql13 = "select count(1) from Xtyjdfsexx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql14 = "select count(1) from Xtykhjcxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql15 = "select count(1) from Xcltyckxx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        String sql16 = "select count(1) from Xtyckfsexx where orgid = ?1 and departid like ?2 and datastatus = '3'";
//        
//        Object result1 = entityManager.createQuery(sql1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result2 = entityManager.createQuery(sql2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result3 = entityManager.createQuery(sql3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result4 = entityManager.createQuery(sql4).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result5 = entityManager.createQuery(sql5).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result6 = entityManager.createQuery(sql6).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result7 = entityManager.createQuery(sql7).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result8 = entityManager.createQuery(sql8).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result9 = entityManager.createQuery(sql9).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result7_1 = entityManager.createQuery(sql7_1).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result7_2 = entityManager.createQuery(sql7_2).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result7_3 = entityManager.createQuery(sql7_3).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result10 = entityManager.createQuery(sql10).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result11 = entityManager.createQuery(sql11).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result12 = entityManager.createQuery(sql12).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result13 = entityManager.createQuery(sql13).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result14 = entityManager.createQuery(sql14).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result15 = entityManager.createQuery(sql15).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        Object result16 = entityManager.createQuery(sql16).setParameter(1,orgid).setParameter(2,departId).getSingleResult();
//        
//        Map<String,String> map = new HashMap<>();
//        map.put("Xcldkxx",result1.toString());
//        map.put("Xdkfsxx",result2.toString());
//        map.put("Xdkdbht",result3.toString());
//        map.put("Xdkdbwx",result4.toString());
//        map.put("Xjrjgfz",result5.toString());
//        map.put("Xftykhx",result6.toString());
//        map.put("Xclgrdkxx",result7.toString());
//        map.put("Xgrdkfsxx",result8.toString());
//        map.put("Xclwtdkxx",result9.toString());
//        map.put("Xjrjgfr",Stringutil.panDuanShuZhiWeiLing(result7_1.toString(),result7_2.toString(),result7_3.toString()));
//        map.put("Xgrkhxx",result10.toString());
//        map.put("Xwtdkfse",result11.toString());
//        map.put("Xcltyjdxx",result12.toString());
//        map.put("Xtyjdfsexx",result13.toString());
//        map.put("Xtykhjcxx",result14.toString());
//        map.put("Xcltyckxx",result15.toString());
//        map.put("Xtyckfsexx",result16.toString());
//        return map;
//    }
    
    @Override
    public Map<String, String> getdscbwCount(String orgid, String departId) {
    	StringBuilder sql = new StringBuilder("");
    	sql.append("select count(1) from Xcldkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xdkdbht where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xdkdbwx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xjrjgfz where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xftykhx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xclgrdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xgrdkfsxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
    	sql.append("select count(1) from Xclwtdkxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        String sql7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";
        String sql7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";
        String sql7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";
        sql.append("select count(1) from Xgrkhxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xwtdkfse where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xcltyjdxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xtyjdfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xtykhjcxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xcltyckxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xtyckfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");

        sql.append("select count(1) from Xclzqtzxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xzqtzfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xclzqfxxx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all ");
        sql.append("select count(1) from Xzqfxfsexx where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'");
        List<Object> result = entityManager.createNativeQuery(sql.toString()).getResultList();
        Object result7_1 = entityManager.createQuery(sql7_1).getSingleResult();
        Object result7_2 = entityManager.createQuery(sql7_2).getSingleResult();
        Object result7_3 = entityManager.createQuery(sql7_3).getSingleResult();
        
        Map<String,String> map = new HashMap<>();
        map.put("Xcldkxx",result.get(0).toString());
        map.put("Xdkfsxx",result.get(1).toString());
        map.put("Xdkdbht",result.get(2).toString());
        map.put("Xdkdbwx",result.get(3).toString());
        map.put("Xjrjgfz",result.get(4).toString());
        map.put("Xftykhx",result.get(5).toString());
        map.put("Xclgrdkxx",result.get(6).toString());
        map.put("Xgrdkfsxx",result.get(7).toString());
        map.put("Xclwtdkxx",result.get(8).toString());
        map.put("Xjrjgfr",Stringutil.panDuanShuZhiWeiLing(result7_1.toString(),result7_2.toString(),result7_3.toString()));
        map.put("Xgrkhxx",result.get(9).toString());
        map.put("Xwtdkfse",result.get(10).toString());
        map.put("Xcltyjdxx",result.get(11).toString());
        map.put("Xtyjdfsexx",result.get(12).toString());
        map.put("Xtykhjcxx",result.get(13).toString());
        map.put("Xcltyckxx",result.get(14).toString());
        map.put("Xtyckfsexx",result.get(15).toString());
        map.put("Xclzqtzxx",result.get(16).toString());
        map.put("Xzqtzfsexx",result.get(17).toString());
        map.put("Xclzqfxxx",result.get(18).toString());
        map.put("Xzqfxfsexx",result.get(19).toString());
        return map;
    }

    @Override
    public void moveToHistoryTable(String tableName, String orgid, String departId) {
    	String sql0 = "update " + tableName + " set operationtime='"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))+"' where orgid = ?1 and departid like ?2 and datastatus = '3'";;
    	entityManager.createNativeQuery(sql0).setParameter(1,orgid).setParameter(2,departId).executeUpdate();
        String sql = "insert into "+ tableName +"H select * from "+tableName+ " where orgid = ?1 and departid like ?2 and datastatus = '3'";

        entityManager.createNativeQuery(sql).setParameter(1,orgid).setParameter(2,departId).executeUpdate();
        String sql1 = "delete from "+ tableName +" where orgid = ?1 and departid like ?2 and datastatus = '3'";
        entityManager.createNativeQuery(sql1).setParameter(1,orgid).setParameter(2,departId).executeUpdate();
    }
    
    @Override
    public void moveToHistoryTable(Sys_UserAndOrgDepartment user,String... tables) {
    	if(tables.length > 0) {
    		for(int i=0;i<tables.length;i++) {
    			String sql0 = "update " + tables[i] + " set operationtime='"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))+"' where orgid = ?1 and departid like ?2 and datastatus = '3'";;
    	    	entityManager.createNativeQuery(sql0).setParameter(1,user.getOrgid()).setParameter(2,user.getDepartid()).executeUpdate();
    			String sql = "insert into "+ tables[i] +"H select * from "+tables[i]+ " where orgid = ?1 and departid like ?2 and datastatus = '3'";
    	        entityManager.createNativeQuery(sql).setParameter(1,user.getOrgid()).setParameter(2,user.getDepartid()).executeUpdate();
    	        String sql1 = "delete from "+ tables[i] +" where orgid = ?1 and departid like ?2 and datastatus = '3'";
    	        entityManager.createNativeQuery(sql1).setParameter(1,user.getOrgid()).setParameter(2,user.getDepartid()).executeUpdate();
    		}
    	}
    }

    @Override
    public void gobackDataById(String className, String id, String handlename, String handledate) {
        String sql = "update "+ className + " set datastatus = '0',checkstatus = '0',operationname = '', operator = '"+ handlename +"',operationtime = '"+ handledate +"' where id in(" + id + ")";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void ngobackDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId) {
        String sql = "update "+ className + " set datastatus = '0',checkstatus = '0',operationname = '',operator = '"+ handlename +"',operationtime = '"+ handledate +"' where datastatus = '3' and orgid = '" + orgId + "' and departid like '" + departId +"'";

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void deleteRepeatData(String className, String field) {
        String sql = "delete from "+ className +" where id in " +
                "(select id from " +
                "(select id from "+className+" where "+field+" in " +
                "(select "+field+" from "+className+" group by "+field+" having count("+field+")>1) and operationtime not in " +
                "(select max(operationtime) from "+className+" group by "+field+" having count("+field+")>1)) t)";
    }

    @Override
    public void synDataById(String className, String id) {
        String sql = "delete from xftykhx";
        String sql1 = "insert into xftykhx (id,finorgcode,customername,customercode,depositnum,countopenbankname,regamt,regamtcreny,actamt,actamtcreny,totalamt,netamt,islistcmpy,fistdate,jobpeoplenum,regarea,regareacode,workarea,workareacode,mngmestus,setupdate,industry,entpmode,entpczjjcf,creditamt,usedamt,isrelation,actctrltype,actctrlidtype,actctrlidcode,'0','0',operator,operationtime,orgid,departid) SELECT id,finorgcode,customername,customercode,depositnum,countopenbankname,regamt,regamtcreny,actamt,actamtcreny,totalamt,netamt,islistcmpy,fistdate,jobpeoplenum,regarea,regareacode,workarea,workareacode,mngmestus,setupdate,industry,entpmode,entpczjjcf,creditamt,usedamt,isrelation,actctrltype,actctrlidtype,actctrlidcode,'0','0',operator,operationtime,orgid,departid FROM xftykhxbl";
    }

    @Override
    public void synDataAll(String className, Map<String, String> param, String handlename, String orgId, String departId) {
        String sql = "delete from xftykhx";
        StringBuffer fields = new StringBuffer("");
        StringBuffer values = new StringBuffer("");
        param.forEach((field,value)->{
        	fields.append(","+field);
        	values.append(",'"+value+"'");
        });
        String sql1 = "insert into xftykhx (id,finorgcode,customername,customercode,depositnum,countopenbankname,regamt,regamtcreny,actamt,actamtcreny,totalamt,netamt,islistcmpy,fistdate,jobpeoplenum,regarea,regareacode,workarea,workareacode,mngmestus,setupdate,industry,entpmode,entpczjjcf,creditamt,usedamt,isrelation,actctrltype,actctrlidtype,actctrlidcode"+fields.toString()+",checkstatus,datastatus,operator,operationname,operationtime,orgid,departid,nopassreason) SELECT id,finorgcode,customername,customercode,depositnum,countopenbankname,regamt,regamtcreny,actamt,actamtcreny,totalamt,netamt,islistcmpy,fistdate,jobpeoplenum,regarea,regareacode,workarea,workareacode,mngmestus,setupdate,industry,entpmode,entpczjjcf,creditamt,usedamt,isrelation,actctrltype,actctrlidtype,actctrlidcode"+values.toString()+",'0','0',operator,' ',operationtime,orgid,departid,' ' FROM xftykhxbl";
        //存量单位贷款基础数据表同步数据
        String sql2 = null;
        if(xDataBaseTypeUtil.equalsMySql()) {
        	sql2 = "update xcldkxx t1,xftykhxbl t2 set t1.financeorgcode = t2.customerfinorgcode,t1.financeorginnum = t2.customerfinorginside,t1.financeorgareacode = t2.customerfinorgarea,t1.brroweridnum = t2.customercode,t1.brrowerindustry = t2.industry,t1.brrowerareacode = t2.regareacode,t1.inverstoreconomy = t2.entpczjjcf,t1.enterprisescale = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny where t1.customernum = t2.customernum";
        }else if(xDataBaseTypeUtil.equalsOracle()){        
        	sql2 = "update xcldkxx t1 set (t1.financeorgcode,t1.financeorginnum,t1.financeorgareacode,t1.brroweridnum,t1.brrowerindustry,t1.brrowerareacode,t1.inverstoreconomy,t1.enterprisescale,t1.isfarmerloan,t1.isgreenloan) = (select t2.customerfinorgcode,t2.customerfinorginside,t2.customerfinorgarea,t2.customercode,t2.industry,t2.regareacode,t2.entpczjjcf,t2.entpmode,t2.regamtcreny,t2.actamtcreny from xftykhxbl t2 where t1.customernum = t2.customernum) where exists(select 1 from xftykhxbl t2 where t1.customernum = t2.customernum)";
        }else if(xDataBaseTypeUtil.equalsSqlServer()) {
        	sql2 = "update t1 set t1.financeorgcode = t2.customerfinorgcode,t1.financeorginnum = t2.customerfinorginside,t1.financeorgareacode = t2.customerfinorgarea,t1.brroweridnum = t2.customercode,t1.brrowerindustry = t2.industry,t1.brrowerareacode = t2.regareacode,t1.inverstoreconomy = t2.entpczjjcf,t1.enterprisescale = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny from xcldkxx t1,xftykhxbl t2 where t1.customernum = t2.customernum";
        }
        //单位贷款发生额同步数据
        String sql3 = null;
        if(xDataBaseTypeUtil.equalsMySql()) {
        	sql3 = "update xdkfsxx t1,xftykhxbl t2 set t1.finorgcode = t2.customerfinorgcode,t1.finorgincode = t2.customerfinorginside,t1.finorgareacode = t2.customerfinorgarea,t1.browidcode = t2.customercode,t1.browinds = t2.industry,t1.browareacode = t2.regareacode,t1.entpczjjcf = t2.entpczjjcf,t1.entpmode = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny where t1.customernum = t2.customernum";
        }else if(xDataBaseTypeUtil.equalsOracle()){        
        	sql3 = "update xdkfsxx t1 set (t1.finorgcode,t1.finorgincode,t1.finorgareacode,t1.browidcode,t1.browinds,t1.browareacode,t1.entpczjjcf,t1.entpmode,t1.isfarmerloan,t1.isgreenloan) = (select t2.customerfinorgcode,t2.customerfinorginside,t2.customerfinorgarea,t2.customercode,t2.industry,t2.regareacode,t2.entpczjjcf,t2.entpmode,t2.regamtcreny,t2.actamtcreny from xftykhxbl t2 where t1.customernum = t2.customernum) where exists(select 1 from xftykhxbl t2 where t1.customernum = t2.customernum)";
        }else if(xDataBaseTypeUtil.equalsSqlServer()) {
        	sql3 = "update t1 set t1.finorgcode = t2.customerfinorgcode,t1.finorgincode = t2.customerfinorginside,t1.finorgareacode = t2.customerfinorgarea,t1.browidcode = t2.customercode,t1.browinds = t2.industry,t1.browareacode = t2.regareacode,t1.entpczjjcf = t2.entpczjjcf,t1.entpmode = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny from xdkfsxx t1,xftykhxbl t2 where t1.customernum = t2.customernum";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
        Query query1 = entityManager.createNativeQuery(sql1);
        query1.executeUpdate();
        Query query2 = entityManager.createNativeQuery(sql2);
        query2.executeUpdate();
        Query query3 = entityManager.createNativeQuery(sql3);
        query3.executeUpdate();
    }

    @Override
    public void updateXcldkxxByCustomernum(String className) {
    	String sql = null;
    	if(xDataBaseTypeUtil.equalsMySql()) {
    		sql = "update xcldkxx t1,xftykhxbl t2 set t1.financeorgcode = t2.customerfinorgcode,t1.financeorginnum = t2.customerfinorginside,t1.financeorgareacode = t2.customerfinorgarea,t1.brroweridnum = t2.customercode,t1.brrowerindustry = t2.industry,t1.brrowerareacode = t2.regareacode,t1.inverstoreconomy = t2.entpczjjcf,t1.enterprisescale = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny where t1.customernum = t2.customernum";
        }else if(xDataBaseTypeUtil.equalsOracle()){        
        	sql = "update xcldkxx t1 set (t1.financeorgcode,t1.financeorginnum,t1.financeorgareacode,t1.brroweridnum,t1.brrowerindustry,t1.brrowerareacode,t1.inverstoreconomy,t1.enterprisescale,t1.isfarmerloan,t1.isgreenloan) = (select t2.customerfinorgcode,t2.customerfinorginside,t2.customerfinorgarea,t2.customercode,t2.industry,t2.regareacode,t2.entpczjjcf,t2.entpmode,t2.regamtcreny,t2.actamtcreny from xftykhxbl t2 where t1.customernum = t2.customernum) where exists(select 1 from xftykhxbl t2 where t1.customernum = t2.customernum)";
        }else if(xDataBaseTypeUtil.equalsSqlServer()) {
    		sql = "update t1 set t1.financeorgcode = t2.customerfinorgcode,t1.financeorginnum = t2.customerfinorginside,t1.financeorgareacode = t2.customerfinorgarea,t1.brroweridnum = t2.customercode,t1.brrowerindustry = t2.industry,t1.brrowerareacode = t2.regareacode,t1.inverstoreconomy = t2.entpczjjcf,t1.enterprisescale = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny from xcldkxx t1,xftykhxbl t2 where t1.customernum = t2.customernum";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }
    
    @Override
    public void updateXdkfsxxByCustomernum(String className) {
    	String sql = null;
        if(xDataBaseTypeUtil.equalsMySql()) {
        	sql = "update xdkfsxx t1,xftykhxbl t2 set t1.finorgcode = t2.customerfinorgcode,t1.finorgincode = t2.customerfinorginside,t1.finorgareacode = t2.customerfinorgarea,t1.browidcode = t2.customercode,t1.browinds = t2.industry,t1.browareacode = t2.regareacode,t1.entpczjjcf = t2.entpczjjcf,t1.entpmode = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny where t1.customernum = t2.customernum";
        }else if(xDataBaseTypeUtil.equalsOracle()){        
        	sql = "update xdkfsxx t1 set (t1.finorgcode,t1.finorgincode,t1.finorgareacode,t1.browidcode,t1.browinds,t1.browareacode,t1.entpczjjcf,t1.entpmode,t1.isfarmerloan,t1.isgreenloan) = (select t2.customerfinorgcode,t2.customerfinorginside,t2.customerfinorgarea,t2.customercode,t2.industry,t2.regareacode,t2.entpczjjcf,t2.entpmode,t2.regamtcreny,t2.actamtcreny from xftykhxbl t2 where t1.customernum = t2.customernum) where exists(select 1 from xftykhxbl t2 where t1.customernum = t2.customernum)";
        }else if(xDataBaseTypeUtil.equalsSqlServer()) {
        	sql = "update t1 set t1.finorgcode = t2.customerfinorgcode,t1.finorgincode = t2.customerfinorginside,t1.finorgareacode = t2.customerfinorgarea,t1.browidcode = t2.customercode,t1.browinds = t2.industry,t1.browareacode = t2.regareacode,t1.entpczjjcf = t2.entpczjjcf,t1.entpmode = t2.entpmode,t1.isfarmerloan = t2.regamtcreny,t1.isgreenloan = t2.actamtcreny from xdkfsxx t1,xftykhxbl t2 where t1.customernum = t2.customernum";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public void deleteDataAllWhithOutDatastatus(String className, Map<String, String> param, String handlename, String orgId, String departId) {
        String sql = "delete from "+ className +" where orgid = '" + orgId + "' and departid like '" + departId +"'";

        for (Map.Entry<String,String> entry : param.entrySet()){
            sql = sql + " and " + entry.getKey() + " like '%" + entry.getValue() + "%'";
        }
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }
    
    @Override
    public int moveToTemporaryTable(String tableName,String handlename,String handledate,String date, String orgid, String departId) {
    	int result = 0;
    	String sql0 = "update " + tableName + " set checkstatus = '0',datastatus = '0',operator = '"+handlename+"',operationname=' ',operationtime='"+handledate+"',nopassreason = ' ' where orgid = ?1 and departid like ?2 and sjrq = ?3";;
    	result += entityManager.createNativeQuery(sql0).setParameter(1,orgid).setParameter(2,departId).setParameter(3,date).executeUpdate();
    	
    	String sql = "insert into "+ tableName.replace("H", "") +" select * from "+tableName+ " where orgid = ?1 and departid like ?2 and sjrq = ?3";
    	result += entityManager.createNativeQuery(sql).setParameter(1,orgid).setParameter(2,departId).setParameter(3,date).executeUpdate();
        
        String sql1 = "delete from "+ tableName +" where orgid = ?1 and departid like ?2 and sjrq = ?3";
        result += entityManager.createNativeQuery(sql1).setParameter(1,orgid).setParameter(2,departId).setParameter(3,date).executeUpdate();
        
        return result;
    }

	@Override
	public void importDelete(String tableName) {
		String sql = "truncate table " + tableName;
		entityManager.createNativeQuery(sql).executeUpdate();
		
	}
	
	@Override
	public void importDelete(String tableName,String departId) {
		String sql = "delete from " + tableName + " where departid like '" +departId+ "'";
		entityManager.createNativeQuery(sql).executeUpdate();
		
	}
	
	@Override
	public int getMaxId(String className) {
		String sql = "select max(id) from " + className;
		Object o = entityManager.createNativeQuery(sql).getSingleResult();
		if(o != null) {
			return (int) o;
		}
		return 0;
	}
}
