package com.geping.etl.UNITLOAN.service.check.processors.zqtzfsexx;


import com.alibaba.druid.sql.visitor.functions.If;
import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xzqtzfsexx;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXzqtzfsexxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 债券投资发生额信息
 */

@Service
public class XzqtzfsexxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Autowired
    private CheckXzqtzfsexxRowData checkXzqtzfsexxRowData;

    /**
     * 获取实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xzqtzfsexx.class.getSimpleName();
    }

    /**
     *获取准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xzqtzfsexx a"+whereStr;
        String sqlcount="select count(*) from xzqtzfsexx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 获取错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"债券代码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xzqtzfsexx xzqtzfsexx= (Xzqtzfsexx) t;
        checkXzqtzfsexxRowData.checkRow(getHeaderTpl(),checkParamContext,xzqtzfsexx);
    }

    /**
     * 表间校验
     * @param checkParamContext
     */
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+债券代码+交易流水号应唯一
        tableCalibrationJS2318(checkParamContext);
        //发行人证件代码与存量债券投资信息的发行人证件代码应该一致
        tableCalibrationJS1742(checkParamContext);
        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
        tableCalibrationJS1816(checkParamContext);
        //发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
        tableCalibrationJS1915(checkParamContext);
    }

    /**
     * 发行人证件代码与存量债券投资信息的发行人证件代码应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1742(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1742")){
            String existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Ff");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"发行人证件代码与存量债券投资信息的发行人证件代码应该一致");
        }
    }

    /**
     * 发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
     * @param checkParamContext
     */
    private void tableCalibrationJS1915(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1915")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Ftkfora");
            }else{
                existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Ftkf");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在");
        }
    }

    /**
     * 金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1816(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1816")){
            String existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Jj");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致");
        }
    }


    /**
     * 数据日期+金融机构代码+债券代码+交易流水号应唯一
     * @param checkParamContext
     */
    private void tableCalibrationJS2318(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2318")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Zsjzora");
            }else{
                existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Zsjz");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+债券代码+交易流水号应唯一");
        }
    }

/*    *//**
     * 数据日期+金融机构代码+债券代码应唯一
     * @param xzqtzfsexxCheckNums
     * @param errorMsg
     * @param errorId
     *//*
    private void tableCalibrationJS2317(List<String> xzqtzfsexxCheckNums,LinkedHashMap<String, String> errorMsg, List<String> errorId) {
        if(xzqtzfsexxCheckNums.contains("JS2317")){
            String existsSql= (String) SpringContextUtil.getBean("Xzqtzfsexx.Sjz");
            checkHelper.inTableCalibration(getHeaderTpl(),errorId,errorMsg,existsSql,"数据日期+金融机构代码+债券代码应唯一");
        }
    }*/


}