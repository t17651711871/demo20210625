package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/30
 */
public interface XftykhxService {
    Xftykhx save(Xftykhx xftykhx);

    List<Xftykhx> saveAll(List<Xftykhx> list);

    Page<Xftykhx> findAll(Specification specification, PageRequest pageRequest);
}
