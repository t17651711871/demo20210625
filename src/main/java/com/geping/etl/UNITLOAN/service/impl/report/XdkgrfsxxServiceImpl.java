package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrdkfsxx;
import com.geping.etl.UNITLOAN.repository.report.XdkfsxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XdkgrfsxxRepository;
import com.geping.etl.UNITLOAN.service.report.XdkfsxxService;
import com.geping.etl.UNITLOAN.service.report.XdkgrfsxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/28
 */
@Service
@Transactional
public class XdkgrfsxxServiceImpl implements XdkgrfsxxService {

    @Autowired
    private XdkgrfsxxRepository xdkgrfsxxRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Xgrdkfsxx save(Xgrdkfsxx xdkgrfsxx) {
        return xdkgrfsxxRepository.save(xdkgrfsxx);
    }

    @Override
    public List<Xgrdkfsxx> saveAll(List<Xgrdkfsxx> list) {
        return xdkgrfsxxRepository.save(list);
    }

    @Override
    public Page<Xgrdkfsxx> findAll(Specification specification, PageRequest pageRequest){
        return xdkgrfsxxRepository.findAll(specification,pageRequest);
    }

    @Override
    public int batchInsert(List<Xgrdkfsxx> list) {
        int result = jdbcTemplate.batchUpdate("INSERT INTO xgrdkfsxx (id,finorgcode,finorgincode,finorgareacode,isfarmerloan,browidcode,browareacode,loanbrowcode,loancontractcode,loanprocode,loanstartdate,loanenddate,loanactenddate,loancurrency,loanamt,loancnyamt,rateisfix,ratelevel,loanfixamttype,rate,loanfinancesupport,loanraterepricedate,gteemethod,isplatformloan,loanstatus,assetproductcode,loanrestructuring,givetakeid,transactionnum,issupportliveloan,sjrq,checkstatus,datastatus,operator,operationname,operationtime,orgid,departid,nopassreason) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1,list.get(i).getId());
                ps.setString(2,list.get(i).getFinorgcode());
                ps.setString(3,list.get(i).getFinorgincode());
                ps.setString(4,list.get(i).getFinorgareacode());
                ps.setString(5,list.get(i).getIsfarmerloan());
                ps.setString(6,list.get(i).getBrowidcode());
                ps.setString(7,list.get(i).getBrowareacode());
                ps.setString(8,list.get(i).getLoanbrowcode());
                ps.setString(9,list.get(i).getLoancontractcode());
                ps.setString(10,list.get(i).getLoanprocode());
                ps.setString(11,list.get(i).getLoanstartdate());
                ps.setString(12,list.get(i).getLoanenddate());
                ps.setString(13,list.get(i).getLoanactenddate());
                ps.setString(14,list.get(i).getLoancurrency());
                ps.setString(15,list.get(i).getLoanamt());
                ps.setString(16,list.get(i).getLoancnyamt());
                ps.setString(17,list.get(i).getRateisfix());
                ps.setString(18,list.get(i).getRatelevel());
                ps.setString(19,list.get(i).getLoanfixamttype());
                ps.setString(20,list.get(i).getRate());
                ps.setString(21,list.get(i).getLoanfinancesupport());
                ps.setString(22,list.get(i).getLoanraterepricedate());
                ps.setString(23,list.get(i).getGteemethod());
                ps.setString(24,list.get(i).getIsplatformloan());
                ps.setString(25,list.get(i).getLoanstatus());
                ps.setString(26,list.get(i).getAssetproductcode());
                ps.setString(27,list.get(i).getLoanrestructuring());
                ps.setString(28,list.get(i).getGivetakeid());
                ps.setString(29,list.get(i).getTransactionnum());
                ps.setString(30,list.get(i).getIssupportliveloan());
                ps.setString(31,list.get(i).getSjrq());
                ps.setString(32,list.get(i).getCheckstatus());
                ps.setString(33,list.get(i).getDatastatus());
                ps.setString(34,list.get(i).getOperator());
                ps.setString(35,list.get(i).getOperationname());
                ps.setString(36,list.get(i).getOperationtime());
                ps.setString(37,list.get(i).getOrgid());
                ps.setString(38,list.get(i).getDepartid());
                ps.setString(39,list.get(i).getNopassreason());

            }
            @Override
            public int getBatchSize() {
                return list.size();
            }
        }).length;
        return result;
    }
}
