package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.repository.report.XftykhxblRepository;
import com.geping.etl.UNITLOAN.service.report.XftykhxblService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XftykhxblServiceImpl implements XftykhxblService {

    @Autowired
    private XftykhxblRepository xftykhxblRepository;

    @Override
    public Xftykhxbl save(Xftykhxbl xftykhxbl) {
        return xftykhxblRepository.save(xftykhxbl);
    }

    @Override
    public List<Xftykhxbl> saveAll(List<Xftykhxbl> list) {
        return xftykhxblRepository.save(list);
    }

    @Override
    public Page<Xftykhxbl> findAll(Specification specification, PageRequest pageRequest){
        return xftykhxblRepository.findAll(specification,pageRequest);
    }

    @Override
    public List<Xftykhxbl> getInfoByCustomernum(String customernum, String orgId, String departId) {
        return xftykhxblRepository.getInfoByCustomernum(customernum,orgId,departId);
    }

    @Override
    public List<Xftykhxbl> findAll() {
        return xftykhxblRepository.findAll();
    }
}
