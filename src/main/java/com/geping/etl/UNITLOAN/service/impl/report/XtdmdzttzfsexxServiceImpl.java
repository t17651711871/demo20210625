package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xcltdmdzttzxx;
import com.geping.etl.UNITLOAN.entity.report.Xtdmdzttzfsexx;
import com.geping.etl.UNITLOAN.repository.report.XcltdmdzttzxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XtdmdzttzfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XtdmdzttzfsexxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 17:37
 * @描述:
 */
@Transactional
@Service
public class XtdmdzttzfsexxServiceImpl implements XtdmdzttzfsexxService {
    @Autowired
    private XtdmdzttzfsexxRepository xcltdmdzttzxxRepository;

    @Override
    public Page<Xtdmdzttzfsexx> findAll(Specification specification, PageRequest pageRequest) {
        return xcltdmdzttzxxRepository.findAll(specification,pageRequest);
    }
}
