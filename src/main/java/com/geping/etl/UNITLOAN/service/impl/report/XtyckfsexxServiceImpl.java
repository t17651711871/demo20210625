package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xtyckfsexx;
import com.geping.etl.UNITLOAN.repository.report.XtyckfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XtyckfsexxService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XtyckfsexxServiceImpl implements XtyckfsexxService {
	@Autowired
	private XtyckfsexxRepository xtyckfsexxRepository;

	@Override
	public Xtyckfsexx save(Xtyckfsexx tyckfsexx) {
		return xtyckfsexxRepository.save(tyckfsexx);
	}

	@Override
	public List<Xtyckfsexx> saveAll(List<Xtyckfsexx> list) {
		return xtyckfsexxRepository.save(list);
	}

	@Override
	public Page<Xtyckfsexx> findAll(Specification specification, PageRequest pageRequest) {
		return xtyckfsexxRepository.findAll(specification, pageRequest);
	}

}
