package com.geping.etl.UNITLOAN.service.check.processors;

import com.geping.etl.UNITLOAN.common.check.CheckProcessorContainerService;
import com.geping.etl.UNITLOAN.common.check.CheckProcessorService;
import com.geping.etl.UNITLOAN.common.check.CheckResultUpdateService;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.common.check.enums.CheckStatusEnum;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.util.JDBCUtils;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/***
 *   校验处理模板类
 * @author liang.xu
 * @date 2021.4.26
 */
public  abstract class CheckProcessorTemplateService  extends CheckProcessorContainerService implements CheckProcessorService {

    private static Logger logger = LoggerFactory.getLogger(CheckProcessorTemplateService.class);

    @Autowired
    private XCheckRuleService checkRuleService;

    @Autowired
    private CheckResultUpdateService checkResultUpdateService;

    @Autowired
    private DataSource dataSource;

    /**
     * 校验数据
     *
     * @param checkParamContext
     */
    public JSONObject checkData(CheckParamContext checkParamContext){
        JSONObject  jSONObject=new JSONObject();
        //1.准备sql
        prepareSQl(checkParamContext);
        //2.判断有没有校验数据
        int checkDataCount=calCheckDataCount(checkParamContext.getCountsql());
        if(checkDataCount==0){
            jSONObject.put("msg", CheckStatusEnum.NO_CHECK_DATA.getStatus());
            return jSONObject;
        }
        checkParamContext.addSetMap("operator",checkParamContext.getSysUser().getLoginid());
        checkParamContext.addSetMap("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        //3.获取校验编码
        List<String> xclzqfxxxCheckNums = checkRuleService.getChecknum(getKey().toLowerCase());
        checkParamContext.setCheckNums(xclzqfxxxCheckNums);
        //4.上下文参数扩展
        enhanceCheckParamContext(checkParamContext);
        //5.标间校验
        tableCalibration(checkParamContext);
        //6.数据行校验
        rowCalibration(checkParamContext);
        //7.校验错误数据束处理
        boolean handleErrorIdFlag= checkResultUpdateService.tryHandleErrorId(checkParamContext);
        //8.校验正确数据处理
        checkResultUpdateService.tryHandleRightId(checkParamContext);
        if(handleErrorIdFlag){
            jSONObject.put("msg", CheckStatusEnum.CHECK_FAIL.getStatus());
        }else{
            jSONObject.put("msg", CheckStatusEnum.CHECK_SUCCESS.getStatus());
        }
        return jSONObject;
    }

    protected  void enhanceCheckParamContext(CheckParamContext checkParamContext){};

    /**
     * 数据行校验
     */
    protected  void rowCalibration(CheckParamContext checkParamContext){
        ResultSet resultSet = JDBCUtils.Query(DataSourceUtils.getConnection(dataSource), checkParamContext.getSql());
        try {
            while (true) {
                if (!resultSet.next()) break;
                Object t=checkParamContext.getClazz().newInstance();
                Stringutil.getEntity(resultSet, t);
                checkRow(checkParamContext, t);
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 校验行
     * @param t
     */
    protected abstract void checkRow(CheckParamContext checkParamContext,Object t);

    /**
     * 标间校验
     * @param  checkParamContext
     */
    protected abstract void tableCalibration(CheckParamContext checkParamContext);

    /**
     * 获取表头模板
     * @return
     */
    protected abstract String getHeaderTpl();

    /**
     * 计算有没有校验的数据
     * @param countsql
     * @return
     */
    protected  int calCheckDataCount(String countsql){
        int checkDataCount=0;
        try {
            ResultSet resultSet = com.geping.etl.UNITLOAN.util.JDBCUtils.Query(DataSourceUtils.getConnection(dataSource),null,countsql);
            if (resultSet.next()) {
                checkDataCount=resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error("【存量债券发行信息】获取检验数据出错-->",e);
        }
        return checkDataCount;
    }


    /**
     * 准备sql
     * @param checkParamContext
     */
    protected abstract void prepareSQl(CheckParamContext checkParamContext);


}