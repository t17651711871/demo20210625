package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseAreaRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAreaService;
@Service
@Transactional
public class BaseAreaServiceImpl implements BaseAreaService{

	@Autowired
	private BaseAreaRepository bar;

	@Override
	public List<BaseArea> findAll() {
		return bar.findAll();
	}

	@Override
	public List<BaseArea> findAll(Specification<BaseArea> spec) {
		return bar.findAll(spec);
	}

	@Override
	public Page<BaseArea> findAll(Specification<BaseArea> spec, Pageable page) {
		return bar.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseArea> list) {
		bar.deleteAll();
		bar.save(list);
	}
}
