package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/28
 */
public interface XdkfsxxService {
    Xdkfsxx save(Xdkfsxx xdkfsxx);

    List<Xdkfsxx> saveAll(List<Xdkfsxx> list);

    Page<Xdkfsxx> findAll(Specification specification, PageRequest pageRequest);
}
