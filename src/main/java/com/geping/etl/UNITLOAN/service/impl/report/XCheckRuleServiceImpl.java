package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.XCheckRule;
import com.geping.etl.UNITLOAN.repository.report.XCheckRuleRepository;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;

/**    
*  
* @author liuweixin  
* @date 2020年9月21日 上午10:10:21  
*/
@Service
@Transactional
public class XCheckRuleServiceImpl implements XCheckRuleService {
	@Autowired
	private XCheckRuleRepository xCheckRuleRepository;

	@Override
	public Page<XCheckRule> findAll(Specification specification, PageRequest pageRequest) {
		return xCheckRuleRepository.findAll(specification,pageRequest);
	}
	
	@Override
	public void importXCheckRuleList(List<XCheckRule> list) {
		xCheckRuleRepository.deleteAll();
		xCheckRuleRepository.save(list);
	}

	@Override
	public List<String> getReportnameList() {
		return xCheckRuleRepository.getReportnameList();
	}

	@Override
	public List<String> getFieldnameList(String reportname) {
		return xCheckRuleRepository.getFieldnameList(reportname);
	}

	@Override
	public void deleteAll() {
		xCheckRuleRepository.deleteAll();
	}

	@Override
	public List<String> getChecknum(String tablename) {
		
		return xCheckRuleRepository.getChecknumList(tablename);
	}

}
