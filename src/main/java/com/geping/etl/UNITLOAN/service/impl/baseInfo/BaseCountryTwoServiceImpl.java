package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseCountryTwoRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCountryTwoService;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:52:48  
*/
@Service
@Transactional
public class BaseCountryTwoServiceImpl implements BaseCountryTwoService{

	@Autowired
	private BaseCountryTwoRepository bctr;
	
	@Override
	public List<BaseCountryTwo> findAll() {
		return bctr.getAll();
	}

	@Override
	public List<BaseCountryTwo> findAll(Specification<BaseCountryTwo> spec) {
		return bctr.findAll(spec);
	}

	@Override
	public Page<BaseCountryTwo> findAll(Specification<BaseCountryTwo> spec, Pageable page) {
		return bctr.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseCountryTwo> list) {
		bctr.deleteAll();
		bctr.save(list);
		
	}

}
