package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbht;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/29
 */
public interface XdkdbhtService {
    Xdkdbht save(Xdkdbht xdkdbht);

    List<Xdkdbht> saveAll(List<Xdkdbht> list);

    Page<Xdkdbht> findAll(Specification specification, PageRequest pageRequest);
}
