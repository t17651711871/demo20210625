package com.geping.etl.UNITLOAN;

import java.text.SimpleDateFormat;

/***
 *  常量类
 * @author liang.xu
 * @date 2021.4.2
 */
public interface SysConstants {

    /**
     * 报表实体包路径
     */
    String REPORT_ENTITY_PKG = "com.geping.etl.UNITLOAN.entity.report.";

    /**
     * 处理结果标识
     */
    String RESULT_KEY = "Result.";

    /**
     * 标识
     */
    String  RES="1";

    /**
     * 字符隔
     */
    String spaceStr = "|";


    /**
     * 年月日日期格式
     */
    String  yyyyMMdd="yyyy-MM-dd";

    /**
     * 错误提示分隔符
     */
    String  ERROR_TIP_SPLIT="]->\n";

    /**
     * 分号
     */
    String  SEMICOLON= ":";

    /**
     * 逗号
     */
    String  COMMA="，";

    /**
     * 百分号S
     */
    String  PERCENTS="%s";

    /**
     * dk
     */
    String  dk="dk";


    /**
     * 逗号
     */
    String  EN_COMMA=",";


    /**
     * .xls 后缀
     */
    String  EXCEL_SUFFIX=".xlsx";

    /**
     * 日期格式
     */
    SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 下划线
     */
    String  UNDERLINE="_";

}