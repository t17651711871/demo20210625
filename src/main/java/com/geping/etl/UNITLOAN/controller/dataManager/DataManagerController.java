package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.statistics.ReportStatisticsService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.XApplicationRunnerImpl;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.service.Report_InfoService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author: wangzd
 * @Date: 17:51 2020/6/8
 */

@RestController
public class DataManagerController {

    @Autowired
    private Report_InfoService riService;

    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    private final String subjectId="25";

    @Autowired
    private ReportStatisticsService reportStatisticsService;

    //跳转到数据管理页面
    @GetMapping("/XDataManager")
    public ModelAndView XDataManager(HttpSession session){
        ModelAndView modelAndView=new ModelAndView();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String orgid = sys_user.getOrgid();
        List<Report_Info> reports = riService.getReportBySubjectId(subjectId);
        reportStatisticsService.reportStatistics(orgid,departId,reports);
        modelAndView.addObject("reportlist",reports);
        modelAndView.setViewName("unitloan/datamanage/report_info");
        return modelAndView;
    }


    /***
     * TODO 具体的报表主页,后面放到具体的类里
     * @param status
     * @return
     */
    @GetMapping("/XGoDataOneUi")
    public ModelAndView XGoDataOneUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
            BaseArea area = new BaseArea();
            area.setAreacode(country.getCountrycode());
            area.setAreaname(country.getCountryname());
            list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/aone/cldkxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/aone/cldkxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/cldkxxbw");
        }
        return modelAndView;
    }

    @GetMapping("/XGoDataTwoUi")
    public ModelAndView XGoDataTwoUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/btwo/dkfsxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/btwo/dkfsxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/dkfsxxbw");
        }
        return modelAndView;
    }

    @GetMapping("/XGoDataThreeUi")
    public ModelAndView XGoDataThreeUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        modelAndView.addObject("currencyList", JSONArray.fromObject(XApplicationRunnerImpl.baseCurrencyList));
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/cthree/dkdbhtdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/cthree/dkdbhtdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/dkdbhtbw");
        }
        return modelAndView;
    }

    @GetMapping("/XGoDataSixUi")
    public ModelAndView XGoDataSixUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/fsix/ftykhxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/fsix/ftykhxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/ftykhxbw");
        }
        return modelAndView;
    }

    @GetMapping("/XGoDataFourUi")
    public ModelAndView XGoDataFourUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
            BaseArea area = new BaseArea();
            area.setAreacode(country.getCountrycode());
            area.setAreaname(country.getCountryname());
            list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/aone/clgrdkxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/aone/clgrdkxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/clgrdkxxbw");
        }
        return modelAndView;
    }


    @GetMapping("/XGoDataSevenUi")
    public ModelAndView XGoDataSeven (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
            BaseArea area = new BaseArea();
            area.setAreacode(country.getCountrycode());
            area.setAreaname(country.getCountryname());
            list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/btwo/dkgrfsxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/btwo/dkgrfsxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/dkgrfsxxbw");
        }
        return modelAndView;
    }


    @GetMapping("/XGoDataXclwtdkxxUi")
    public ModelAndView XGoDataXclwtdkxxUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
            BaseArea area = new BaseArea();
            area.setAreacode(country.getCountrycode());
            area.setAreaname(country.getCountryname());
            list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/aone/clwtdkxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/aone/clwtdkxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/clwtdkxxbw");
        }
        return modelAndView;
    }



    //访问单位贷款担保物信息页面
    @RequestMapping(value="/XshowDWDKDBWXX",method=RequestMethod.GET)
    public ModelAndView XshowDWDKDBWXXUi(String status){
        ModelAndView model = new ModelAndView();
        if("dtj".equals(status)) {
            model.setViewName("unitloan/datamanage/dwdkdbwxx_dtj");
        }else if("dsh".equals(status)) {
            model.setViewName("unitloan/datamanage/dwdkdbwxx_dsh");
        }
        return model;
    }
}
