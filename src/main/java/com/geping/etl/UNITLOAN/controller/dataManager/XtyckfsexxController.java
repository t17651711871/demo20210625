package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.Xtyckfsexx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XtyckfsexxService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData2;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataTyckfse;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:30:09  
*/
@RestController
public class XtyckfsexxController {

	@Autowired
	XtyckfsexxService xtyckfsexxService;
	
	@Autowired
    private SUpOrgInfoSetService suisService;
	
	@Autowired
    private ExcelUploadUtil excelUploadUtil;
	
	@Autowired
    private CustomSqlUtil customSqlUtil;
	
	@Autowired
    private XcommonService commonService;
	
	@Autowired
    private XCheckRuleService checkRuleService;
	
	@Autowired
    private  XDataBaseTypeUtil dataBaseTypeUtil;
	
	@Autowired
    private DepartUtil departUtil;
	
	private final String tableName="xtyckfsexx";
	
	private final String fileName="同业存款发生额信息";
	
	private static String target="";
	
	@GetMapping("/XGetXtyckfsexxDataUi")
    public ModelAndView XGoDataTwoUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tyckfsexxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tyckfsexxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/tyckfsexxbw");
        }
        return modelAndView;
    }
	
	//查询数据
    @PostMapping("XGetXtyckfsexxData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String financeorgcodeParam,String contractcodeParam,String operationnameParam,String jydszjlxParam,
       String jydsdmParam,String checkstatusParam,String datastatus,HttpServletRequest request){
        page = page - 1;
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(financeorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+financeorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(contractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("contractcode"), "%"+contractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(jydszjlxParam)){
                    predicates.add(criteriaBuilder.like(root.get("jydszjlx"), "%"+jydszjlxParam+"%"));
                }
                if (StringUtils.isNotBlank(jydsdmParam)){
                    predicates.add(criteriaBuilder.like(root.get("jydsdm"), "%"+jydsdmParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xtyckfsexx> all = xtyckfsexxService.findAll(specification, pageRequest);
        List<Xtyckfsexx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }
    
  //导入
    @PostMapping(value = "XimmportExceltyckfsexx",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
    	Sys_UserAndOrgDepartment sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","内部机构号","业务类型","交易对手证件类型","交易对手代码","存款账户编码","存款协议代码","协议起始日期","协议到期日期","币种","交易金额","交易金额折人民币","交易日期","交易流水号","利率水平","交易账户号","交易账户开户行号","交易对手账户号","交易方向","数据日期"};
                String[] values = {"financeorgcode","financeorginnum","ywlx","jydszjlx","jydsdm","ckzhbm","ckxydm","startdate","enddate","currency","receiptbalance","receiptcnybalance","jyrq","jylsh","llsp","jyzhh","jyzhkhhh","jydszhh","jyfx","sjrq"};
                String[] rateFields = {"llsp"};
                String[] amountFields = {"receiptcnybalance","receiptbalance","loancnyamt"};
                String[] dateFields = {"startdate", "enddate","jyrq","sjrq"};
                XCommonExcel commonExcel = new XCommonExcel(sys_user, opcPackage, customSqlUtil, new Xtyckfsexx(),headers,values, amountFields,dateFields, null,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }


            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    //校验
    @PostMapping("XCheckDataXtyckfsexx")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String loancontractcodeParam,String loanbrowcodeParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean oracle = dataBaseTypeUtil.equalsOracle();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStra = whereStra + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loanbrowcodeParam)){
                whereStr = whereStr + " and loanbrowcode like '%"+ loanbrowcodeParam +"%'";
                whereStra = whereStra + " and a.loanbrowcode like '%"+ loanbrowcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xtyckfsexx "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xtyckfsexx");
                //标识名称
                String errorcode = "交易对手代码";
                String errorcode2="存款账户编码";

                String sqlexist;
                String errorinfo;

                if (checknumList.contains("JS2314")){
                    if(dataBaseTypeUtil.equalsSqlServer()){
                        sqlexist="select id,jydsdm,ckzhbm from xtyckfsexx t1 "+whereStra.replace("a.", "t1.")+"  and exists (select 1 from (select sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency from xtyckfsexx where operationname != '申请删除' group by sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.ckzhbm = t2.ckzhbm and t1.ckxydm = t2.ckxydm and t1.jylsh=t2.jylsh and t1.jyfx=t2.jyfx and t1.currency=t2.currency)";
                        errorinfo = "数据日期+金融机构代码+存款账户编码+存款协议代码+交易流水号+交易方向+币种应唯一";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                        sqlexist="select id,jydsdm,ckzhbm from xtyckfsexx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency from xtyckfsexxh where operationname != '申请删除' group by sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency  having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.ckzhbm = t2.ckzhbm and t1.ckxydm = t2.ckxydm and t1.jylsh=t2.jylsh and t1.jyfx=t2.jyfx and t1.currency=t2.currency)";
                        errorinfo = "数据日期+金融机构代码+存款账户编码+存款协议代码+交易流水号+交易方向+币种应唯一(历史表)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    }else {
                        sqlexist="select id,jydsdm,ckzhbm from xtyckfsexx "+whereStr+" and (sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency ) in (select sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency  from xtyckfsexx  where operationname != '申请删除'  group by sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency  having  count(1) > 1)";
                        errorinfo = "数据日期+金融机构代码+存款账户编码+存款协议代码+交易流水号+交易方向+币种应唯一";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        sqlexist="select id,jydsdm,ckzhbm from xtyckfsexx "+whereStr+" and (sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency ) in (select sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency  from xtyckfsexxh  where operationname != '申请删除'  group by sjrq,financeorgcode,ckzhbm,ckxydm,jylsh,jyfx,currency  having  count(1) > 1)";
                        errorinfo = "数据日期+金融机构代码+存款账户编码+存款协议代码+交易流水号+交易方向+币种应唯一(历史表)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    }
                }

                //境外同业客户的交易对手证件类型不应该是统一社会信用代码或者组织机构代码或者SPV代码
                if (checknumList.contains("JS2089")){
                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xtykhjcxx b on a.sjrq = b.sjrq  and a.jydsdm = b.khcode "+whereStra+" and b.dqdm  like '000%' and b.dqdm != '000156'  and a.jydszjlx  in ('A01','A02','C01','C02')";
                    errorinfo = "境外同业客户的交易对手证件类型不应该是统一社会信用代码或者组织机构代码或者SPV代码";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xtykhjcxxh b on a.sjrq = b.sjrq  and a.jydsdm = b.khcode "+whereStra+" and b.dqdm  like '000%' and b.dqdm != '000156' and a.jydszjlx  in ('A01','A02','C01','C02')";
                    errorinfo = "境外同业客户的交易对手证件类型不应该是统一社会信用代码或者组织机构代码或者SPV代码(历史表)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //境内同业客户的交易对手证件类型应该是统一社会信用代码或者组织机构代码或者SPV代码
                if (checknumList.contains("JS2088")){
                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xtykhjcxx b on a.sjrq = b.sjrq  and a.jydsdm = b.khcode "+whereStra+" and b.dqdm not like '000%' and a.jydszjlx not in ('A01','A02','C01','C02','Z99')";
                    errorinfo = "境内同业客户的交易对手证件类型应该是统一社会信用代码或者组织机构代码或者SPV代码";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xtykhjcxxh b on a.sjrq = b.sjrq  and a.jydsdm = b.khcode "+whereStra+" and b.dqdm not like '000%' and a.jydszjlx not in ('A01','A02','C01','C02','Z99')";
                    errorinfo = "境内同业客户的交易对手证件类型应该是统一社会信用代码或者组织机构代码或者SPV代码(历史表)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //交易对手代码应该在同业客户基础信息.客户代码中存在
                if (checknumList.contains("JS1909")){
                    sqlexist="select a.id,a.jydsdm,a.ckzhbm  from xtyckfsexx a where a.datastatus='0' and a.checkstatus='0' and a.jydszjlx in('A01','A02','L01') and (NOT EXISTS (SELECT 1 FROM xtykhjcxx b WHERE a.sjrq=b.sjrq AND a.jydsdm =b.khcode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xtykhjcxxh b WHERE a.sjrq=b.sjrq AND a.jydsdm =b.khcode))";
                    errorinfo = "交易对手代码应该在同业客户基础信息.客户代码中存在";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }

                //交易对手代码与存量同业存款信息的交易对手代码应该一致
                if (checknumList.contains("JS1728")){
                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xcltyckxx b on a.financeorginnum = b.financeorginnum and a.ckzhbm =b.ckzhbm and a.ckxydm =b.ckxydm and a.sjrq = b.sjrq "+whereStra+" and  a.jydsdm != b.jydsdm and a.jydsdm is not null and b.jydsdm is not null and b.operationname != '申请删除'";
                    errorinfo = "交易对手代码与存量同业存款信息的交易对手代码应该一致";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xcltyckxxh b on a.financeorginnum = b.financeorginnum and a.ckzhbm =b.ckzhbm and a.ckxydm =b.ckxydm and a.sjrq = b.sjrq "+whereStra+" and  a.jydsdm != b.jydsdm and a.jydsdm is not null and b.jydsdm is not null and b.operationname != '申请删除'";
                    errorinfo = "交易对手代码与存量同业存款信息的交易对手代码应该一致(历史表)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //业务类型与存量同业存款信息的业务类型应该一致
                if (checknumList.contains("JS1727")){
                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a inner join xcltyckxx b on a.financeorginnum = b.financeorginnum and a.ckzhbm =b.ckzhbm and a.ckxydm =b.ckxydm and a.sjrq = b.sjrq "+whereStra+" and a.ywlx != b.ywlx and a.ywlx is not null and b.ywlx is not null and b.operationname != '申请删除'";
                    errorinfo = "业务类型与存量同业存款信息的业务类型应该一致";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexxh a inner join xcltyckxxh b on a.financeorginnum = b.financeorginnum and a.ckzhbm =b.ckzhbm and a.ckxydm =b.ckxydm and a.sjrq = b.sjrq "+whereStra+" and a.ywlx != b.ywlx and a.ywlx is not null and b.ywlx is not null and b.operationname != '申请删除'";
                    errorinfo = "交易对手代码与存量同业存款信息的交易对手代码应该一致(历史表)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
                if(checknumList.contains("JS1726")) {
                    errorinfo  = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
                    sqlexist ="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a INNER JOIN xjrjgfz b on a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.financeorgcode != b.finorgcode and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo  = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致（历史表）";
                    sqlexist ="select a.id,a.jydsdm,a.ckzhbm from xtyckfsexx a INNER JOIN xjrjgfzh b on a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.financeorgcode != b.finorgcode";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

            	resultSet = JDBCUtils.Query(connection, sql);
            	while (true) {
                    if (!resultSet.next()) break;
                     Xtyckfsexx xtyckfsexx = new Xtyckfsexx();
                    Stringutil.getEntity(resultSet, xtyckfsexx);
                    CheckAllDataTyckfse.checkXtyckfsexx(customSqlUtil,checknumList, xtyckfsexx, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData2.fzList != null) {
                CheckAllData2.fzList.clear();
                CheckAllData2.fzList = null;
            }
            if(CheckAllData2.frList != null) {
                CheckAllData2.frList.clear();
                CheckAllData2.frList = null;
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
            rightId.clear();
        }

        customSqlUtil.saveLog("同业存款发生额信息","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckXtyckfsexx")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }
}
