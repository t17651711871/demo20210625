package com.geping.etl.UNITLOAN.controller.createReport;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.report.*;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: wangzd
 * @Date: 10:29 2020/6/12
 */
@RestController
public class XcldkxxReportController {

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private CustomSqlUtil customSqlUtil;
    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @PostMapping("XCreateReportOne")
    public void XCreateReportOne(HttpServletResponse response, HttpSession session,HttpServletRequest request){

        List<Object> list = Stringutil.getReportList(new Xcldkxx(), "xcldkxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        String date = request.getParameter("date");

        try {
//            if (list!=null && list.size()>0){
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_CLDWDK_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);



                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xcldkxx",orgid,departId);

                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xcldkxx",3,"reduce",list.size());
//            }
                PrintWriter out=response.getWriter();
                out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @PostMapping("XCreateReportTwo")
    public void XCreateReportTwo(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xdkfsxx(), "xdkfsxx");

        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");

        try {
//            if (list!=null && list.size()>0){
            	String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_DWDKFS_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);
                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xdkfsxx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xdkfsxx",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("XCreateReportThree")
    public void XCreateReportThree(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xdkdbht(), "xdkdbht");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
//            if (list!=null && list.size()>0){
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_DBHTXX_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                boolean isDepart="yes".equals(sUpOrgInfoSet.getIsusedepart())?true:false;
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xdkdbht",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xdkdbht",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("XCreateReportFour")
    public void XCreateReportFour(HttpServletResponse response, HttpSession session, HttpServletRequest request){
        List<Object> list = Stringutil.getReportList(new Xdkdbwx(), "xdkdbwx");
        String createdate = request.getParameter("date");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        try {
//            if (list!=null && list.size()>0){
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(createdate));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_DBWXX_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);
                //修改状态
                /*Map<String,String> whereMap=new HashMap<>();
                Map<String,String> setMap=new HashMap<>();
                setMap.put("datastatus","4");
                whereMap.put("datastatus","3");
                customSqlUtil.updateByWhere("xdkdbwx",setMap,whereMap);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xdkdbwx",3,"reduce",list.size());*/
                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xdkdbwx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                countUtil.handleCount("xdkdbwx",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("XCreateReportFive")
    public void XCreateReportFive(HttpServletResponse response,HttpServletRequest request,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xjrjgfz(), "xjrjgfz");
        String createdate = request.getParameter("date");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        try {
//            if (list!=null && list.size()>0){
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(createdate));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_JRJGFZ_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                //修改状态
                /*Map<String,String> whereMap=new HashMap<>();
                Map<String,String> setMap=new HashMap<>();
                setMap.put("datastatus","4");
                whereMap.put("datastatus","3");
                customSqlUtil.updateByWhere("xjrjgfz",setMap,whereMap);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xjrjgfz",3,"reduce",list.size());*/
                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xjrjgfz",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                countUtil.handleCount("xjrjgfz",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("XCreateReportSix")
    public void XCreateReportSix(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xftykhx(), "xftykhx");

        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
            //if (list!=null && list.size()>0){
            	String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_FTYKHX_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);
                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xftykhx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce"); 
                countMap.put(3,"add");
                countUtil.handleCount("xftykhx",3,"reduce",list.size());
            //}
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @PostMapping("XCreateReportSeven")
    public void XCreateReportSeven(HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xftykhx(), "xftykhx");

        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        try {
            if (list!=null && list.size()>0){
                String localDate=LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_FTYKHX_"+localDate+".data";
                DownloadUtil.writeCSV(list,target,fileName);
                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xftykhx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce"); 
                countMap.put(3,"add");
                countUtil.handleCount("xftykhx",3,"reduce",list.size());
            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @PostMapping("XCreateReportEight")
    public void XCreateReportEight(HttpServletResponse response, HttpSession session,HttpServletRequest request){

        List<Object> list = Stringutil.getReportList(new Xclgrdkxx(), "xclgrdkxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        String date = request.getParameter("date");

        try {
//            if (list!=null && list.size()>0){
            String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
            String target=messagePath+ File.separator+"report"+File.separator+localDate;
            String fileName=bankcodewangdian+"_CLGRDK_"+localDate+".dat";
            fileName = DownloadUtil.writeZIP(list,target,fileName);



            Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
            String departId = "%%";
            String orgid = sys_user.getOrgid();
            commonService.moveToHistoryTable("xclgrdkxx",orgid,departId);

            XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo);
            Map<Integer,String> countMap=new HashMap<>();
            countMap.put(0,"reduce");
            countMap.put(3,"add");
            countUtil.handleCount("xclgrdkxx",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @PostMapping("XCreateReportNine")
    public void XCreateReportNine(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xgrdkfsxx(), "xgrdkfsxx");

        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();

        String date = request.getParameter("date");

        try {
//            if (list!=null && list.size()>0){
            String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
            String target=messagePath+ File.separator+"report"+File.separator+localDate;
            String fileName=bankcodewangdian+"_GRDKFS_"+localDate+".dat";
            fileName = DownloadUtil.writeZIP(list,target,fileName);
            Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
            String departId = "%%";
            String orgid = sys_user.getOrgid();
            commonService.moveToHistoryTable("xgrdkfsxx",orgid,departId);
            XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo);
            Map<Integer,String> countMap=new HashMap<>();
            countMap.put(0,"reduce");
            countMap.put(3,"add");
            countUtil.handleCount("xgrdkfsxx",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("XCreateReportTen")
    public void XCreateReportTen(HttpServletResponse response, HttpSession session,HttpServletRequest request){

        List<Object> list = Stringutil.getReportList(new Xclwtdkxx(), "xclwtdkxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        String date = request.getParameter("date");

        try {
//            if (list!=null && list.size()>0){
            String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
            String target=messagePath+ File.separator+"report"+File.separator+localDate;
            String fileName=bankcodewangdian+"_CLWTDK_"+localDate+".dat";
            fileName = DownloadUtil.writeZIP(list,target,fileName);



            Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
            String departId = "%%";
            String orgid = sys_user.getOrgid();
            commonService.moveToHistoryTable("xclwtdkxx",orgid,departId);

            XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo);
            Map<Integer,String> countMap=new HashMap<>();
            countMap.put(0,"reduce");
            countMap.put(3,"add");
            countUtil.handleCount("xclwtdkxx",3,"reduce",list.size());
//            }
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
