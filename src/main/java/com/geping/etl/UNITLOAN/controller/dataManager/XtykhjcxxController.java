package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.Xtykhjcxx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XtykhjcxxService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData2;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataTykh;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:30:09  
*/
@RestController
public class XtykhjcxxController {

	@Autowired
	XtykhjcxxService xtykhjcxxService;
	
	@Autowired
    private SUpOrgInfoSetService suisService;
	
	@Autowired
    private ExcelUploadUtil excelUploadUtil;
	
	@Autowired
    private CustomSqlUtil customSqlUtil;
	
	@Autowired
    private XcommonService commonService;
	
	@Autowired
    private XCheckRuleService checkRuleService;
	
	@Autowired
    private  XDataBaseTypeUtil dataBaseTypeUtil;
	
	@Autowired
    private DepartUtil departUtil;
	
	private final String tableName="xtykhjcxx";
	
	private final String fileName="同业客户基础信息";
	
	private static String target="";
	
	@GetMapping("/XGetXtykhjcxxDataUi")
    public ModelAndView XGoDataTwoUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tykhjcxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tykhjcxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/tykhjcxxbw");
        }
        return modelAndView;
    }
	
	//查询数据
    @PostMapping("XGetXtykhjcxxData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String financeorgcodeParam,String khnameParam,String operationnameParam,String khtypeParam,
       String khcodeParam,String checkstatusParam,String datastatus,HttpServletRequest request){
        page = page - 1;
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(financeorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+financeorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(khnameParam)){
                    predicates.add(criteriaBuilder.like(root.get("khname"), "%"+khnameParam+"%"));
                }
                if (StringUtils.isNotBlank(khtypeParam)){
                    predicates.add(criteriaBuilder.like(root.get("khtype"), "%"+khtypeParam+"%"));
                }
                if (StringUtils.isNotBlank(khcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("khcode"), "%"+khcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xtykhjcxx> all = xtykhjcxxService.findAll(specification, pageRequest);
        List<Xtykhjcxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }
    
  //导入
    @PostMapping(value = "XimmportExceltykhjcxx",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
    	Sys_UserAndOrgDepartment sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","客户名称","客户代码","客户金融机构编码","客户内部编码","基本存款账号","基本账户开户行名称","注册地址","地区代码","客户类别","成立日期","是否关联方","客户经济成分","客户国民经济部门","客户信用级别总等级数","客户信用评级","数据日期"};
                String[] values = {"financeorgcode","khname","khcode","khjrjgbm","khnbbm","jbckzh","jbzhkhhmc","zcdz","dqdm","khtype","setupdate","sfglf","khjjcf","khgmjjbm","khxyjbzdjs","khxypj","sjrq"};
//                String[] rateFields = {"ratelevel"};
//                String[] amountFields = {"loanamt","loancnyamt"};
                String[] dateFields = {"setupdate","sjrq"};
                XCommonExcel commonExcel = new XCommonExcel(sys_user, opcPackage, customSqlUtil, new Xtykhjcxx(),headers,values, null,dateFields, null,null);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }


            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    //校验
    @PostMapping("XCheckDataXtykhjcxx")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String loancontractcodeParam,String loanbrowcodeParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean oracle = dataBaseTypeUtil.equalsOracle();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStra = whereStra + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loanbrowcodeParam)){
                whereStr = whereStr + " and loanbrowcode like '%"+ loanbrowcodeParam +"%'";
                whereStra = whereStra + " and a.loanbrowcode like '%"+ loanbrowcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xtykhjcxx "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
                //标识名称
                String errorcode = "客户金融机构编码";
                String errorcode2="客户内部编码";
                String sqlexist;
                String errorinfo;

                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xtykhjcxx");

                //数据日期+金融机构代码+存款账户编码+存款协议代码应唯一
                if (checknumList.contains("JS2310")){
                    if (!oracle){
                         errorinfo  = "数据日期+客户代码应唯一";
                         sqlexist ="select id,khjrjgbm,khnbbm from xtykhjcxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,khcode from xtykhjcxx where operationname != '申请删除' group by sjrq,khcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.khcode = t2.khcode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+客户代码应唯一（历史表）";
                        sqlexist ="select id,khjrjgbm,khnbbm from xtykhjcxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,khcode from xtykhjcxxh group by sjrq,khcode having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.khcode = t2.khcode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                         errorinfo  = "数据日期+客户代码应唯一";
                         sqlexist ="select id,khjrjgbm,khnbbm from xtykhjcxx "+whereStr+" and (sjrq,khcode) in (select sjrq,khcode from xtykhjcxx where operationname != '申请删除' group by sjrq,khcode having  count(1) > 1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+客户代码应唯一（历史表）";
                        sqlexist = "select id,khjrjgbm,khnbbm from xtykhjcxx "+whereStr+" and (sjrq,khcode) in (select sjrq,khcode from xtykhjcxxh group by sjrq,khcode having  count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }


                //当客户发生过同业存单投资或存放同业业务时，客户信用评级不能为空
                if(checknumList.contains("JS1576")) {
                    if(dataBaseTypeUtil.equalsSqlServer()){
                        sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyckfsexx b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and substring(b.ywlx,1,3) in ('T02','T04') and b.operationname != '申请删除')";
                        errorinfo = "当客户发生过同业存单投资或存放同业业务时，客户信用评级不能为空";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                        sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyckfsexxh b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and substring(b.ywlx,1,3)  in ('T02','T04') and b.operationname != '申请删除')";
                        errorinfo = "当客户发生过同业存单投资或存放同业业务时，客户信用评级不能为空（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else{
                        sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyckfsexx b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and substr(b.ywlx,1,3) in ('T02','T04') and b.operationname != '申请删除')";
                        errorinfo = "当客户发生过同业存单投资或存放同业业务时，客户信用评级不能为空";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                        sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyckfsexxh b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and substr(b.ywlx,1,3)  in ('T02','T04') and b.operationname != '申请删除')";
                        errorinfo = "当客户发生过同业存单投资或存放同业业务时，客户信用评级不能为空（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }

                //当客户有存量的资产类同业借贷业务时，客户信用评级不能为空
                if(checknumList.contains("JS1577")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xcltyjdxx b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and b.zcfzlx ='AL01' and b.operationname != '申请删除')";
                    errorinfo = "当客户有存量的资产类同业借贷业务时，客户信用评级不能为空";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xcltyjdxxh b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and b.zcfzlx ='AL01' and b.operationname != '申请删除')";
                    errorinfo = "当客户有存量的资产类同业借贷业务时，客户信用评级不能为空（历史表）";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //当客户发生过资产类同业借贷业务时，客户信用评级不能为空
                if(checknumList.contains("JS1578")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyjdfsexx b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and b.zcfzlx ='AL01' and b.operationname != '申请删除')";
                    errorinfo = "当客户发生过资产类同业借贷业务时，客户信用评级不能为空";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and exists (select 1 from xtyjdfsexxh b where a.khcode = b.jydsdm and a.sjrq = b.sjrq and (a.khxypj is null or ltrim(a.khxypj) = '') and b.zcfzlx ='AL01' and b.operationname != '申请删除')";
                    errorinfo = "当客户发生过资产类同业借贷业务时，客户信用评级不能为空（历史表）";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                //存量同业借贷SPV客户不应该在同业客户基础信息表中报送
                if(checknumList.contains("JS2090")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and (EXISTS (SELECT 1 FROM xcltyjdxx b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydsdmlb in('C01','C02') AND b.operationname != '申请删除') and EXISTS (SELECT 1 FROM xcltyjdxxh b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydsdmlb in('C01','C02')))";
                    errorinfo = "存量同业借贷SPV客户不应该在同业客户基础信息表中报送";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }

                //同业借贷SPV客户不应该在同业客户基础信息表中报送
                if(checknumList.contains("JS2091")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and (EXISTS (SELECT 1 FROM xtyjdfsexx b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydsdmlb in('C01','C02') AND b.operationname != '申请删除') and EXISTS (SELECT 1 FROM xtyjdfsexxh b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydsdmlb in('C01','C02')))";
                    errorinfo = "存量同业借贷SPV客户不应该在同业客户基础信息表中报送";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }

                //存量同业存款SPV客户不应该在同业客户基础信息表中报送
                if(checknumList.contains("JS2092")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and (EXISTS (SELECT 1 FROM xcltyckxx b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydszjlx in('C01','C02') AND b.operationname != '申请删除') and EXISTS (SELECT 1 FROM xcltyckxxh b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydszjlx in('C01','C02')))";
                    errorinfo = "存量同业存款SPV客户不应该在同业客户基础信息表中报送";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }

                //同业存款SPV客户不应该在同业客户基础信息表中报送
                if(checknumList.contains("JS2093")) {
                    sqlexist = "select a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a "+whereStra+" and (EXISTS (SELECT 1 FROM xtyckfsexx b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydszjlx in('C01','C02') AND b.operationname != '申请删除') and EXISTS (SELECT 1 FROM xtyckfsexxh b WHERE a.sjrq=b.sjrq AND a.khcode =b.jydsdm AND b.jydszjlx in('C01','C02')))";
                    errorinfo = "存量同业存款SPV客户不应该在同业客户基础信息表中报送";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }
                //正常运营的同业客户若在本机构开立基本户，则基本存款账号可以关联存量同业存款信息
                if(checknumList.contains("JS1920")) {
                    if(dataBaseTypeUtil.equalsSqlServer()){
                        sqlexist = "SELECT a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a inner join xcltyckxx b on a.sjrq = b.sjrq " +
                                "inner join xjrjgfz c on a.sjrq = c.sjrq  "+whereStra+" and a.jbzhkhhmc in (SELECT finorgname FROM xjrjgfz d WHERE d.sjrq=a.sjrq and d.operationname != '申请删除') and substring(b.ywlx,1,3) = 'T01' and a.jbckzh not in(SELECT e.ckzhbm FROM xcltyckxx e WHERE e.sjrq = a.sjrq and e.operationname != '申请删除')";
                        errorinfo = "正常运营的同业客户若在本机构开立基本户，则基本存款账号可以关联存量同业存款信息";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                        sqlexist = "SELECT a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a inner join xcltyckxxh b on a.sjrq = b.sjrq " +
                                "inner join xjrjgfzh c on a.sjrq = c.sjrq  "+whereStra+" and a.jbzhkhhmc in (SELECT finorgname FROM xjrjgfzh d WHERE d.sjrq = a.sjrq) and substring(b.ywlx,1,3) = 'T01' and a.jbckzh not in(SELECT e.ckzhbm FROM xcltyckxx e WHERE e.sjrq =a.sjrq)";
                        errorinfo = "正常运营的同业客户若在本机构开立基本户，则基本存款账号可以关联存量同业存款信息（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else{
                        sqlexist = "SELECT a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a inner join xcltyckxx b on a.sjrq = b.sjrq " +
                                "inner join xjrjgfz c on a.sjrq = c.sjrq  "+whereStra+" and a.jbzhkhhmc in (SELECT finorgname FROM xjrjgfz d WHERE d.sjrq=a.sjrq and d.operationname != '申请删除') and substr(b.ywlx,1,3) = 'T01' and a.jbckzh not in(SELECT e.ckzhbm FROM xcltyckxx e WHERE e.sjrq = a.sjrq and e.operationname != '申请删除')";
                        errorinfo = "正常运营的同业客户若在本机构开立基本户，则基本存款账号可以关联存量同业存款信息";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                        sqlexist = "SELECT a.id,a.khjrjgbm,a.khnbbm from xtykhjcxx a inner join xcltyckxxh b on a.sjrq = b.sjrq " +
                                "inner join xjrjgfzh c on a.sjrq = c.sjrq  "+whereStra+" and a.jbzhkhhmc in (SELECT finorgname FROM xjrjgfzh d WHERE d.sjrq = a.sjrq) and substr(b.ywlx,1,3) = 'T01' and a.jbckzh not in(SELECT e.ckzhbm FROM xcltyckxx e WHERE e.sjrq =a.sjrq)";
                        errorinfo = "正常运营的同业客户若在本机构开立基本户，则基本存款账号可以关联存量同业存款信息（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }



                //SELECT * from xtykhjcxx a  inner join xcltyckxx b on a.sjrq = b.sjrq
                //inner join xjrjgfz c on a.sjrq = c.sjrq  WHERE a.jbzhkhhmc in (SELECT finorgname FROM xjrjgfz d WHERE d.operationname != '申请删除') and substr(b.ywlx,1,3) = 'T01' and a.jbckzh not in(SELECT e.ckzhbm FROM xcltyckxx e WHERE e.operationname != '申请删除')
                
            	resultSet = JDBCUtils.Query(connection, sql);
            	while (true) {
                    if (!resultSet.next()) break;
                     Xtykhjcxx xtykhjcxx = new Xtykhjcxx();
                    Stringutil.getEntity(resultSet, xtykhjcxx);
                    CheckAllDataTykh.checkXtykhjcxx(customSqlUtil,checknumList, xtykhjcxx, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData2.fzList != null) {
                CheckAllData2.fzList.clear();
                CheckAllData2.fzList = null;
            }
            if(CheckAllData2.frList != null) {
                CheckAllData2.frList.clear();
                CheckAllData2.frList = null;
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
            rightId.clear();
        }

        customSqlUtil.saveLog("同业客户基础信息","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckXtykhjcxx")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }
}
