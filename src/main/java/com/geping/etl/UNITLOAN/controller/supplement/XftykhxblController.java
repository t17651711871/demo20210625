package com.geping.etl.UNITLOAN.controller.supplement;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.XfieldEnum;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XftykhxblService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class XftykhxblController {

    @Autowired
    private XftykhxblService xftykhxblService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XcommonService commonService;

    @Autowired
    private CalcCountUtil countUtil;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;

    private final String fileName="非同业单位客户基础信息补录";

    @GetMapping("/XftykhxblUi")
    public ModelAndView XftykhxblUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("baseAreaList", XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList", XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList", XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList", XApplicationRunnerImpl.baseCurrencyList);
        modelAndView.setViewName("unitloan/supplement/ftykhxbl");
        return modelAndView;
    }

    //查询数据
    @PostMapping("XGetXftykhxblData")
    public ResponseResult XGetXftykhxblData(int page, int rows, String finorgcodeParam,String customernameParam,String customercodeParam,String regamtcrenyParam){
        page = page - 1;
        sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(customernameParam)){
                    predicates.add(criteriaBuilder.like(root.get("customername"), "%"+customernameParam+"%"));
                }
                if (StringUtils.isNotBlank(customercodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("customercode"), "%"+customercodeParam+"%"));
                }
                if (StringUtils.isNotBlank(regamtcrenyParam)){
                    predicates.add(criteriaBuilder.like(root.get("regamtcreny"), "%"+regamtcrenyParam+"%"));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xftykhxbl> all = xftykhxblService.findAll(specification, pageRequest);
        List<Xftykhxbl> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportXftykhxbl",produces = "text/plain;charset=UTF-8")
    public void XimmportXftykhxbl(HttpServletRequest request, HttpServletResponse response){
    	sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                List<String> valueList = XfieldEnum.Xftykhxbl.getCodeToList();
                List<String> headerList = XfieldEnum.Xftykhxbl.getMessageToList();
                String[] headers = headerList.toArray(new String[headerList.size()]);
                String[] values = valueList.toArray(new String[valueList.size()]);
                String[] amountFields = {"regamt","workarea","actamt","creditamt","usedamt","totalamt"};
                String[] dateFields = {"fistdate", "setupdate"};
                List<Xftykhxbl> xftykhxblList = new ArrayList<>();
                XCommonExcel<Xftykhxbl> commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xftykhxbl(), headers, values, amountFields,dateFields, xftykhxblList,null);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete("xftykhxbl",departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                    commonService.deleteRepeatData("Xftykhxbl","customername");
                    //countUtil.handleCount("xftykhx",0,"add",commonExcel.count);//更新数量
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
