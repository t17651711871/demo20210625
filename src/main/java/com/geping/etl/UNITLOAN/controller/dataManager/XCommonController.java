package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XftykhxblService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XgetterAndSetter;
import com.geping.etl.UNITLOAN.util.excelutil.ExcelJDBCExport;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class XCommonController {

    @Autowired
    private XcommonService commonService;

    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private XftykhxblService ftykhxblService;
    
    @Autowired
    private DepartUtil departUtil;

    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    //新增/修改
    @PostMapping(value = "Xsave*")
    public void XadditionalRecord(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        response.setCharacterEncoding("utf-8");
        Sys_UserAndOrgDepartment user = (Sys_UserAndOrgDepartment) session.getAttribute("sys_User");
        JSONObject jsonObject = new JSONObject();
        PrintWriter writer = null;
        try {
            writer =  response.getWriter();
            String uri = request.getRequestURI();
            String className = uri.substring(uri.lastIndexOf("/") + 6);
            Class<?> clazz = Class.forName("com.geping.etl.UNITLOAN.entity.report." + className);
            Object o = clazz.newInstance();

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (StringUtils.isNotBlank(request.getParameter(field.getName()))){
                    XgetterAndSetter.setter(o,field.getName(),request.getParameter(field.getName()),String.class);
                }
            }
            if (StringUtils.isBlank(request.getParameter("id"))){
                XgetterAndSetter.setter(o,"id",Stringutil.getUUid(),String.class);
            }
            if (!"Xftykhxbl".equals(className)){
                XgetterAndSetter.setter(o,"datastatus","0",String.class);
                XgetterAndSetter.setter(o,"checkstatus","0",String.class);
                XgetterAndSetter.setter(o,"operationname"," ",String.class);
                XgetterAndSetter.setter(o,"nopassreason"," ",String.class);
            }
            XgetterAndSetter.setter(o,"operator",user.getLoginid(),String.class);
            XgetterAndSetter.setter(o,"orgid",user.getOrgid(),String.class);
            XgetterAndSetter.setter(o,"departid",user.getDepartid(),String.class);
            XgetterAndSetter.setter(o,"operationtime",format.format(new Date()),String.class);
            commonService.save(o);
            /*if (StringUtils.isBlank(request.getParameter("id"))){
                countUtil.handleCount(className.toLowerCase(),0,"add",1);//更新数量
            }*/
            jsonObject.put("status", "1");
        } catch (Exception e) {
            jsonObject.put("status", "0");
            e.printStackTrace();
        }finally {
            writer.write(jsonObject.toString());
            writer.flush();
        }
    }
    
    //个人新增/修改
    @PostMapping(value = "Xssave*")
    public void XadditionalRecord2(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        response.setCharacterEncoding("utf-8");
        Sys_UserAndOrgDepartment user = (Sys_UserAndOrgDepartment) session.getAttribute("sys_User");
        JSONObject jsonObject = new JSONObject();
        PrintWriter writer = null;
        try {
            writer =  response.getWriter();
            String uri = request.getRequestURI();
            String className = uri.substring(uri.lastIndexOf("/") + 7);
            Class<?> clazz = Class.forName("com.geping.etl.UNITLOAN.entity.report." + className);
            Object o = clazz.newInstance();

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (StringUtils.isNotBlank(request.getParameter(field.getName()))){
                	if("id".equals(field.getName())) {
                		XgetterAndSetter.setter(o,field.getName(),Integer.valueOf(request.getParameter(field.getName())),Integer.class);
                	}else {
                		XgetterAndSetter.setter(o,field.getName(),request.getParameter(field.getName()),String.class);
                	}                   
                }
            }
            if (StringUtils.isBlank(request.getParameter("id"))){
            	int id = commonService.getMaxId(className)+1;
            	int idh = commonService.getMaxId(className+"h")+1;
            	int idi = idh >= id?idh:id;
                XgetterAndSetter.setter(o,"id",idi,Integer.class);
            }
            if (!"Xftykhxbl".equals(className)){
                XgetterAndSetter.setter(o,"datastatus","0",String.class);
                XgetterAndSetter.setter(o,"checkstatus","0",String.class);
                XgetterAndSetter.setter(o,"operationname"," ",String.class);
                XgetterAndSetter.setter(o,"nopassreason"," ",String.class);
            }
            XgetterAndSetter.setter(o,"operator",user.getLoginid(),String.class);
            XgetterAndSetter.setter(o,"orgid",user.getOrgid(),String.class);
            XgetterAndSetter.setter(o,"departid",user.getDepartid(),String.class);
            XgetterAndSetter.setter(o,"operationtime",format.format(new Date()),String.class);
            commonService.save(o);
            /*if (StringUtils.isBlank(request.getParameter("id"))){
                countUtil.handleCount(className.toLowerCase(),0,"add",1);//更新数量
            }*/
            jsonObject.put("status", "1");
        } catch (Exception e) {
            jsonObject.put("status", "0");
            e.printStackTrace();
        }finally {
            writer.write(jsonObject.toString());
            writer.flush();
        }
    }

    //删除
    @PostMapping(value = "XDelete*")
    public Integer XdeleteData(HttpServletRequest request, @RequestParam("datastatus") String datastatus, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 8);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.deleteDataById(className,id);
        }else {
            commonService.deleteDataAllWhithOutDatastatus(className,param,handlename,orgId,departId);
        }
        //countUtil.handleCount(className.toLowerCase(),0,"reduce",1);//更新数量
        return 0;
    }

    //提交
    @PostMapping(value = "Xsubmit*")
    public Integer XsubmitData(HttpServletRequest request, @RequestParam("datastatus") String datastatus, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 8);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.submitDataById(className,id,handlename,format.format(new Date()));
        }else {
            commonService.submitDataAll(className,param,handlename,orgId,datastatus,departId,format.format(new Date()));
        }
        return 0;
    }

    //导出
    @GetMapping(value = "XExport*")
    public void XexportToExcel(HttpServletRequest request, HttpServletResponse response) {
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String datastatus = request.getParameter("datastatus");
        XCommenFileOut.fileOutToExcel(response,request, commonService,datastatus,departId);
    }
    
    //个人报表导出
    @GetMapping("XEExport*")
    public void excelExprort(HttpServletRequest request, HttpServletResponse response) {
    	OutputStream out = null;
        Workbook workbook = null;
        Connection connection = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
        	long a = System.currentTimeMillis();
        	
        	Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
        	String departId = departUtil.getDepart(sys_user);
            
            Map<String,String> headers = new LinkedHashMap<>();
            String url = request.getRequestURI();
            String fileName = "";
            String className = url.substring(url.lastIndexOf("/") + 9);
            if ("Xgrkhxx".equals(className)||"Xgrkhxxh".equals(className)){
            	fileName = "个人客户基础信息.xlsx";
                headers = XfieldEnum.Xgrkhxx.getAllEnumToMap();
            }else if("Xclgrdkxx".equals(className)||"XclgrdkxxH".equals(className)) {
            	fileName = "存量个人贷款信息.xlsx";
                headers = XfieldEnum.Xclgrdkxx.getAllEnumToMap();
            }else if("Xgrdkfsxx".equals(className)||"XgrdkfsxxH".equals(className)) {
            	fileName = "个人贷款发生额信息.xlsx";
                headers = XfieldEnum.Xgrdkfsxx.getAllEnumToMap();
            }
            
        	workbook = new SXSSFWorkbook();
            response.setContentType("octets/stream");
            response.setHeader("Content-Type","application/msexcel");
            response.setContentType("application/octet-stream;charset=UTF-8;");
            response.setHeader("Content-Disposition", "attachment;filename="+new String(fileName.getBytes("gb2312"),"ISO-8859-1"));
        
            
            
            connection = JDBCUtils.getConnection();
            String sql = "select * from "+className+" where 1=1";
            
            Enumeration<String> enumeration = request.getParameterNames();
            Map<String,String> param = new HashMap<>();
            while (enumeration.hasMoreElements()){
                String key = enumeration.nextElement();
                if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                    param.put(key.substring(0,key.length() - 5),request.getParameter(key));
                }
            }
            String datastatus = request.getParameter("datastatus");
            String orgId= sys_user.getOrgid();
            param.put("departid",departId);
            param.put("orgid",orgId);
            String id = request.getParameter("id");
            if (StringUtils.isNotBlank(id)){
                id = id.substring(0,id.length()-1);
                id = "'" + id.replace(",","','")  + "'";
                param.put("id",id);
            }
            if (StringUtils.isNotBlank(datastatus)){
                param.put("datastatus",datastatus);
            }
            for (Map.Entry<String,String> entry : param.entrySet()){
                if ("id".equals(entry.getKey())){
                    sql = sql + " and id in ("+entry.getValue()+")";
                    continue;
                }
                sql = sql + " and "+entry.getKey() + " like '%"+entry.getValue()+"%'";
            }
            ps = connection.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            
            ExcelJDBCExport.createSheet(headers, workbook, "sheet", rs);
        
            out = response.getOutputStream();
            workbook.write(out);
            long b = System.currentTimeMillis();
            System.out.println("导出用时："+(b-a)/1000+"秒");
        }catch(Exception e) {
        	
        }finally {
        	if(rs != null) {
        		try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        	if(ps != null) {
        		try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        	JDBCUtils.close();
        }
    }

    //申请删除
    @PostMapping(value = "XApplyDelete*")
    public Integer XApplyDeleteData(HttpServletRequest request, @RequestParam("datastatus") String datastatus, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 13);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.applyDataById(className,id,handlename,handledate,"申请删除");
        }else {
            commonService.applyDataAll(className,param,handlename,orgId,datastatus,handledate,"申请删除",departId);
        }
        return 0;
    }

    //申请修改
    @PostMapping(value = "XApplyEdit*")
    public Integer XApplyEditData(HttpServletRequest request, @RequestParam("datastatus") String datastatus, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 11);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.applyDataById(className,id,handlename,handledate, "申请补足");
        }else {
            commonService.applyDataAll(className,param,handlename,orgId,datastatus,handledate, "申请补足",departId);
        }
        return 0;
    }

    //同意申请
    @PostMapping(value = "XAgreeApply*")
    public Integer XAgreeApplyData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 12);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        List<SUpOrgInfoSet> list = suisService.findAll();
        boolean shifoushenheziji="yes".equals(list.get(0).getShifoushenheziji())?true:false;
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            //commonService.agreeApplyDataById(className,id,handlename,handledate);
            commonService.deleteDataById(className,id);
        }else {
            commonService.agreeApplyDataAll(className,param,handlename,orgId,handledate,departId,shifoushenheziji);
        }
        //commonService.updateReportInfo(className);
        return 0;
    }

    //拒绝申请
    @PostMapping(value = "XNoAgreeApply*")
    public Integer XNoAgreeApplyData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 14);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        List<SUpOrgInfoSet> list = suisService.findAll();
        boolean shifoushenheziji="yes".equals(list.get(0).getShifoushenheziji())?true:false;
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.noAgreeApplyDataById(className,id,handlename,handledate);
        }else {
            commonService.noAgreeApplyDataAll(className,param,handlename,orgId,handledate,departId,shifoushenheziji);
        }
        return 0;
    }

    //审核通过
    @PostMapping(value = "XAgreeAudit*")
    public Integer XAgreeAuditData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 12);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        List<SUpOrgInfoSet> list = suisService.findAll();
        boolean shifoushenheziji="yes".equals(list.get(0).getShifoushenheziji())?true:false;
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.agreeAuditDataById(className,id,handlename,handledate);
        }else {
            commonService.agreeAuditDataAll(className,param,handlename,orgId,handledate,departId,shifoushenheziji);
        }
        //commonService.updateReportInfo(className);
        return 0;
    }

    //审核不通过
    @PostMapping(value = "XNoAgreeAudit*")
    public Integer XNoAgreeAuditData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String reason = request.getParameter("reason");
        String className = uri.substring(uri.lastIndexOf("/") + 14);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        List<SUpOrgInfoSet> list = suisService.findAll();
        boolean shifoushenheziji="yes".equals(list.get(0).getShifoushenheziji())?true:false;
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.noAgreeAuditDataById(className,id,handlename,handledate,reason);
        }else {
            commonService.noAgreeAuditDataAll(className,param,handlename,orgId,handledate,reason,departId,shifoushenheziji);
        }
        //commonService.updateReportInfo(className);
        return 0;
    }

    //根据客户号查询信息
    @PostMapping(value = "XgetInfoByCustomernum")
    public Object XgetInfoByCustomernum(HttpServletRequest request){
        String customernum = request.getParameter("customernum");
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
        String orgId= sys_user.getOrgid();
        String departId = departUtil.getDepart(sys_user);
        List<Xftykhxbl> xftykhxblList =  ftykhxblService.getInfoByCustomernum(customernum,orgId,departId);
        if (xftykhxblList.size() > 0){
            return xftykhxblList.get(0);
        }
        return null;
    }

    //打回
    @PostMapping(value = "Xgoback*")
    public Integer XgobackData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 8);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.gobackDataById(className,id,handlename,handledate);
        }else {
            commonService.ngobackDataAll(className,param,handlename,orgId,handledate,departId);
        }
        //commonService.updateReportInfo(className);
        return 0;
    }

    //同步数据
    @PostMapping(value = "XsynData*")
    public Integer XsynData(HttpServletRequest request, HttpSession session) {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> param = new HashMap<>();
        while (enumeration.hasMoreElements()){
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                param.put(key.substring(0,key.length() - 5),request.getParameter(key));
            }
        }
        String uri = request.getRequestURI();
        String className = uri.substring(uri.lastIndexOf("/") + 9);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String orgId= sys_user.getOrgid();
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            commonService.synDataById(className,id);
        }else {
            commonService.synDataAll(className,param,handlename,orgId,departId);
        }
        //countUtil.handleCount(className.toLowerCase(),0,"reduce",1);//更新数量
        return 0;
    }
    
    //历史数据打回
    @PostMapping(value = "Xdataback*")
    public Integer Xdataback(HttpServletRequest request, HttpSession session) {
        String uri = request.getRequestURI();
        String tableName = uri.substring(uri.lastIndexOf("/") + 10);
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String handlename = sys_user.getLoginid();
        String handledate = format.format(new Date());
        String orgId= sys_user.getOrgid();
        String date  = request.getParameter("date");
        int result = 0;
        result = commonService.moveToTemporaryTable(tableName,handlename,handledate,date,orgId,departId);
        //commonService.updateReportInfo(className);
        return result;
    }
}
