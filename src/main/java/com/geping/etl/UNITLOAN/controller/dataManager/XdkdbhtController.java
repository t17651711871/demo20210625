package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbht;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XdkdbhtService;
import com.geping.etl.UNITLOAN.service.report.XftykhxblService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: wangzd
 * @Date: 17:03 2020/6/9
 */
@RestController
public class XdkdbhtController {

    @Autowired
    private XdkdbhtService xdkdbhtService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XftykhxblService ftykhxblService;

    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;
    
    @Autowired
    private XCheckRuleService xCheckRuleService;
    
    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;
    
    private final String tableName="xdkdbht";

    private final String fileName="单位贷款担保合同信息";

    private static String target="";
    //查询数据
    @PostMapping("XGetXdkdbhtData")
    public ResponseResult XGetXdkdbhtData(int page, int rows,String gteecontractcodeParam,String loancontractcodeParam,String operationnameParam,String checkstatusParam,String datastatus){
        page = page - 1;
        sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(gteecontractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%"+gteecontractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loancontractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%"+loancontractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xdkdbht> all = xdkdbhtService.findAll(specification, pageRequest);
        List<Xdkdbht> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelThree",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelThree(HttpServletRequest request,HttpServletResponse response){
    	sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        long a = System.currentTimeMillis();
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"担保合同编码","被担保合同编码","担保合同类型","担保合同起始日期","担保合同到期日期",
                        "币种", "担保合同金额", "担保合同金额折人民币", "担保人证件类型", "担保人证件代码","金融机构代码",
                        "内部机构号","交易类型", "抵质押率", "担保人国民经济部门", "担保人行业", "担保人地区代码",
                        "担保人企业规模","数据日期"
                };
                String[] values = {"gteecontractcode","loancontractcode","gteecontracttype","gteestartdate",
                        "gteeenddate","gteecurrency","gteeamount","gteecnyamount","gteeidtype","gteeidnum",
                        "financeorgcode","financeorginnum", "transactiontype","pledgerate","isgreenloan","brrowerindustry",
                        "brrowerareacode","enterprisescale","sjrq"
                };
                String[] amountFields = {"gteeamount","gteecnyamount","pledgerate"};
                String[] dateFields = {"gteestartdate", "gteeenddate","sjrq"};
                List<Xftykhxbl> xftykhxblList = new ArrayList<>();
                XCommonExcel<Xdkdbht> commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xdkdbht(),headers,values, amountFields,dateFields, xftykhxblList,null);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                    //countUtil.handleCount("xdkdbht",0,"add",commonExcel.count);//更新数量
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            long b = System.currentTimeMillis();
            System.out.println("导入用时： "+(b-a)/1000+"秒");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataThree")
    public void XCheckDataThree(HttpServletRequest request, HttpServletResponse response,String gteecontractcodeParam,String loancontractcodeParam){
    	long a = System.currentTimeMillis();
    	//记录是否有错
        boolean b=false;
        sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStr2=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStr2 = whereStr2 + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(gteecontractcodeParam)){
                whereStr = whereStr + " and gteecontractcode like '%"+ gteecontractcodeParam +"%'";
                whereStr2 = whereStr2 + " and a.gteecontractcode like '%"+ gteecontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStr2 = whereStr2 + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStr2 = whereStr2 + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from "+tableName+" "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;

        try {
        	connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
        
        //标识名称
        String errorcode = "担保合同编码";
        String sqlexist;
        String errorinfo;
        
        
//        	//若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
//            sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and (gteeidnum is null or ltrim(gteeidnum) = '') and "
//            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' ')) from xdkdbht where operationname != '申请删除' group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having (gteeidnum is null or ltrim(gteeidnum) = '') and count(1) > 1)";
//            errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一";
//            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//            
//            //若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
//            sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and (gteeidnum is null or ltrim(gteeidnum) = '') and "
//            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' ')) from xdkdbhth group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having (gteeidnum is null or ltrim(gteeidnum) = '') and count(1) > 1)";
//            errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一（历史表）";
//            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//            
//            //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
//            sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and gteeidnum is not null and ltrim(gteeidnum) <> '' and "
//            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '),ifnull(gteeidnum,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '),ifnull(gteeidnum,' ')) from xdkdbht where operationname != '申请删除' group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having gteeidnum is not null and ltrim(gteeidnum) <> '' and count(1) > 1)";
//            errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一";
//            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//            
//            //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
//            sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and gteeidnum is not null and ltrim(gteeidnum) <> '' and "
//            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '),ifnull(gteeidnum,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(financeorgcode,' '),ifnull(gteecontractcode,' '),ifnull(loancontractcode,' '),ifnull(gteeidnum,' ')) from xdkdbhth group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having gteeidnum is not null and ltrim(gteeidnum) <> '' and count(1) > 1)";
//            errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一（历史表）";
//            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            /*
            //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xjrjgfz b on "
            		+ "a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "
            		+ whereStr2 + " and a.financeorgcode <> b.finorgcode";
            errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码不一致";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            */
            /*
            //担保物信息不为空，则担保合同信息不能为空
            sqlexist = "";
            errorinfo = "担保物信息不为空，则担保合同信息不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            */
        List<String> checknumList = xCheckRuleService.getChecknum("xdkdbht");
        if(xDataBaseTypeUtil.equalsSqlServer()) {
        	if(checknumList.contains("JS2624")) {
                //若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
                sqlexist = "select t1.id id,t1.gteecontractcode gteecontractcode from xdkdbht t1 "+whereStr2.replace("a.", "t1.")+" and ltrim(t1.gteeidnum) is null and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode from xdkdbht where operationname != '申请删除' and ltrim(gteeidnum) is null group by sjrq,financeorgcode,gteecontractcode,loancontractcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode)";
                errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
                sqlexist = "select t1.id id,t1.gteecontractcode gteecontractcode from xdkdbht t1 "+whereStr2.replace("a.", "t1.")+" and ltrim(t1.gteeidnum) is null and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode from xdkdbhth where ltrim(gteeidnum) is null group by sjrq,financeorgcode,gteecontractcode,loancontractcode having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode)";
                errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
        	if(checknumList.contains("JS2303")) {
                //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
                sqlexist = "select t1.id id,t1.gteecontractcode gteecontractcode from xdkdbht t1 "+whereStr2.replace("a.", "t1.")+" and ltrim(t1.gteeidnum) is not null and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum from xdkdbht where ltrim(gteeidnum) is not null group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode and t1.gteeidnum = t2.gteeidnum)";
                errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
                sqlexist = "select t1.id id,t1.gteecontractcode gteecontractcode from xdkdbht t1 "+whereStr2.replace("a.", "t1.")+" and ltrim(t1.gteeidnum) is not null and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum from xdkdbhth where ltrim(gteeidnum) is not null group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode and t1.gteeidnum = t2.gteeidnum)";
                errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
        }else {
        	if(checknumList.contains("JS2624")) {
                //若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
                sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and ltrim(gteeidnum) is null and "
                		+ "(sjrq,financeorgcode,gteecontractcode,loancontractcode) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode from xdkdbht where operationname != '申请删除' and ltrim(gteeidnum) is null group by sjrq,financeorgcode,gteecontractcode,loancontractcode having count(1) > 1)";
                errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一
                sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and ltrim(gteeidnum) is null and "
                		+ "(sjrq,financeorgcode,gteecontractcode,loancontractcode) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode from xdkdbhth where ltrim(gteeidnum) is null group by sjrq,financeorgcode,gteecontractcode,loancontractcode having count(1) > 0)";
                errorinfo = "若担保人证件代码为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码应唯一（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
        	if(checknumList.contains("JS2303")) {
                //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
                sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and ltrim(gteeidnum) is not null and "
                		+ "(sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum from xdkdbht where operationname != '申请删除' group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having ltrim(gteeidnum) is not null and count(1) > 1)";
                errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一
                sqlexist = "select id,gteecontractcode from xdkdbht "+whereStr+" and ltrim(gteeidnum) is not null and "
                		+ "(sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum from xdkdbhth group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteeidnum having ltrim(gteeidnum) is not null and count(1) > 0)";
                errorinfo = "若担保人证件代码不为空，则数据日期+金融机构代码+担保合同编码+被担保合同编码+担保人证件代码应唯一（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
        }
               
        if(xDataBaseTypeUtil.equalsMySql()) {
        	if(checknumList.contains("JS1956")) {
            //存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除' group by sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.productcetegory like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '01'";
            errorinfo = "存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxxh group by sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.productcetegory like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '01'";
            errorinfo = "存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
            if(checknumList.contains("JS1959")) {
            //单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除' group by sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.productcetegory not like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '02'";
            errorinfo = "单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxxh group by sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.productcetegory not like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '02'";
            errorinfo = "单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }
            if(checknumList.contains("JS2645")) {
            //当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除' group by financeorginnum,sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.guaranteemethod like 'C%' and (a.gteeidtype is null or ltrim(a.gteeidtype) = '')";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxxh group by financeorginnum,sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.guaranteemethod like 'C%' and (a.gteeidtype is null or ltrim(a.gteeidtype) = '')";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }
            if(checknumList.contains("JS2646")) {
            //当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除' group by financeorginnum,sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.guaranteemethod like 'C%' and (a.gteeidnum is null or ltrim(a.gteeidnum) = '')";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空
            sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxxh group by financeorginnum,sjrq,contractcode) b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.guaranteemethod like 'C%' and (a.gteeidnum is null or ltrim(a.gteeidnum) = '')";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }
        }else {       	
        	if(checknumList.contains("JS1956")) {
            //存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除') b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.productcetegory like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '01'";
            errorinfo = "存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xcldkxxh b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.productcetegory like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '01'";
            errorinfo = "存量单位贷款信息中非金融机构买入返售业务对应的担保合同信息的交易类型应该为01-回购类交易（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1959")) {
            //单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除') b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.productcetegory not like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '02'";
            errorinfo = "单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xcldkxxh b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.productcetegory not like 'F06%' and a.loancontractcode not like '%,%' and a.transactiontype <> '02'";
            errorinfo = "单位贷款业务对应的担保合同信息的交易类型应该为02-信贷交易（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS2645")) {
            //当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除') b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.guaranteemethod like 'C%' and ltrim(a.gteeidtype) is null";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xcldkxxh b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.guaranteemethod like 'C%' and ltrim(a.gteeidtype) is null";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中的担保人证件类型不为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS2646")) {
            //当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join (select * from xcldkxx where operationname != '申请删除') b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.operationname != '申请删除' and b.guaranteemethod like 'C%' and ltrim(a.gteeidnum) is null";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空
            sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xcldkxxh b on "
            		+ "a.sjrq = b.sjrq and a.loancontractcode = b.contractcode "
            		+ whereStr2 + " and b.guaranteemethod like 'C%' and ltrim(a.gteeidnum) is null";
            errorinfo = "当单位贷款的担保方式为保证贷款时，担保合同信息中担保人证件代码不为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        }
        
        if(xDataBaseTypeUtil.equalsSqlServer()) {
        	//担保合同金额折人民币一般应在1000元至1000亿元范围内
            if(checknumList.contains("JS2776")) {
            	sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a "+whereStr2+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode from xdkdbht where ltrim(gteecnyamount) is null group by sjrq,financeorgcode,gteecontractcode having sum(cast(gteecnyamount as bigint)) < 1000 or sum(cast(gteecnyamount as bigint)) > 100000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode)";
            	errorinfo = "担保合同金额折人民币一般应在1000元至1000亿元范围内";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }
        }else {
        	//担保合同金额折人民币一般应在1000元至1000亿元范围内
            if(checknumList.contains("JS2776")) {
            	sqlexist = "select a.id id,a.gteecontractcode gteecontractcode from xdkdbht a "+whereStr2+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode from xdkdbht where ltrim(gteecnyamount) is null group by sjrq,financeorgcode,gteecontractcode having sum(gteecnyamount) < 1000 or sum(gteecnyamount) > 100000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode)";
            	errorinfo = "担保合同金额折人民币一般应在1000元至1000亿元范围内";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }
        }
        
        
        
//        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
//        sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xjrjgfz b on a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "
//        		+ whereStr2 + " and a.financeorgcode <> b.finorgcode";
//        errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        
//        //金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在
//        sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a "
//        		+ whereStr2 + " and not exists(select 1 from xjrjgfrbaseinfo b where a.sjrq = b.sjrq and a.financeorgcode = b.finorgcode) and not exists(select 1 from xjrjgfz b where a.sjrq = b.sjrq and a.financeorgcode = b.finorgcode) and not exists(select 1 from xjrjgfrbaseinfoh b where a.sjrq = b.sjrq and a.financeorgcode = b.finorgcode) and not exists(select 1 from xjrjgfzh b where a.sjrq = b.sjrq and a.financeorgcode = b.finorgcode)";
//        errorinfo = "金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在";
//        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        
//        //若报送机构存在分支机构，则内部机构号必须在金融机构（分支机构）基础信息表.内部机构号中存在
//        sqlexist = "select distinct a.id id,a.gteecontractcode gteecontractcode from xdkdbht a inner join xjrjgfz b on a.sjrq = b.sjrq "
//        		+ whereStr2 + " and a.financeorginnum <> b.inorgnum";
//        errorinfo = "若报送机构存在分支机构，则内部机构号必须在金融机构（分支机构）基础信息表.内部机构号中存在";
//        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        
            resultSet = JDBCUtils.Query(connection, sql);
            while (true) {
                if (!resultSet.next()) break;
                Xdkdbht xdkdbht = new Xdkdbht();
                Stringutil.getEntity(resultSet, xdkdbht);
                CheckAllData3.checkXdkdbht(customSqlUtil, xdkdbht, errorMsg, errorId, rightId,checknumList);
            }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData3.fzList != null) {
            	CheckAllData3.fzList.clear();
            	CheckAllData3.fzList = null;
            }
            if(CheckAllData3.frList != null) {
            	CheckAllData3.frList.clear();
            	CheckAllData3.frList = null;
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
        	StringBuffer errorMsgAll = new StringBuffer("");
        	for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
        		errorMsgAll.append(entry.getValue()+"\r\n");
        	}
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
//            errorId.forEach(errId->{
//                whereMap.put("id",errId);
//                customSqlUtil.updateByWhere(tableName,setMap,whereMap);
//            });
            customSqlUtil.updateByWhereBatch(tableName, setMap, errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
//            rightId.forEach(rigId->{
//                whereMap.put("id",rigId);
//                customSqlUtil.updateByWhere(tableName,setMap,whereMap);
//            });
            customSqlUtil.updateByWhereBatch(tableName, setMap, rightId);
            rightId.clear();
        }

        //校验结束之后 没有写审核-暂时在这里修改状态为 审核通过
        /*setMap.put("datastatus","3");
        setMap.put("operationname","");
        whereMap.put("datastatus","0");
        whereMap.put("checkstatus","1");
        int xdkdbht1 = customSqlUtil.updateByWhere(tableName, setMap, whereMap);
        Map<Integer,String> countMap=new HashMap<>();
        countMap.put(0,"reduce");
        countMap.put(3,"add");
        countUtil.handleMoreCount(tableName,countMap,xdkdbht1);*/
        customSqlUtil.saveLog("单位贷款担保合同校验成功","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckThree")
    public void XDownLoadCheckThree(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    public void executeCheckSql(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo) {
    	List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            exist.forEach(errId->{
                errorId.add(String.valueOf(errId[0]));
                whereMap.put("id", String.valueOf(errId[0]));
                //customSqlUtil.updateByWhere(tableName,setMap,whereMap);
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                	errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"]->\r\n");
                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
            });
            whereMap.clear();
            setMap.clear();
            exist.clear();
        }
    }
}
