package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCheckRuleExcel;
import com.geping.etl.UNITLOAN.entity.report.XCheckRule;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.JDBCUtils;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.stream.Collectors;

/**    
*  
* @author liuweixin  
* @date 2020年9月21日 上午10:21:28  
*/
@RestController
public class XCheckRuleController {
	@Autowired
	private XCheckRuleService xCheckRuleService;
	
	@Autowired
    private ExcelUploadUtil excelUploadUtil;
	
	@Autowired
    private CustomSqlUtil customSqlUtil;
	
	private Sys_UserAndOrgDepartment sys_user;

	//跳转到数据币种页面
    @GetMapping(value = "/XCheckRuleUi")
    public ModelAndView XCurrencyUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        List<String> reportnameList = xCheckRuleService.getReportnameList();
        ModelAndView model = new ModelAndView();
        model.addObject("reportnameList",reportnameList);
        model.setViewName("shgjg/uporginfoset/xcheckrule");
        return model;
    }
    
    //导入
    @PostMapping(value = "XCheckRuleImport",produces = "text/plain;charset=UTF-8")
	public void XCheckRuleImport(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		PrintWriter out = null;
		File uploadFile = null;
        OPCPackage opcPackage=null;
		try {
			
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			uploadFile = excelUploadUtil.uploadFile(file);
            opcPackage = OPCPackage.open(uploadFile);
			String[] headers = {"检核标准编号","报文名称","字段名称","规则说明","涉及的表",
                    "规则实现逻辑", "限定条件", "关联关系", "是否启用"
            };
            String[] values = {"checknum","reportname","fieldname","checkrule",
                    "tablesj","rulesx","rulelimit","rulegl","isstart"
            };
            String[] amountFields = null;
            String[] dateFields = null;
            Map<String,String> tablenameMap = new HashMap<String,String>();
            tablenameMap = Arrays.asList("存量单位贷款信息:xcldkxx","担保合同信息:xdkdbht","单位贷款发生额信息:xdkfsxx","担保物信息:xdkdbwx","非同业单位客户基础信息:xftykhx","金融机构（法人）基础信息-基本情况统计表:xjrjgfrbaseinfo","金融机构（法人）基础信息-资产负债及风险统计表:xjrjgfrassets","金融机构（法人）基础信息-利润及资本统计表:xjrjgfrprofit","金融机构（分支机构）基础信息:xjrjgfz","个人客户基础信息:xgrkhxx","存量个人贷款信息:xclgrdkxx"
            							,"个人贷款发生额信息:xgrdkfsxx","存量委托贷款信息:xclwtdkxx","委托贷款发生额信息:xwtdkfse","存量同业借贷信息:xcltyjdxx","同业借贷发生额信息:xtyjdfsexx","同业客户基础信息:xtykhjcxx","存量同业存款信息:xcltyckxx","同业存款发生额信息:xtyckfsexx","存量债券投资信息:xclzqtzxx","债券投资发生额信息:xzqtzfsexx","存量债券发行信息:xclzqfxxx","债券发行发生额信息:xzqfxfsexx"
										,"存量股权投资信息:xclgqtzxx","股权投资发生额信息:xgqtzfsexx","存量特定目的载体投资信息:xcltdmdzttzxx","特定目的载体投资发生额信息:xtdmdzttzfsexx")
            		.stream().map(ele->ele.split(":"))
            		.collect(Collectors.toMap(e->e[0], e->e[1]));
            XCheckRuleExcel commonExcel = new XCheckRuleExcel(sys_user, opcPackage, customSqlUtil, headers,values, amountFields,dateFields, null,tablenameMap);
            xCheckRuleService.deleteAll();
            commonExcel.process(1);
            json.put("msg","导入成功");
            out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				opcPackage.close();
				if (out != null) {
					out.flush();
					out.close();
				}
				if(uploadFile.exists()) {
					uploadFile.delete();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
    
    //显示数据
    @PostMapping(value = "XCheckRuleList")
    public ResponseResult getXCheckRuleList(HttpServletRequest request,HttpServletResponse response) {
    	try {
    		request.setCharacterEncoding("UTF-8");
    		response.setContentType("text/html");
    		response.setContentType("text/plain; charset=utf-8");
    		
    		String currentPageNumber = request.getParameter("page"); // 获取查询页数
			String currentPageSize = request.getParameter("rows"); // 获取每页条数
			
			String reportname = request.getParameter("reportname");
			String fieldname = request.getParameter("fieldname");
			String status = request.getParameter("status");

			int page = Integer.valueOf(currentPageNumber) - 1;
			int size = Integer.valueOf(currentPageSize);
    		
    		Specification specification = new Specification() {
    			@Override
    			public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
    				List<Predicate> predicates = new ArrayList<>();
    				if(StringUtils.isNotBlank(reportname)) {
    					predicates.add(criteriaBuilder.equal(root.get("reportname"), reportname));
    				}
    				if(StringUtils.isNotBlank(fieldname)) {
    					predicates.add(criteriaBuilder.equal(root.get("fieldname"), fieldname));
    				}
    				if(StringUtils.isNotBlank(status)) {
    					predicates.add(criteriaBuilder.equal(root.get("isselect"), status));
    				}
    				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    			}
    		};
    		Sort sort = new Sort(Sort.Direction.ASC, "tablename","fieldname");
    		
    		PageRequest pageRequest = new PageRequest(page, size, sort);
    		Page<XCheckRule> all = xCheckRuleService.findAll(specification, pageRequest);
    		List<XCheckRule> content = all.getContent();
    		long totalCount = all.getTotalElements();
    		return ResponseResult.success(totalCount, content);
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
		return null;
		
    }
	
    //保存配置
    @RequestMapping(value="XSavePeiZhi",method=RequestMethod.POST)
	public void DCSaveFieldSet(HttpServletResponse response,HttpServletRequest request) {
		PrintWriter out = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			JDBCUtils jdbc = new JDBCUtils();
		    conn = jdbc.getConnection();
			
		    String idstr = request.getParameter("idstr");
		    String selectidstr = request.getParameter("selectidstr");
		    String reportname = request.getParameter("reportname");
		    String fieldname = request.getParameter("fieldname");
		    String status = request.getParameter("status");
		    
			StringBuffer noselectsql = new StringBuffer("update xcheckrule set isselect='n' where 1=1");
		    StringBuffer yesselectsql = new StringBuffer("update xcheckrule set isselect='y' where 1=1");
		    
//			if(reportname != null) {
//				if(!reportname.equals("")) {
//					yesselectsql.append(" and reportname = '"+reportname+"'");
//					noselectsql.append(" and reportname = '"+reportname+"'");
//				}
//			}
//			
//			if(fieldname != null) {
//				if(!fieldname.equals("")) {
//					yesselectsql.append(" and fieldname = '"+fieldname+"'");
//					noselectsql.append(" and fieldname = '"+fieldname+"'");
//				}
//			}
			
//			if(status != null) {
//				if(!status.equals("")) {
//					yesselectsql.append(" and isselect = '"+status+"'");
//					noselectsql.append(" and isselect = '"+status+"'");
//				}
//			}
			
			if(StringUtils.isNotBlank(idstr)) {
				noselectsql.append(" and id in ");
				noselectsql.append(idstr);
				ps = conn.prepareStatement(noselectsql.toString());
				ps.executeUpdate();
			}
			
			if(StringUtils.isNotBlank(selectidstr)) {
				yesselectsql.append(" and id in ");
				yesselectsql.append(selectidstr);
				ps = conn.prepareStatement(yesselectsql.toString());
				ps.executeUpdate();
			}
			
			ps.close();
			conn.close();
			
			out = response.getWriter();
			out.write("0");
		} catch (Exception e) {
			e.printStackTrace();
			out.write("1");
		}finally {
			if(out != null) {
				out.flush();
				out.close();
			}
		}
		
	}
    
    //根据报文名获取字段名
    @PostMapping(value = "/XgetFieldnameJson",produces = "application/json;charset=UTF-8")
	public List<Map<String,String>> XgetFieldnameJson(String reportname){
    	 List<String> list = xCheckRuleService.getFieldnameList(reportname);
    	 List<Map<String,String>> list2 = new ArrayList<Map<String,String>>();
    	 for(String s : list) {
    		 Map<String,String> map = new HashMap<String,String>();
    		 map.put("id", s);
    		 map.put("text", s);
    		 list2.add(map);
    	 }
    	 return list2;
    }
}
