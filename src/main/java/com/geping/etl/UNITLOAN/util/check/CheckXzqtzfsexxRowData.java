package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xzqtzfsexx;
import com.geping.etl.UNITLOAN.util.CheckDataUtils;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXzqtzfsexxRowData implements CheckRowData {

    @Autowired
    private CustomSqlUtil customSqlUtil;

    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {

        //债券投资发生额信息
        Xzqtzfsexx xzqtzfsexx = (Xzqtzfsexx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String> checknumList = checkParamContext.getCheckNums();
        //发行人经济成分
        if (checkXzqtzfsexxRowZqfstze(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //成交金额折人民币
        if (checkXzqtzfsexxRowCjjezrmb(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人企业规模
        if (checkXzqtzfsexxRowFxrqygm(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人国民经济部门
        if (checkXzqtzfsexxRowFxrgmjjbm(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人证件代码
        if (checkXzqtzfsexxRowFxrzjdm(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //交易日期
        if (checkXzqtzfsexxRowJyrq(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //票面利率
        if (checkXzqtzfsexxRowPmll(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //成交金额
        if (checkXzqtzfsexxRowCjje(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //债券债务登记日
        if (checkXzqtzfsexxRowZqzwdjr(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //起息日
        if (checkXzqtzfsexxRowQxr(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人地区代码
        if (checkXzqtzfsexxRowFxrdqdm(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //非空判断
        if (checkXzqtzfsexxRowFkpd(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //兑付日期
        if (checkXzqtzfsexxRowDfrq(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //长度校验
        if (checkXzqtzfsexxRowCd(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人行业
        if (checkXzqtzfsexxRowFxrhy(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //债券总托管机构
        if (checkXzqtzfsexxRowZqztgjg(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //债券品种
        if (checkXzqtzfsexxRowZqpz(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //债券信用级别
        if (checkXzqtzfsexxRowZqxyjb(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkXzqtzfsexxRowBz(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人行业
        if (checkXzqtzfsexxRowFxrdhy(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //买入/卖出标志
        if (checkXzqtzfsexxRowMrmcbz(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //特殊字符
        if (checkXzqtzfsexxRowTszf(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }
        //数据日期
        if (checkXzqtzfsexxRowSjrq(checknumList, xzqtzfsexx, tempMsg)) {
            isError = true;
        }


        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        //单条实体交验完,处理结果
        checkHelper.handleRowCheckResult(isError, xzqtzfsexx.getId(), String.format(header, xzqtzfsexx.getFinanceorgcode(), xzqtzfsexx.getZqdm()), tempMsg.toString(), checkParamContext);

    }

    private boolean checkXzqtzfsexxRowSjrq(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xzqtzfsexx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)) {
            if (sjrq.length() == 10) {
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1) {
                    if (sjrq.compareTo("1800-01-01") < 0 || sjrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                } else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            } else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        } else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowMrmcbz(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String mrmcbz = xzqtzfsexx.getMrmcbz();
        if (checknumList.contains("JS0039") && StringUtils.isNotBlank(mrmcbz)) {
            if (!(mrmcbz.equals("0") || mrmcbz.equals("1"))) {
                result = true;
                tempMsg.append("债券投资发生额信息中的买入/卖出标志需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrdhy(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrhy = xzqtzfsexx.getFxrhy();
        //List baseCode = customSqlUtil.getBaseCode(BaseAindustry.class);
        List baseCode = ZqFieldUtils.fxrhyList;
        if (checknumList.contains("JS0036") && StringUtils.isNotBlank(fxrhy) && !CheckDataUtils.comma(fxrhy)) {
            if (!baseCode.contains(fxrhy) && !"2".equals(fxrhy)) {
                result = true;
                tempMsg.append("债券投资发生额信息中的发行人行业需在《国民经济行业分类》（GB/T 4754）最新标准门类范围或为'2'" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowBz(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xzqtzfsexx.getBz();

        List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
        if (checknumList.contains("JS0034") && StringUtils.isNotBlank(bz)) {
            if (!baseCode.contains(bz)) {
                result = true;
                tempMsg.append("债券投资发生额信息中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }

        if (checknumList.contains("JS0034") && StringUtils.isNotBlank(bz)) {
            if (false) {
                result = true;
                tempMsg.append("债券投资发生额信息中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowZqxyjb(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String zqxyjg = xzqtzfsexx.getZqxyjg();//债券信用级别
        if (checknumList.contains("JS0033") && StringUtils.isNotBlank(zqxyjg)) {
            if (!ZqFieldUtils.zqxyjbList().contains(zqxyjg)) {
                result = true;
                tempMsg.append("债券投资发生额信息中的债券信用级别需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowZqpz(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String zqpz = xzqtzfsexx.getZqpz();//债券品种
        if (checknumList.contains("JS0032") && StringUtils.isNotBlank(zqpz)) {
            try {
                if (!ZqFieldUtils.zqpzList.contains(zqpz)&&!zqpz.substring(0,5).equals("TB99-")) {
                    result = true;
                    tempMsg.append("债券投资发生额信息中的债券品种需在符合要求的值域范围内 或者为'TB99-’开头加债券中文名称" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowZqztgjg(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String zqztgjg = xzqtzfsexx.getZqztgjg();//债券总托管机构
        if (checknumList.contains("JS0031") && StringUtils.isNotBlank(zqztgjg)) {
            if (!(zqztgjg.equals("300") || zqztgjg.equals("400") || zqztgjg.equals("900") || zqztgjg.equals("000"))) {
                result = true;
                tempMsg.append("债券投资发生额信息中的债券总托管机构需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrhy(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String zqpz = xzqtzfsexx.getZqpz();//债券品种
        String fxrhy = xzqtzfsexx.getFxrhy();//发行人行业
        if (checknumList.contains("JS2071") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            if (zqpz.equals("MDBB") && !fxrhy.substring(0, 1).equals("T")) {
                result = true;
                tempMsg.append("国际开发机构的债权发行人行业应该为T-国际组织" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowTszf(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xzqtzfsexx.getFinanceorgcode();//金融机构到代码
        String zqdm = xzqtzfsexx.getZqdm();//债券代码
        String fxrzjdm = xzqtzfsexx.getFxrzjdm();//发行人证件代码
        String jylsh = xzqtzfsexx.getJylsh();//交易流水号
        String financeorginnum = xzqtzfsexx.getFinanceorginnum();//内部机构号
        String pmll = xzqtzfsexx.getPmll();//片面利率
        if (checknumList.contains("JS0717") && StringUtils.isNotBlank(jylsh)) {
            if (CheckUtil.checkStr(jylsh)) {
                result = true;
                tempMsg.append("交易流水号字段内容中出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0718") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0719") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0720") && StringUtils.isNotBlank(zqdm)) {
            if (CheckUtil.checkStr(zqdm)) {
                result = true;
                tempMsg.append("债券代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。 " + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0721") && StringUtils.isNotBlank(fxrzjdm)) {
            if (CheckUtil.checkStr(fxrzjdm)) {
                result = true;
                tempMsg.append("发行人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0722") && StringUtils.isNotBlank(pmll)) {
            if (CheckUtil.checkPerSign(pmll)) {
                result = true;
                tempMsg.append("当票面利率不为空时，票面利率不能包含‰或%" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowCd(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        //交易流水号           JS0723
        String jylsh = xzqtzfsexx.getJylsh();
        //金融机构代码         JS0724
        String financeorgcode = xzqtzfsexx.getFinanceorgcode();
        //内部机构号          JS0725
        String financeorginnum = xzqtzfsexx.getFinanceorginnum();
        //债券代码            JS0726
        String zqdm = xzqtzfsexx.getZqdm();
        //发行人证件代码       JS0727
        String fxrzjdm = xzqtzfsexx.getFxrzjdm();
        //票面利率            JS0728
        String pmll = xzqtzfsexx.getPmll();
        //成交金额            JS0729
        String cjje = xzqtzfsexx.getCjje();
        //成交金额折人民币    JS0730
        String cjjezrmb = xzqtzfsexx.getCjjezrmb();
        //债券品种
        String zqpz = xzqtzfsexx.getZqpz();
        //发行人地区地区代码
        String fxrdqdm = xzqtzfsexx.getFxrdqdm();
        //发行人行业
        String fxrhy = xzqtzfsexx.getFxrhy();
        //发行人企业规模
        String fxrqygm = xzqtzfsexx.getFxrqygm();
        //发行人经济成分
        String fxrjjcf = xzqtzfsexx.getFxrjjcf();
        //发行人基金部门
        String fxrgmjjbm = xzqtzfsexx.getFxrgmjjbm();

        if (checknumList.contains("JS2943") && StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqpz.equals("SMECN1") || zqpz.equals("SMECN2") || zqpz.substring(0, 4).equals("TB99")) && fxrgmjjbm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人国民经济部门长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2944") && StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99")) && fxrgmjjbm.length() > 3) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人国民经济部门长度不能超过3位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2941") && StringUtils.isNotBlank(fxrjjcf) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqpz.equals("SMECN1") || zqpz.equals("SMECN2") || zqpz.substring(0, 4).equals("TB99")) && fxrjjcf.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人经济成分长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2942") && StringUtils.isNotBlank(fxrjjcf) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99")) && fxrjjcf.length() > 3) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人经济成分长度不能超过3位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2939") && StringUtils.isNotBlank(fxrqygm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqpz.equals("SMECN1") || zqpz.equals("SMECN2") || zqpz.substring(0, 4).equals("TB99")) && fxrqygm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人企业规模长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2940") && StringUtils.isNotBlank(fxrqygm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99")) && fxrqygm.length() > 4) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人企业规模长度不能超过4位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2937") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqpz.equals("SMECN1") || zqpz.equals("SMECN2") || zqpz.substring(0, 4).equals("TB99")) && fxrhy.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人行业长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2938") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99")) && fxrhy.length() > 1) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人行业长度不能超过1位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2935") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqpz.equals("SMECN1") || zqpz.equals("SMECN2") || zqpz.substring(0, 4).equals("TB99")) && fxrdqdm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人地区代码长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2936") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99")) && fxrdqdm.length() > 6) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人地区代码长度不能超过6位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2934") && StringUtils.isNotBlank(fxrzjdm)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqpz.equals("SMECN2") && !zqpz.substring(0, 4).equals("TB99"))&&fxrzjdm.length() > 60) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2' 且不是'TB99'开头时，发行人证件代码字符长度不能超过60" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2917") && StringUtils.isNotBlank(zqpz)){
            try {
                if (zqpz.substring(0,4).equals("TB99") && zqpz.length() > 200){
                    result = true;
                    tempMsg.append("债券品种为TB99开头时，长度不能超过200" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2918") && StringUtils.isNotBlank(zqpz)){
            try {
                if (!zqpz.substring(0,4).equals("TB99") && zqpz.length() > 6){
                    result = true;
                    tempMsg.append("债券品种不为TB99开头时，长度不能超过6" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0723") && StringUtils.isNotBlank(jylsh)) {
            if (jylsh.length() > 60) {
                result = true;
                tempMsg.append("交易流水号字符长度不能超过60" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0724") && StringUtils.isNotBlank(financeorgcode)) {
            if (financeorgcode.length() != 18) {
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0725") && StringUtils.isNotBlank(financeorginnum)) {
            if (financeorginnum.length() > 30) {
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0726") && StringUtils.isNotBlank(zqdm)) {
            if (zqdm.length() > 60) {
                result = true;
                tempMsg.append("债券代码字符长度不能超过60" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0727") && StringUtils.isNotBlank(fxrzjdm)) {
            try {
                if (fxrzjdm.length() > 1000 &&(zqpz.equals("SMECN1")||zqdm.equals("SMECN2")||zqdm.substring(0,4).equals("TB99"))) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者 TB99开头时，发行人证件代码字符长度不能超过1000" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0728") && StringUtils.isNotBlank(pmll)) {
            if (pmll.length() > 10 || !CheckUtil.checkDecimal(pmll, 5)) {
                result = true;
                tempMsg.append("当票面利率不为空时，票面利率总长度不能超过10位，小数位必须为5位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0729") && StringUtils.isNotBlank(cjje)) {
            if (cjje.length() > 20 || !CheckUtil.checkDecimal(cjje, 2)) {
                result = true;
                tempMsg.append("成交金额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0730") && StringUtils.isNotBlank(cjjezrmb)) {
            if (cjjezrmb.length() > 20 || !CheckUtil.checkDecimal(cjjezrmb, 2)) {
                result = true;
                tempMsg.append("成交金额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }

        return result;
    }

    private boolean checkXzqtzfsexxRowDfrq(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String dfrq = xzqtzfsexx.getDfrq();
        if (checknumList.contains("JS0734") && StringUtils.isNotBlank(dfrq)) {
            if (!CheckUtil.checkDate(dfrq, "yyyy-MM-dd") || dfrq.compareTo("1800-01-01") < 0 || dfrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("兑付日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFkpd(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        //交易流水号不能为空     JS1359
        String jylsh = xzqtzfsexx.getJylsh();
        //金融机构代码不能为空        JS1360
        String financeorgcode = xzqtzfsexx.getFinanceorgcode();
        //债券代码不能为空      JS1361
        String zqdm = xzqtzfsexx.getZqdm();
        //债券总托管机构不能为空       JS1362
        String zqztgjg = xzqtzfsexx.getZqztgjg();
        //债券品种不能为空      JS1363
        String zqpz = xzqtzfsexx.getZqpz();
        //债券信用级别不能为空        JS1364
        String zqxyjg = xzqtzfsexx.getZqxyjg();
        //币种不能为空        JS1365
        String bz = xzqtzfsexx.getBz();
        // 债权债务登记日不能为空      JS1366
        String zqzwdj = xzqtzfsexx.getZqzwdj();
        //起息日不能为空       JS1367
        String qxr = xzqtzfsexx.getQxr();
        //兑付日期不能为空      JS1368
        String dfrq = xzqtzfsexx.getDfrq();
        //发行人证件代码不能为空       JS1369
        String fxrzjdm = xzqtzfsexx.getFxrzjdm();
        // 发行人地区代码不能为空      JS1370
        String fxrdqdm = xzqtzfsexx.getFxrdqdm();
        //发行人行业不能为空     JS1371
        String fxrhy = xzqtzfsexx.getFxrhy();
        //当发行人不为个人、境外非居民时，发行人企业规模不能为空       JS1372
        String fxrqygm = xzqtzfsexx.getFxrqygm();
        //当发行人不为个人、境外非居民以及境内非企业时，发行人经济成分不能为空        JS1373
        String fxrjjcf = xzqtzfsexx.getFxrjjcf();
        // 交易日期不能为空     JS1374
        String jyrq = xzqtzfsexx.getJyrq();
        //成交金额不能为空      JS1375
        String cjje = xzqtzfsexx.getCjje();
        // 成交金额折人民币不能为空     JS1376
        String cjjezrmb = xzqtzfsexx.getCjjezrmb();
        //买入/卖出标志不能为空       JS1377
        String mrmcbz = xzqtzfsexx.getMrmcbz();
        //内部机构号不能为空     JS1580
        String financeorginnum = xzqtzfsexx.getFinanceorginnum();
        if (checknumList.contains("JS1359") && StringUtils.isBlank(jylsh)) {
            result = true;
            tempMsg.append("交易流水号不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1360") && StringUtils.isBlank(financeorgcode)) {
            result = true;
            tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1361") && StringUtils.isBlank(zqdm)) {
            result = true;
            tempMsg.append("债券代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1362") && StringUtils.isBlank(zqztgjg)) {
            result = true;
            tempMsg.append("债券总托管机构不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1363") && StringUtils.isBlank(zqpz)) {
            result = true;
            tempMsg.append("债券品种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1364") && StringUtils.isBlank(zqxyjg)) {
            result = true;
            tempMsg.append("债券信用级别不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1365") && StringUtils.isBlank(bz)) {
            result = true;
            tempMsg.append("币种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1366") && StringUtils.isBlank(zqzwdj)) {
            result = true;
            tempMsg.append("债权债务登记日不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1367") && StringUtils.isBlank(qxr)) {
            result = true;
            tempMsg.append("起息日不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1368") && StringUtils.isBlank(dfrq)) {
            result = true;
            tempMsg.append("兑付日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1369") && StringUtils.isBlank(fxrzjdm)) {
            result = true;
            tempMsg.append("发行人证件代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1370") && StringUtils.isBlank(fxrdqdm)) {
            result = true;
            tempMsg.append("发行人地区代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1371") && StringUtils.isBlank(fxrhy)) {
            result = true;
            tempMsg.append("发行人行业不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1374") && StringUtils.isBlank(jyrq)) {
            result = true;
            tempMsg.append("交易日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1375") && StringUtils.isBlank(cjje)) {
            result = true;
            tempMsg.append("成交金额不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1376") && StringUtils.isBlank(cjjezrmb)) {
            result = true;
            tempMsg.append("成交金额折人民币不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1377") && StringUtils.isBlank(mrmcbz)) {
            result = true;
            tempMsg.append("买入/卖出标志不能为空 " + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1580") && StringUtils.isBlank(financeorginnum)) {
            result = true;
            tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
        }

        if (checknumList.contains("JS1372") && StringUtils.isNotBlank(fxrhy)) {
            if (!(fxrhy.equals("100") || fxrhy.equals("200")) && StringUtils.isBlank(fxrqygm)) {
                result = true;
                tempMsg.append("当发行人不为个人、境外非居民时，发行人企业规模不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1373") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(fxrqygm)) {
            if (!fxrhy.equals("100") && !fxrhy.equals("200") && !fxrqygm.equals("CS05") && StringUtils.isBlank(fxrjjcf)) {
                tempMsg.append("当发行人不为个人、境外非居民以及境内非企业时，发行人经济成分不能为空" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrdqdm(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrdqdm = xzqtzfsexx.getFxrdqdm();//发行人地区代码
        String fxrhy = xzqtzfsexx.getFxrhy();//发行人行业
        String zqpz = xzqtzfsexx.getZqpz();//债券品种
        List baseCodeGj = customSqlUtil.getBaseCode(BaseCountry.class);//国家地区代码C0013
        List baseCodeXz = customSqlUtil.getBaseCode(BaseArea.class);//行政区划代码C0009
        if (checknumList.contains("JS0035") && StringUtils.isNotBlank(fxrdqdm) && !CheckDataUtils.comma(fxrdqdm)) {
            if ((!baseCodeGj.contains(fxrdqdm) && !baseCodeXz.contains(fxrdqdm)) && !"000000".equals(fxrdqdm)) {
                result = true;
                tempMsg.append("债券投资发生额信息中的发行人地区代码需在符合要求的值域范围内，境内机构为《中华人民共和国行政区划代码》（GB/T 2260）最新标准的县（区）级数字码值域范围内（不包括港澳台），境外机构为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码，发行人为财政部和中国人民银行的填写000000" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2067") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            if ((zqpz.equals("GB01") || zqpz.equals("GB02") || zqpz.equals("GB03") || zqpz.equals("CBN")) && !fxrdqdm.equals("000000")) {
                result = true;
                tempMsg.append("债券品种为国债、央票时，发行人地区代码应该为000000" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2068") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            if (!(zqpz.equals("GB01") || zqpz.equals("GB02") || zqpz.equals("GB03") || zqpz.equals("CBN")) && fxrdqdm.equals("000000")) {
                result = true;
                tempMsg.append("债券品种不是国债、央票时，发行人地区代码不应该为000000" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2069") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(fxrhy)) {
            if (fxrhy.equals("200")) {
                try {
                    if (!fxrdqdm.substring(0, 3).equals("000") || fxrdqdm.equals("000000")) {
                        result = true;
                        tempMsg.append("债券发行人行业为200-境外时，发行人地区代码为000开头且不是000000" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("债券发行人行业为200-境外时，发行人地区代码为000开头且不是000000" + SysConstants.spaceStr);
                }
            }
        }
        if (checknumList.contains("JS2070") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(fxrhy)) {
            if (!fxrhy.equals("200")) {
                try {
                    if (fxrdqdm.substring(0, 3).equals("000") || fxrdqdm.equals("000000")) {
                        result = true;
                        tempMsg.append("债券发行人行业不是200-境外时，发行人地区代码不是000开头或者应该为000000" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("债券发行人行业不是200-境外时，发行人地区代码不是000开头或者应该为000000" + SysConstants.spaceStr);
                }
            }
        }

        return result;
    }

    private boolean checkXzqtzfsexxRowQxr(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String qxr = xzqtzfsexx.getQxr();//起息日
        String sjrq = xzqtzfsexx.getSjrq();//数据日期
        String dfrq = xzqtzfsexx.getDfrq();//对付日期

        if (checknumList.contains("JS0733") && StringUtils.isNotBlank(qxr)) {
            if (!CheckUtil.checkDate(qxr, "yyyy-MM-dd") || qxr.compareTo("1800-01-01") < 0 || qxr.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("起息日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2253") && StringUtils.isNotBlank(qxr) && StringUtils.isNotBlank(sjrq)) {
            if (qxr.compareTo(sjrq) > 0) {
                result = true;
                tempMsg.append("起息日应小于等于数据日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2254") && StringUtils.isNotBlank(qxr) && StringUtils.isNotBlank(dfrq)) {
            if (qxr.compareTo(dfrq) > 0) {
                result = true;
                tempMsg.append("起息日应小于等于兑付日期" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowZqzwdjr(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String zqzwdj = xzqtzfsexx.getZqzwdj();//债券债务登记日
        String qxr = xzqtzfsexx.getQxr();//起息日
        String sjrq = xzqtzfsexx.getSjrq();//数据日期
        if (checknumList.contains("JS0732") && StringUtils.isNotBlank(zqzwdj)) {
            if (!CheckUtil.checkDate(zqzwdj, "yyyy-MM-dd") || zqzwdj.compareTo("1800-01-01") < 0 || zqzwdj.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("债券债务登记日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2255") && StringUtils.isNotBlank(zqzwdj) && StringUtils.isNotBlank(sjrq)) {
            if (zqzwdj.compareTo(sjrq) > 0) {
                result = true;
                tempMsg.append("债权债务登记日应小于等于数据日期" + SysConstants.spaceStr);
            }
        }
//        if (checknumList.contains("JS2256") && StringUtils.isNotBlank(zqzwdj) && StringUtils.isNotBlank(qxr)) {
//            if (zqzwdj.compareTo(qxr) > 0) {
//                result = true;
//                tempMsg.append("债权债务登记日应小于等于起息日" + SysConstants.spaceStr);
//            }
//        }
        return result;
    }

    private boolean checkXzqtzfsexxRowCjje(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String cjje = xzqtzfsexx.getCjje();//成交金额
        if (checknumList.contains("JS2257") && StringUtils.isNotBlank(cjje)) {
            if (cjje.compareTo("0") <= 0) {
                result = true;
                tempMsg.append("成交金额应大于等于0" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowPmll(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        BigDecimal bigDecimal1 = new BigDecimal(0);
        BigDecimal bigDecimal2 = new BigDecimal(30);
        String pmll = xzqtzfsexx.getPmll();
        if (checknumList.contains("JS2261")) {
            if (StringUtils.isNotBlank(pmll)) {
                try {
                    if (new BigDecimal(pmll).compareTo(bigDecimal1) < 0 || new BigDecimal(pmll).compareTo(bigDecimal2) > 0) {
                        result = true;
                        tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                }
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowJyrq(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String jyrq = xzqtzfsexx.getJyrq();//交易日期
        String qxr = xzqtzfsexx.getQxr();//起息日
        String dfrq = xzqtzfsexx.getDfrq();//兑付日期
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String monthStr;
        if (month < 10) {
            monthStr = year + "-0" + month;
        } else {
            monthStr = year + "-" + month;
        }
        if (checknumList.contains("JS0735") && StringUtils.isNotBlank(jyrq)) {
            if (!CheckUtil.checkDate(jyrq, "yyyy-MM-dd") || jyrq.compareTo("1800-01-01") < 0 || jyrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2259") && StringUtils.isNotBlank(jyrq) && StringUtils.isNotBlank(qxr)) {
            if (jyrq.compareTo(qxr) < 0) {
                tempMsg.append("交易日期应大于等于起息日" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2260") && StringUtils.isNotBlank(jyrq) && StringUtils.isNotBlank(dfrq)) {
            if (jyrq.compareTo(dfrq) > 0) {
                tempMsg.append("交易日期应小于等于兑付日期" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2565") && StringUtils.isNotBlank(jyrq)) {
            try {
                if (!jyrq.substring(0, 7).equals(monthStr)) {
                    tempMsg.append("交易日期应该在当月范围内" + SysConstants.spaceStr);
                    result = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrzjdm(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrzjdm = xzqtzfsexx.getFxrzjdm();//发行人证件代码
        String fxrdqdm = xzqtzfsexx.getFxrdqdm();//发行人地区代码
//        if (checknumList.contains("JS2622") && StringUtils.isNotBlank(fxrzjdm) && StringUtils.isNotBlank(fxrdqdm)) {
//            try {
//                if (!fxrdqdm.substring(0, 3).equals("000")) {
//                    if (StringUtils.isNotBlank(fxrzjdm)||fxrdqdm.equals("000000")) {
//                        if (fxrzjdm.length() != 18) {
//                            tempMsg.append("境内发行人证件代码字符长度应该为18位" + SysConstants.spaceStr);
//                            result = true;
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrgmjjbm(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrgmjjbm = xzqtzfsexx.getFxrgmjjbm();//发行人国民经济部门
        String fxrqygm = xzqtzfsexx.getFxrqygm();//发行人企业规模
        String fxrdqdm = xzqtzfsexx.getFxrdqdm();//发行人地区代码

        if (checknumList.contains("JS2597") && StringUtils.isNotBlank(fxrgmjjbm) && !CheckDataUtils.comma(fxrgmjjbm)) {
            if (!ZqFieldUtils.zqpzList.contains(fxrgmjjbm) || fxrgmjjbm.equals("D01") || fxrgmjjbm.equals("E05")) {
                result = true;
                tempMsg.append("发行人国民经济部门需在符合要求的最底层值域范围内且不能为个人" + SysConstants.spaceStr);
            }
        }

        if (checknumList.contains("JS2598")) {
            if (StringUtils.isBlank(fxrgmjjbm)) {
                tempMsg.append("发行人国民经济部门不能为空" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2601")) {
            if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrdqdm)) {
                try {
                    if (fxrdqdm.substring(0, 3).equals("000") && !fxrdqdm.equals("000000")) {
                        if (!fxrgmjjbm.substring(0, 1).equals("E")) {
                            tempMsg.append("发行人地区代码为000开头的且不是000000时，国民经济部门应为E开头的非居民部门" + SysConstants.spaceStr);
                            result = true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (checknumList.contains("JS2602")) {
            if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrdqdm)) {
                try {
                    if ((!fxrdqdm.substring(0, 3).equals("000") || fxrdqdm.equals("000000"))) {
                        if (fxrgmjjbm.substring(0, 1).equals("E")) {
                            tempMsg.append("发行人地区代码不是000开头的或为000000时，国民经济部门不应为E开头的非居民部门" + SysConstants.spaceStr);
                            result = true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (checknumList.contains("JS2641") && StringUtils.isNotBlank(fxrqygm) && StringUtils.isNotBlank(fxrgmjjbm) && !CheckDataUtils.comma(fxrgmjjbm)) {
            if (fxrqygm.equals("CS01") || fxrqygm.equals("CS02") || fxrqygm.equals("CS03") || fxrqygm.equals("CS04")) {
                try {
                    if (!(fxrgmjjbm.substring(0, 1).equals("B") || fxrgmjjbm.substring(0, 1).equals("C"))) {
                        tempMsg.append("发行人企业规模为CS01至CS04的，发行人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + SysConstants.spaceStr);
                        result = true;
                    }
                } catch (Exception e) {
                    tempMsg.append("发行人企业规模为CS01至CS04的，发行人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowFxrqygm(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrgmjjbm = xzqtzfsexx.getFxrgmjjbm();//发行国民经济部门
        String fxrqygm = xzqtzfsexx.getFxrqygm();//发行人企业规模

        if (checknumList.contains("JS0037") && StringUtils.isNotBlank(fxrqygm) && !CheckDataUtils.comma(fxrqygm)) {
            if (!ZqFieldUtils.fxrqygmList.contains(fxrqygm)) {
                result = true;
                tempMsg.append("若发行人企业规模不为空，需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2642")) {
            try {
                if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrqygm) && !CheckDataUtils.comma(fxrqygm)) {
                    String substring1 = fxrgmjjbm.substring(0, 1);
                    String substring = fxrqygm.substring(0, 4);
                    if (substring1.equals("C") && !(substring.equals("CS01") || substring.equals("CS02") || substring.equals("CS03") || substring.equals("CS04"))) {
                        tempMsg.append("发行人国民经济部门为C开头且不是C99的非金融企业部门，则发行人企业规模应该在CS01至CS04范围内" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowCjjezrmb(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String cjjezrmb = xzqtzfsexx.getCjjezrmb();//成交金额折人名币
        String cjje = xzqtzfsexx.getCjje();//成交金额
        String bz = xzqtzfsexx.getBz();//币种
        if (checknumList.contains("JS2258") && StringUtils.isNotBlank(cjjezrmb)) {
            if (cjjezrmb.compareTo("0") <= 0) {
                tempMsg.append("成交金额折人民币应大于0" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2699")) {
            if (StringUtils.isNotBlank(cjjezrmb) && StringUtils.isNotBlank(cjje) && StringUtils.isNotBlank(bz)) {
                if (bz.equals("CNY")) {
                    try {
                        if (cjjezrmb.compareTo(cjje) != 0) {
                            tempMsg.append("币种为人民币的，成交金额折人民币应该与成交金额的值相等" + SysConstants.spaceStr);
                            result = true;
                        }
                    } catch (Exception e) {
                        tempMsg.append("币种为人民币的，成交金额折人民币应该与成交金额的值相等" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private boolean checkXzqtzfsexxRowZqfstze(List<String> checknumList, Xzqtzfsexx xzqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrjjcf = xzqtzfsexx.getFxrjjcf();
        String fxrqygm = xzqtzfsexx.getFxrqygm();

        if (checknumList.contains("JS0038") && StringUtils.isNotBlank(fxrjjcf) && !CheckDataUtils.comma(fxrjjcf)) {
            if (!ZqFieldUtils.fxrjjcfList.contains(fxrjjcf)) {
                result = true;
                tempMsg.append("若发行人经济成分不为空，需在符合要求最底层类别或最底层类别的父类中" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2868")) {
            if (StringUtils.isNotBlank(fxrqygm)) {
                if (StringUtils.isNotBlank(fxrjjcf) && fxrqygm.equals("CS05")) {
                    tempMsg.append("当发行人为境外非居民或境内非企业时，发行人经济成分应为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }
}