package com.geping.etl.UNITLOAN.util.tyckfse;

/**
 *
 * @author  liang.xu
 * @date 2021年4月16日
 */
public class TyckfseHqUtils {

    /**
     * 判断是否活期业务类型
     * @param ywlx
     * @return
     */
     public static  boolean isHqYwlx(String ywlx){
         return ywlx.equals("T021");
     }


    public static void main(String[] args) {
        System.out.println(TyckfseHqUtils.isHqYwlx("T0215"));
    }

}