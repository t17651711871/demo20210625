package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.repository.report.EnhanceDataDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util
 * @USER: tangshuai
 * @DATE: 2021/5/24
 * @TIME: 10:06
 * @描述:
 */
@Component
@Transactional
public class EnhanceDataDictionary {
    @Autowired
    private EnhanceDataDictionaryRepository enhanceDataDictionaryRepository;

    public List<String> findCode(int type) {
        return enhanceDataDictionaryRepository.getReportCodeList(type);
    }
}
