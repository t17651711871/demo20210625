package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.SysConstants;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 日期工具类
 * @author liang.xu
 * @date 2021.4.27
 */
public class DateUtils {

    public static  boolean inCurrentMM(String yyyyMMdd){
        if(StringUtils.isBlank(yyyyMMdd)){
            return false;
        }
        SimpleDateFormat df = new SimpleDateFormat(SysConstants.yyyyMMdd);
        String date=df.format(new Date());
        String MM= null;//截取系统月份
        String _MM= null;//截取参数月份
        try {
            MM = date.substring(5, 7);
            _MM = yyyyMMdd.substring(5, 7);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MM.equals(_MM);
    }
    public static  boolean isInMonth(String s1,String s2){
        if(StringUtils.isBlank(s1)){
            return false;
        }
        if(StringUtils.isBlank(s2)){
            return false;
        }
        SimpleDateFormat df = new SimpleDateFormat(SysConstants.yyyyMMdd);
        String MM=s1.substring(5, 7);//截取系统月份
        String _MM=s2.substring(5, 7);//截取参数月份
        return MM.equals(_MM);
    }
    public static void main(String[] args) {
        System.out.println(DateUtils.inCurrentMM("2021-05-12"));
    }
}