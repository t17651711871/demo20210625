package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author: wangzd
 * @Date: 17:31 2019/10/18
 */
public class DownloadUtil {
	public static Map<String,String> map = new HashMap<String,String>();
	static {
		map.put("gteeidnum", "Gteeidtype");
		map.put("customercode", "Regamtcreny");
		map.put("actctrlidcode", "Actctrlidtype");
		map.put("browidcode", "Isfarmerloan");
		map.put("brroweridnum","Isfarmerloan");
		map.put("bailorgidnum","Bailorgfarmerloan");
		map.put("wtrcode","Wtrtype");
	}

    public static void downLoad(String result,String target,String fileName) {
        ByteArrayInputStream byteArrayInputStream=null;
        FileOutputStream fileOutputStream=null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        target=target+System.getProperty("file.separator")+fileName;

        File file1=new File(target);
        //如果文件存在，则追加内容；如果文件不存在，则创建文件
        if (file1.exists()){
            FileWriter fw = null;
            PrintWriter pw = null;
            try {
                fw = new FileWriter(file1, false);//本项目中直接覆盖false
                pw = new PrintWriter(fw);
                pw.println(result);
                pw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    if (fw!=null) {
                        fw.flush();
                    }
                    if (pw!=null) {
                        pw.close();
                    }
                    if (fw!=null) {
                        fw.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            byteArrayInputStream=new ByteArrayInputStream(result.getBytes());
            try {
                fileOutputStream=new FileOutputStream(target);
                int len=0;
                byte[] bytes=new byte[1024];
                while ((len=byteArrayInputStream.read(bytes))!=-1) {
                    fileOutputStream.write(bytes, 0, len);
//            response.getOutputStream().write(bytes, 0, len);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                try {
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    if (byteArrayInputStream!=null){
                        byteArrayInputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void downLoad2(String result,String target,String fileName,boolean isFirst) {
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        target=target+System.getProperty("file.separator")+fileName;

        File file1=new File(target);
        //如果文件存在，则追加内容；如果文件不存在，则创建文件
        if (file1.exists()){
        	if(isFirst) {//第一次就存在表示是以前的校验文件，直接覆盖
        		try {
                	bw=new BufferedWriter(new FileWriter(file1, false));//本项目中直接覆盖false
                    bw.write(result);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                    	if(bw!=null) {
                        	bw.flush();
                        }
                        if(bw!=null) {
                        	bw.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        	}else {
        		try {
                	bw=new BufferedWriter(new FileWriter(file1, true));//第二次及以后，直接追加
                    bw.write(result);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                    	if(bw!=null) {
                        	bw.flush();
                        }
                        if(bw!=null) {
                        	bw.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        	}
            
        }else {
        	
            try {
               bw=new BufferedWriter(new FileWriter(file1));
               bw.write(result);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                try {
                	if(bw!=null) {
                    	bw.flush();
                    }
                    if(bw != null){
                    	bw.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    //宝马
//    public static void downLoad2(String result,String target,String fileName,boolean isFirst) {
//        BufferedWriter bw = null;
//        File file=new File(target);
//        if (!file.exists()){
//            file.mkdirs();
//        }
//        target=target+System.getProperty("file.separator")+fileName;
//
//        File file1=new File(target);
//        //如果文件存在，则追加内容；如果文件不存在，则创建文件
//        if (file1.exists()){
//        	if(isFirst) {//第一次就存在表示是以前的校验文件，直接覆盖
//        		try {
//                	bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file1, false), "gb18030"));//本项目中直接覆盖false
//                    bw.write(new String(result.getBytes("gb18030"),"gb18030"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }finally {
//                    try {
//                    	if(bw!=null) {
//                        	bw.flush();
//                        }
//                        if(bw!=null) {
//                        	bw.close();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//        	}else {
//        		try {
//        			bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file1, true), "gb18030"));//第二次及以后，直接追加
//                    bw.write(new String(result.getBytes("gb18030"),"gb18030"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }finally {
//                    try {
//                    	if(bw!=null) {
//                        	bw.flush();
//                        }
//                        if(bw!=null) {
//                        	bw.close();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//        	}
//            
//        }else {
//        	
//            try {
//            	bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file1), "gb18030"));//第二次及以后，直接追加
//                bw.write(new String(result.getBytes("gb18030"),"gb18030"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }finally {
//                try {
//                	if(bw!=null) {
//                    	bw.flush();
//                    }
//                    if(bw != null){
//                    	bw.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
    
    /**
     * 下载校验错误的文件
     * @param response
     * @param target 指定地址
     * @param fileName 文件名 xxx.txt
     */
    public static void downLoadCheckFile(HttpServletResponse response, String target, String fileName) {
        InputStream inputStream = null;
        File file=null;
        // 设置输出的格式
        try {
            response.reset();
            response.setContentType("octets/stream");
            response.addHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"),"ISO-8859-1") );
            String filePath = target + File.separator + fileName;
            file = new File(filePath);
            if (file.exists()){
                inputStream = new FileInputStream(file);
                int len = 0;
                byte[] bytes = new byte[1024];
                while ((len = inputStream.read(bytes)) != -1) {
                    response.getOutputStream().write(bytes, 0, len);
                }
                response.getOutputStream().flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
            	
                if (inputStream != null) {
                    inputStream.close();
                }
                if (file!=null){
                    file.delete();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//    /**
//     * 下载校验错误的文件(宝马gb18030环境，还是乱码，待改)
//     * @param response
//     * @param target 指定地址
//     * @param fileName 文件名 xxx.txt
//     */
//    public static void downLoadCheckFile(HttpServletResponse response, String target, String fileName) {
//        InputStream inputStream = null;
//        File file=null;
//        // 设置输出的格式
//        try {
//            response.reset();
//            response.setContentType("octets/stream");
//            response.addHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes("gb18030"),"gb18030") );
//            System.out.println(fileName);
//            String filePath = target + File.separator + fileName;
//            file = new File(filePath);
//            if (file.exists()){
//                inputStream = new FileInputStream(file);
//                int len = 0;
//                byte[] bytes = new byte[1024];
//                while ((len = inputStream.read(bytes)) != -1) {
//                    response.getOutputStream().write(bytes, 0, len);
//                }
//                response.getOutputStream().flush();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (inputStream != null) {
//                    inputStream.close();
//                }
//                if (file!=null){
//                    file.delete();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
    
    /**
     * 写出CSV文件，每一行数据为一个Object对象
     * @param data 写入数据
     * @param target 路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static void writeCSV(List<?> data, String target,String fileName) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split="|";
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        String filePath=target+File.separator+fileName;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "GB18030"));//sz1024
            // 填充数据
            if (!CollectionUtils.isEmpty(data)) {
                Class<? extends Object> objClass = data.get(0).getClass();
                Field[] fields = objClass.getDeclaredFields();
                List<Field> fields1=new ArrayList<>();
                for (Field field : fields) {
                    fields1.add(field);
                }
                String[] fieldName;
                if (fileName.indexOf("CLDKXX") == -1 && fileName.indexOf("DKFSXX") == -1){
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason"};
                }else {
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "customernum"};
                }
                for (int i = 0; i < fieldName.length; i++) {
                    Field declaredField = objClass.getDeclaredField(fieldName[i]);
                    fields1.remove(declaredField);
                }
                for (int i = 0; i < data.size(); i++) {
                    for (int j = 0; j < fields1.size(); j++) {
                        Method[] methods = objClass.getDeclaredMethods();
                        for (Method method : methods) {
                            if (method.getName().equalsIgnoreCase("get" + fields1.get(j).getName())) {
                                String property = (String) method.invoke(data.get(i), null);
                                bw.append(property == null ? "" : property);
                                break;
                            }
                        }
                        if (j != fields1.size() - 1) {
                            bw.append(split);
                        }
                    }
                    if (i != data.size() - 1) {
                        bw.newLine();
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
        }
    }
    
    /**
     * 写出dat文件和log文件并压缩成zip，每一行数据为一个Object对象
     * @param data 写入数据
     * @param target 路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static String writeZIP(List<?> data, String target,String fileName) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split="|";
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        String filePath=target+File.separator+fileName;   
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));//sz1024
            // 填充数据
            if (!CollectionUtils.isEmpty(data)) {
                Class<? extends Object> objClass = data.get(0).getClass();
                Field[] fields = objClass.getDeclaredFields();
                List<Field> fields1=new ArrayList<>();
                for (Field field : fields) {
                    fields1.add(field);
                }
                String[] fieldName;
                if (fileName.indexOf("CLDWDK") >=0 || fileName.indexOf("DWDKFS") >=0) {
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "customernum", "sjrq"};
                }else if (fileName.indexOf("JRJGFZ") >=0){
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "workarea","workareacode", "sjrq"};
                }else if (fileName.indexOf("DBWXX") >=0){
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "gteegoodsstataus", "mortgagepgerate", "sjrq"};
                }else {
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "sjrq"};
                }
                for (int i = 0; i < fieldName.length; i++) {
                    Field declaredField = objClass.getDeclaredField(fieldName[i]);
                    fields1.remove(declaredField);
                }
                for (int i = 0; i < data.size(); i++) {
                    for (int j = 0; j < fields1.size(); j++) {
                        Method[] methods = objClass.getDeclaredMethods();
                        for (Method method : methods) {
                            if (method.getName().equalsIgnoreCase("get" + fields1.get(j).getName())) {
                                String property = (String) method.invoke(data.get(i), null);
                                if(map.containsKey(fields1.get(j).getName())) {
                                	String type = (String) objClass.getMethod("get"+map.get(fields1.get(j).getName()), null).invoke(data.get(i), null);
                                	if(StringUtils.isNotBlank(type) && StringUtils.isNotBlank(property)) {
                                		String[] strArr = property.split("，|,");
                                		String[] typeArr = type.split("，|,");
                                		property = "";
                                		for(int k = 0; k < typeArr.length; k++) {
                                			if(typeArr[k].startsWith("B")) {
                                				if("B01".equals(typeArr[k]) || "B08".equals(typeArr[k])) {
                                        			property = property + TuoMinUtils.getB01(strArr[k])+",";
                                        		}else {
                                        			property = property + TuoMinUtils.getB01otr(strArr[k])+",";
                                        		}
                                			}else {
                                				property = property + strArr[k] + ",";
                                			}
                                		}
                                		property = property.substring(0, property.length() - 1);
                                	}
                                }
                                bw.append(property == null ? "" : property);
                                break;
                            }
                        }
                        if (j != fields1.size() - 1) {
                            bw.append(split);
                        }
                    }
                    if (i != data.size() - 1) {
                        bw.newLine();
                    }
                }
            }
            bw.flush();
            bw.close();
            String filePathLog = filePath.replace(".dat", ".log");
            FileInputStream in = new FileInputStream(filePath);
            String filePathMd5 = DigestUtils.md5DigestAsHex(in);
            in.close();
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathLog), "GB18030"));
            //文件名
            bw.append(fileName);
            bw.newLine();
            //文件 MD5 码
            if (!CollectionUtils.isEmpty(data)) {
            	bw.append(filePathMd5);
            }
            bw.newLine();
            //文件大小（字节）
            bw.append(String.valueOf(new File(filePath).length()));
            bw.newLine();
            //文件创建完成时间（YYYY-MM-DD HH:MM:SS）
            bw.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            bw.newLine();
            //文件记录数（行数）
            bw.append(String.valueOf(data.size()));
            
            bw.flush();
            bw.close();
            
            String filepathZip = target+File.separator+fileName.replace(".dat", ".zip");
            FileOutputStream out = new FileOutputStream(new File(filepathZip));
            ZipUtils.toZip(new File(target), out, false);
            out.close();
            return fileName.replace(".dat", ".zip");
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
        }
    }
    /**
     * 个人贷款发生额信息
     * 写出dat文件和log文件并压缩成zip，每一行数据为一个Object对象
     * @param data 写入数据
     * @param target 路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static String writeZIPgrdkfsxx(XDataBaseTypeUtil xDataBaseTypeUtil,String target,String fileName) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split="|";
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        boolean isFirst = false;
        String filePath=target+File.separator+fileName;
        int total = 0;
        int size = 100000;
        int page = 0;
        Connection connection = null;
        ResultSet rs=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        try {
            int count = 0;
            String sql =null;
            String sqlcount = "select count(*) from xgrdkfsxx where datastatus='3'";
            connection = JDBCUtils.getConnection();
            rsCount = JDBCUtils.Query(connection, ps, sqlcount);
            if (rsCount.next()) {
                total = rsCount.getInt(1);
            }
            if(total != 0) {
                page = total % size == 0 ? total/size : total/size+1;
                for(int i = 0; i < page; i++) {
                    if(xDataBaseTypeUtil.equalsMySql()) {
                        sql = "select * from xgrdkfsxx where datastatus='3' limit "+i*size+","+size+"";
                    }else if(xDataBaseTypeUtil.equalsOracle()) {
                        sql = "select * from (select ROWNUM as rowno, t.* from xgrdkfsxx t where datastatus='3' and ROWNUM <= "+size*(i+1)+" order by id) t1 where t1.rowno > "+(size*i)+"";
                    }else if(xDataBaseTypeUtil.equalsSqlServer()) {
                        sql = "select * from (select *,ROW_NUMBER() over (order by id) as rowid from xgrdkfsxx where datastatus='3') b where b.rowid BETWEEN "+(i*size+1)+" and "+(i+1)*size+"";
                    }
                    if(i != 0) {
                        isFirst = true;
                    }
                    long axun = System.currentTimeMillis();
                    rs = JDBCUtils.Query(connection,ps, sql);
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));//sz1024
                    // 填充数据
                    while (rs.next()) {
                        count++;
//                    System.out.println(count);
                        bw.append(isNull(rs.getString("finorgcode"))+split);
                        bw.append(isNull(rs.getString("finorgincode"))+split);
                        bw.append(isNull(rs.getString("finorgareacode"))+split);
                        bw.append(isNull(rs.getString("isfarmerloan"))+split);
                        if(isNull(rs.getString("isfarmerloan")).startsWith("B")) {
                            if("B01".equals(isNull(rs.getString("isfarmerloan"))) || "B08".equals(isNull(rs.getString("isfarmerloan")))) {
                                bw.append(TuoMinUtils.getB01(isNull(rs.getString("browidcode")))+split);
                            }else {
                                bw.append(TuoMinUtils.getB01otr(isNull(rs.getString("browidcode")))+split);
                            }
                        }else {
                            bw.append(isNull(rs.getString("browidcode"))+split);
                        }

                        bw.append(isNull(rs.getString("browareacode"))+split);
                        bw.append(isNull(rs.getString("loanbrowcode"))+split);
                        bw.append(isNull(rs.getString("loancontractcode"))+split);
                        bw.append(isNull(rs.getString("loanprocode"))+split);
                        bw.append(isNull(rs.getString("loanstartdate"))+split);
                        bw.append(isNull(rs.getString("loanenddate"))+split);
                        bw.append(isNull(rs.getString("loanactenddate"))+split);
                        bw.append(isNull(rs.getString("loancurrency"))+split);
                        bw.append(isNull(rs.getString("loanamt"))+split);
                        bw.append(isNull(rs.getString("loancnyamt"))+split);
                        bw.append(isNull(rs.getString("rateisfix"))+split);
                        bw.append(isNull(rs.getString("ratelevel"))+split);
                        bw.append(isNull(rs.getString("loanfixamttype"))+split);
                        bw.append(isNull(rs.getString("rate"))+split);
                        bw.append(isNull(rs.getString("loanfinancesupport"))+split);
                        bw.append(isNull(rs.getString("loanraterepricedate"))+split);
                        bw.append(isNull(rs.getString("gteemethod"))+split);
                        bw.append(isNull(rs.getString("isplatformloan"))+split);
                        bw.append(isNull(rs.getString("loanstatus"))+split);
                        bw.append(isNull(rs.getString("assetproductcode"))+split);
                        bw.append(isNull(rs.getString("loanrestructuring"))+split);
                        bw.append(isNull(rs.getString("givetakeid"))+split);
                        bw.append(isNull(rs.getString("transactionnum"))+split);
                        bw.append(isNull(rs.getString("issupportliveloan")));
                        bw.newLine();
                    }
                    bw.flush();
                    bw.close();
                    long bxun = System.currentTimeMillis();
                    System.out.println("第"+(i+1)+"批10w数据生成报文用时："+(bxun-axun)/1000+"秒");
                    rs.close();
                }
            }else {
                bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));
                bw.flush();
                bw.close();
            }

            String filePathLog = filePath.replace(".dat", ".log");
            FileInputStream in = new FileInputStream(filePath);
            String filePathMd5 = DigestUtils.md5DigestAsHex(in);
            in.close();
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathLog), "utf-8"));
            //文件名
            bw.append(fileName);
            bw.newLine();
            //文件 MD5 码
            if (count != 0) {
                bw.append(filePathMd5);
            }
            bw.newLine();
            //文件大小（字节）
            bw.append(String.valueOf(new File(filePath).length()));
            bw.newLine();
            //文件创建完成时间（YYYY-MM-DD HH:MM:SS）
            bw.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            bw.newLine();
            //文件记录数（行数）
            bw.append(String.valueOf(count));

            bw.flush();
            bw.close();

            String filepathZip = target+File.separator+fileName.replace(".dat", ".zip");
            FileOutputStream out = new FileOutputStream(new File(filepathZip));
            ZipUtils.toZip(new File(target), out, false);
            out.close();
            return fileName.replace(".dat", ".zip");
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
            JDBCUtils.close();
        }
    }



    /**
     * 存量个人贷款信息
     * 写出dat文件和log文件并压缩成zip，每一行数据为一个Object对象
     * @param data 写入数据
     * @param target 路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static String writeZIPclgrdkxx(XDataBaseTypeUtil xDataBaseTypeUtil,String target,String fileName) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split="|";
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        boolean isFirst = false;
        String filePath=target+File.separator+fileName;
        int total = 0;
        int size = 100000;
        int page = 0;
        Connection connection = null;
        ResultSet rs=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        try {
            int count = 0;
            String sql = null;
            String sqlcount = "select count(*) from xclgrdkxx where datastatus='3'";
            connection = JDBCUtils.getConnection();
            rsCount = JDBCUtils.Query(connection, ps, sqlcount);
            if (rsCount.next()) {
                total = rsCount.getInt(1);
            }
            if(total != 0) {
                page = total % size == 0 ? total/size : total/size+1;
                for(int i = 0; i < page; i++) {
                    if(xDataBaseTypeUtil.equalsMySql()) {
                        sql = "select * from xclgrdkxx where datastatus='3' limit "+i*size+","+size+"";
                    }else if(xDataBaseTypeUtil.equalsOracle()) {
                        sql = "select * from (select ROWNUM as rowno, t.* from xclgrdkxx t where datastatus='3' and ROWNUM <= "+size*(i+1)+" order by id) t1 where t1.rowno > "+(size*i)+"";
                    }else if(xDataBaseTypeUtil.equalsSqlServer()) {
                        sql = "select * from (select *,ROW_NUMBER() over (order by id) as rowid from xclgrdkxx where datastatus='3') b where b.rowid BETWEEN "+(i*size+1)+" and "+(i+1)*size+"";
                    }
                    if(i != 0) {
                        isFirst = true;
                    }
                    long axun = System.currentTimeMillis();
                    rs = JDBCUtils.Query(connection,ps, sql);
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));//sz1024
                    // 填充数据
                    while (rs.next()) {
                        count++;
                        bw.append(isNull(rs.getString("financeorgcode"))+split);
                        bw.append(isNull(rs.getString("financeorginnum"))+split);
                        bw.append(isNull(rs.getString("financeorgareacode"))+split);
                        bw.append(isNull(rs.getString("isfarmerloan"))+split);
                        if(isNull(rs.getString("isfarmerloan")).startsWith("B")) {
                            if("B01".equals(isNull(rs.getString("isfarmerloan"))) || "B08".equals(isNull(rs.getString("isfarmerloan")))) {
                                bw.append(TuoMinUtils.getB01(isNull(rs.getString("brroweridnum")))+split);
                            }else {
                                bw.append(TuoMinUtils.getB01otr(isNull(rs.getString("brroweridnum")))+split);
                            }
                        }else {
                            bw.append(isNull(rs.getString("brroweridnum"))+split);
                        }

                        bw.append(isNull(rs.getString("brrowerareacode"))+split);
                        bw.append(isNull(rs.getString("receiptcode"))+split);
                        bw.append(isNull(rs.getString("contractcode"))+split);
                        bw.append(isNull(rs.getString("productcetegory"))+split);
                        bw.append(isNull(rs.getString("loanstartdate"))+split);
                        bw.append(isNull(rs.getString("loanenddate"))+split);
                        bw.append(isNull(rs.getString("extensiondate"))+split);
                        bw.append(isNull(rs.getString("currency"))+split);
                        bw.append(isNull(rs.getString("receiptbalance"))+split);
                        bw.append(isNull(rs.getString("receiptcnybalance"))+split);
                        bw.append(isNull(rs.getString("interestisfixed"))+split);
                        bw.append(isNull(rs.getString("interestislevel"))+split);
                        bw.append(isNull(rs.getString("fixpricetype"))+split);
                        bw.append(isNull(rs.getString("baseinterest"))+split);
                        bw.append(isNull(rs.getString("loanfinancesupport"))+split);
                        bw.append(isNull(rs.getString("loaninterestrepricedate"))+split);
                        bw.append(isNull(rs.getString("guaranteemethod"))+split);
                        bw.append(isNull(rs.getString("isplatformloan"))+split);
                        bw.append(isNull(rs.getString("loanquality"))+split);
                        bw.append(isNull(rs.getString("loanstatus"))+split);
                        bw.append(isNull(rs.getString("overduetype"))+split);
                        bw.append(isNull(rs.getString("issupportliveloan")));
                        bw.newLine();
                    }
                    bw.flush();
                    bw.close();
                    long bxun = System.currentTimeMillis();
                    System.out.println("第"+(i+1)+"批10w数据生成报文用时："+(bxun-axun)/1000+"秒");
                    rs.close();
                }
            }else {
                bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));
                bw.flush();
                bw.close();
            }

            String filePathLog = filePath.replace(".dat", ".log");
            FileInputStream in = new FileInputStream(filePath);
            String filePathMd5 = DigestUtils.md5DigestAsHex(in);
            in.close();
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathLog), "utf-8"));
            //文件名
            bw.append(fileName);
            bw.newLine();
            //文件 MD5 码
            if (count != 0) {
                bw.append(filePathMd5);
            }
            bw.newLine();
            //文件大小（字节）
            bw.append(String.valueOf(new File(filePath).length()));
            bw.newLine();
            //文件创建完成时间（YYYY-MM-DD HH:MM:SS）
            bw.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            bw.newLine();
            //文件记录数（行数）
            bw.append(String.valueOf(count));

            bw.flush();
            bw.close();

            String filepathZip = target+File.separator+fileName.replace(".dat", ".zip");
            FileOutputStream out = new FileOutputStream(new File(filepathZip));
            ZipUtils.toZip(new File(target), out, false);
            out.close();
            return fileName.replace(".dat", ".zip");
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
            JDBCUtils.close();
        }
    }
    /**
     * 个人客户信息
     * 写出dat文件和log文件并压缩成zip，每一行数据为一个Object对象
     * @param data 写入数据
     * @param target 路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static String writeZIPgrkhxx(XDataBaseTypeUtil xDataBaseTypeUtil,String target,String fileName) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split="|";
        BufferedWriter bw = null;
        File file=new File(target);
        if (!file.exists()){
            file.mkdirs();
        }
        boolean isFirst = false;
        String filePath=target+File.separator+fileName;
        int total = 0;
        int size = 100000;
        int page = 0;
        Connection connection = null;
        ResultSet rs=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        try {
        	int count = 0;
        	String sqlcount = "select count(*) from xgrkhxx where datastatus='3'";
        	String sql = null;
        	connection = JDBCUtils.getConnection();
        	rsCount = JDBCUtils.Query(connection, ps, sqlcount); 
        	if (rsCount.next()) {	
            	total = rsCount.getInt(1);
            }
        	if(total != 0) {
        		page = total % size == 0 ? total/size : total/size+1;
            	for(int i = 0; i < page; i++) { 
            		if(xDataBaseTypeUtil.equalsMySql()) {
            			sql = "select * from xgrkhxx where datastatus='3' limit "+i*size+","+size+"";
            		}else if(xDataBaseTypeUtil.equalsOracle()) {
            			sql = "select * from (select ROWNUM as rowno, t.* from xgrkhxx t where datastatus='3' and ROWNUM <= "+size*(i+1)+" order by id) t1 where t1.rowno > "+(size*i)+"";
            		}else if(xDataBaseTypeUtil.equalsSqlServer()) {
            			sql = "select * from (select *,ROW_NUMBER() over (order by id) as rowid from XGRKHXX where datastatus='3') b where b.rowid BETWEEN "+(i*size+1)+" and "+(i+1)*size+"";
            		}
            		if(i != 0) {
            			isFirst = true;
            		}
            		long axun = System.currentTimeMillis();
                    rs = JDBCUtils.Query(connection,ps, sql);
                bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));//sz1024
                // 填充数据           
                while (rs.next()) {
                    count++;
//                    System.out.println(count);
                    bw.append(isNull(rs.getString("finorgcode"))+split);
                    bw.append(isNull(rs.getString("regamtcreny"))+split);
                    if(isNull(rs.getString("regamtcreny")).startsWith("B")) {
                        if("B01".equals(isNull(rs.getString("regamtcreny"))) || "B08".equals(isNull(rs.getString("regamtcreny")))) {
                            bw.append(TuoMinUtils.getB01(isNull(rs.getString("customercode")))+split);
                        }else {
                            bw.append(TuoMinUtils.getB01otr(isNull(rs.getString("customercode")))+split);
                        }
                    }else {
                        bw.append(isNull(rs.getString("customercode"))+split);
                    }
                    bw.append(isNull(rs.getString("country"))+split);
                    bw.append(isNull(rs.getString("nation"))+split);
                    bw.append(isNull(rs.getString("sex"))+split);
                    bw.append(isNull(rs.getString("education"))+split);
                    bw.append(isNull(rs.getString("birthday"))+split);
                    bw.append(isNull(rs.getString("regareacode"))+split);
                    bw.append(isNull(rs.getString("grincome"))+split);
                    bw.append(isNull(rs.getString("familyincome"))+split);
                    bw.append(isNull(rs.getString("marriage"))+split);
                    bw.append(isNull(rs.getString("isrelation"))+split);
                    bw.append(isNull(rs.getString("creditamt"))+split);
                    bw.append(isNull(rs.getString("usedamt"))+split);
                    bw.append(isNull(rs.getString("grkhsfbs"))+split);
                    bw.append(isNull(rs.getString("gtgshyyzzdm"))+split);
                    bw.append(isNull(rs.getString("xwqyshtyxydm"))+split);
                    bw.append(isNull(rs.getString("workareacode"))+split);
                    bw.append(isNull(rs.getString("actctrltype")));
                    bw.newLine();
                }
                bw.flush();
                bw.close();
                long bxun = System.currentTimeMillis();
                System.out.println("第"+(i+1)+"批10w数据生成报文用时："+(bxun-axun)/1000+"秒");
                rs.close();
            	}
        	}else {
        		bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath,isFirst), "utf-8"));
        		bw.flush();
                bw.close();
        	}
        	
            String filePathLog = filePath.replace(".dat", ".log");
            FileInputStream in = new FileInputStream(filePath);
            String filePathMd5 = DigestUtils.md5DigestAsHex(in);
            in.close();
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePathLog), "utf-8"));
            //文件名
            bw.append(fileName);
            bw.newLine();
            //文件 MD5 码
            if (count != 0) {
                bw.append(filePathMd5);
            }
            bw.newLine();
            //文件大小（字节）
            bw.append(String.valueOf(new File(filePath).length()));
            bw.newLine();
            //文件创建完成时间（YYYY-MM-DD HH:MM:SS）
            bw.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            bw.newLine();
            //文件记录数（行数）
            bw.append(String.valueOf(count));

            bw.flush();
            bw.close();

            String filepathZip = target+File.separator+fileName.replace(".dat", ".zip");
            FileOutputStream out = new FileOutputStream(new File(filepathZip));
            ZipUtils.toZip(new File(target), out, false);
            out.close();
            return fileName.replace(".dat", ".zip");
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
            JDBCUtils.close();
        }
    }
    /**
     *金融法人生成报文并压缩成zip，每一行数据为一个Object对象
     *
     * @param data     写入数据
     * @param target   路径
     * @param fileName 文件名
     * @throws Exception
     */
    public static String writeZIP1(List<?> data, String target, String fileName,String iFileName,String ywsjbz,String bsjgdm) throws Exception {
        //CSV文件分隔符，一般为","这里特别指定半角字符，GB18030编码为1207
        String split = "|";
        BufferedWriter bw = null;
        File file = new File(target);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = target + File.separator + fileName;
        String iFilePath = target + File.separator + iFileName;
        String reportNum = fileName.substring(fileName.lastIndexOf(".")-5,fileName.lastIndexOf("."));
        String tou = "I00001";//关键字代码       
        String sjsx = "";//数据属性
        String currency = "";//币种
        String dw = "";//单位
        String szlx = "";//数值类型
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));//sz1024
            // 填充数据
            if (!CollectionUtils.isEmpty(data)) {
                Class<? extends Object> objClass = data.get(0).getClass();
                Field[] fields = objClass.getDeclaredFields();
                List<Field> fields1 = new ArrayList<>();
                for (Field field : fields) {
                    fields1.add(field);
                }
                String[] fieldName;
             /*   if (fileName.indexOf("CLDWDK") >=0 || fileName.indexOf("DWDKFS") >=0) {
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "customernum", "sjrq"};
                }else if (fileName.indexOf("JRJGFZ") >=0){
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "workarea","workareacode", "sjrq"};
                }else if (fileName.indexOf("DBWXX") >=0){
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "gteegoodsstataus", "mortgagepgerate", "sjrq"};
                }else {
                    fieldName= new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "sjrq"};
                }*/
                fieldName = new String[]{"id", "checkstatus", "datastatus", "operator", "operationname", "operationtime", "orgid", "departid", "nopassreason", "sjrq"};                
                int zbmc = 0;
                if("20201".equals(reportNum)) {
                	zbmc = 20201000;
                	sjsx = "1";
                	currency = "BWB0001";
                	dw = "1";
                	szlx = "1";
                }else if("20202".equals(reportNum)) {
                	zbmc = 20202000;
                	sjsx = "1";
                	currency = "BWB0001";
                	dw = "1";
                	szlx = "1";
                }else if("20203".equals(reportNum)) {
                	zbmc = 20203000;
                	sjsx = "1";
                	currency = "BWB0001";
                	dw = "1";
                	szlx = "1";
                }

                for (int i = 0; i < fieldName.length; i++) {
                    Field declaredField = objClass.getDeclaredField(fieldName[i]);
                    fields1.remove(declaredField);
                }
                for (int i = 0; i < data.size(); i++) {
                    for (int j = 0; j < fields1.size(); j++) {
                    	zbmc++;
                        Method[] methods = objClass.getDeclaredMethods();
                        for (Method method : methods) {
                            if (method.getName().equalsIgnoreCase("get" + fields1.get(j).getName())) {
                                String property = (String) method.invoke(data.get(i), null);
                                if (map.containsKey(fields1.get(j).getName())) {
                                    String type = (String) objClass.getMethod("get" + map.get(fields1.get(j).getName()), null).invoke(data.get(i), null);

                                    if (StringUtils.isNotBlank(type)) {
                                        String[] strArr = property.split("，|,");
                                        String[] typeArr = type.split("，|,");
                                        property = "";

                                        for (int k = 0; k < typeArr.length; k++) {
                                            if (typeArr[k].startsWith("B")) {
                                                if ("B01".equals(typeArr[k]) || "B08".equals(typeArr[k])) {
                                                    property = property + TuoMinUtils.getB01(strArr[k]) + ",";
                                                } else {
                                                    property = property + TuoMinUtils.getB01otr(strArr[k]) + ",";
                                                }
                                            } else {
                                                property = property + strArr[k] + ",";
                                            }
                                        }
                                        property = property.substring(0, property.length() - 1);
                                    }
                                }
                                bw.append(tou + split + zbmc + split);
                                bw.append(property == null ? "" : property);
                                if (j != fields1.size() - 1) {
                                    bw.newLine();
                                }
                                break;
                            }
                        }
                        
                    }
                }
            }
            bw.flush();
            bw.close();

            //idx文件
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(iFilePath), "GB18030"));
            bw.append(tou + split);//关键字代码
            bw.append(reportNum+ split);//表单代码
            bw.append(sjsx + split);//数据属性
            bw.append(currency + split);//币种
            bw.append(dw + split);//单位
            bw.append(ywsjbz + split);//业务数据标志
            bw.append(szlx + split);//数值类型
            bw.append(bsjgdm);

            bw.flush();
            bw.close();

            //压缩成zip
            String filepathZip = target + File.separator + iFileName.replace(".idx", ".zip");
            FileOutputStream out = new FileOutputStream(new File(filepathZip));
            ZipUtils.toZip(new File(target), out, false);
            out.close();
            return iFileName.replace(".idx", ".zip");
        } catch (Exception e) {
            throw e;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
        }
    }
    
    public static String isNull(String str) {
    	if(StringUtils.isBlank(str)) {
    		return "";
    	}
		return str;
    	
    }
    
    public static void main(String[] args) {
		Xgrkhxx x = new Xgrkhxx();
		Class clazz = x.getClass();
		Field[] field = clazz.getDeclaredFields();
		for(Field f : field) {
			System.out.println("bw.append(isNull(rs.getString(\""+f.getName()+"\"))+split);");
			
		}
	}
}
