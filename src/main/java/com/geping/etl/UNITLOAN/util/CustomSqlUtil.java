package com.geping.etl.UNITLOAN.util;

import com.geping.etl.common.entity.Logs;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.service.LogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: wangzd
 * @Date: 16:18 2020/6/9
 * @Description:自定义sql工具
 */

@Component
@Transactional
public class CustomSqlUtil<T> {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private LogsService logsService;

    @Autowired
    private HttpServletRequest request;

    private final int BATCH_SIZE=1000;

    private Sys_UserAndOrgDepartment sys_user;

    /**
     * 查询一张表所有数据-只用于基础表数据
     * @param sClass 实体类
     * @param <S> entity泛型
     * @return List
     * 注：实体名必须是表名首字母大写演变
     * 表名：baseDemo->实体名-:BaseDemo
     */
    @Transactional(readOnly = true)
    public <S extends T> List<String> getBaseCode(Class<S> sClass){
        String simpleName = Stringutil.getClassName(sClass.getSimpleName());
        String field="";
        Field[] fields = sClass.getDeclaredFields();
        for (int j = 0; j < fields.length; j++) {
            String name = fields[j].getName();
            if (name.endsWith("code")){
                field=name;
            }
        }
        String sql="select "+field+" from "+simpleName;
        List<String> resultList = em.createNativeQuery(sql).getResultList();
        return resultList;
    }

    /**
     * 保存日志
     * @param logContext 日志内容
     * @param logType 日志类型
     * @return Logs对象
     */
    public Logs saveLog(String logContext,String logType){
    	sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        //创建Logs对象
        String uuid= UUID.randomUUID().toString().replace("-","");
        String localDateTime= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Logs logs = new Logs();
        logs.setId(uuid);
        logs.setLoginId(sys_user.getLoginid());
        logs.setLogDate(localDateTime);
        logs.setLogOrgCode(sys_user.getOrgid());

        logs.setLogContent(logContext);
        logs.setLogType(logType);
        return logsService.save(logs);
    }

    /**
     * 保存单条数据-通用接口
     * @param entity 实体类
     * @param <S> 实体泛型
     * @return entity
     */
    public <S extends T> S saveT(S entity){
        em.merge(entity);
        return entity;
    }

    /**
     * 保存多条数据-通用接口
     * @param entities 传入集合
     * @param <S> entities泛型
     * @return entities
     */
    public <S extends T> List<S> saveAllT(Iterable<S> entities) {
        List<S> result = new ArrayList<>();
        int index = 0;
        if (entities == null) {
            return result;
        } else {
            Iterator<S> var3 = entities.iterator();
            while (var3.hasNext()) {
                S entity = var3.next();
                em.persist(entity);
                result.add(entity);
                index++;
                if (index % BATCH_SIZE == 0) {
                    em.flush();
                    em.clear();
                }
            }
            if (index % BATCH_SIZE != 0) {
                em.flush();
                em.clear();
            }
            return result;
        }
    }

    /**
     * 根据条件修改-通用接口
     * @param tableName 表名
     * @param setMap 设置值
     * @param whereMap 条件
     * @return int
     */
    public int updateByWhere(@NotNull String tableName,Map<String,Object> setMap,Map<String,Object> whereMap){
        StringBuffer setString=new StringBuffer(" set ");
        setMap.forEach((field,value)->{
            setString.append(field+"='"+value+"',");
        });
        setString.deleteCharAt(setString.length()-1);

        StringBuffer whereSql=new StringBuffer();
        whereMap.forEach((field,value)->{
            whereSql.append(" and "+field+"='"+value+"'");
        });
        String sql="update "+tableName+" "+ setString +" where 1=1 "+whereSql;
        int i = em.createNativeQuery(sql).executeUpdate();
        return i;
    }
    
    /**
     * 根据id批量修改-通用接口
     * @param tableName 表名
     * @param setMap 设置值
     * @param whereMap 条件
     * @return int
     */
    public int updateByWhereBatch(@NotNull String tableName,Map<String,Object> setMap,List<String> ids){
        StringBuffer setString=new StringBuffer(" set ");
        setMap.forEach((field,value)->{
            setString.append(field+"='"+value+"',");
        });
        setString.deleteCharAt(setString.length()-1);

        int count = 0;
        int i = 0;
        StringBuffer whereSql=new StringBuffer(" and id in (");
        for(String id : ids) {
        	whereSql.append("'"+id+"',");
        	count++;
        	if(count == 500) {
        		whereSql.deleteCharAt(whereSql.length()-1);
        		whereSql.append(")");
                String sql="update "+tableName+" "+ setString +" where 1=1 "+whereSql;
                i = i + em.createNativeQuery(sql).executeUpdate();
                whereSql.setLength(0);
                whereSql=new StringBuffer(" and id in (");
                count = 0;
        	}
        }
        
        if(count != 0) {
        	whereSql.deleteCharAt(whereSql.length()-1);
    		whereSql.append(")");
            String sql="update "+tableName+" "+ setString +" where 1=1 "+whereSql;
            i = i + em.createNativeQuery(sql).executeUpdate();
            whereSql.setLength(0);
            count = 0;
        }
        return i;
    }
    
    
    /**个人
     * 根据id批量修改-通用接口
     * @param tableName 表名
     * @param setMap 设置值
     * @param whereMap 条件
     * @return int
     */
    public int updateByWhereBatchGR(@NotNull String tableName,Map<String,Object> setMap,List<String> ids){
        StringBuffer setString=new StringBuffer(" set ");
        setMap.forEach((field,value)->{
            setString.append(field+"='"+value+"',");
        });
        setString.deleteCharAt(setString.length()-1);

        int count = 0;
        int i = 0;
        StringBuffer whereSql=new StringBuffer(" and id in (");
        for(String id : ids) {
        	whereSql.append(""+id+",");
        	count++;
        	if(count == 1000) {
        		whereSql.deleteCharAt(whereSql.length()-1);
        		whereSql.append(")");
                String sql="update "+tableName+" "+ setString +" where 1=1 "+whereSql;
                i = i + em.createNativeQuery(sql).executeUpdate();
                whereSql.setLength(0);
                whereSql=new StringBuffer(" and id in (");
                count = 0;
        	}
        }
        
        if(count != 0) {
        	whereSql.deleteCharAt(whereSql.length()-1);
    		whereSql.append(")");
            String sql="update "+tableName+" "+ setString +" where 1=1 "+whereSql;
            i = i + em.createNativeQuery(sql).executeUpdate();
            whereSql.setLength(0);
            count = 0;
        }
        return i;
    }


    /**
     * 根据多个字段查重
     * @param tableName 表名
     * @param fields 字段集
     * @param whereStr 查询条件
     * @param fieldName
     * @return List<String> id集合
     */
    @Transactional(readOnly = true)
    public List<Object[]> isRepeat(@NotNull String tableName, List<String> fields, String whereStr, String fieldName){
        StringBuffer whereField=new StringBuffer();
        fields.forEach(field->{
            whereField.append(field+"+");
        });
        whereField.deleteCharAt(whereField.length()-1);
        String whereField2=whereField.toString().replace("+",",");
        String sql="select id,"+ fieldName +" from "+tableName+whereStr+" and ("+whereField+") in (select "+whereField+" from "+tableName+" GROUP BY "+whereField2+" HAVING count(1)>1)";
        List<Object[]> resultList = em.createNativeQuery(sql).getResultList();
        return resultList;
    }
    
    /**
     * 执行sql查询
     * @param tableName 表名
     * @param fields 字段集
     * @param whereStr 查询条件
     * @param fieldName
     * @return List<String> id集合
     */
    @Transactional(readOnly = true)
    public List<Object[]> executeQuery(String sql){
        List<Object[]> resultList = em.createNativeQuery(sql).getResultList();
        return resultList;
    }

    /**
     * 删除所有
     * @param tClass 类
     * @return int
     */
    public int deleteAll(Class<T> tClass){
        String delSql="delete from "+tClass.getSimpleName();
        return em.createQuery(delSql).executeUpdate();
    }
}
