package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DateUtils;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXgqtzfsexxRowData implements CheckRowData {

    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {

        //债券投资发生额信息
        Xgqtzfsexx xgqtzfsexx = (Xgqtzfsexx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String>checknumList=checkParamContext.getCheckNums();
        //数据日期
        if (checkXgqtzfsexxRowSjrq(checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //股权类型
        if (checkxgqtzfsexxRowGqlx(checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //地区代码
        if (checkxgqtzfsexxRowDqdm(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkxgqtzfsexxRowBz(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //交易方向
        if (checkxgqtzfsexxRowJyfx(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //机构类型
        if (checkxgqtzfsexxRowJglx(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //特殊符号
        if (checkxgqtzfsexxRowTszf(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //长度校验
        if (checkxgqtzfsexxRowCdjy(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //非空校验
        if (checkxgqtzfsexxRowFkjy(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //交易日期
        if (checkxgqtzfsexxRowJyrq(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //数据金额
        if (checkxgqtzfsexxRowSjje(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }
        //交易金额折人民币
        if (checkxgqtzfsexxRowJyjezrmb(checkParamContext,checknumList, xgqtzfsexx, tempMsg)) {
            isError = true;
        }

        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        checkHelper.handleRowCheckResult(isError,  xgqtzfsexx.getId(), String.format(header, xgqtzfsexx.getFinanceorgcode(), xgqtzfsexx.getPzbm()), tempMsg.toString(), checkParamContext);
    }

    private boolean checkxgqtzfsexxRowJyjezrmb(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String jyjezrmb = xgqtzfsexx.getJyjezrmb();//交易金额折人民币
        String jyje = xgqtzfsexx.getJyje();//交易金额折人民币
        String bz = xgqtzfsexx.getBz();//币种
        if (checknumList.contains("JS2075")&&StringUtils.isNotBlank(jyjezrmb)&&StringUtils.isNotBlank(bz)&&StringUtils.isNotBlank(jyje)){
            try {
                if (bz.equals("CNY")&&Double.parseDouble(jyje)!=Double.parseDouble(jyjezrmb)){
                    result = true;
                    tempMsg.append("币种为人民币的，交易金额折人民币应该与交易金额的值相等" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("币种为人民币的，交易金额折人民币应该与交易金额的值相等" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowSjje(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String jyje = xgqtzfsexx.getJyje();//交易金额
        String jyjezrmb = xgqtzfsexx.getJyjezrmb();//交易金额折人民币
        if (checknumList.contains("JS2287")&&StringUtils.isNotBlank(jyje)){
            try {
                if (Double.parseDouble(jyje)<=0){
                    result = true;
                    tempMsg.append("交易金额应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("交易金额应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2288")&&StringUtils.isNotBlank(jyjezrmb)){
            try {
                if (Double.parseDouble(jyjezrmb)<=0){
                    result = true;
                    tempMsg.append("交易金额折人民币应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("交易金额折人民币应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowJyrq(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String jyrq = xgqtzfsexx.getJyrq();//交易日期
        String sjrq = xgqtzfsexx.getSjrq();//数据日期
        if (checknumList.contains("JS2286")&&StringUtils.isNotBlank(jyrq)&&StringUtils.isNotBlank(sjrq)){
            if (jyrq.compareTo(sjrq)>0){
                result = true;
                tempMsg.append("交易日期应小于数据日期" + SysConstants.spaceStr);
            }
        }

        if (checknumList.contains("JS2569")&&StringUtils.isNotBlank(jyrq)){
            if (!DateUtils.isInMonth(jyrq,sjrq)) {
                tempMsg.append("交易日期应该在当月范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowFkjy(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xgqtzfsexx.getFinanceorgcode();//金融机构代码
        String pzbm = xgqtzfsexx.getPzbm();//凭证编码
        String gqlx = xgqtzfsexx.getGqlx();//股权类型
        String jglx = xgqtzfsexx.getJglx();//机构类型
        String jgzjdm = xgqtzfsexx.getJgzjdm();//机构证件代码
        String dqdm = xgqtzfsexx.getDqdm();//地区代码
        String jyrq = xgqtzfsexx.getJyrq();//交易日期
        String bz = xgqtzfsexx.getBz();//币种
        String jyje = xgqtzfsexx.getJyje();//交易金额
        String jyjezrmb = xgqtzfsexx.getJyjezrmb();//交易金额折人民币
        String jyfx = xgqtzfsexx.getJyfx();//交易方向
        String financeorginnum = xgqtzfsexx.getFinanceorginnum();//内部机构号
        if (checknumList.contains("JS1425")&&StringUtils.isBlank(financeorgcode)){
            result = true;
            tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1426")&&StringUtils.isBlank(pzbm)){
            result = true;
            tempMsg.append("凭证编码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1427")&&StringUtils.isBlank(gqlx)){
            result = true;
            tempMsg.append("股权类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1428")&&StringUtils.isBlank(jglx)){
            result = true;
            tempMsg.append("机构类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1429")&&StringUtils.isBlank(jgzjdm)){
            result = true;
            tempMsg.append("机构证件代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1430")&&StringUtils.isBlank(dqdm)){
            result = true;
            tempMsg.append("地区代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1431")&&StringUtils.isBlank(jyrq)){
            result = true;
            tempMsg.append("交易日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1432")&&StringUtils.isBlank(bz)){
            result = true;
            tempMsg.append("币种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1433")&&StringUtils.isBlank(jyje)){
            result = true;
            tempMsg.append("交易金额不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1434")&&StringUtils.isBlank(jyjezrmb)){
            result = true;
            tempMsg.append("交易金额折人民币不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1435")&&StringUtils.isBlank(jyfx)){
            result = true;
            tempMsg.append("交易方向不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1561")&&StringUtils.isBlank(financeorginnum)){
            result = true;
            tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowCdjy(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xgqtzfsexx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xgqtzfsexx.getFinanceorginnum();//内部机构号
        String pzbm = xgqtzfsexx.getPzbm();//凭证编码
        String dqdm = xgqtzfsexx.getDqdm();//地区代码
        String jgzjdm = xgqtzfsexx.getJgzjdm();//机构证件代码
        String jyje = xgqtzfsexx.getJyje();//交易金额
        String jyjezrmb = xgqtzfsexx.getJyjezrmb();//交易金额折人民币
        String jyrq = xgqtzfsexx.getJyrq();//交易日期
        if (checknumList.contains("JS0782")&&StringUtils.isNotBlank(financeorgcode)){
            if (financeorgcode.length()!=18){
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0783")&&StringUtils.isNotBlank(financeorginnum)){
            if (financeorginnum.length()>30){
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0784")&&StringUtils.isNotBlank(pzbm)){
            if (pzbm.length()>100){
                result = true;
                tempMsg.append("凭证编码字符长度不能超过100" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0785")&&StringUtils.isNotBlank(jgzjdm)){
                if (jgzjdm.length()>60){
                    result = true;
                    tempMsg.append("机构证件代码字符长度不能超过60" + SysConstants.spaceStr);
                }
        }
        if (checknumList.contains("JS0897")&&StringUtils.isNotBlank(jgzjdm)&&StringUtils.isNotBlank(dqdm)){
            try {
                if (jgzjdm.length()!=18&&!dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境内客户的机构证件代码字符长度应该为18位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内客户的机构证件代码字符长度应该为18位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0786")&&StringUtils.isNotBlank(jyje)){
            if (jyje.length() > 20 || !CheckUtil.checkDecimal(jyje, 2)) {
                result = true;
                tempMsg.append("交易金额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0787")&&StringUtils.isNotBlank(jyjezrmb)){
            if (jyjezrmb.length() > 20 || !CheckUtil.checkDecimal(jyjezrmb, 2)) {
                result = true;
                tempMsg.append("交易金额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0789") && StringUtils.isNotBlank(jyrq)) {
            if (!CheckUtil.checkDate(jyrq, "yyyy-MM-dd") || jyrq.compareTo("1800-01-01") < 0 || jyrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowTszf(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xgqtzfsexx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xgqtzfsexx.getFinanceorginnum();//内部机构号
        String pzbm = xgqtzfsexx.getPzbm();//凭证编码
        String jgzjdm = xgqtzfsexx.getJgzjdm();//机构证件代码

        if (checknumList.contains("JS0778") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0779") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0780") && StringUtils.isNotBlank(pzbm)) {
            if (CheckUtil.checkStr(pzbm)) {
                result = true;
                tempMsg.append("凭证编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0781") && StringUtils.isNotBlank(jgzjdm)) {
            if (CheckUtil.checkStr(jgzjdm)) {
                result = true;
                tempMsg.append("机构证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }

        return result;
    }

    private boolean checkxgqtzfsexxRowJglx(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result =false;
        String jglx = xgqtzfsexx.getJglx();
        List jglxList = checkParamContext.getDataDictionarymap().get("jglxList");//机构类型
        if (checknumList.contains("JS0203")&&StringUtils.isNotBlank(jglx)){
            if (!jglxList.contains(jglx)){
                result = true;
                tempMsg.append("股权投资发生额信息表中的机构类型需在符合要求的最底层值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowJyfx(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result =false;
        String jyfx = xgqtzfsexx.getJyfx();//交易方向
        List baseCode = checkParamContext.getDataDictionarymap().get("jyfxList");//交易方向List
        if (checknumList.contains("JS0056")&&StringUtils.isNotBlank(jyfx)){
            if (!baseCode.contains(jyfx)){
                result = true;
                tempMsg.append("股权投资发生额信息表中的交易方向需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowBz(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result =false;
        String bz = xgqtzfsexx.getBz();//币种
        List baseCode = checkParamContext.getDataDictionarymap().get("baseCode");//币种代码
        if (checknumList.contains("JS0055")&&StringUtils.isNotBlank(bz)){
            if (!baseCode.contains(bz)){
                result = true;
                tempMsg.append("股权投资发生额信息表中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowDqdm(CheckParamContext checkParamContext, List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String dqdm = xgqtzfsexx.getDqdm();//地区代码
        List baseCodeGj = checkParamContext.getDataDictionarymap().get("baseCodeGj");//国家地区代码C0013
        List baseCodeXz = checkParamContext.getDataDictionarymap().get("baseCodeXz");//国家地区代码C0013
        String jglx = xgqtzfsexx.getJglx();//机构类型
        if (checknumList.contains("JS0054")&&StringUtils.isNotBlank(dqdm)){
            if ((!baseCodeGj.contains(dqdm)&&!baseCodeXz.contains(dqdm))){
                result = true;
                tempMsg.append("股权投资发生额信息表中的地区代码需在符合要求的值域范围内，境内客户为《中华人民共和国行政区划代码》（GB/T 2260）规定的最新行政区划代码（不包括港澳台），境外客户为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2075")&&StringUtils.isNotBlank(dqdm)&&StringUtils.isNotBlank(jglx)){
            try {
                if (jglx.substring(0,1).equals("E")&&!dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境外被投资机构的地区代码应该为000开头" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境外被投资机构的地区代码应该为000开头" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2076")&&StringUtils.isNotBlank(dqdm)&&StringUtils.isNotBlank(jglx)){
            try {
                if (!jglx.substring(0,1).equals("E")&&dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境内被投资机构的地区代码不应该为000开头" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内被投资机构的地区代码不应该为000开头" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkxgqtzfsexxRowGqlx(List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String gqlx = xgqtzfsexx.getGqlx();//股权类型
        if (checknumList.contains("JS0053")&& StringUtils.isNotBlank(gqlx)){
            if (!ZqFieldUtils.gqlxList.contains(gqlx)){
                tempMsg.append("股权投资发生额信息表中的股权类型需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }


    private boolean checkXgqtzfsexxRowSjrq(List<String> checknumList, Xgqtzfsexx xgqtzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xgqtzfsexx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)){
            if (sjrq.length()==10){
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1){
                    if (sjrq.compareTo("1800-01-01")<0 || sjrq.compareTo("2100-12-31")>0){
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                }else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            }else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        }else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }


}