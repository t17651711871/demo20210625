package com.geping.etl.UNITLOAN.util.check;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xcltyjdxx;
import com.geping.etl.UNITLOAN.entity.report.Xtyjdfsexx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;

/**    
*  
* @author liuweixin  
* @date 2021年2月24日 上午9:06:48  
*/
public class CheckAllDatatyModule {

	//币种代码
    private static List<String> currencyList;
	
	//存量同业借贷信息
	public static void checkXcltyjdxx(CustomSqlUtil customSqlUtil, List<String> checknumList,Xcltyjdxx xcltyjdxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
		boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String spaceStr = "|";
		
		String financeorgcode = xcltyjdxx.getFinanceorgcode();
		String financeorginnum = xcltyjdxx.getFinanceorginnum();
		String jydsdm = xcltyjdxx.getJydsdm();
		String jydsdmlb = xcltyjdxx.getJydsdmlb();
		String contractcode = xcltyjdxx.getContractcode();
		String zcfzlx = xcltyjdxx.getZcfzlx();
		String productcetegory = xcltyjdxx.getProductcetegory();
		String startdate = xcltyjdxx.getStartdate();
		String enddate = xcltyjdxx.getEnddate();
		String currency = xcltyjdxx.getCurrency();
		String receiptbalance = xcltyjdxx.getReceiptbalance();
		String receiptcnybalance = xcltyjdxx.getReceiptcnybalance();
		String interestisfixed = xcltyjdxx.getInterestisfixed();
		String interestislevel = xcltyjdxx.getInterestislevel();
		String fixpricetype = xcltyjdxx.getFixpricetype();
		String baseinterest = xcltyjdxx.getBaseinterest();
		String jxfs = xcltyjdxx.getJxfs();
		String sjrq = xcltyjdxx.getSjrq();
		
		//金融机构代码
		if (StringUtils.isNotBlank(financeorgcode)){
            if (CheckUtil.checkStr(financeorgcode)){
                if (checknumList.contains("JS0255")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }
            
            if (financeorgcode.length() !=18 ){
                if (checknumList.contains("JS0261")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }
        }else {
            if (checknumList.contains("JS0963")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }
		
		//内部机构号
		if (StringUtils.isNotBlank(financeorginnum)){
            
            if (checkStr2(financeorginnum)){
                if (checknumList.contains("JS0256")){     tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }
            
            if (financeorginnum.length() > 30){
                if (checknumList.contains("JS0262")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS0964")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }
		
		//交易对手代码
		if(StringUtils.isNotBlank(jydsdm)) {
			if(checknumList.contains("JS0257")) {
				if(CheckUtil.checkStr(jydsdm)) {
					tempMsg.append("交易对手代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0263")) {
				if(jydsdm.length() > 60) {
					tempMsg.append("交易对手代码字符长度不能超过60"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0925")) {
				if(StringUtils.isNotBlank(jydsdmlb) && ("A01".equals(jydsdmlb) || "A02".equals(jydsdmlb))) {
					if(jydsdm.length() != 9 && jydsdm.length() != 18) {
						tempMsg.append("境内非SPV客户的交易对手代码字符长度应该为9位或18位"+spaceStr);
	                    isError=true;
					}
				}
			}
		}else {
			if(checknumList.contains("JS0965")) {
				tempMsg.append("交易对手代码不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//交易对手代码类别
		if(StringUtils.isNotBlank(jydsdmlb)) {
			if(checknumList.contains("JS0074")) {
				if(!CheckUtil.checkJydszjlx(jydsdmlb)) {
					tempMsg.append("交易对手代码类别应该在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS1527")) {
				tempMsg.append("交易对手代码类别不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//合同编码
		if(StringUtils.isNotBlank(contractcode)) {
			if(checknumList.contains("JS0258")) {
				if(CheckUtil.checkStr(contractcode)) {
					tempMsg.append("合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0264")) {
				if(contractcode.length() > 100) {
					tempMsg.append("合同编码字符长度不能超过100"+spaceStr);
                    isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0966")) {
				tempMsg.append("合同编码不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//资产负债类型
		if(StringUtils.isNotBlank(zcfzlx)) {
			if(checknumList.contains("JS0066")) {
				if(!CheckUtil.checkZcfzlx(zcfzlx)) {
					tempMsg.append("资产负债类型需在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0967")) {
				tempMsg.append("资产负债类型不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//产品类别
		if(StringUtils.isNotBlank(productcetegory)) {
			if(checknumList.contains("JS0067")) {
				if(!CheckUtil.checkCplb_F06(productcetegory)) {
					tempMsg.append("产品类别需在符合要求的最底层值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0968")) {
				tempMsg.append("产品类别不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//合同起始日期
		if(StringUtils.isNotBlank(startdate)) {
        	if(checknumList.contains("JS0269")) {
        		boolean b1 = CheckUtil.checkDate(startdate, "yyyy-MM-dd");
				if (b1) {
					if (startdate.compareTo("1800-01-01") < 0 || startdate.compareTo("2100-12-31") > 0) {
						tempMsg.append("合同起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("合同起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2411")) {
        		if(StringUtils.isNotBlank(sjrq) && startdate.compareTo(sjrq) > 0) {
        			tempMsg.append("合同起始日期应小于等于数据日期" + spaceStr);
					isError = true;
            	}
        	}
        	if(checknumList.contains("JS2412")) {
        		if(StringUtils.isNotBlank(enddate) && startdate.compareTo(enddate) > 0) {
        			tempMsg.append("合同起始日期应小于等于合同到期日期" + spaceStr);
					isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS0969")){ tempMsg.append("合同起始日期不能为空" + spaceStr);isError = true; }
        }
		
		//合同到期日期
		if(StringUtils.isNotBlank(enddate)) {
        	if(checknumList.contains("JS0270")) {
        		boolean b1 = CheckUtil.checkDate(enddate, "yyyy-MM-dd");
				if (b1) {
					if (enddate.compareTo("1800-01-01") < 0 || enddate.compareTo("2100-12-31") > 0) {
						tempMsg.append("合同到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("合同到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        }else {
        	if (checknumList.contains("JS0970")){ tempMsg.append("合同到期日期不能为空" + spaceStr);isError = true; }
        }
		
		//币种
		if(StringUtils.isNotBlank(currency)) {
        	if(currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
        	if(checknumList.contains("JS0068")) {
        		if(!currencyList.contains(currency)) {
            		tempMsg.append("币种需在值域范围内" + spaceStr);
    				isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS0971")){ tempMsg.append("币种不能为空" + spaceStr);isError = true; }
        }
		
		//合同余额
		if(StringUtils.isNotBlank(receiptbalance)) {
        	if(checknumList.contains("JS0266")) {
        		if (receiptbalance.length() > 20 || !CheckUtil.checkDecimal(receiptbalance, 2)) {
					tempMsg.append("合同余额总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2415")) {
        		try {
        			if(new BigDecimal(receiptbalance).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("合同余额应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("合同余额应大于0" + spaceStr);
					isError = true;
        		}
        		
        	}
        }else {
        	if (checknumList.contains("JS0972")){ tempMsg.append("合同余额不能为空" + spaceStr);isError = true; }
        }
		
		//合同余额折人民币
		if(StringUtils.isNotBlank(receiptcnybalance)) {
        	if(checknumList.contains("JS0267")) {
        		if (receiptcnybalance.length() > 20 || !CheckUtil.checkDecimal(receiptcnybalance, 2)) {
					tempMsg.append("合同余额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2416")) {
        		try {
        			if(new BigDecimal(receiptcnybalance).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("合同余额折人民币应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("合同余额折人民币应大于0" + spaceStr);
					isError = true;
        		}
        	}
        	if(checknumList.contains("JS2688")) {
        		if(StringUtils.isNotBlank(currency) && "CNY".equals(currency)) {
        			if(StringUtils.isNotBlank(receiptbalance) && !receiptcnybalance.equals(receiptbalance)) {
        				tempMsg.append("币种为人民币的，合同余额折人民币应该与合同余额的值相等" + spaceStr);
    					isError = true;
        			}
        		}
        	}
        }else {
        	if (checknumList.contains("JS0973")){ tempMsg.append("合同余额折人民币不能为空" + spaceStr);isError = true; }
        }
		
		//利率是否固定
		if(StringUtils.isNotBlank(interestisfixed)) {
			if(checknumList.contains("JS0069")) {
				if(!Arrays.asList(CheckUtil.llsfgd).contains(interestisfixed)){
					tempMsg.append("利率是否固定需在符合要求的值域范围内" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS0974")){ tempMsg.append("利率是否固定不能为空" + spaceStr);isError = true; }
		}
		
		//利率水平
		if(StringUtils.isNotBlank(interestislevel)) {
			if(checknumList.contains("JS0259")) {
				if(interestislevel.contains("%") || interestislevel.contains("‰")) {
					tempMsg.append("利率水平不能包含‰或%" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0268")) {
        		if (interestislevel.length() > 10 || !CheckUtil.checkDecimal(interestislevel, 5)) {
					tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
					isError = true;
				}
        	}
			if(checknumList.contains("JS2413")) {
				try {
        			if(new BigDecimal(interestislevel).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(interestislevel).compareTo(new BigDecimal("30")) == 1) {
        				tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
    					isError = true;
        			}
        		}catch(Exception e) {
        			tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
					isError = true;
        		}
			}
			if(checknumList.contains("JS2414")) {
				if(StringUtils.isNotBlank(jxfs) && "B05".equals(jxfs)) {
					if(new BigDecimal(interestislevel).compareTo(BigDecimal.ZERO) != 0) {
						tempMsg.append("当计息方式为不计息时，利率水平应等于0" + spaceStr);
						isError = true;
					}
				}
			}
		}else {
			if (checknumList.contains("JS0975")){ tempMsg.append("利率水平不能为空" + spaceStr);isError = true; }
		}
		
		//定价基准类型
		if(StringUtils.isNotBlank(fixpricetype)) {
			if(checknumList.contains("JS0070")) {
				if(!Arrays.asList(CheckUtil.loanjzlx).contains(fixpricetype)) {
					tempMsg.append("定价基准类型需在符合要求的值域范围内" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS0976")){ tempMsg.append("定价基准类型不能为空" + spaceStr);isError = true; }
		}
		
		//基准利率
		if(StringUtils.isNotBlank(baseinterest)) {
			if(checknumList.contains("JS0260")) {
				if(baseinterest.contains("%") || baseinterest.contains("‰")) {
					tempMsg.append("基准利率不能包含‰或%" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0265")) {
        		if (baseinterest.length() > 10 || !CheckUtil.checkDecimal(baseinterest, 5)) {
					tempMsg.append("基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
					isError = true;
				}
        	}
			if(checknumList.contains("JS2349")) {
				if(StringUtils.isNotBlank(interestisfixed) && "RF02".equals(interestisfixed)) {
					try {
	        			if(new BigDecimal(baseinterest).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(baseinterest).compareTo(new BigDecimal("30")) == 1) {
	        				tempMsg.append("当利率为浮动利率时，基准利率应大于等于0且小于等于30" + spaceStr);
	    					isError = true;
	        			}
	        		}catch(Exception e) {
	        			tempMsg.append("当利率为浮动利率时，基准利率应大于等于0且小于等于30" + spaceStr);
						isError = true;
	        		}
				}
			}
			if(checknumList.contains("JS2155")) {
				if(StringUtils.isNotBlank(interestisfixed) && "RF01".equals(interestisfixed)) {
					tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS1528")){ 
				if(StringUtils.isNotBlank(interestisfixed) && "RF02".equals(interestisfixed)) {
					tempMsg.append("当利率为浮动利率时，基准利率不能为空" + spaceStr);
					isError = true; 
				}
			}
		}
		
		//计息方式
		if(StringUtils.isNotBlank(jxfs)) {
			if(checknumList.contains("JS0071")) {
				if(!CheckUtil.checkJxfs(jxfs)) {
					tempMsg.append("计息方式需在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0977")) {
				tempMsg.append("计息方式不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//数据日期
		isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期",false);
		
		//#整表
        if (isError) {
            if(!errorMsg.containsKey(xcltyjdxx.getId())) {
                errorMsg.put(xcltyjdxx.getId(), "contractcode:"+xcltyjdxx.getContractcode()+"，"+"交易对手代码:"+xcltyjdxx.getJydsdm()+"]->\r\n");
            }
            String str = errorMsg.get(xcltyjdxx.getId());
            str = str + tempMsg;
            errorMsg.put(xcltyjdxx.getId(), str);
            errorId.add(xcltyjdxx.getId());
        } else {
            if(!errorId.contains(xcltyjdxx.getId())) {
                rightId.add(xcltyjdxx.getId());
            }
        }
	}
	
	//同业借贷发生额信息
	public static void checkXtyjdfsexx(CustomSqlUtil customSqlUtil, List<String> checknumList,Xtyjdfsexx xtyjdfsexx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
		boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String spaceStr = "|";
		
		String financeorgcode = xtyjdfsexx.getFinanceorgcode();
		String financeorginnum = xtyjdfsexx.getFinanceorginnum();
		String jydsdm = xtyjdfsexx.getJydsdm();
		String jydsdmlb = xtyjdfsexx.getJydsdmlb();
		String contractcode = xtyjdfsexx.getContractcode();
		String jylsh = xtyjdfsexx.getJylsh();
		String zcfzlx = xtyjdfsexx.getZcfzlx();
		String productcetegory = xtyjdfsexx.getProductcetegory();
		String startdate = xtyjdfsexx.getStartdate();
		String enddate = xtyjdfsexx.getEnddate();
		String finaldate = xtyjdfsexx.getFinaldate();
		String currency = xtyjdfsexx.getCurrency();
		String receiptbalance = xtyjdfsexx.getReceiptbalance();
		String receiptcnybalance = xtyjdfsexx.getReceiptcnybalance();
		String interestisfixed = xtyjdfsexx.getInterestisfixed();
		String interestislevel = xtyjdfsexx.getInterestislevel();
		String fixpricetype = xtyjdfsexx.getFixpricetype();
		String baseinterest = xtyjdfsexx.getBaseinterest();
		String fsjqbs = xtyjdfsexx.getFsjqbs();
		String jxfs = xtyjdfsexx.getJxfs();
		String sjrq = xtyjdfsexx.getSjrq();
		
		//金融机构代码
		if (StringUtils.isNotBlank(financeorgcode)){
            if (CheckUtil.checkStr(financeorgcode)){
                if (checknumList.contains("JS0272")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }
            
            if (financeorgcode.length() !=18 ){
                if (checknumList.contains("JS0279")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }
        }else {
            if (checknumList.contains("JS0979")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }
		
		//内部机构号
		if (StringUtils.isNotBlank(financeorginnum)){
            
            if (checkStr2(financeorginnum)){
                if (checknumList.contains("JS0273")){     tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }
            
            if (financeorginnum.length() > 30){
                if (checknumList.contains("JS0280")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS0980")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }
		
		//交易对手代码
		if(StringUtils.isNotBlank(jydsdm)) {
			if(checknumList.contains("JS0274")) {
				if(CheckUtil.checkStr(jydsdm)) {
					tempMsg.append("交易对手代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0281")) {
				if(jydsdm.length() > 60) {
					tempMsg.append("交易对手代码字符长度不能超过60"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0951")) {
				if(StringUtils.isNotBlank(jydsdmlb) && ("A01".equals(jydsdmlb) || "A02".equals(jydsdmlb))) {
					if(jydsdm.length() != 9 && jydsdm.length() != 18) {
						tempMsg.append("境内非SPV客户的交易对手代码字符长度应该为9位或18位"+spaceStr);
	                    isError=true;
					}
				}
			}
		}else {
			if(checknumList.contains("JS0981")) {
				tempMsg.append("交易对手代码不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//交易对手代码类别
		if(StringUtils.isNotBlank(jydsdmlb)) {
			if(checknumList.contains("JS0082")) {
				if(!CheckUtil.checkJydszjlx(jydsdmlb)) {
					tempMsg.append("交易对手代码类别应该在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS1568")) {
				tempMsg.append("交易对手代码类别不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//合同编码
		if(StringUtils.isNotBlank(contractcode)) {
			if(checknumList.contains("JS0275")) {
				if(CheckUtil.checkStr(contractcode)) {
					tempMsg.append("合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0282")) {
				if(contractcode.length() > 100) {
					tempMsg.append("合同编码字符长度不能超过100"+spaceStr);
                    isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0982")) {
				tempMsg.append("合同编码不能为空"+spaceStr);
				isError=true;
			}
		}

		//交易流水号
		if(StringUtils.isNotBlank(jylsh)) {
			if(checknumList.contains("JS0276")) {
				if(CheckUtil.checkStr(jylsh)) {
					tempMsg.append("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
				}
			}
			if(checknumList.contains("JS0283")) {
				if(jylsh.length() > 60) {
					tempMsg.append("交易流水号字段长度不能超过60"+spaceStr);
                    isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS1569")) {
				tempMsg.append("交易流水号不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//资产负债类型
		if(StringUtils.isNotBlank(zcfzlx)) {
			if(checknumList.contains("JS0075")) {
				if(!CheckUtil.checkZcfzlx(zcfzlx)) {
					tempMsg.append("资产负债类型需在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0983")) {
				tempMsg.append("资产负债类型不能为空"+spaceStr);
				isError=true;
			}
		}
		
		//产品类别
		if(StringUtils.isNotBlank(productcetegory)) {
			if(checknumList.contains("JS0076")) {
				if(!CheckUtil.checkCplb_F06(productcetegory)) {
					tempMsg.append("产品类别需在符合要求的最底层值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0984")) {
				tempMsg.append("产品类别不能为空"+spaceStr);
				isError=true;
			}
		}
				
		//合同起始日期
		if(StringUtils.isNotBlank(startdate)) {
        	if(checknumList.contains("JS0288")) {
        		boolean b1 = CheckUtil.checkDate(startdate, "yyyy-MM-dd");
				if (b1) {
					if (startdate.compareTo("1800-01-01") < 0 || startdate.compareTo("2100-12-31") > 0) {
						tempMsg.append("合同起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("合同起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2417")) {
        		if(StringUtils.isNotBlank(sjrq) && startdate.compareTo(sjrq) > 0) {
        			tempMsg.append("合同起始日期应小于等于数据日期" + spaceStr);
					isError = true;
            	}
        	}
        	if(checknumList.contains("JS2418")) {
        		if(StringUtils.isNotBlank(enddate) && startdate.compareTo(enddate) > 0) {
        			tempMsg.append("合同起始日期应小于等于合同到期日期" + spaceStr);
					isError = true;
            	}
        	}
        	if(checknumList.contains("JS2574")) {
        		if(StringUtils.isNotBlank(fsjqbs) && "1".equals(fsjqbs)) {
        			try {
        				if(!startdate.substring(0,7).equals(sjrq.substring(0,7))) {
            				tempMsg.append("当月新发生的业务，合同起始日期应该在当月范围内" + spaceStr);
        					isError = true;
            			}
        			}catch(Exception e){
        				tempMsg.append("当月新发生的业务，合同起始日期应该在当月范围内" + spaceStr);
    					isError = true;
        			}
        			
            	}
        	}
        }else {
        	if (checknumList.contains("JS0985")){ tempMsg.append("合同起始日期不能为空" + spaceStr);isError = true; }
        }
		
		//合同到期日期
		if(StringUtils.isNotBlank(enddate)) {
        	if(checknumList.contains("JS0289")) {
        		boolean b1 = CheckUtil.checkDate(enddate, "yyyy-MM-dd");
				if (b1) {
					if (enddate.compareTo("1800-01-01") < 0 || enddate.compareTo("2100-12-31") > 0) {
						tempMsg.append("合同到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("合同到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        }else {
        	if (checknumList.contains("JS0986")){ tempMsg.append("合同到期日期不能为空" + spaceStr);isError = true; }
        }
		
		//合同实际终止日期
		if(StringUtils.isNotBlank(finaldate)) {
        	if(checknumList.contains("JS0884")) {
        		boolean b1 = CheckUtil.checkDate(finaldate, "yyyy-MM-dd");
				if (b1) {
					if (finaldate.compareTo("1800-01-01") < 0 || finaldate.compareTo("2100-12-31") > 0) {
						tempMsg.append("合同实际终止日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("合同实际终止日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2372")) {
        		if(StringUtils.isNotBlank(sjrq) && finaldate.compareTo(sjrq) > 0) {
        			tempMsg.append("合同实际终止日期应小于等于数据日期 且应该在报送期内" + spaceStr);
					isError = true;
            	}
        		try {
    				if(!finaldate.substring(0,7).equals(sjrq.substring(0,7))) {
        				tempMsg.append("合同实际终止日期应小于等于数据日期 且应该在报送期内" + spaceStr);
    					isError = true;
        			}
    			}catch(Exception e){
    				tempMsg.append("合同实际终止日期应小于等于数据日期 且应该在报送期内" + spaceStr);
					isError = true;
    			}
        	}
        	if(checknumList.contains("JS2361")) {
        		if(StringUtils.isNotBlank(startdate) && finaldate.compareTo(startdate) < 0) {
        			tempMsg.append("合同实际终止日期应大于等于合同起始日期" + spaceStr);
					isError = true;
            	}
        	}
        }
		
		//币种
		if(StringUtils.isNotBlank(currency)) {
        	if(currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
        	if(checknumList.contains("JS0077")) {
        		if(!currencyList.contains(currency)) {
            		tempMsg.append("币种需在值域范围内" + spaceStr);
    				isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS0987")){ tempMsg.append("币种不能为空" + spaceStr);isError = true; }
        }
		
		//发生金额
		if(StringUtils.isNotBlank(receiptbalance)) {
        	if(checknumList.contains("JS0285")) {
        		if (receiptbalance.length() > 20 || !CheckUtil.checkDecimal(receiptbalance, 2)) {
					tempMsg.append("发生金额总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2421")) {
        		try {
        			if(new BigDecimal(receiptbalance).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("发生金额应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("发生金额应大于0" + spaceStr);
					isError = true;
        		}
        		
        	}
        }else {
        	if (checknumList.contains("JS0988")){ tempMsg.append("发生金额不能为空" + spaceStr);isError = true; }
        }
				
		//发生金额折人民币
		if(StringUtils.isNotBlank(receiptcnybalance)) {
        	if(checknumList.contains("JS0286")) {
        		if (receiptcnybalance.length() > 20 || !CheckUtil.checkDecimal(receiptcnybalance, 2)) {
					tempMsg.append("发生金额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2422")) {
        		try {
        			if(new BigDecimal(receiptcnybalance).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("发生金额折人民币应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("发生金额折人民币应大于0" + spaceStr);
					isError = true;
        		}
        	}
        	if(checknumList.contains("JS2689")) {
        		if(StringUtils.isNotBlank(currency) && "CNY".equals(currency)) {
        			if(StringUtils.isNotBlank(receiptbalance) && !receiptcnybalance.equals(receiptbalance)) {
        				tempMsg.append("币种为人民币的，发生金额折人民币应该与发生金额的值相等" + spaceStr);
    					isError = true;
        			}
        		}
        	}
        }else {
        	if (checknumList.contains("JS0989")){ tempMsg.append("发生金额折人民币不能为空" + spaceStr);isError = true; }
        }
		
		//利率是否固定
		if(StringUtils.isNotBlank(interestisfixed)) {
			if(checknumList.contains("JS0078")) {
				if(!Arrays.asList(CheckUtil.llsfgd).contains(interestisfixed)){
					tempMsg.append("利率是否固定需在符合要求的值域范围内" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS0990")){ tempMsg.append("利率是否固定不能为空" + spaceStr);isError = true; }
		}
		
		//利率水平
		if(StringUtils.isNotBlank(interestislevel)) {
			if(checknumList.contains("JS0277")) {
				if(interestislevel.contains("%") || interestislevel.contains("‰")) {
					tempMsg.append("利率水平不能包含‰或%" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0287")) {
        		if (interestislevel.length() > 10 || !CheckUtil.checkDecimal(interestislevel, 5)) {
					tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
					isError = true;
				}
        	}
			if(checknumList.contains("JS2419")) {
				try {
        			if(new BigDecimal(interestislevel).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(interestislevel).compareTo(new BigDecimal("30")) == 1) {
        				tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
    					isError = true;
        			}
        		}catch(Exception e) {
        			tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
					isError = true;
        		}
			}
			if(checknumList.contains("JS2420")) {
				if(StringUtils.isNotBlank(jxfs) && "B05".equals(jxfs)) {
					if(new BigDecimal(interestislevel).compareTo(BigDecimal.ZERO) != 0) {
						tempMsg.append("当计息方式为不计息时，利率水平应等于0" + spaceStr);
						isError = true;
					}
				}
			}
		}else {
			if (checknumList.contains("JS0991")){ tempMsg.append("利率水平不能为空" + spaceStr);isError = true; }
		}
		
		//定价基准类型
		if(StringUtils.isNotBlank(fixpricetype)) {
			if(checknumList.contains("JS0079")) {
				if(!Arrays.asList(CheckUtil.loanjzlx).contains(fixpricetype)) {
					tempMsg.append("定价基准类型需在符合要求的值域范围内" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS0992")){ tempMsg.append("定价基准类型不能为空" + spaceStr);isError = true; }
		}
		
		//基准利率
		if(StringUtils.isNotBlank(baseinterest)) {
			if(checknumList.contains("JS0278")) {
				if(baseinterest.contains("%") || baseinterest.contains("‰")) {
					tempMsg.append("基准利率不能包含‰或%" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0284")) {
        		if (baseinterest.length() > 10 || !CheckUtil.checkDecimal(baseinterest, 5)) {
					tempMsg.append("基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
					isError = true;
				}
        	}
			if(checknumList.contains("JS2362")) {
				if(StringUtils.isNotBlank(interestisfixed) && "RF02".equals(interestisfixed)) {
					try {
	        			if(new BigDecimal(baseinterest).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(baseinterest).compareTo(new BigDecimal("30")) == 1) {
	        				tempMsg.append("当利率为浮动利率时，基准利率应大于等于0且小于等于30" + spaceStr);
	    					isError = true;
	        			}
	        		}catch(Exception e) {
	        			tempMsg.append("当利率为浮动利率时，基准利率应大于等于0且小于等于30" + spaceStr);
						isError = true;
	        		}
				}
			}
			if(checknumList.contains("JS2156")) {
				if(StringUtils.isNotBlank(interestisfixed) && "RF01".equals(interestisfixed)) {
					tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);
					isError = true;
				}
			}
		}else {
			if (checknumList.contains("JS1570")){ 
				if(StringUtils.isNotBlank(interestisfixed) && "RF02".equals(interestisfixed)) {
					tempMsg.append("当利率为浮动利率时，基准利率不能为空" + spaceStr);
					isError = true; 
				}
			}
		}
		
		//计息方式
		if(StringUtils.isNotBlank(jxfs)) {
			if(checknumList.contains("JS0080")) {
				if(!CheckUtil.checkJxfs(jxfs)) {
					tempMsg.append("计息方式需在符合要求的值域范围内"+spaceStr);
					isError=true;
				}
			}
		}else {
			if(checknumList.contains("JS0993")) {
				tempMsg.append("计息方式不能为空"+spaceStr);
				isError=true;
			}
		}

		//数据日期
		isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期",false);
		
		//#整表
        if (isError) {
            if(!errorMsg.containsKey(xtyjdfsexx.getId())) {
                errorMsg.put(xtyjdfsexx.getId(), "contractcode:"+xtyjdfsexx.getContractcode()+"，"+"交易对手代码:"+xtyjdfsexx.getJydsdm()+"]->\r\n");
            }
            String str = errorMsg.get(xtyjdfsexx.getId());
            str = str + tempMsg;
            errorMsg.put(xtyjdfsexx.getId(), str);
            errorId.add(xtyjdfsexx.getId());
        } else {
            if(!errorId.contains(xtyjdfsexx.getId())) {
                rightId.add(xtyjdfsexx.getId());
            }
        }
		
	}
}
