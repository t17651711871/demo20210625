package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;

/***
 *   row校验处理器
 * @author liang.xu
 * @date 2021.4.20
 */
public interface CheckRowData {


    void checkRow(String header, CheckParamContext checkParamContext, Object object);

}