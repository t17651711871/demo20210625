package com.geping.etl.UNITLOAN.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.StringUtils;

//import com.ctc.wstx.util.StringUtil;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrBaseinfo;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

/**
 * 对象工具
 * @author WuZengWen
 * @date 2020年7月23日 下午3:50:51
 */
public class ObjectUtils {

	public static Object zhengLiDuiXiang(Object obj,Sys_UserAndOrgDepartment sys_user) {
		String dx = obj.getClass().getTypeName();
		String lei = dx.substring((dx.lastIndexOf('.')+1));
		if("Xcldkxx".equals(lei)) {
			
		}else if("Xdkdbht".equals(lei)) {
			
		}else if("Xdkdbwx".equals(lei)) {
			//单位贷款担保物信息 对象整理
			Xdkdbwx dkdbwx = (Xdkdbwx)obj;
			if(StringUtils.isBlank(dkdbwx.getId())) {
				dkdbwx.setId(Stringutil.getUUid());
			}
			//评估价值
			dkdbwx.setAssessvalue(Stringutil.zhengLiJinE(dkdbwx.getAssessvalue()));
			//担保物账面价值
			dkdbwx.setGteegoodsamt(Stringutil.zhengLiJinE(dkdbwx.getGteegoodsamt()));
			//优先受偿权数额
			dkdbwx.setFirstrightamt(Stringutil.zhengLiJinE(dkdbwx.getFirstrightamt()));
			//抵质押率
			dkdbwx.setMortgagepgerate(Stringutil.zhengLiJinE(dkdbwx.getMortgagepgerate()));
			dkdbwx.setCheckstatus("0");
			dkdbwx.setDatastatus("0");
			dkdbwx.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			dkdbwx.setOperator(sys_user.getLoginid());
			dkdbwx.setOrgid(sys_user.getOrgid());
			dkdbwx.setDepartid(sys_user.getDepartid());
			return dkdbwx;
		}else if("Xdkfsxx".equals(lei)) {
			
		}else if("Xftykhx".equals(lei)) {
			
		}else if("XjrjgfrAssets".equals(lei)) {
			//法人资产 对象整理
			XjrjgfrAssets jrjgfrAssets = (XjrjgfrAssets)obj;
			if(StringUtils.isBlank(jrjgfrAssets.getId())) {
				jrjgfrAssets.setId(Stringutil.getUUid());
			}
			//各项存款
			jrjgfrAssets.setGxck(Stringutil.zhengLiJinE(jrjgfrAssets.getGxck()));
			//各项贷款
			jrjgfrAssets.setGxdk(Stringutil.zhengLiJinE(jrjgfrAssets.getGxdk()));
			//资产总计
			jrjgfrAssets.setZczj(Stringutil.zhengLiJinE(jrjgfrAssets.getZczj()));
			//负债总计
			jrjgfrAssets.setFzzj(Stringutil.zhengLiJinE(jrjgfrAssets.getFzzj()));
			//所有者权益合计
			jrjgfrAssets.setSyzqyhj(Stringutil.zhengLiJinE(jrjgfrAssets.getSyzqyhj()));
			//生息资产
			jrjgfrAssets.setSxzc(Stringutil.zhengLiJinE(jrjgfrAssets.getSxzc()));
			//付息负债
			jrjgfrAssets.setFxzc(Stringutil.zhengLiJinE(jrjgfrAssets.getFxzc()));
			//流动性资产
			jrjgfrAssets.setLdxzc(Stringutil.zhengLiJinE(jrjgfrAssets.getLdxzc()));
			//流动性负债
			jrjgfrAssets.setLdxfz(Stringutil.zhengLiJinE(jrjgfrAssets.getLdxfz()));
			//正常类贷款
			jrjgfrAssets.setZcldk(Stringutil.zhengLiJinE(jrjgfrAssets.getZcldk()));
			//关注类贷款
			jrjgfrAssets.setGzldk(Stringutil.zhengLiJinE(jrjgfrAssets.getGzldk()));
			//次级类贷款
			jrjgfrAssets.setCjldk(Stringutil.zhengLiJinE(jrjgfrAssets.getCjldk()));
			//可疑类贷款
			jrjgfrAssets.setJyldk(Stringutil.zhengLiJinE(jrjgfrAssets.getJyldk()));
			//损失类贷款
			jrjgfrAssets.setSsldk(Stringutil.zhengLiJinE(jrjgfrAssets.getSsldk()));
			//逾期贷款
			jrjgfrAssets.setYqdk(Stringutil.zhengLiJinE(jrjgfrAssets.getYqdk()));
			//逾期90天以上贷款
			jrjgfrAssets.setYqninetytysdk(Stringutil.zhengLiJinE(jrjgfrAssets.getYqninetytysdk()));
			//贷款减值准备
			jrjgfrAssets.setDkjzzb(Stringutil.zhengLiJinE(jrjgfrAssets.getDkjzzb()));
			jrjgfrAssets.setCheckstatus("0");
			jrjgfrAssets.setDatastatus("0");
			jrjgfrAssets.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			jrjgfrAssets.setOperator(sys_user.getLoginid());
			jrjgfrAssets.setOrgid(sys_user.getOrgid());
			jrjgfrAssets.setDepartid(sys_user.getDepartid());
			return jrjgfrAssets;
		}else if("XjrjgfrBaseinfo".equals(lei)) {
			//法人基础 对象整理
			XjrjgfrBaseinfo jrjgfrBaseinfo = (XjrjgfrBaseinfo)obj;
			if(StringUtils.isBlank(jrjgfrBaseinfo.getId())) {
				jrjgfrBaseinfo.setId(Stringutil.getUUid());
			}
			//注册资本
			jrjgfrBaseinfo.setRegamt(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getRegamt()));
			//第一大股东持股比例
			jrjgfrBaseinfo.setOnestockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getOnestockprop()));
			//第二大股东持股比例
			jrjgfrBaseinfo.setTwostockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getTwostockprop()));
			//第三大股东持股比例
			jrjgfrBaseinfo.setThreestockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getThreestockprop()));
			//第四大股东持股比例
			jrjgfrBaseinfo.setFourstockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getFourstockprop()));
			//第五大股东持股比例
			jrjgfrBaseinfo.setFivestockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getFivestockprop()));
			//第六大股东持股比例
			jrjgfrBaseinfo.setSixstockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getSixstockprop()));
			//第七大股东持股比例
			jrjgfrBaseinfo.setSevenstockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getSevenstockprop()));
			//第八大股东持股比例
			jrjgfrBaseinfo.setEightstockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getEightstockprop()));
			//第九大股东持股比例
			jrjgfrBaseinfo.setNinestockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getNinestockprop()));
			//第十大股东持股比例
			jrjgfrBaseinfo.setTenstockprop(Stringutil.zhengLiJinE(jrjgfrBaseinfo.getTenstockprop()));
			jrjgfrBaseinfo.setCheckstatus("0");
			jrjgfrBaseinfo.setDatastatus("0");
			jrjgfrBaseinfo.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			jrjgfrBaseinfo.setOperator(sys_user.getLoginid());
			jrjgfrBaseinfo.setOrgid(sys_user.getOrgid());
			jrjgfrBaseinfo.setDepartid(sys_user.getDepartid());
			return jrjgfrBaseinfo;
		}else if("XjrjgfrProfit".equals(lei)) {
			//法人利润 对象整理
			XjrjgfrProfit jrjgfrProfit = (XjrjgfrProfit)obj;
			if(StringUtils.isBlank(jrjgfrProfit.getId())) {
				jrjgfrProfit.setId(Stringutil.getUUid());
			}
			//营业收入
			jrjgfrProfit.setYysr(Stringutil.zhengLiJinE(jrjgfrProfit.getYysr()));
			//利息净收入
			jrjgfrProfit.setLxjsr(Stringutil.zhengLiJinE(jrjgfrProfit.getLxjsr()));
			//利息收入
			jrjgfrProfit.setLxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getLxsr()));
			//金融机构往来利息收入
			jrjgfrProfit.setJrjgwllxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getJrjgwllxsr()));
			//其中：系统内往来利息收入
			jrjgfrProfit.setXtnwllxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getXtnwllxsr()));
			//各项贷款利息收入
			jrjgfrProfit.setGxdklxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getGxdklxsr()));
			//债券利息收入
			jrjgfrProfit.setZqlxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getZqlxsr()));
			//其他利息收入
			jrjgfrProfit.setQtlxsr(Stringutil.zhengLiJinE(jrjgfrProfit.getQtlxsr()));
			//利息支出
			jrjgfrProfit.setLxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getLxzc()));
			//金融机构往来利息支出
			jrjgfrProfit.setJrjgwllxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getJrjgwllxzc()));
			//其中：系统内往来利息支出
			jrjgfrProfit.setXtnwllxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getXtnwllxzc()));
			//各项存款利息支出
			jrjgfrProfit.setGxcklxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getGxcklxzc()));
			//债券利息支出
			jrjgfrProfit.setZqlxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getZqlxzc()));
			//其他利息支出
			jrjgfrProfit.setQtlxzc(Stringutil.zhengLiJinE(jrjgfrProfit.getQtlxzc()));
			//手续费及佣金净收入
			jrjgfrProfit.setSxfjyjjsr(Stringutil.zhengLiJinE(jrjgfrProfit.getSxfjyjjsr()));
			//手续费及佣金收入
			jrjgfrProfit.setSxfjyjsr(Stringutil.zhengLiJinE(jrjgfrProfit.getSxfjyjsr()));
			//手续费及佣金支出
			jrjgfrProfit.setJxfjyjzc(Stringutil.zhengLiJinE(jrjgfrProfit.getJxfjyjzc()));
			//租赁收益
			jrjgfrProfit.setZlsy(Stringutil.zhengLiJinE(jrjgfrProfit.getZlsy()));
			//投资收益
			jrjgfrProfit.setTzsy(Stringutil.zhengLiJinE(jrjgfrProfit.getTzsy()));
			//债券投资收益
			jrjgfrProfit.setZqtzsy(Stringutil.zhengLiJinE(jrjgfrProfit.getZqtzsy()));
			//股权投资收益
			jrjgfrProfit.setGqtzsy(Stringutil.zhengLiJinE(jrjgfrProfit.getGqtzsy()));
			//其他投资收益
			jrjgfrProfit.setQttzsy(Stringutil.zhengLiJinE(jrjgfrProfit.getQttzsy()));
			//公允价值变动收益
			jrjgfrProfit.setGyjzbdsy(Stringutil.zhengLiJinE(jrjgfrProfit.getGyjzbdsy()));
			//汇兑净收益
			jrjgfrProfit.setHdjsy(Stringutil.zhengLiJinE(jrjgfrProfit.getHdjsy()));
			//资产处置收益
			jrjgfrProfit.setZcczsy(Stringutil.zhengLiJinE(jrjgfrProfit.getZcczsy()));
			//其他业务收入
			jrjgfrProfit.setQtywsr(Stringutil.zhengLiJinE(jrjgfrProfit.getQtywsr()));
			//营业支出
			jrjgfrProfit.setYyzc(Stringutil.zhengLiJinE(jrjgfrProfit.getYyzc()));
			//业务及管理费
			jrjgfrProfit.setYwjglf(Stringutil.zhengLiJinE(jrjgfrProfit.getYwjglf()));
			//其中:职工工资
			jrjgfrProfit.setZggz(Stringutil.zhengLiJinE(jrjgfrProfit.getZggz()));
			//福利费
			jrjgfrProfit.setFlf(Stringutil.zhengLiJinE(jrjgfrProfit.getFlf()));
			//住房公积金和住房补贴
			jrjgfrProfit.setZfgjjhzfbt(Stringutil.zhengLiJinE(jrjgfrProfit.getZfgjjhzfbt()));
			//税金及附加
			jrjgfrProfit.setSjjfj(Stringutil.zhengLiJinE(jrjgfrProfit.getSjjfj()));
			//资产减值损失
			jrjgfrProfit.setZcjzss(Stringutil.zhengLiJinE(jrjgfrProfit.getZcjzss()));
			//其他业务支出
			jrjgfrProfit.setQtywzc(Stringutil.zhengLiJinE(jrjgfrProfit.getQtywzc()));
			//营业利润
			jrjgfrProfit.setYylr(Stringutil.zhengLiJinE(jrjgfrProfit.getYylr()));
			//营业外收入（加）
			jrjgfrProfit.setYywsr(Stringutil.zhengLiJinE(jrjgfrProfit.getYywsr()));
			//营业外支出（减）
			jrjgfrProfit.setYywzc(Stringutil.zhengLiJinE(jrjgfrProfit.getYywzc()));
			//利润总额
			jrjgfrProfit.setLrze(Stringutil.zhengLiJinE(jrjgfrProfit.getLrze()));
			//所得税（减）
			jrjgfrProfit.setSds(Stringutil.zhengLiJinE(jrjgfrProfit.getSds()));
			//净利润
			jrjgfrProfit.setJlr(Stringutil.zhengLiJinE(jrjgfrProfit.getJlr()));
			//年度损益调整（加）
			jrjgfrProfit.setNdsytz(Stringutil.zhengLiJinE(jrjgfrProfit.getNdsytz()));
			//留存利润
			jrjgfrProfit.setLclr(Stringutil.zhengLiJinE(jrjgfrProfit.getLclr()));
			//未分配利润
			jrjgfrProfit.setWfplr(Stringutil.zhengLiJinE(jrjgfrProfit.getWfplr()));
			//应纳增值税
			jrjgfrProfit.setYnzzs(Stringutil.zhengLiJinE(jrjgfrProfit.getYnzzs()));
			//核心一级资本净额
			jrjgfrProfit.setHxyjzbje(Stringutil.zhengLiJinE(jrjgfrProfit.getHxyjzbje()));
			//一级资本净额
			jrjgfrProfit.setYjzbje(Stringutil.zhengLiJinE(jrjgfrProfit.getYjzbje()));
			//资本净额
			jrjgfrProfit.setZbje(Stringutil.zhengLiJinE(jrjgfrProfit.getZbje()));
			//应用资本底线及校准后的风险加权资产合计
			jrjgfrProfit.setYgzbjfxjqzchj(Stringutil.zhengLiJinE(jrjgfrProfit.getYgzbjfxjqzchj()));
			jrjgfrProfit.setCheckstatus("0");
			jrjgfrProfit.setDatastatus("0");
			jrjgfrProfit.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			jrjgfrProfit.setOperator(sys_user.getLoginid());
			jrjgfrProfit.setOrgid(sys_user.getOrgid());
			jrjgfrProfit.setDepartid(sys_user.getDepartid());
			return jrjgfrProfit;
		}else if("Xjrjgfz".equals(lei)) {
			//法人分支 对象整理
			//Xjrjgfz jrjgfz = (Xjrjgfz)obj;
		}
		return obj;
	}
}
