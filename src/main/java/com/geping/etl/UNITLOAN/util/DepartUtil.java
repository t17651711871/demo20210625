package com.geping.etl.UNITLOAN.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

/**    
*  
* @author liuweixin  
* @date 2021年3月11日 上午9:25:13  
*/
@Component
public class DepartUtil {
	@Autowired
    private SUpOrgInfoSetService suisService;

	public String getDepart(Sys_UserAndOrgDepartment sys_user) {
		//admin不分部门
		if(sys_user == null || "admin".equals(sys_user.getLoginid())) {
			return "%%";
		}
		List<SUpOrgInfoSet> list = suisService.findAll();
        boolean isDepart="yes".equals(list.get(0).getIsusedepart())?true:false;
		if(isDepart) {
			return sys_user.getDepartid();
		}else {
			return "%%";
		}
	}
}
