package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xcltdmdzttzxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXcltdmdzttzxxRowData implements CheckRowData {

    @Autowired
    private CustomSqlUtil customSqlUtil;

    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {

        //存量特定目的载体投资信息
        Xcltdmdzttzxx xcltdmdzttzxx = (Xcltdmdzttzxx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String> checknumList = checkParamContext.getCheckNums();
        //数据日期
        if (checkXcltdmdzttzxxRowSjrq(checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //特定目的载体类型
        if (checkXcltdmdzttzxxRowTdmdztlx(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //发行人地区代码
        if (checkXcltdmdzttzxxRowDqdm(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //运行方式
        if (checkXcltdmdzttzxxRowYxfs(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkXcltdmdzttzxxRowBz(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //特殊字符
        if (checkXcltdmdzttzxxRowTszf(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //长度校验
        if (checkXcltdmdzttzxxRowCdjy(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //非空检验
        if (checkXcltdmdzttzxxRowFkjy(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }
        //数据比较
        if (checkXcltdmdzttzxxRowSjbj(checkParamContext, checknumList, xcltdmdzttzxx, tempMsg)) {
            isError = true;
        }


        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        checkHelper.handleRowCheckResult(isError, xcltdmdzttzxx.getId(), String.format(header, xcltdmdzttzxx.getFinanceorgcode(), xcltdmdzttzxx.getTdmdztdm()), tempMsg.toString(), checkParamContext);
    }

    private boolean checkXcltdmdzttzxxRowSjbj(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String rgrq = xcltdmdzttzxx.getRgrq();//认购日期
        String sjrq = xcltdmdzttzxx.getSjrq();//数据日期
        String dqrq = xcltdmdzttzxx.getDqrq();//到期日期
        String bz = xcltdmdzttzxx.getBz();//币种
        String tzye = xcltdmdzttzxx.getTzye();//投资余额
        String tzyezrmb = xcltdmdzttzxx.getTzyezrmb();//投资余额折人民币
        if (checknumList.contains("JS2289") && StringUtils.isNotBlank(rgrq) && StringUtils.isNotBlank(sjrq)) {
            if (rgrq.compareTo(sjrq) > 0) {
                result = true;
                tempMsg.append("认购日期应小于等于数据日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2290") && StringUtils.isNotBlank(rgrq) && StringUtils.isNotBlank(dqrq)) {
            if (rgrq.compareTo(dqrq) > 0) {
                result = true;
                tempMsg.append("认购日期应小于等于到期日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2291") && StringUtils.isNotBlank(tzye)) {
            try {
                if (Double.parseDouble(tzye) <= 0) {
                    result = true;
                    tempMsg.append("投资余额应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("投资余额应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2292") && StringUtils.isNotBlank(tzyezrmb)) {
            try {
                if (Double.parseDouble(tzyezrmb) <= 0) {
                    result = true;
                    tempMsg.append("投资余额折人民币应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("投资余额折人民币应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2706") && StringUtils.isNotBlank(tzyezrmb) && StringUtils.isNotBlank(tzye) && StringUtils.isNotBlank(bz)) {
            try {
                if (bz.equals("CNY") && Double.parseDouble(tzye) != Double.parseDouble(tzyezrmb)) {
                    result = true;
                    tempMsg.append("币种为人民币的，投资余额折人民币应该与投资余额的值相等" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("币种为人民币的，投资余额折人民币应该与投资余额的值相等" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXcltdmdzttzxxRowFkjy(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xcltdmdzttzxx.getFinanceorgcode();//金融机构代码
        String tdmdztlx = xcltdmdzttzxx.getTdmdztlx();//特定目的载体类型
        String zgcptjbm = xcltdmdzttzxx.getZgcptjbm();//资管产品统计编码
        String fxrdm = xcltdmdzttzxx.getFxrdm();//发行人代码
        String fxrdqdm = xcltdmdzttzxx.getFxrdqdm();//发行人地区代码
        String yxfs = xcltdmdzttzxx.getYxfs();//运行方式
        String rgrq = xcltdmdzttzxx.getRgrq();//认购日期
        String dqrq = xcltdmdzttzxx.getDqrq();//到期日期
        String bz = xcltdmdzttzxx.getBz();//币种
        String tzye = xcltdmdzttzxx.getTzye();//投资余额
        String tzyezrmb = xcltdmdzttzxx.getTzyezrmb();//投资余额折人民币
        String financeorginnum = xcltdmdzttzxx.getFinanceorginnum();//内部机构号
        String tdmdztdm = xcltdmdzttzxx.getTdmdztdm();//特定目的载体代码
        if (checknumList.contains("JS1437") && StringUtils.isBlank(financeorgcode)) {
            result = true;
            tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1438") && StringUtils.isBlank(tdmdztlx)) {
            result = true;
            tempMsg.append("特定目的载体类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1439") && StringUtils.isBlank(zgcptjbm)) {
            result = true;
            tempMsg.append("资管产品统计编码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1440") && StringUtils.isBlank(fxrdm)) {
            result = true;
            tempMsg.append("发行人代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1441") && StringUtils.isBlank(fxrdqdm)) {
            result = true;
            tempMsg.append("发行人地区代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1442") && StringUtils.isBlank(yxfs)) {
            result = true;
            tempMsg.append("运行方式不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1443") && StringUtils.isBlank(rgrq)) {
            result = true;
            tempMsg.append("认购日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1444") && StringUtils.isBlank(dqrq)) {
            result = true;
            tempMsg.append("到期日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1445") && StringUtils.isBlank(bz)) {
            result = true;
            tempMsg.append("币种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1446") && StringUtils.isBlank(tzye)) {
            result = true;
            tempMsg.append("投资余额不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1447") && StringUtils.isBlank(tzyezrmb)) {
            result = true;
            tempMsg.append("投资余额折人民币不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1524") && StringUtils.isBlank(financeorginnum)) {
            result = true;
            tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS2616") && StringUtils.isBlank(tdmdztdm)) {
            result = true;
            tempMsg.append("特定目的载体代码不能为空" + SysConstants.spaceStr);
        }

        return result;
    }

    private boolean checkXcltdmdzttzxxRowCdjy(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xcltdmdzttzxx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xcltdmdzttzxx.getFinanceorginnum();//内部机构号
        String zgcptjbm = xcltdmdzttzxx.getZgcptjbm();//资管产品统计编码
        String tdmdztdm = xcltdmdzttzxx.getTdmdztdm();//特定目的载体代码
        String fxrdqdm = xcltdmdzttzxx.getFxrdqdm();//发行人地区代码
        String fxrdm = xcltdmdzttzxx.getFxrdm();//发行人代码
        String tzye = xcltdmdzttzxx.getTzye();//投资余额
        String tzyezrmb = xcltdmdzttzxx.getTzyezrmb();//投资余额折人民币
        String rgrq = xcltdmdzttzxx.getRgrq();//认购日期
        String dqrq = xcltdmdzttzxx.getDqrq();//到期日期

        if (checknumList.contains("JS0796") && StringUtils.isNotBlank(financeorgcode)) {
            if (financeorgcode.length() != 18) {
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0797") && StringUtils.isNotBlank(financeorginnum)) {
            if (financeorginnum.length() > 30) {
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0798") && StringUtils.isNotBlank(zgcptjbm)) {
            if (zgcptjbm.length() > 30) {
                result = true;
                tempMsg.append("资管产品统计代码字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0799") && StringUtils.isNotBlank(tdmdztdm)) {
            if (tdmdztdm.length() > 30) {
                result = true;
                tempMsg.append("当特定目的载体代码不为空时，特定目的载体代码字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0800") && StringUtils.isNotBlank(fxrdm) && StringUtils.isNotBlank(fxrdqdm)) {
            try {
                if (fxrdm.length() != 18 && !fxrdqdm.substring(0, 3).equals("000")) {
                    result = true;
                    tempMsg.append("境内发行人代码字符长度应该为18位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内发行人代码字符长度应该为18位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2619") && StringUtils.isNotBlank(fxrdm)) {
            try {
                if (fxrdm.length() > 60 ) {
                    result = true;
                    tempMsg.append("发行人代码字符长度不能超过60位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("发行人代码字符长度不能超过60位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0801") && StringUtils.isNotBlank(tzye)) {
            if (tzye.length() > 20 || !CheckUtil.checkDecimal(tzye, 2)) {
                result = true;
                tempMsg.append("投资余额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0802") && StringUtils.isNotBlank(tzyezrmb)) {
            if (tzyezrmb.length() > 20 || !CheckUtil.checkDecimal(tzyezrmb, 2)) {
                result = true;
                tempMsg.append("投资余额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0803") && StringUtils.isNotBlank(rgrq)) {
            if (!CheckUtil.checkDate(rgrq, "yyyy-MM-dd") || rgrq.compareTo("1800-01-01") < 0 || rgrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("认购日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0804") && StringUtils.isNotBlank(dqrq)) {
            if (!CheckUtil.checkDate(dqrq, "yyyy-MM-dd") || dqrq.compareTo("1800-01-01") < 0 || dqrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXcltdmdzttzxxRowTszf(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xcltdmdzttzxx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xcltdmdzttzxx.getFinanceorginnum();//内部机构号
        String zgcptjbm = xcltdmdzttzxx.getZgcptjbm();//资管产统计品编码
        String fxrdm = xcltdmdzttzxx.getFxrdm();//发行人代码
        String tdmdztdm = xcltdmdzttzxx.getTdmdztdm();//特定目的载体代码
        if (checknumList.contains("JS0790") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0791") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0792") && StringUtils.isNotBlank(zgcptjbm)) {
            if (CheckUtil.checkStr(zgcptjbm)) {
                result = true;
                tempMsg.append("资管产品统计编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0794") && StringUtils.isNotBlank(fxrdm)) {
            if (CheckUtil.checkStr(fxrdm)) {
                result = true;
                tempMsg.append("发行人代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0795") && StringUtils.isNotBlank(tdmdztdm)) {
            if (CheckUtil.checkStr(tdmdztdm)) {
                result = true;
                tempMsg.append("特定目的载体代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }

        return result;

    }

    private boolean checkXcltdmdzttzxxRowBz(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xcltdmdzttzxx.getBz();//币种
        List baseCode = checkParamContext.getDataDictionarymap().get("baseCode");//币种代码
        if (checknumList.contains("JS0060") && StringUtils.isNotBlank(bz)) {
            if (!baseCode.contains(bz)) {
                result = true;
                tempMsg.append("存量特定载体投资信息表中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXcltdmdzttzxxRowYxfs(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String yxfs = xcltdmdzttzxx.getYxfs();
        if (checknumList.contains("JS0059") && StringUtils.isNotBlank(yxfs)) {
            if (!checkParamContext.getDataDictionarymap().get("yxfsList").contains(yxfs)) {
                tempMsg.append("存量特定目的载体投资信息表中的运行方式需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkXcltdmdzttzxxRowDqdm(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String dqdm = xcltdmdzttzxx.getFxrdqdm();//地区代码
        List baseCodeGj = checkParamContext.getDataDictionarymap().get("baseCodeGj");//国家地区代码C0013
        List baseCodeXz = checkParamContext.getDataDictionarymap().get("baseCodeXz");//国家地区代码C0013
        if (checknumList.contains("JS0058") && StringUtils.isNotBlank(dqdm)) {
            if ((!baseCodeGj.contains(dqdm) && !baseCodeXz.contains(dqdm))) {
                result = true;
                tempMsg.append("存量特定目的载体投资信息表中的地区代码需在符合要求的值域范围内，境内客户为《中华人民共和国行政区划代码》（GB/T 2260）规定的最新行政区划代码（不包括港澳台），境外客户为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXcltdmdzttzxxRowTdmdztlx(CheckParamContext checkParamContext, List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String tdmdztlx = xcltdmdzttzxx.getTdmdztlx();
        if (checknumList.contains("JS0057") && StringUtils.isNotBlank(tdmdztlx)) {
            if (!checkParamContext.getDataDictionarymap().get("ztlxList").contains(tdmdztlx)) {
                tempMsg.append("存量特定目的载体投资信息表中的特定目的载体类型需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }


    private boolean checkXcltdmdzttzxxRowSjrq(List<String> checknumList, Xcltdmdzttzxx xcltdmdzttzxx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xcltdmdzttzxx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)) {
            if (sjrq.length() == 10) {
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1) {
                    if (sjrq.compareTo("1800-01-01") < 0 || sjrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                } else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            } else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        } else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }


}