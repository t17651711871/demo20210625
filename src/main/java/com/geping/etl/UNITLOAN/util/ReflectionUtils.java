package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Table;

/***
 * 反射工具类
 * @author liang.xu
 * @date 2021.4.1
 */
public class ReflectionUtils {

    private static Logger logger = LoggerFactory.getLogger(ReflectionUtils.class);

    /**
     * 获取实体表名
     *
     * @param clz
     * @return
     */
    public static String getTableName(Class clz) {
        Table annotation = (Table) clz.getAnnotation(Table.class);
        if (annotation == null) {
            return "";
        }
        return annotation.name();
    }

    /**
     * 获取实体Class
     *
     * @param entityName
     * @return
     */
    public static Class getEntityClass(String entityName) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(SysConstants.REPORT_ENTITY_PKG + entityName);
        } catch (Exception e) {
            logger.error("加载class发生错误", e);
        }
        return clazz;
    }

    public static void main(String[] args) {
        ReflectionUtils.getTableName(Xcldkxx.class);
    }
}
