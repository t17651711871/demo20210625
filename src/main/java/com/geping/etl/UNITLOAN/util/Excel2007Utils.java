package com.geping.etl.UNITLOAN.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrBaseinfo;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import net.sf.json.JSONObject;
/**
 * excel2007百万级数据导出工具
 * @author WuZengWen
 * @date 2019年4月12日 下午6:23:29
 */
public class Excel2007Utils {

	private static final int DEFAULT_COLUMN_SIZE = 30;

	/**
	 *      * 断言Excel文件写入之前的条件      *      * @param directory 目录      * @param
	 * fileName  文件名      * @return file      * @throws IOException      
	 */
	private static File assertFile(String directory, String fileName) throws IOException {
		File tmpFile = new File(directory + File.separator + fileName + ".xlsx");
		if (tmpFile.exists()) {
			if (tmpFile.isDirectory()) {
				throw new IOException("File '" + tmpFile + "' exists but is a directory");
			}
			if (!tmpFile.canWrite()) {
				throw new IOException("File '" + tmpFile + "' cannot be written to");
			}
		} else {
			File parent = tmpFile.getParentFile();
			if (parent != null) {
				if (!parent.mkdirs() && !parent.isDirectory()) {
					throw new IOException("Directory '" + parent + "' could not be created");
				}
			}
		}
		return tmpFile;
	}

	private static File createFile(File tmpFile) throws IOException {
		if (tmpFile.exists()) {
			if (tmpFile.isDirectory()) {
				throw new IOException("File '" + tmpFile + "' exists but is a directory");
			}
			if (!tmpFile.canWrite()) {
				throw new IOException("File '" + tmpFile + "' cannot be written to");
			}
		} else {
			File parent = tmpFile.getParentFile();
			if (parent != null) {
				if (!parent.mkdirs() && !parent.isDirectory()) {
					throw new IOException("Directory '" + parent + "' could not be created");
				}
			}
		}
		return tmpFile;
	}
	
	/**
	 *      * 日期转化为字符串,格式为yyyy-MM-dd HH:mm:ss      
	 */
	private static String getCnDate(Date date) {
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * 写Excel标题
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param columnNames 列头 非null
	 * @param sheetTitle 标题 可null
	 * @param flag 追加写入 非null
	 * @return Excel
	 * @throws IOException
	 */
	public static File writeExcelTitle(String filePath, String excelName, String sheetName, List<String> columnNames,
			String sheetTitle, boolean flag) throws IOException {
		File tmpFile = assertFile(filePath, excelName);
		return exportExcelTitle(tmpFile, sheetName, columnNames, sheetTitle, flag);
	}

	/**
	 *      * 导出字符串数据      *      * @param file        文件名      * @param columnNames
	 * 表头      * @param sheetTitle  sheet页Title      * @param append      是否追加写文件  
	 *    * @return file      * @throws ReportInternalException      
	 */
	private static File exportExcelTitle(File file, String sheetName, List<String> columnNames, String sheetTitle,
			boolean append) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists() && append) {
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		//不为空就创建标题
		if(StringUtils.isNotBlank(sheetTitle) && sheetTitle != null) {
			// 合并单元格
			sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, columnNames.size() - 1));
			// 产生表格标题行
			Row rowMerged = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			Cell mergedCell = rowMerged.createCell(0);
			mergedCell.setCellStyle(headStyle);
			mergedCell.setCellValue(new XSSFRichTextString(sheetTitle));
		}
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	/**
	 * 写入excel
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param objects 写入数据 非null
	 * @return excel
	 * @throws IOException
	 */
	public static File writeExcelData(String filePath, String excelName, String sheetName, List<List<Object>> objects)
			throws IOException {
		File tmpFile = assertFile(filePath, excelName);
		return exportExcelData(tmpFile, sheetName, objects);
	}

	/**
	 *      * 导出字符串数据      *      * @param file    文件名      * @param objects 目标数据  
	 *    * @return      * @throws ReportInternalException      
	 */
	private static File exportExcelData(File file, String sheetName, List<List<Object>> objects) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists()) {
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}

		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式
		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		int onlyrow = sheet.getPhysicalNumberOfRows();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}else {
			if(onlyrow > 0) {
				lastRowIndex = onlyrow;
			}
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			Row row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	/**
	 * 写入excel
	 * @param filePath 目录 非null
	 * @param excelName 文件名 非null
	 * @param sheetName sheet名 可null
	 * @param columnNames 列标题 非null
	 * @param sheetTitle sheet标题 可null
	 * @param objects 写入数据 非null
	 * @param flag 是否追加写入 非null
	 * @return excel
	 * @throws IOException
	 */
	public static File writeExcel(String filePath, String excelName, String sheetName, List<String> columnNames,
			String sheetTitle, List<List<Object>> objects, boolean flag) throws IOException {
		File tmpFile;
		tmpFile = assertFile(filePath, excelName);
		return exportExcel(tmpFile, sheetName, columnNames, sheetTitle, objects, flag);
	}

	/**
	 *      * 导出字符串数据      *      * @param file        文件名      * @param columnNames
	 * 表头      * @param sheetTitle  sheet页Title      * @param objects     目标数据    
	 *  * @param append      是否追加写文件      * @return      * @throws
	 * ReportInternalException      
	 */

	private static File exportExcel(File file, String sheetName, List<String> columnNames, String sheetTitle,
			List<List<Object>> objects, boolean append) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists() && append) {
			// 声明一个工作薄
			workBook = new XSSFWorkbook(new FileInputStream(file));
		} else {
			workBook = new XSSFWorkbook();
		}
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = null;
		if(StringUtils.isNotBlank(sheetName) && sheetName != null) {
			sheet = workBook.getSheet(sheetName);
			if (sheet == null) {
				sheet = workBook.createSheet(sheetName);
			}
		}else {
			sheet = workBook.getSheet("sheet1");
			if (sheet == null) {
				sheet = workBook.createSheet("sheet1");
			}
		}
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		//不为空就创建标题
		if(StringUtils.isNotBlank(sheetTitle) && sheetTitle != null) {
			// 合并单元格
			sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, columnNames.size() - 1));
			// 产生表格标题行
			Row rowMerged = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			Cell mergedCell = rowMerged.createCell(0);
			mergedCell.setCellStyle(headStyle);
			mergedCell.setCellValue(new XSSFRichTextString(sheetTitle));
		}
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		lastRowIndex++;
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			OutputStream ops = new FileOutputStream(file);
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return file;
	}

	/**
	 *      * 单元格样式列表      
	 */
	private static Map<String, CellStyle> styleMap(Workbook workbook) {
		Map<String, CellStyle> styleMap = new LinkedHashMap<>();
		styleMap.put("head", createCellHeadStyle(workbook));
		styleMap.put("content", createCellContentStyle(workbook));
		styleMap.put("integer", createCellContent4IntegerStyle(workbook));
		styleMap.put("double", createCellContent4DoubleStyle(workbook));
		return styleMap;
	}

	/**
	 *      * 创建单元格表头样式      *      * @param workbook 工作薄      
	 */
	private static CellStyle createCellHeadStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 表头样式
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 把字体应用到当前的样式
		style.setFont(font);
		return style;
	}

	/**
	 * 创建单元格正文样式
	 *
	 * @param workbook
	 *            工作薄
	 */
	private static CellStyle createCellContentStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		return style;
	}

	/**
	 * 单元格样式(Integer)列表
	 */
	private static CellStyle createCellContent4IntegerStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));// 数据格式只显示整数
		return style;
	}

	/**
	 * 单元格样式(Double)列表
	 */
	private static CellStyle createCellContent4DoubleStyle(Workbook workbook) {
		CellStyle style = workbook.createCellStyle();
		// 设置边框样式
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		// 设置对齐样式
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		// 生成字体
		Font font = workbook.createFont();
		// 正文样式
		style.setFillPattern(XSSFCellStyle.NO_FILL);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style.setFont(font);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));// 保留两位小数点
		return style;
	}

	/**
	 * 网页下载默认路径
	 * @param response 结果集
	 * @param columnNames 列标题 非null
	 * @param objects 写入数据
	 * @throws IOException 
	 */
	public static void writeExcelResponse(HttpServletResponse response, List<String> columnNames, List<List<Object>> objects) throws IOException {
		exportExcelResponse(response, columnNames, objects);
	}
	
	/**
	 * 网页下载默认路径
	 * @param response 结果集
	 * @param columnNames 左列数据
	 * @param objects 右列数据
	 * @param heards 第一行标题
	 * @throws IOException
	 */
	public static void writeExcelResponse(HttpServletResponse response, List<String> columnNames, List<List<Object>> objects,List<String> heards) throws IOException {
		exportExcelResponse(response, columnNames, objects,heards);
	}
	
	/**
	 * 网页导出数据
	 * @param response 结果集
	 * @param columnNames 列头名
	 * @param objects 写入数据
	 * @throws IOException
	 */
	private static void exportExcelResponse(HttpServletResponse response, List<String> columnNames,List<List<Object>> objects) throws IOException {
		OutputStream ops = null;
		// 声明一个工作薄
		Workbook workBook = new SXSSFWorkbook();
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = workBook.createSheet("sheet1");
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		lastRowIndex++;
		for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		for (List<Object> dataRow : objects) {
			row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int j = 0; j < dataRow.size(); j++) {
				Cell contentCell = row.createCell(j);
				Object dataObject = dataRow.get(j);
				if (dataObject != null) {
					if (dataObject instanceof Integer) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentIntegerStyle);
						contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
					} else if (dataObject instanceof Double) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
						contentCell.setCellStyle(contentDoubleStyle);
						contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
					} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
					} else if (dataObject instanceof Date) {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(getCnDate((Date) dataObject));
					} else {
						contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
						contentCell.setCellStyle(contentStyle);
						contentCell.setCellValue(dataObject.toString());
					}
				} else {
					contentCell.setCellStyle(contentStyle);
					// 设置单元格内容为字符型
					contentCell.setCellValue("");
				}
			}
		}
		try {
			ops = new BufferedOutputStream(response.getOutputStream());// 输出流
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				if (ops != null) {
					ops.flush();
					ops.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 网页导出数据竖向排列
	 * @param response 结果集
	 * @param columnNames 左列
	 * @param objects 右列
	 * @param heards 第一行标题
	 * @throws IOException
	 */
	private static void exportExcelResponse(HttpServletResponse response, List<String> columnNames,List<List<Object>> objects,List<String> heards) throws IOException {
		OutputStream ops = null;
		// 声明一个工作薄
		Workbook workBook = new SXSSFWorkbook();
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		Sheet sheet = workBook.createSheet("sheet1");
		// 最新Excel列索引,从0开始
		int lastRowIndex = sheet.getLastRowNum();
		if (lastRowIndex > 0) {
			lastRowIndex++;
		}
		// 设置表格默认列宽度
		sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
		// 产生表格表头列标题行
		Row row = sheet.createRow(lastRowIndex);
		lastRowIndex++;
		for(int i=0;i<heards.size();i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(heards.get(i));
			cell.setCellValue(text);
		}
		/*for (int i = 0; i < columnNames.size(); i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(headStyle);
			RichTextString text = new XSSFRichTextString(columnNames.get(i));
			cell.setCellValue(text);
		}*/
		int bianhao = 0;
		if(columnNames.size() == 38) {
			bianhao = 20201001;
		}else if(columnNames.size() == 18) {
			bianhao = 20202001;
		}else if(columnNames.size() == 49) {
			bianhao = 20203001;
		}
		// 遍历集合数据,产生数据行,前两行为标题行与表头行
		//for (List<Object> dataRow : objects) {
			//row = sheet.createRow(lastRowIndex);
			//lastRowIndex++;
			for (int j = 0; j < columnNames.size(); j++) {
				row = sheet.createRow(lastRowIndex);
				lastRowIndex++;
				Cell contentCell0 = row.createCell(0);
				contentCell0.setCellStyle(contentStyle);
				contentCell0.setCellValue(bianhao);
				bianhao++;
				Cell contentCell1 = row.createCell(1);
				contentCell1.setCellStyle(contentStyle);
				contentCell1.setCellValue(columnNames.get(j));
				Cell contentCell2 = row.createCell(2);
				contentCell2.setCellStyle(contentStyle);
				
				if(objects != null && objects.size() > 0) {
					Object dataObject = objects.get(0).get(j);
					if (dataObject != null) {
						if (dataObject instanceof Integer) {
							contentCell2.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
							contentCell2.setCellStyle(contentIntegerStyle);
							contentCell2.setCellValue(Integer.parseInt(dataObject.toString()));
						} else if (dataObject instanceof Double) {
							contentCell2.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
							contentCell2.setCellStyle(contentDoubleStyle);
							contentCell2.setCellValue(Double.parseDouble(dataObject.toString()));
						} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
							contentCell2.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell2.setCellStyle(contentStyle);
							contentCell2.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
						} else if (dataObject instanceof Date) {
							contentCell2.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell2.setCellStyle(contentStyle);
							contentCell2.setCellValue(getCnDate((Date) dataObject));
						} else {
							contentCell2.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell2.setCellStyle(contentStyle);
							contentCell2.setCellValue(dataObject.toString());
						}
					} else {
						contentCell2.setCellStyle(contentStyle);
						// 设置单元格内容为字符型
						contentCell2.setCellValue("");
					}
				}else {
					contentCell2.setCellValue("");
				}
			}
		//}
		try {
			ops = new BufferedOutputStream(response.getOutputStream());// 输出流
			workBook.write(ops);
			ops.flush();
			ops.close();
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				if (ops != null) {
					ops.flush();
					ops.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 写入数据到指定文件
	 * @param file 指定文件
	 * @param columnNames 列头名
	 * @param objects 数据集合
	 * @param sheetName sheet名字
	 * @throws IOException 异常
	 */
	public static void writeExcelToFile(File file, List<String> columnNames,List<List<Object>> objects,String sheetName) throws IOException {
		// 声明一个工作薄
		Workbook workBook;
		if (file.exists()) {
			// 声明一个工作薄
			//workBook = new XSSFWorkbook(new FileInputStream(file));
			workBook = new HSSFWorkbook(new FileInputStream(file));
		} else {
			File tempfile = createFile(file);
			//workBook = new XSSFWorkbook(new FileInputStream(tempfile));
			workBook = new HSSFWorkbook(new FileInputStream(tempfile));
		}
		Map<String, CellStyle> cellStyleMap = styleMap(workBook);
		// 表头样式
		CellStyle headStyle = cellStyleMap.get("head");
		// 正文样式
		CellStyle contentStyle = cellStyleMap.get("content");
		// 正文整数样式

		CellStyle contentIntegerStyle = cellStyleMap.get("integer");
		// 正文带小数整数样式
		CellStyle contentDoubleStyle = cellStyleMap.get("double");
		// 生成一个表格若未设置表格取默认名
		OutputStream ops = null;
		try {
			// 生成一个表格
			Sheet sheet = workBook.createSheet(sheetName);
			// 最新Excel列索引,从0开始
			int lastRowIndex = sheet.getLastRowNum();
			if (lastRowIndex > 0) {
				lastRowIndex++;
			}
			// 设置表格默认列宽度
			sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
			// 产生表格表头列标题行
			Row row = sheet.createRow(lastRowIndex);
			lastRowIndex++;
			for (int u = 0; u < columnNames.size(); u++) {
				Cell cell = row.createCell(u);
				cell.setCellStyle(headStyle);
				RichTextString text = new XSSFRichTextString(columnNames.get(u));
				cell.setCellValue(text);
			}
			// 遍历集合数据,产生数据行,前两行为标题行与表头行
			for (List<Object> dataRow : objects) {
				row = sheet.createRow(lastRowIndex);
				lastRowIndex++;
				for (int j = 0; j < dataRow.size(); j++) {
					Cell contentCell = row.createCell(j);
					Object dataObject = dataRow.get(j);
					if (dataObject != null) {
						if (dataObject instanceof Integer) {
							contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
							contentCell.setCellStyle(contentIntegerStyle);
							contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
						} else if (dataObject instanceof Double) {
							contentCell.setCellType(XSSFCell.CELL_TYPE_NUMERIC);
							contentCell.setCellStyle(contentDoubleStyle);
							contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
						} else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
							contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell.setCellStyle(contentStyle);
							contentCell.setCellValue(getCnDate(new Date(Long.parseLong(dataObject.toString()))));
						} else if (dataObject instanceof Date) {
							contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell.setCellStyle(contentStyle);
							contentCell.setCellValue(getCnDate((Date) dataObject));
						} else {
							contentCell.setCellType(XSSFCell.CELL_TYPE_STRING);
							contentCell.setCellStyle(contentStyle);
							contentCell.setCellValue(dataObject.toString());
						}
					} else {
						contentCell.setCellStyle(contentStyle);
						// 设置单元格内容为字符型
						contentCell.setCellValue("");
					}
				}
			}
			ops = new FileOutputStream(file);
			workBook.write(ops);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e);
		}finally {
			if(ops!=null) {
				ops.flush();
				ops.close();
			}
		}
	}
	
	public static JSONObject daoruwenjian(MultipartFile file,String[] heards) {
		Workbook wb = null;
		InputStream in = null;
		//int result = 0;
		JSONObject json = new JSONObject();
		try {
			if (file == null) {
				json.put("code", "1");
				json.put("msg", "导入文件失败,文件为空!");
				//result = 1;
			} else {
				in = file.getInputStream();
				wb = WorkbookFactory.create(in);
				int sheetn = wb.getNumberOfSheets(); // 获取Excel中总sheet的数量
				if(sheetn > 1) {
					json.put("code","2");
					json.put("msg","导入失败，总sheet>1");
					//result = 2;
				}else {
					for (int i = 0; i < sheetn; i++) { // 遍历sheet
						Sheet sheet = wb.getSheetAt(i); // 得到当前遍历的sheet对象
						int rown = sheet.getLastRowNum(); // 获取当前sheet的总行数
						Row row = sheet.getRow(0); // 获取第一行，即标题行
						int cells = row.getLastCellNum(); // 获取第一行单元格数量即列
						DecimalFormat df = new DecimalFormat("####.####");   //解决导入时纯数字值的科学计数法问题
						if(cells != heards.length) {
							json.put("code","3");
							json.put("msg","导入失败，标题长度不符");
							//result = 3;
							break;
						}else {
							for(int m=0;m<heards.length;m++) {
								//判断标题内容是否与导入的标题相同否则报错
								String cellName = row.getCell(m).getStringCellValue();
								if(!heards[m].equals(cellName)) {
									json.put("code","4");
									json.put("msg","导入失败，标题不符");
									//result = 4;
									break;
								}
							}
							List<List<Object>> objects = new LinkedList<>();
							//List<Object> dataA = new LinkedList<>();
							Cell cell = null;
							for (int j = 1; j <= rown; j++) {
								//开始拿数据
								row = sheet.getRow(j);
								if (row != null) {
									List<Object> dataA = new LinkedList<>();
									for (int n = 0; n < heards.length; n++) {
										//获取每一列的数据
										cell = row.getCell(n);
										//如果该单元格即该列内容不为空
										if (cell != null) {
											if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
												try {
													dataA.add(df.format(cell.getNumericCellValue()));
												} catch (Exception e) {
													e.printStackTrace();
													System.out.println("尝试将字符型数据转化成数值型数据时出错...");
													dataA.add(cell.getStringCellValue().trim());
												}
											} else {
												dataA.add(cell.getStringCellValue().trim());
											}
										}
									}
									objects.add(dataA);
								}
							}
							json.put("code","0");
							json.put("data",objects);
						}
					} // 遍历sheet结尾
				}
			} // 判断文件是否存在结尾
			
		} catch (Exception e) {
			e.printStackTrace();
			json.put("code","5");
			json.put("msg","导入失败，程序异常");
		} finally {
			try {
				if(wb != null) {
					wb.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return json;
	}
	
	/**
	 * 判断要导入的文件
	 * @param file 导入的文件
	 * @param code 类型
	 * @param customSqlUtil sql工具
	 * @param sys_user 操作者
	 * @return
	 */
	public static int panduandaoruwenjian(MultipartFile file,TableCodeEnum code,CustomSqlUtil customSqlUtil,Sys_UserAndOrgDepartment sys_user) {
		Workbook wb = null;
		InputStream in = null;
		int result = 0;
		try {
			if (file == null) {
				//json.put("msg", "导入文件失败,文件为空!");
				result = 1;
			} else {
				in = file.getInputStream();
				wb = WorkbookFactory.create(in);
				int sheetn = wb.getNumberOfSheets(); // 获取Excel中总sheet的数量
				if(sheetn > 1) {
					//json.put("msg","导入失败，总sheet>1");
					result = 2;
				}else {
					for (int i = 0; i < sheetn; i++) { // 遍历sheet
						Sheet sheet = wb.getSheetAt(i); // 得到当前遍历的sheet对象
						int rown = sheet.getLastRowNum(); // 获取当前sheet的总行数
						Row row = sheet.getRow(0); // 获取第一行，即标题行
						int cells = row.getLastCellNum(); // 获取第一行单元格数量即列
						DecimalFormat df = new DecimalFormat("####.####");   //解决导入时纯数字值的科学计数法问题
						SimpleDateFormat dm = new SimpleDateFormat("yyyy-MM");
						SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
						if(cells != 3) {
							//json.put("msg","导入失败，第一行长度不对");
							result = 3;
							break;
						}else {
							if(TableCodeEnum.ZCFZJFXTJ.equals(code)) {
								if (rown == 18 || sheet.getPhysicalNumberOfRows() == 18) {
									for (int m = 0; m < cells; m++) { //判断标题内容是否与导入的标题相同否则报错
										String cellName = row.getCell(m).getStringCellValue();
										if (m==0 && "指标编码".equals(cellName)) {
											
										}else if(m==1 && "指标名称".equals(cellName)) {
											
										}else if(m==2 && "本外币金额".equals(cellName)) {
											
										}else {
											//json.put("msg","导入失败，第一行名称不对");
											result = 5;
											break;
										}
									}
									XjrjgfrAssets xa = new XjrjgfrAssets();
									Field[] declaredFields = xa.getClass().getDeclaredFields();
									Cell cell = null;
									for (int j = 1; j <= rown; j++) {
										//开始拿数据
										row = sheet.getRow(j);
										if (row != null) {
											//获取每一列的数据
											String name = declaredFields[j].getName();
											String sname = name.substring(0, 1).toUpperCase() + name.substring(1);
											Method m = xa.getClass().getMethod("set"+sname,String.class);
						                    m.invoke(xa,getCell(row.getCell(2),df));
										}
									}
									customSqlUtil.saveT(ObjectUtils.zhengLiDuiXiang(xa,sys_user));
								}else {
									//json.put("msg","导入失败，行数不对");
									result = 4;
									break;
								}
							}else if(TableCodeEnum.JCQKTJ.equals(code)) {
								if (rown == 38 || sheet.getPhysicalNumberOfRows() == 38) {
									for (int m = 0; m < cells; m++) { //判断标题内容是否与导入的标题相同否则报错
										String cellName = row.getCell(m).getStringCellValue();
										if (m==0 && "指标编码".equals(cellName)) {
											
										}else if(m==1 && "指标名称".equals(cellName)) {
											
										}else if(m==2 && "指标值".equals(cellName)) {
											
										}else {
											result = 5;
											break;
										}
									}
									XjrjgfrBaseinfo xb = new XjrjgfrBaseinfo();
									Field[] declaredFields = xb.getClass().getDeclaredFields();
									Cell cell = null;
									for (int j = 1; j <= rown; j++) {
										//开始拿数据
										row = sheet.getRow(j);
										if (row != null) {
											//获取每一列的数据
											String name = declaredFields[j].getName();
											String sname = name.substring(0, 1).toUpperCase() + name.substring(1);
											Method m = xb.getClass().getMethod("set"+sname,String.class);
//						                    if(j == 8) {
//						                    	m.invoke(xb,checkDateDefaultFormat(row.getCell(2).toString()));
//						                    }else {
//						                    	m.invoke(xb,getCell(row.getCell(2),df));
//						                    }
											if(j == 17) {//从业人员数取整
												try {
													m.invoke(xb,new BigDecimal(getCell(row.getCell(2),df)).setScale(0, BigDecimal.ROUND_DOWN).toPlainString());
												}catch(Exception e) {
													m.invoke(xb,"");
												}
											}else if(j >= 28 && j <= 37){//统一数字型字段
												try {
													new BigDecimal(getCell(row.getCell(2),df));
													m.invoke(xb,getCell(row.getCell(2),df));
												}catch(Exception e) {
													m.invoke(xb,"");
												}
											}else {
												m.invoke(xb,getCell(row.getCell(2),df));
											}
											
										}
									}
									customSqlUtil.saveT(ObjectUtils.zhengLiDuiXiang(xb,sys_user));
								}else {
									result = 4;
									break;
								}
							}else if(TableCodeEnum.LRJZBTJ.equals(code)) {
								if (rown == 49 || sheet.getPhysicalNumberOfRows() == 49) {
									for (int m = 0; m < cells; m++) { //判断标题内容是否与导入的标题相同否则报错
										String cellName = row.getCell(m).getStringCellValue();
										if (m==0 && "指标编码".equals(cellName)) {
											
										}else if(m==1 && "指标名称".equals(cellName)) {
											
										}else if(m==2 && "本外币金额".equals(cellName)) {
											
										}else {
											result = 5;
											break;
										}
									}
									XjrjgfrProfit xp = new XjrjgfrProfit();
									Field[] declaredFields = xp.getClass().getDeclaredFields();
									Cell cell = null;
									for (int j = 1; j <= rown; j++) {
										//开始拿数据
										row = sheet.getRow(j);
										if (row != null) {
											//获取每一列的数据
											//cell = row.getCell(1);
											String name = declaredFields[j].getName();
											String sname = name.substring(0, 1).toUpperCase() + name.substring(1);
											Method m = xp.getClass().getMethod("set"+sname,String.class);
						                    m.invoke(xp,getCell(row.getCell(2),df));
											//xp.setYysr(getCell(row.getCell(1),df));
											//如果该单元格即该列内容不为空
											/*if (cell != null) {
												if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
													try {
														xp.setYysr(df.format(cell.getNumericCellValue()));
													} catch (Exception e) {
														System.out.println("尝试将字符型数据转化成数值型数据时出错...");
													}
												} else {
													xp.setYysr(cell.getStringCellValue().trim());
												}
											}*/
										}
									}
									customSqlUtil.saveT(ObjectUtils.zhengLiDuiXiang(xp,sys_user));
								}else {
									result = 4;
									break;
								}
							}
						}
						
					} // 遍历sheet结尾
				}
			} // 判断文件是否存在结尾
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		} finally {
			try {
				if(wb != null) {
					wb.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 获取单元格值
	 * @param cell 单元格
	 * @param df 格式
	 * @return
	 */
	public static String getCell(Cell cell,DecimalFormat df) {
		if (cell != null) {
			if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				try {
				short format = cell.getCellStyle().getDataFormat();  
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    Date d = cell.getDateCellValue();
                    DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                    return formater.format(d);
                }else if(format == 14 || format == 31 || format == 57 || format == 58){  
                    //日期  
                    DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");  
                    Date date = DateUtil.getJavaDate(cell.getNumericCellValue());  
                    return formater.format(date);  
                }else {				
                	return df.format(cell.getNumericCellValue());
                }
				} catch (Exception e) {
					System.out.println("尝试将字符型数据转化成数值型数据时出错...");
				}
			}else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA){
                try {
                	NumberFormat numberFormat = NumberFormat.getInstance();
                	numberFormat.setMaximumFractionDigits(2);
                    return numberFormat.format(cell.getNumericCellValue()).replace(",","");
                } catch (IllegalStateException e) {
                    return String.valueOf(cell.getRichStringCellValue());
                }
            } else {
				return cell.getStringCellValue().trim();
			}
		}
		return "";
	}
	
	/**
	 * 获取单元格值
	 * @param cell 单元格
	 * @param df 格式
	 * @return
	 */
	public static String getDateCell(Cell cell,SimpleDateFormat df) {
		if (cell != null) {
			short format = cell.getCellStyle().getDataFormat();
			if (format == 14 || format == 31 || format == 57 || format == 58
					|| format == 184 || format == 185 || format == 189
					|| format == 187 || format == 190 || format == 191
					|| format == 192 || format == 176 || format == 177
					|| format == 178 || format == 166) {
				// excel日期单元格的编码值
				try {
					double dataValue = cell.getNumericCellValue();
					Date date = DateUtil.getJavaDate(dataValue);
					return df.format(date);
				} catch (Exception e) {
					e.printStackTrace();
					return cell.getStringCellValue().trim();
				}
			}
		}
		return "";
	}
	
	/**
	 * 处理单元格日期
	 * @param String 
	 * @return
	 */
	public static String checkDateDefaultFormat(String date){
	  	if (StringUtils.isNotBlank(date)){
	        try {
	            String[] split = date.split("-");
	            String[] split1 = date.split("/");
	            if (split.length > 1){
	                if (split[1].length() == 1){
	                    split[1] = "0"+split[1];
	                }
	                if (split[2].length() == 1){
	                    split[2] = "0"+split[2];
	                }
	                date = split[0]+"-"+split[1]+"-"+split[2];
	            }
	            if (split1.length > 1){
	
	                if (split1[1].length() == 1){
	                    split1[1] = "0"+split1[1];
	                }
	                if (split1[2].length() == 1){
	                    split1[2] = "0"+split1[2];
	                }
	                date = split1[0]+"-"+split1[1]+"-"+split1[2];
	            }
	            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	            Date parse = dateFormat.parse(date);
	            return date;
	        }catch (Exception e){
	            return "";
	        }
	    }
	    return "";
	}
}
