package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: wangzd
 * @Date: 20:39 2020/6/10
 */
public class CheckUtil {
	//计息方式
	public static final String[] jxfs = {"B01","B02","B03","B04","B05","B99"};
	//产品类别_去除F06
	public static final String[] cplb_F06 = {"F03","F061","F062","F063","F064","F065","F069"};
	//资产负债类型
	public static final String[] zcfzlx = {"AL01","AL02"};
	//交易对手证件类型
	public static final String[] jydszjlx = {"A01","A02","C01","C02","L01","Z99"};
    //贷款财政扶持方式
    public static final String[] loanczfc={"A0101","A0102","A0201","A0202","B","Z","C"};
    //贷款产品类别
    public static final String[] loancplb={"F04","F05"};
    //贷款定价基准类型
    public static final String[] loanjzlx={"TR01","TR02","TR03","TR04","TR05","TR06","TR07","TR08","TR09","TR99"};
    //贷款状态
    public static final String[] loanstatus={"LS01","LS02","LS03","LS04"};
    //贷款状态2
    public static final String[] loanstatus2={"FS05","FS06","FS07"};
    //贷款状态3
    public static final String[] loanstatus3={"FS01","FS04","FS05","FS06","FS07","FS08","FS99"};
    public static final String[] loanstatus4={"LF02","LF03","LF04","LF07","LF08","LF06"};
    public static final String[] loanstatus5={"LF01","LF02","LF03","LF04","LF05","LF06","LF07","LF08","LF99"};
    public static final String[] loanstatus6={"LF02","LF03","LF06","LF07","LF08"};
    //利率是否固定
    public static final String[] llsfgd={"RF01","RF02"};
    //借款人经济成分
    public static final String[] qyczrjjcf={"A01","A0101","A0102","A02","A0201","A0202","B01","B0101","B0102","B02","B0201","B0202","B03","B0301","B0302"};
    //借款人行业
    public static final String[] jkrhy={"100","200"};
    //企业规模
    public static final String[] qygm={"CS01","CS02","CS03","CS04"};
    //借款人企业规模2
    public static final String[] qygm2={"CS01","CS02","CS03","CS04","CS05"};
    //贷款用途or是否首次贷款
    public static final String[] sfcommon={"0","1"};
    //担保人证件类型
    public static final String[] bzrzjlx={"A01","A02","A03","B01","B02","B03", "B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};
    //担保人证件类型2
    public static final String[] bzrzjlx2={"A01","A02","A03","A04","A99","B01","B02","B03", "B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};
    //担保合同类型
    public static final String[] dbhtlx={"01","02"};
    //经营状态
    public static final String[] jyzt={"01","02","03","04","05","06","07","99"};
    //贷款担保方式
    public static final String[] dkdbfs={"A","B01","B99","C01","C99","D","E","Z"};
    //客户国民经济部门
    public static final String[] gmjjbm={"A","A01","A02","A03","A04","A05","A06","A99","B","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","C","C01","C02","C99","D","D01","E05","D02","E","E01","E02","E03","E04"};
    //客户国民经济部门3
    public static final String[] gmjjbm3={"A","A01","A02","A03","A04","A05","A06","A99","B","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","C","C01","C02","C99","D","D02","E","E01","E02","E03","E04"};
    //客户国民经济部门2
    public static final String[] gmjjbm2={"A","A01","A02","A03","A04","A05","A06","A99","B","B01","B02","C","C01","C02","C99","D","D02","E","E01","E02","E04"};

    //   同业客户基础信息 客户国民经济部门
    public static final String[] gmjjbmkh={"B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","E03"};

    //借款人国民经济部门
    public static final String[] jkrgmjjbm={"A01","A02","A03","A04","A05","A06","A99","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","C01","C02","C99","D01","D02","E","E01","E02","E03","E04","E05"};

    //借款人国民经济部门2
    public static final String[] wtjkrgmjjbm={"A01","A02","A03","A04","A05","A06","A99","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","C01","C02","C99","D01","D02","E01","E02","E03","E04","E05"};
    
    
    //交易类型or逾期类型
    public static final String[] jylx={"01","02","03"};
    //贷款重组方式
    public static final String[] dkczfs={"01","02","09"};
    //客户证件类型or借款人证件类型
    public static final String[] khzjlx={"A01","A02","A03"};

    //委托贷款 借款人证件类型
    public static final String[] wtdkzjlx={"A01","A02","A03","A04","A99","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};
    //委托贷款 委托人证件类型
    public static final String[] wtdkzjlx2={"A01","A02","A99","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};


    // 个人 客户证件类型or借款人证件类型
    public static final String[] grkhzjlx={"A01","A02","A03","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};

    //个人贷款客户证件类型or借款人证件类型
    public static final String[] grzjlx={"A01","A02","A03","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","B12","B99"};

    //贷款产品类别
    public static final String[] dkcplb={"F022","F023","F03","F041","F042","F043","F061","F062","F063","F064","F065","F069","F081","F082","F09","F12","F13"};

   //个人贷款发生额产品类别
    public static final String[] dkfsecplb={"F0211","F0212","F02131","F02132","F0219","F022","F023","F03","F041","F042","F043","F061","F062","F063","F064","F065","F069","F081","F082","F09","F12","F13"};

    //借款人国民经济部门-单位贷款校验规则
    public  static final String[] dwdkjkrgmjjbm={"A01","A02","A03","A04","A05","A06","A99","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","E03","C01","C02","C99","D02","E01","E02","E04"};


    //贷款类别
    public static final String[] dklb={"F01","F03","F06","F07"};

    //贷款质量
    public static final String[] dkzl={"FQ01","FQ02","FQ03","FQ04","FQ05"};

    //机构类别
    public static  final String[] jglb={"A01","A02","B01","B02","C01","C02","C03","C04","C05","C06","C07","C08","C09","C10","C11","C12"
    ,"D01","D02","D03","D04","D05","D06","D07","D08","E","F","G","H","Z"
    };

    //性别
    public static final String[] sex={"01","02","03"};
    
    //婚姻情况
    public static final String[] marriage={"M01","M02","M06","M07"};
    
    //个人客户身份标识
    public static final String[] grkhsfbs={"1","2","3","9"};

    //个人客户身份标识
    public static final String[] tzyw={"F041","F042","F043"};


    //借款人国民经济部门委托
    public static final String[] gmjjbmwt={"D01","E05"};

   //委托人证件类型
   public static final String[] wtrzjlx={"B06","B12","B07","B09","B11"};

    //委托   借款人证件类型
    public static final String[] wtrzjlx2={"B01","B08"};

    //同业存款 业务类型
    public static final  String[] ywlx ={"T011","T012","T021","T022","T03","T04"};

    //缴存准备金方式
    public static final  String[] jczbjfs={"DR01","DR02","DR03","DR04"};

    //同业客户类别
    public static final  String[] tykhlb={"B0311","B0312","B03131","B03132","B03133","B03134","B032","B033","B034","B035",
            "B036","B041","B042","B043", "B044","B045","B046","B047","B051","B052","B053","B054","B055","B056","B057","B058",
            "B060","B061","B062","B063","B064","B065","B066","B067", "B068","B07","B08","B091","B092","B093","B094","B095",
            "B096","B097","B098","B099","B10","E012"};

    //是有含有特殊字符
    //private static final Pattern strPattern = Pattern.compile("[ _`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\\n|\\r|\\t");
    private static final Pattern strPattern = Pattern.compile("[？?！!^]");
    //是有含有特殊字符2（少-）
    private static final Pattern strPattern2 = Pattern.compile("[？?！!^]");
    //是有含有特殊字符3（少-|）
    private static final Pattern strPattern3 = Pattern.compile("[？?！!^]");
    //是有含有特殊字符4（少-并加了首尾空格校验）
    private static final Pattern strPattern4 = Pattern.compile("[？?！!^]");
    //是有含有特殊字符5（少-|、单引号、双引号、回车符、换行符、空格）
    private static final Pattern strPattern5 = Pattern.compile("[？?！!^]");
    //是有含有特殊字符6（少-*|、单引号、双引号、回车符、换行符、空格）
    private static final Pattern strPattern6 = Pattern.compile("[？?！!^]");
    //特殊字符校验
    private static final Pattern strPatternNew = Pattern.compile("[？?！!^]");
    //是否为数字
    private static final Pattern mathPattern=Pattern.compile("^[-\\+]?[\\d]*$");
    //是否为数字1
    private static final Pattern mathPattern1=Pattern.compile("^([0-9]{1}[0-9]{0,99}\\.[0-9]{1,99})?$");
    //是否为数字
    private static final Pattern mathPattern2=Pattern.compile("^([-\\ 0-9]{1}[0-9]{0,99}\\.[0-9]{1,99})?$");
    //是有包含%‰
    private static final Pattern perSignPattern=Pattern.compile("^[^%‰]*$");
    //从业人员数
    private static final Pattern cyryPattern=Pattern.compile("^[0-9]{1}|[1-9][0-9]*$");

    //不能超过三位的整数
    private static final Pattern mathPattern3=Pattern.compile("^[0-9]{1,3}$");

    private static final String nullError="不能为空";
    private static final String lengthError="超过限制长度";
    private static String spaceStr="|";
    public static Boolean grantNullAndLength(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()>length){
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStr(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr(fieldValue)){
                    errorMsg.append(fieldName+"不能包含空格和特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStr2(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr2(fieldValue)){
                    errorMsg.append(fieldName+"不能包含空格和特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantLengthAndStr2(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr2(fieldValue)){
                    errorMsg.append(fieldName+"不能包含空格和特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStr3(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr3(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStr4(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr4(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantLengthAndStr4(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr4(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStr5(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStr5(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantNullAndLengthAndStrNew(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStrNew(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    public static Boolean grantLengthAndStrNew(String fieldValue,int length,StringBuffer errorMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=length){
                if (checkStrNew(fieldValue)){
                    errorMsg.append(fieldName+"不能包含特殊字符"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+lengthError+length+spaceStr);isError=true;
            }
        }
        return isError;
    }
    public static Boolean nullAndDate(String fieldValue,StringBuffer errorMsg,boolean isError,String fieldName,boolean isHasNull){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()==10){
                boolean b1 = checkDate(fieldValue, "yyyy-MM-dd");
                if (b1){
                    if (fieldValue.compareTo("1800-01-01")<0 || fieldValue.compareTo("2100-12-31")>0){
                        errorMsg.append(fieldName+"需晚于1800-01-01早于2100-12-31"+spaceStr);isError=true;
                    }
                }else {
                    errorMsg.append(fieldName+"不符合yyyy-MM-dd格式"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+"长度不等于10"+spaceStr);isError=true;
            }
        }else {
            if (!isHasNull){
                errorMsg.append(fieldName+nullError+spaceStr);isError=true;
            }

        }
        return isError;
    }
    public static Boolean   equalNullAndCode(String fieldValue, int length, List<String> list,StringBuffer errorMsg, boolean isError, String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()==length){
                if (!list.contains(fieldValue)){
                    errorMsg.append(fieldName+"不在规定的代码范围内"+spaceStr);isError=true;
                }
            }else {
                errorMsg.append(fieldName+"长度不等于"+length+spaceStr);isError=true;
            }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    //币种判断
    public static Boolean checkCurrency(String fieldValue, List<String> currencyList, CustomSqlUtil customSqlUtil,StringBuffer tempMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            String[] strings = fieldValue.split(",");            
            for (String s : strings) {
                if (s.length()==3){                   
                    if (!currencyList.contains(fieldValue)){
                        tempMsg.append(fieldName+"不在货币与资金代码表中"+spaceStr);isError=true;
                        break;
                    }
                }else {
                    tempMsg.append(fieldName+"长度不等于3"+spaceStr);isError=true;
                    break;
                }
            }
        }else {
            tempMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }
    //贷款财政扶持方式
    public static Boolean checkLoanczfcfs(String fieldValue,StringBuffer tempMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()<=5){
                if (!ArrayUtils.contains(loanczfc,fieldValue)){
                    tempMsg.append(fieldName+"不在值域范围最底层类别中"+spaceStr);isError=true;
                }
            }else {
                tempMsg.append(fieldName+lengthError+"5"+spaceStr);isError=true;
            }
        }
        return isError;
    }
    //贷款担保方式
    public static Boolean checkdkdbfs(String fieldValue,StringBuffer tempMsg,boolean isError,String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (!ArrayUtils.contains(dkdbfs,fieldValue)){
                tempMsg.append(fieldName+"不在值域范围最底层类别中"+spaceStr);isError=true;
            }
        }else {
            tempMsg.append(fieldName+"不能为空"+spaceStr);isError=true;
        }
        return isError;
    }
    //借款人经济成分
    public static Boolean checkEntpczrjjcf(String industry,String entpmode,String entpczjjcf,StringBuffer tempMsg,boolean isError){
    	if (StringUtils.isNotBlank(entpczjjcf)) {
            if (entpczjjcf.length() <= 5) {
                if (!ArrayUtils.contains(qyczrjjcf, entpczjjcf)) {
                    tempMsg.append("客户经济成分不在符合要求的值域范围" + spaceStr);isError = true;
                }
            }else {
                tempMsg.append("客户经济成分" + lengthError + 5 + spaceStr);isError = true;
            }
        }
    	if (StringUtils.isNotBlank(industry) && !ArrayUtils.contains(jkrhy,industry) &&
                StringUtils.isNotBlank(entpmode) && ArrayUtils.contains(qygm, entpmode)) {
    		if(StringUtils.isBlank(entpczjjcf)) {
    			tempMsg.append("客户经济成分" + nullError + spaceStr);isError = true;
    		}
        }
        return isError;
    }


    /**
     * 校验日期
     * @param date
     * @return
     */
    public static boolean checkDate(String date,String pattern){
        boolean convertSuccess=true;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            Date parse = format.parse(date);
            return date.equals(format.format(parse));
        } catch (Exception e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }
    /**
     * 判断时间格式是否正确
     * @param str
     * @return
     */
    public static boolean isValidDate(String str) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //这里的时间格式根据自己需求更改（注意：格式区分大小写、格式区分大小写、格式区分大小写）
        try{
            Date date = (Date)formatter.parse(str);
            return str.equals(formatter.format(date));
        }catch(Exception e){
            return false;
        }
    }


    /**
     * 校验是否存在特殊字符
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr(String str){
        Matcher m = strPattern.matcher(str);
        return m.find();
    }

    /**
     * 校验是否存在特殊字符2
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr2(String str){
        Matcher m = strPattern2.matcher(str);
        return m.find();
    }
    
    /**
     * 校验是否存在特殊字符3
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr3(String str){
        Matcher m = strPattern3.matcher(str);
        return m.find();
    }
    
    /**
     * 校验是否存在特殊字符4
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr4(String str){
        Matcher m = strPattern4.matcher(str);
        return m.find();
    }
    
    /**
     * 校验是否存在特殊字符5
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr5(String str){
        Matcher m = strPattern5.matcher(str);
        return m.find();
    }
    /**
     * 校验是否存在特殊字符5
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr6(String str){
        Matcher m = strPattern6.matcher(str);
        return m.find();
    }

    /**
     * 校验是否存在特殊字符
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStrNew(String str){
        Matcher m = strPatternNew.matcher(str);
        return m.find();
    }

    
    /**
     * 校验数字是否保留 K 位小数
     * @param math 字符串
     * @param k 保留K位小数
     * @return true:保留了K位小数;false:没有保留
     */
    public static boolean  checkDecimal(String math,int k){
        Matcher matcher;
        if (math.indexOf(".") == -1){
            matcher = mathPattern.matcher(math);
        }else {
            matcher = mathPattern2.matcher(math);
        }
        if (matcher.matches()){
            String[] split = math.split("\\.");
            if (split.length > 1 && split[1].length()==k){
                return true;
            }else {
                return false;
            }
        }
        return false;
    }


    /**
     * 校验数字是否保留 K 位小数
     * @param math 字符串
     * @param k 保留K位小数
     * @return true:保留了K位小数;false:没有保留
     */
    public static boolean checkZhenshu(String math){
        Matcher matcher = mathPattern3.matcher(math);
        if (matcher.matches()){
                return true;
        }
        return false;
    }
    
    /**
     * 校验是否含有%
     * @param perSignStr
     * @return true:存在%;false:不存在
     */
    public static boolean checkPerSign(String perSignStr){
        Matcher matcher = perSignPattern.matcher(perSignStr);
        //matcher.matches()返回true为不包含
        if (matcher.matches()){
            return false;
        }
        return true;
    }

    /*
    * 校验是否为正整数
    * */
    public static boolean checkeCyryPattern(String s){
        Matcher matcher = cyryPattern.matcher(s);
        if (matcher.matches()){
            return true;
        }
        return false;
    }

    public static String checkDateDefaultFormat(String date){
        if (StringUtils.isNotBlank(date)){
            try {
            	if(date.length() == 8) {
            		String dateStr = "";
            		dateStr = date.substring(0,4)+"-"+date.substring(4,6)+"-"+date.substring(6);
            		return dateStr;
            	}
            	if(date.contains("年")&&date.contains("月")) {
            		date = date.replaceAll("年", "-");
            		date = date.replaceAll("月", "-");
            		if(date.contains("日")) {
            			date = date.replaceAll("日", "");
            		}
            	}
                String[] split = date.split("-");
                String[] split1 = date.split("/");
                if (split.length > 1){
                    if (split[1].length() == 1){
                        split[1] = "0"+split[1];
                    }
                    if (split[2].length() == 1){
                        split[2] = "0"+split[2];
                    }
                    date = split[0]+"-"+split[1]+"-"+split[2];
                }
                if (split1.length > 1){

                    if (split1[1].length() == 1){
                        split1[1] = "0"+split1[1];
                    }
                    if (split1[2].length() == 1){
                        split1[2] = "0"+split1[2];
                    }
                    date = split1[0]+"-"+split1[1]+"-"+split1[2];
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date parse = dateFormat.parse(date);
                return date;
            }catch (Exception e){
                return "";
            }
        }
        return "";
    }

    public static boolean   checkEconamicSector(String fieldValue, List<String> list, StringBuffer errorMsg, boolean isError, String fieldName) {
        if (StringUtils.isNotBlank(fieldValue)){
                if (!list.contains(fieldValue)){
                    errorMsg.append(fieldName+"不在规定的代码范围内"+spaceStr);isError=true;
                }
        }else {
            errorMsg.append(fieldName+nullError+spaceStr);isError=true;
        }
        return isError;
    }

    //判断交易对手证件类型
    public static boolean checkJydszjlx(String fieldValue) {
    	if(StringUtils.isBlank(fieldValue)) {
    		return false;
    	}
    	if(Arrays.asList(jydszjlx).contains(fieldValue)) {
    		return true;
    	}
    	return false;
    }
    
    //判断资产负债类型
    public static boolean checkZcfzlx(String fieldValue) {
    	if(StringUtils.isBlank(fieldValue)) {
    		return false;
    	}
    	if(Arrays.asList(zcfzlx).contains(fieldValue)) {
    		return true;
    	}
    	return false;
    }
    
    //判断产品类别_去除F06
    public static boolean checkCplb_F06(String fieldValue) {
    	if(StringUtils.isBlank(fieldValue)) {
    		return false;
    	}
    	if(Arrays.asList(cplb_F06).contains(fieldValue)) {
    		return true;
    	}
    	return false;
    }
    
    //判断计息方式
    public static boolean checkJxfs(String fieldValue) {
    	if(StringUtils.isBlank(fieldValue)) {
    		return false;
    	}
    	if(Arrays.asList(jxfs).contains(fieldValue)) {
    		return true;
    	}
    	return false;
    }
    
    public static void main(String[] args) {
        String date=" HYLINK DIGITAL SOLUTION CO.,LTD ";
        Matcher m = strPattern4.matcher(date);
        System.out.println(CheckUtil.checkeCyryPattern("66"));
    }
}
