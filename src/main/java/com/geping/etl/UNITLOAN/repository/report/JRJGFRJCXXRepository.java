package com.geping.etl.UNITLOAN.repository.report;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;
@Repository
public interface JRJGFRJCXXRepository extends JpaRepository<Xdkdbwx,Integer>,JpaSpecificationExecutor<Xdkdbwx> {


	//更改校验状态
	@Modifying
	@Query("UPDATE Xdkdbwx s SET s.checkstatus = ?1 where s.checkstatus = '0' or s.checkstatus = '2'")
	public Integer updateXdkdbwxOnCheckstatus(String checkStatus);
	
	//更改数据状态
	@Modifying
	@Query("UPDATE Xdkdbwx s SET s.datastatus = ?1 where s.checkstatus = '1'")
	public Integer updateXdkdbwxOnDatastatus(String dataStatus);
	
}
