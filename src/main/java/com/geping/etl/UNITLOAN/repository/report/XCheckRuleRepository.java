package com.geping.etl.UNITLOAN.repository.report;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.XCheckRule;

/**    
*  
* @author liuweixin  
* @date 2020年9月21日 上午10:07:19  
*/
@Repository
public interface XCheckRuleRepository extends JpaRepository<XCheckRule, String>, JpaSpecificationExecutor<XCheckRule> {

	@Query("select distinct reportname from XCheckRule order by reportname")
	public List<String> getReportnameList();
	
	@Query("select distinct fieldname from XCheckRule where reportname = ?1 order by fieldname")
	public List<String> getFieldnameList(String reportname);
	
	@Query("select checknum from XCheckRule where tablename = ?1 and isselect = 'y'")
	public List<String> getChecknumList(String tablename);
	
}
