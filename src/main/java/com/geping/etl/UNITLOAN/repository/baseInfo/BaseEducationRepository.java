package com.geping.etl.UNITLOAN.repository.baseInfo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:48:24  
*/
@Repository
public interface BaseEducationRepository extends JpaRepository<BaseEducation,String>,JpaSpecificationExecutor<BaseEducation>{

	@Query("from BaseEducation order by educationCode")
	public List<BaseEducation> getAll();
}
