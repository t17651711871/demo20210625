package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/30
 */
public interface XftykhxblRepository extends JpaRepository<Xftykhxbl, String>, JpaSpecificationExecutor<Xftykhxbl> {

    @Query("select x from Xftykhxbl x where x.customernum = ?1 and x.orgid = ?2 and x.departid like ?3")
    List<Xftykhxbl> getInfoByCustomernum(String customernum, String orgId, String departId);
}
