package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:00
 * @描述:
 */
public interface XclzqfxxxRepository extends JpaRepository<Xclzqfxxx, String>, JpaSpecificationExecutor<Xclzqfxxx> {

}
