package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 16:47
 * @描述:
 */
@Repository
public interface XgqtzfsexxRepository extends JpaRepository<Xgqtzfsexx, String>, JpaSpecificationExecutor<Xgqtzfsexx> {
}
