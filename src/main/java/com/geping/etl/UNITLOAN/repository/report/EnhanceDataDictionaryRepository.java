package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.baseInfo.DataDictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/5/24
 * @TIME: 10:26
 * @描述:
 */
@Repository
public interface EnhanceDataDictionaryRepository extends JpaRepository<DataDictionary,String>, JpaSpecificationExecutor<DataDictionary> {

    @Query("select code from DataDictionary  where type = ?1")
    List<String> getReportCodeList(int type);
}
