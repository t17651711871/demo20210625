package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * 一级行业代码 
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="baseaindustry")
public class BaseAindustry {

	@Id
	private String id;

	/**
	 * 一级行业代码
	 */
	private String aindustrycode;

	/**
	 * 一级行业名称
	 */
	private String aindustryname;

	/**
	 * 以及行业描述
	 */
	private String aindustrydesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getAindustrycode() {
    return aindustrycode;
  }

  public void setAindustrycode(String aindustrycode) {
    this.aindustrycode = aindustrycode;
  }


  public String getAindustryname() {
    return aindustryname;
  }

  public void setAindustryname(String aindustryname) {
    this.aindustryname = aindustryname;
  }


  public String getAindustrydesc() {
    return aindustrydesc;
  }

  public void setAindustrydesc(String aindustrydesc) {
    this.aindustrydesc = aindustrydesc;
  }

}
