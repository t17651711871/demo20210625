package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description  
 * @Author  chenggen
 * @Date 2021-01-06
 * 单位贷款
 */

@Entity
@Table (name ="xclwtdkxxH")
public class XclwtdkxxH {

	@Id
	private String id;

	/**
     * 金融机构代码
	 */
	private String financeorgcode;

	/**
	 * 内部机构号
	 */
	private String financeorginnum;

	/**
	 * 金融机构地区代码
	 */
	private String financeorgareacode;

    /**
     * 借款人证件类型
     */
    private String isfarmerloan;

	/**
	 * 借款人证件代码
	 */
	private String brroweridnum;

    /**
     * 借款人国民经济部门
     */
    private String isgreenloan;


	/**
	 * 借款人行业
	 */
	private String brrowerindustry;

	/**
	 * 借款人地区代码
	 */
	private String brrowerareacode;

	/**
	 * 借款人经济成分
	 */
	private String inverstoreconomy;

	/**
	 * 借款人企业规模
	 */
	private String enterprisescale;

	/**
	 * 委托贷款借据编码
	 */
	private String receiptcode;

	/**
	 * 委托贷款合同编码
	 */
	private String contractcode;


	/**
	 * 贷款实际投向
	 */
	private String loanactualdirection;

	/**
	 * 委托贷款发放日期
	 */
	private String loanstartdate;

	/**
	 * 委托贷款到期日期
	 */
	private String loanenddate;

	/**
	 * 委托贷款展期到期日期
	 */
	private String extensiondate;

	/**
	 * 币种
	 */
	private String currency;

	/**
	 * 委托贷款余额
	 */
	private String receiptbalance;

	/**
	 * 委托贷款余额折人民币
	 */
	private String receiptcnybalance;

	/**
	 * 利率是否固定
	 */
	private String interestisfixed;

	/**
	 * 利率水平
	 */
	private String interestislevel;


    /**
     * 手续费金额折人民币
     */
    private String procedurebalance;


	/**
	 * 贷款担保方式
	 */
	private String guaranteemethod;


	/**
	 * 贷款质量
	 */
	private String loanquality;

	/**
	 * 贷款状态
	 */
	private String loanstatus;


    /**
     * 委托人国民经济部门
     */
    private String bailorgreenloan;

    /**
     * 委托人证件类型
     */
    private String bailorgfarmerloan;

    /**
     * 委托人证件代码
     */
    private String bailorgidnum;

    /**
     * 委托人行业
     */
    private String bailorgindustry;

    /**
     * 委托人地区代码
     */
    private String bailorgareacode;

    /**
     * 委托人经济成分
     */
    private String bailorgeconomy;

    /**
     * 委托人企业规模
     */
    private String bailorgenterprise;


	/**
	 * 贷款用途
	 */
	private String issupportliveloan;


	/**
     * 数据日期
     */

    private String sjrq;

    /**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFinanceorgcode() {
        return financeorgcode;
    }

    public void setFinanceorgcode(String financeorgcode) {
        this.financeorgcode = financeorgcode;
    }

    public String getFinanceorginnum() {
        return financeorginnum;
    }

    public void setFinanceorginnum(String financeorginnum) {
        this.financeorginnum = financeorginnum;
    }

    public String getFinanceorgareacode() {
        return financeorgareacode;
    }

    public void setFinanceorgareacode(String financeorgareacode) {
        this.financeorgareacode = financeorgareacode;
    }

    public String getIsfarmerloan() {
        return isfarmerloan;
    }

    public void setIsfarmerloan(String isfarmerloan) {
        this.isfarmerloan = isfarmerloan;
    }

    public String getBrroweridnum() {
        return brroweridnum;
    }

    public void setBrroweridnum(String brroweridnum) {
        this.brroweridnum = brroweridnum;
    }

    public String getIsgreenloan() {
        return isgreenloan;
    }

    public void setIsgreenloan(String isgreenloan) {
        this.isgreenloan = isgreenloan;
    }

    public String getBrrowerindustry() {
        return brrowerindustry;
    }

    public void setBrrowerindustry(String brrowerindustry) {
        this.brrowerindustry = brrowerindustry;
    }

    public String getBrrowerareacode() {
        return brrowerareacode;
    }

    public void setBrrowerareacode(String brrowerareacode) {
        this.brrowerareacode = brrowerareacode;
    }

    public String getInverstoreconomy() {
        return inverstoreconomy;
    }

    public void setInverstoreconomy(String inverstoreconomy) {
        this.inverstoreconomy = inverstoreconomy;
    }

    public String getEnterprisescale() {
        return enterprisescale;
    }

    public void setEnterprisescale(String enterprisescale) {
        this.enterprisescale = enterprisescale;
    }

    public String getReceiptcode() {
        return receiptcode;
    }

    public void setReceiptcode(String receiptcode) {
        this.receiptcode = receiptcode;
    }

    public String getContractcode() {
        return contractcode;
    }

    public void setContractcode(String contractcode) {
        this.contractcode = contractcode;
    }

    public String getLoanactualdirection() {
        return loanactualdirection;
    }

    public void setLoanactualdirection(String loanactualdirection) {
        this.loanactualdirection = loanactualdirection;
    }

    public String getLoanstartdate() {
        return loanstartdate;
    }

    public void setLoanstartdate(String loanstartdate) {
        this.loanstartdate = loanstartdate;
    }

    public String getLoanenddate() {
        return loanenddate;
    }

    public void setLoanenddate(String loanenddate) {
        this.loanenddate = loanenddate;
    }

    public String getExtensiondate() {
        return extensiondate;
    }

    public void setExtensiondate(String extensiondate) {
        this.extensiondate = extensiondate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getReceiptbalance() {
        return receiptbalance;
    }

    public void setReceiptbalance(String receiptbalance) {
        this.receiptbalance = receiptbalance;
    }

    public String getReceiptcnybalance() {
        return receiptcnybalance;
    }

    public void setReceiptcnybalance(String receiptcnybalance) {
        this.receiptcnybalance = receiptcnybalance;
    }

    public String getInterestisfixed() {
        return interestisfixed;
    }

    public void setInterestisfixed(String interestisfixed) {
        this.interestisfixed = interestisfixed;
    }

    public String getInterestislevel() {
        return interestislevel;
    }

    public void setInterestislevel(String interestislevel) {
        this.interestislevel = interestislevel;
    }

    public String getProcedurebalance() {
        return procedurebalance;
    }

    public void setProcedurebalance(String procedurebalance) {
        this.procedurebalance = procedurebalance;
    }

    public String getGuaranteemethod() {
        return guaranteemethod;
    }

    public void setGuaranteemethod(String guaranteemethod) {
        this.guaranteemethod = guaranteemethod;
    }

    public String getLoanquality() {
        return loanquality;
    }

    public void setLoanquality(String loanquality) {
        this.loanquality = loanquality;
    }

    public String getLoanstatus() {
        return loanstatus;
    }

    public void setLoanstatus(String loanstatus) {
        this.loanstatus = loanstatus;
    }

    public String getBailorgreenloan() {
        return bailorgreenloan;
    }

    public void setBailorgreenloan(String bailorgreenloan) {
        this.bailorgreenloan = bailorgreenloan;
    }

    public String getBailorgfarmerloan() {
        return bailorgfarmerloan;
    }

    public void setBailorgfarmerloan(String bailorgfarmerloan) {
        this.bailorgfarmerloan = bailorgfarmerloan;
    }

    public String getBailorgidnum() {
        return bailorgidnum;
    }

    public void setBailorgidnum(String bailorgidnum) {
        this.bailorgidnum = bailorgidnum;
    }

    public String getBailorgindustry() {
        return bailorgindustry;
    }

    public void setBailorgindustry(String bailorgindustry) {
        this.bailorgindustry = bailorgindustry;
    }

    public String getBailorgareacode() {
        return bailorgareacode;
    }

    public void setBailorgareacode(String bailorgareacode) {
        this.bailorgareacode = bailorgareacode;
    }

    public String getBailorgeconomy() {
        return bailorgeconomy;
    }

    public void setBailorgeconomy(String bailorgeconomy) {
        this.bailorgeconomy = bailorgeconomy;
    }

    public String getBailorgenterprise() {
        return bailorgenterprise;
    }

    public void setBailorgenterprise(String bailorgenterprise) {
        this.bailorgenterprise = bailorgenterprise;
    }


    public String getIssupportliveloan() {
        return issupportliveloan;
    }

    public void setIssupportliveloan(String issupportliveloan) {
        this.issupportliveloan = issupportliveloan;
    }

    public String getSjrq() {
        return sjrq;
    }

    public void setSjrq(String sjrq) {
        this.sjrq = sjrq;
    }

    public String getCheckstatus() {
        return checkstatus;
    }

    public void setCheckstatus(String checkstatus) {
        this.checkstatus = checkstatus;
    }

    public String getDatastatus() {
        return datastatus;
    }

    public void setDatastatus(String datastatus) {
        this.datastatus = datastatus;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperationname() {
        return operationname;
    }

    public void setOperationname(String operationname) {
        this.operationname = operationname;
    }

    public String getOperationtime() {
        return operationtime;
    }

    public void setOperationtime(String operationtime) {
        this.operationtime = operationtime;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getDepartid() {
        return departid;
    }

    public void setDepartid(String departid) {
        this.departid = departid;
    }

    public String getNopassreason() {
        return nopassreason;
    }

    public void setNopassreason(String nopassreason) {
        this.nopassreason = nopassreason;
    }
}
