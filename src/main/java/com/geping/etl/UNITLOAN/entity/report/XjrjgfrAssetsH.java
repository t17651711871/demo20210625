package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

/**
 * 金融机构（法人）基础信息-资产负债及风险统计表正式表
 * @Author  wuzengwen
 * @Date 2020-07-08 
 */

@Entity
@Table (name ="xjrjgfrassetsH")
public class XjrjgfrAssetsH {

	@Id
	private String id;

	/**
	 * 各项存款

	 */
	private String gxck;

	/**
	 * 各项贷款

	 */
	private String gxdk;

	/**
	 * 资产总计

	 */
	private String zczj;

	/**
	 * 负债总计

	 */
	private String fzzj;

	/**
	 * 所有者权益合计

	 */
	private String syzqyhj;

	/**
	 * 生息资产

	 */
	private String sxzc;

	/**
	 * 付息负债

	 */
	private String fxzc;

	/**
	 * 流动性资产

	 */
	private String ldxzc;

	/**
	 * 流动性负债

	 */
	private String ldxfz;

	/**
	 * 正常类贷款

	 */
	private String zcldk;

	/**
	 * 关注类贷款

	 */
	private String gzldk;

	/**
	 * 次级类贷款

	 */
	private String cjldk;

	/**
	 * 可疑类贷款

	 */
	private String jyldk;

	/**
	 * 损失类贷款

	 */
	private String ssldk;

	/**
	 * 逾期贷款

	 */
	private String yqdk;

	/**
	 * 逾期90天以上贷款

	 */
	private String yqninetytysdk;

	/**
	 * 贷款减值准备
	 */
	private String dkjzzb;

    public String getSjrq() {
        return sjrq;
    }

    public void setSjrq(String sjrq) {
        this.sjrq = sjrq;
    }

    /*
     *   数据日期
     */
    private  String sjrq;

	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getGxck() {
    return gxck;
  }

  public void setGxck(String gxck) {
    this.gxck = gxck;
  }


  public String getGxdk() {
    return gxdk;
  }

  public void setGxdk(String gxdk) {
    this.gxdk = gxdk;
  }


  public String getZczj() {
    return zczj;
  }

  public void setZczj(String zczj) {
    this.zczj = zczj;
  }


  public String getFzzj() {
    return fzzj;
  }

  public void setFzzj(String fzzj) {
    this.fzzj = fzzj;
  }


  public String getSyzqyhj() {
    return syzqyhj;
  }

  public void setSyzqyhj(String syzqyhj) {
    this.syzqyhj = syzqyhj;
  }


  public String getSxzc() {
    return sxzc;
  }

  public void setSxzc(String sxzc) {
    this.sxzc = sxzc;
  }


  public String getFxzc() {
    return fxzc;
  }

  public void setFxzc(String fxzc) {
    this.fxzc = fxzc;
  }


  public String getLdxzc() {
    return ldxzc;
  }

  public void setLdxzc(String ldxzc) {
    this.ldxzc = ldxzc;
  }


  public String getLdxfz() {
    return ldxfz;
  }

  public void setLdxfz(String ldxfz) {
    this.ldxfz = ldxfz;
  }


  public String getZcldk() {
    return zcldk;
  }

  public void setZcldk(String zcldk) {
    this.zcldk = zcldk;
  }


  public String getGzldk() {
    return gzldk;
  }

  public void setGzldk(String gzldk) {
    this.gzldk = gzldk;
  }


  public String getCjldk() {
    return cjldk;
  }

  public void setCjldk(String cjldk) {
    this.cjldk = cjldk;
  }


  public String getJyldk() {
    return jyldk;
  }

  public void setJyldk(String jyldk) {
    this.jyldk = jyldk;
  }


  public String getSsldk() {
    return ssldk;
  }

  public void setSsldk(String ssldk) {
    this.ssldk = ssldk;
  }


  public String getYqdk() {
    return yqdk;
  }

  public void setYqdk(String yqdk) {
    this.yqdk = yqdk;
  }


  public String getYqninetytysdk() {
    return yqninetytysdk;
  }

  public void setYqninetytysdk(String yqninetytysdk) {
    this.yqninetytysdk = yqninetytysdk;
  }


  public String getDkjzzb() {
    return dkjzzb;
  }

  public void setDkjzzb(String dkjzzb) {
    this.dkjzzb = dkjzzb;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

}
