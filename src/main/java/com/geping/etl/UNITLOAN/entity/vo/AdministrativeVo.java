package com.geping.etl.UNITLOAN.entity.vo;

import java.util.List;

/**
 * @Author: David
 * @Date: Created in 15:44 2019/5/14
 * @Description: ${DESCRIPTION}
 * @Modified By:
 * @Version: ${VERSION}
 */

public class AdministrativeVo {

    private String id;

    private String text;

    private String state;

    private List<AdministrativeVo> children;

    public AdministrativeVo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<AdministrativeVo> getChildren() {
        return children;
    }

    public void setChildren(List<AdministrativeVo> children) {
        this.children = children;
    }
}
