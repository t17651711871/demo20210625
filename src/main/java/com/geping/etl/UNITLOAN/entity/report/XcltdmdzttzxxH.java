package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

@Entity
@Table (name ="xcltdmdzttzxxh")
public class XcltdmdzttzxxH {
  @Id
  private String id;
  private String financeorgcode; //金融机构代码
  private String financeorginnum;//内部机构号
  private String tdmdztlx;//特定目的载体类型
  private String zgcptjbm;//资管产品统计编码
  private String tdmdztdm;//特定目的载体代码
  private String fxrdm;//发行人代码
  private String fxrdqdm;//发行人地区代码
  private String yxfs;//运行方式
  private String rgrq;//认购日期
  private String dqrq;//到期日期
  private String bz;//币种
  private String tzye;//投资余额
  private String tzyezrmb;//投资余额折人民币
  private String checkstatus;
  private String datastatus;
  private String operator;
  private String operationname;
  private String operationtime;
  private String orgid;
  private String departid;
  private String nopassreason;
  private String sjrq;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getFinanceorgcode() {
    return financeorgcode;
  }

  public void setFinanceorgcode(String financeorgcode) {
    this.financeorgcode = financeorgcode;
  }


  public String getFinanceorginnum() {
    return financeorginnum;
  }

  public void setFinanceorginnum(String financeorginnum) {
    this.financeorginnum = financeorginnum;
  }


  public String getTdmdztlx() {
    return tdmdztlx;
  }

  public void setTdmdztlx(String tdmdztlx) {
    this.tdmdztlx = tdmdztlx;
  }


  public String getZgcptjbm() {
    return zgcptjbm;
  }

  public void setZgcptjbm(String zgcptjbm) {
    this.zgcptjbm = zgcptjbm;
  }


  public String getTdmdztdm() {
    return tdmdztdm;
  }

  public void setTdmdztdm(String tdmdztdm) {
    this.tdmdztdm = tdmdztdm;
  }


  public String getFxrdm() {
    return fxrdm;
  }

  public void setFxrdm(String fxrdm) {
    this.fxrdm = fxrdm;
  }


  public String getFxrdqdm() {
    return fxrdqdm;
  }

  public void setFxrdqdm(String fxrdqdm) {
    this.fxrdqdm = fxrdqdm;
  }


  public String getYxfs() {
    return yxfs;
  }

  public void setYxfs(String yxfs) {
    this.yxfs = yxfs;
  }


  public String getRgrq() {
    return rgrq;
  }

  public void setRgrq(String rgrq) {
    this.rgrq = rgrq;
  }


  public String getDqrq() {
    return dqrq;
  }

  public void setDqrq(String dqrq) {
    this.dqrq = dqrq;
  }


  public String getBz() {
    return bz;
  }

  public void setBz(String bz) {
    this.bz = bz;
  }


  public String getTzye() {
    return tzye;
  }

  public void setTzye(String tzye) {
    this.tzye = tzye;
  }


  public String getTzyezrmb() {
    return tzyezrmb;
  }

  public void setTzyezrmb(String tzyezrmb) {
    this.tzyezrmb = tzyezrmb;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

  public String getSjrq() {
    return sjrq;
  }

  public void setSjrq(String sjrq) {
    this.sjrq = sjrq;
  }

}
