package com.geping.etl.UNITLOAN.common.excel;

import java.util.HashMap;
import java.util.Map;

/***
 *   Excel 表头中文映射
 * @author liang.xu
 * @date 2021.4.12
 */
public class HeadersZh {

    /**
     * 表头中文映射
     */
    private static Map<String, String[]> HEADERS_ZH_MAP = new HashMap<>();

    /**
     * 存量债券投资信息
     */
    static {
        HEADERS_ZH_MAP.put("Xclzqtzxx", new String[]{"金融机构代码","内部机构号","债券代码","债券总托管机构","债券品种","债券信用级别","币种","债券余额","债券余额折人民币","债权债务登记日","起息日","兑付日期","票面利率","发行人证件代码","发行人地区代码","发行人行业","发行人企业规模","发行人经济成分","发行人国民经济部门","数据日期"});

    }
    /**
     * 存量债券投资信息历史表
     */
    static {
        HEADERS_ZH_MAP.put("XclzqtzxxH", new String[]{"金融机构代码","内部机构号","债券代码","债券总托管机构","债券品种","债券信用级别","币种","债券余额","债券余额折人民币","债权债务登记日","起息日","兑付日期","票面利率","发行人证件代码","发行人地区代码","发行人行业","发行人企业规模","发行人经济成分","发行人国民经济部门","数据日期"});

    }

    /**
     * 债券投资发生额信息
     */
    static {
        HEADERS_ZH_MAP.put("Xzqtzfsexx", new String[]{"金融机构代码","内部机构号","债券代码","债券总托管机构","债券品种","债券信用级别","币种","债权债务登记日","起息日","兑付日期","票面利率","发行人证件代码","发行人地区代码","发行人行业","发行人企业规模","发行人经济成分","发行人国民经济部门","交易日期","交易流水号","成交金额","成交金额折人名币","买入/卖出标志","数据日期"});
    }
    /**
     * 债券投资发生额信息历史表
     */
    static {
        HEADERS_ZH_MAP.put("XzqtzfsexxH", new String[]{"金融机构代码","内部机构号","债券代码","债券总托管机构","债券品种","债券信用级别","币种","债权债务登记日","起息日","兑付日期","票面利率","发行人证件代码","发行人地区代码","发行人行业","发行人企业规模","发行人经济成分","发行人国民经济部门","交易日期","交易流水号","成交金额","成交金额折人名币","买入/卖出标志","数据日期"});
    }

    /**
     *  存量债券发行信息
     */

    static {
        HEADERS_ZH_MAP.put("Xclzqfxxx", new String[]{"金融机构代码","债券代码","债券总托管机构","债券品种","债券信用级别","续发次数","币种","期末债券面值","期末债券面值折人民币","期末债券余额","期末债券余额折人名币","债权债务登记日","起息日","兑付日期","付息方式","票面利率","数据日期"});

    }

    /**
     *  存量债券发行信息历史表
     */

    static {
        HEADERS_ZH_MAP.put("XclzqfxxxH", new String[]{"金融机构代码","债券代码","债券总托管机构","债券品种","债券信用级别","续发次数","币种","期末债券面值","期末债券面值折人民币","期末债券余额","期末债券余额折人名币","债权债务登记日","起息日","兑付日期","付息方式","票面利率","数据日期"});

    }

    /**
     * 债券发行发生额信息
     */
    static {
        HEADERS_ZH_MAP.put("Xzqfxfsexx", new String[]{"金融机构代码","债券代码","债券总托管机构","债券品种","债券信用级别","续发次数","币种","发行/兑付债券面值","发行/兑付债券面值折人民币","发行/兑付债券金额","发行/兑付债券金额折人民币","债权债务登记日","起息日","兑付日期","交易日期","票面利率","发行兑付标识","数据日期"});
    }
    /**
     * 债券发行发生额信息历史表
     */
    static {
        HEADERS_ZH_MAP.put("XzqfxfsexxH", new String[]{"金融机构代码","债券代码","债券总托管机构","债券品种","债券信用级别","续发次数","币种","发行/兑付债券面值","发行/兑付债券面值折人民币","发行/兑付债券金额","发行/兑付债券金额折人民币","债权债务登记日","起息日","兑付日期","交易日期","票面利率","发行兑付标识","数据日期"});
    }
    /**
     * 存量股权投资信息
     */
    static {
        HEADERS_ZH_MAP.put("Xclgqtzxx", new String[]{"金融机构代码","内部机构号","凭证编码","股权类型","机构类型","机构证件代码","地区代码","币种","投资余额","投资余额折人民币","数据日期"});
    }
    static {
        HEADERS_ZH_MAP.put("XclgqtzxxH", new String[]{"金融机构代码","内部机构号","凭证编码","股权类型","机构类型","机构证件代码","地区代码","币种","投资余额","投资余额折人民币","数据日期"});
    }
    /**
     * 股权投资发生额信息
     */
    static {
        HEADERS_ZH_MAP.put("Xgqtzfsexx", new String[]{"金融机构代码","内部机构号","凭证编码","股权类型","机构类型","机构证件代码","地区代码","交易日期","币种","交易金额","交易金额折人民币","交易方向","数据日期"});
    }
    static {
        HEADERS_ZH_MAP.put("XgqtzfsexxH", new String[]{"金融机构代码","内部机构号","凭证编码","股权类型","机构类型","机构证件代码","地区代码","交易日期","币种","交易金额","交易金额折人民币","交易方向","数据日期"});
    }
    /**
     * 存量特定目的载体投资信息
     */
    static {
        HEADERS_ZH_MAP.put("Xcltdmdzttzxx", new String[]{"金融机构代码","内部机构号","特定目的载体类型","资管产品统计编码","特定目的载体代码","发行人代码","发行人地区代码","运行方式","认购日期","到期日期","币种","投资余额","投资余额折人民币","数据日期"});
    }
    static {
        HEADERS_ZH_MAP.put("XcltdmdzttzxxH", new String[]{"金融机构代码","内部机构号","特定目的载体类型","资管产品统计编码","特定目的载体代码","发行人代码","发行人地区代码","运行方式","认购日期","到期日期","币种","投资余额","投资余额折人民币","数据日期"});
    }
    /**
     * 特定目的载体投资发生额信息
     */
    static {
        HEADERS_ZH_MAP.put("Xtdmdzttzfsexx", new String[]{"金融机构代码","内部机构号","特定目的载体类型","资管产品统计编码","特定目的载体代码","发行人代码","发行人地区代码","运行方式","认购日期","到期日期","交易日期","币种","交易金额","交易金额折人民币","交易方向","数据日期"});
    }
    static {
        HEADERS_ZH_MAP.put("XtdmdzttzfsexxH", new String[]{"金融机构代码","内部机构号","特定目的载体类型","资管产品统计编码","特定目的载体代码","发行人代码","发行人地区代码","运行方式","认购日期","到期日期","交易日期","币种","交易金额","交易金额折人民币","交易方向","数据日期"});
    }





    public static Map<String, String[]> getHeadersZhMap() {
        return HEADERS_ZH_MAP;
    }

    public static void main(String[] args) {
        StringBuffer stringBuffer = new StringBuffer("");
        String[] xgqtzfsexxes = HeadersZh.getHeadersZhMap().get("XtdmdzttzfsexxH");
        for (int i = 0; i < xgqtzfsexxes.length; i++) {
            stringBuffer.append(xgqtzfsexxes[i]);
        }
        System.out.println("xgqtzfsexxes = " + stringBuffer);
    }
}
