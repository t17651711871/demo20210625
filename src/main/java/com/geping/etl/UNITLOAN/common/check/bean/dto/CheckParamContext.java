package com.geping.etl.UNITLOAN.common.check.bean.dto;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import java.util.*;

/***
 *  基础校验类
 * @author liang.xu
 * @date 2021.4.12
 */
public class CheckParamContext {

    /**
     * 数据IDS
     */
    private String id;

    /**
     * 类名
     */
    private String className;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 自定义参数
     */
    private String param;


    /**
     * 保存目标地址
     */
    private String target;

    /**
     * 用户
     */
    private Sys_UserAndOrgDepartment sysUser;

    /**
     * 计算数量的SQL
     */
    private  String countsql;

    /**
     * 数量
     */
    private  int  count;

    /**
     * 获取数据的SQL
     */
    private  String sql;

    /**
     * 目报表类
     */
    private  Class clazz;


    /**
     * 条件
     */
    private  String  whereCond;


    /**
     * 错误信息
     */
    private LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();

    /**
     * 校验错误记录ID
     */
    private List<String> errorId = new ArrayList<>();

    /**
     * 校验正确记录ID
     */
    private List<String> rightId = new ArrayList<>();

    /**
     * 更新条件
     */
    private Map<String,String> setMap=new HashMap<>();

    /**
     * 校验规则
     */
    private  List<String> checkNums;
    /**
     * 数据字典
     */
    private  Map<String,List> dataDictionarymap;

    public Map<String, List> getDataDictionarymap() {
        return dataDictionarymap;
    }

    public void setDataDictionarymap(Map<String, List> dataDictionarymap) {
        this.dataDictionarymap = dataDictionarymap;
    }

    public String getTarget() {
        return target;
    }

    public CheckParamContext setTarget(String target) {
        this.target = target;
        return this;
    }


    public String getId() {
        return id;
    }

    public CheckParamContext setId(String id) {
        this.id = id;
        return this;
    }

    public String getParam() {
        return param;
    }

    public CheckParamContext setParam(String param) {
        this.param = param;
        return this;
    }


    public String getClassName() {
        return className;
    }

    public CheckParamContext setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public CheckParamContext setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }


    public Sys_UserAndOrgDepartment getSysUser() {
        return sysUser;
    }

    public void setSysUser(Sys_UserAndOrgDepartment sysUser) {
        this.sysUser = sysUser;
    }


    public String getCountsql() {
        return countsql;
    }

    public CheckParamContext setCountsql(String countsql) {
        this.countsql = countsql;
        return this;
    }

    public int getCount() {
        return count;
    }

    public CheckParamContext setCount(int count) {
        this.count = count;
        return this;
    }

    public String getSql() {
        return sql;
    }

    public CheckParamContext setSql(String sql) {
        this.sql = sql;
        return this;
    }

    public Class getClazz() {
        return clazz;
    }

    public CheckParamContext setClazz(Class clazz) {
        this.clazz = clazz;
        return this;
    }

    public String getWhereCond() {
        return whereCond;
    }

    public CheckParamContext setWhereCond(String whereCond) {
        this.whereCond = whereCond;
        return this;
    }

    public LinkedHashMap<String, String> getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(LinkedHashMap<String, String> errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<String> getErrorId() {
        return errorId;
    }

    public void setErrorId(List<String> errorId) {
        this.errorId = errorId;
    }

    public List<String> getRightId() {
        return rightId;
    }

    public void setRightId(List<String> rightId) {
        this.rightId = rightId;
    }

    public Map<String, String> getSetMap() {
        return setMap;
    }

    public void setSetMap(Map<String, String> setMap) {
        this.setMap = setMap;
    }

    public void addSetMap(String key,String value){
        this.setMap.put(key,value);
    }

    public List<String> getCheckNums() {
        return checkNums;
    }

    public void setCheckNums(List<String> checkNums) {
        this.checkNums = checkNums;
    }
}