package com.geping.etl.UNITLOAN.common.excel.utils;

import com.geping.etl.UNITLOAN.common.excel.HeadersEnglish;
import com.geping.etl.UNITLOAN.common.excel.HeadersZh;

import java.util.LinkedHashMap;
import java.util.Map;

/***
 *   Excel  工具类
 * @author liang.xu
 * @date 2021.4.12
 */
public class ExcelUtils {


    /**
     * 获取excel 表头
     *
     * @return
     */
    public static Map<String, String> getExcelHeads(String entityName) {
        Map<String, String[]> headersZhMap = HeadersZh.getHeadersZhMap();
        Map<String, String[]> headersEngMap = HeadersEnglish.getHeadersEngMap();
        String[] headersZh = headersZhMap.get(entityName);
        String[] headersEng = headersEngMap.get(entityName);
        Map<String, String> excelHeads = new LinkedHashMap<>();
        for (int i = 0; i < headersEng.length; i++) {
            excelHeads.put(headersEng[i], headersZh[i]);
        }
        return excelHeads;
    }


    public static void main(String[] args) {

        System.out.println(ExcelUtils.getExcelHeads("Xclzqfxxx"));
    }
}