package com.geping.etl.UNITLOAN.common.check;

import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/***
 *   校验处理抽象类
 * @author liang.xu
 * @date 2021.4.10
 */
public abstract class CheckProcessorContainerService implements CheckProcessorService {

    private static Map<String, CheckProcessorService> checkProcessors = new HashMap<>();

    public CheckProcessorContainerService() {
        CheckProcessorService checkProcessorService = this;
        checkProcessors.put(checkProcessorService.getKey(), checkProcessorService);
    }

    /**
     * 执行校验
     *
     * @param key
     * @param checkParamContext
     */
    public  static JSONObject doCheckData(String key, CheckParamContext checkParamContext) {
       return  checkProcessors.get(key).checkData(checkParamContext);
    }

}