package com.geping.etl.UNITLOAN.common.excel;


import com.geping.etl.UNITLOAN.common.excel.service.ExcelImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 *  Excel 导入
 * @author liang.xu
 * @date 2021.4.12
 */
@RestController
public class ExcelImportController {

    @Autowired
    private ExcelImportService excelImportService;


    //导入excel
    @PostMapping(value = "importExcel*")
    public void importExcel(HttpServletRequest request, HttpServletResponse response) {
        excelImportService.importExcel(request, response);
    }
}
