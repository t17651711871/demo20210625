package com.geping.etl.UNITLOAN.common.check.enums;

/***
 *  校验状态枚举
 * @author liang.xu
 * @date 2021.4.19
 */
public enum CheckStatusEnum {

    /**
     * 校验成功
     */
    CHECK_SUCCESS(1, "校验成功"),

    /**
     * 没有可校验的数据
     */
    NO_CHECK_DATA(-1, "没有可校验的数据！"),
    /**
     * 校验失败
     */
    CHECK_FAIL(0, "校验失败");


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private Integer status;
    private String text;

    private CheckStatusEnum(Integer status, String text) {
        this.status = status;
        this.text = text;
    }
}
