package com.geping.etl.UNITLOAN.common.excel;

import java.util.HashMap;
import java.util.Map;

/***
 *   Excel amount 映射
 * @author liang.xu
 * @date 2021.4.12
 */
public class AmountFields {

    /**
     * amount 映射
     */
    private static Map<String, String[]> AMOUNT_MAP = new HashMap<>();


    static {
        AMOUNT_MAP.put("A", new String[]{"name"});
    }

    public static Map<String, String[]> getAmountMap() {
        return AMOUNT_MAP;
    }

}