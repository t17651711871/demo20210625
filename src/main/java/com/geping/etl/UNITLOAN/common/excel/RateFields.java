package com.geping.etl.UNITLOAN.common.excel;

import java.util.HashMap;
import java.util.Map;

/***
 *   Excel rate 映射
 * @author liang.xu
 * @date 2021.4.12
 */
public class RateFields {

    /**
     * rate 映射
     */
    private static Map<String, String[]> RATE_MAP = new HashMap<>();


    static {
        RATE_MAP.put("A", new String[]{"name"});
    }

    public static Map<String, String[]> getRateMap() {
        return RATE_MAP;
    }
}