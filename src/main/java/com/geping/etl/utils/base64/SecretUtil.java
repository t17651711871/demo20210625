package com.geping.etl.utils.base64;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.commons.lang.StringUtils;

/**
 * @time 2018-09-09
 * @author David
 * 加密/解密工具类
 */
public class SecretUtil {
	
	private static final Base64.Decoder decoder = Base64.getDecoder();
	
	private static final Base64.Encoder encoder = Base64.getEncoder();
	
	/**
	 * 解密字符串
	 * @param text 需要解密的字符串
	 * @return
	 */
	public static String decoder (String text) {
		String value = null;
		if (StringUtils.isNotBlank(text)) {
			byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
			value = new String(decoder.decode(bytes), StandardCharsets.UTF_8);
		}
		return value;
	}
	
	/**
	 * 加密字符串
	 * @param text 需要加密的字符串
	 * @return
	 */
	public static String encoder (String text) {
		String value= "";
		if (StringUtils.isNotBlank(text)) {
			byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
			value = new String(decoder.decode(bytes), StandardCharsets.UTF_8);
		}
		return value;
	}
	
}	
