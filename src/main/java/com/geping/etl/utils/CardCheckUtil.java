package com.geping.etl.utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author David
 *	校验证件号码
 */
public class CardCheckUtil {

    private static int [] centerWeightedfactors = {1,3,5,7,11,2,13,1,1,17,19,97,23,29};

    private static String creditLegalValue = "0123456789ABCDEFGHJKLMNPQRTUWXY";

    private static int creditWeightedfactors[] = new int[]{1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28};

    private static String[] creditCheckCodeStr = new String[]{"0","1","2","3","4","5","6","7","8","9",
            "A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","T","U","W","X","Y"};

    private static String organizationLegalValue = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static int organizationWeightedfactors[] = new int[]{3, 7, 9, 10, 5, 8, 4, 2};

    private static String twLegalValue = "ABCDEFGHIJKMNOPQTUVWXZ";

    private static int firstCode[] = new int[]{10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 21, 22, 35, 23, 24, 27,
            28, 29, 32, 30, 33};

    private static int twWeightedfactors[] = new int[]{1,9,8,7,6,5,4,3,2,1,1};

    private static int chinaWeightedfactors[] = new int[]{7, 9, 10, 5, 8, 4, 2, 1 , 6, 3, 7, 9, 10, 5, 8, 4, 2};

    private static String chinaCodeValue[] = new String[]{"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2", "1"};

    private static String hkLegalValue = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static int hkWeightedfactors[] = new int[]{7, 6, 5, 4, 3, 2};
    
    /**
     * 自然人
     */
    private static final String ONE = "1";

    /**
     * 组织机构
     */
    private static final String TWO = "2";

    /**
     * 中证码
     * @param code
     * @return
     */
    private static boolean centerCode(String code) {
        if(code.length() == 16) {
            int sum = 0;
            for(int i = 0; i < 14; i++) {
                if(i < 3) {
                    if((int)code.charAt(i) >= 65 && (int)code.charAt(i) <= 90) {
                        sum = sum + centerWeightedfactors[i] * ((int)code.charAt(i) - 55);
                    }else {
                        sum = sum + centerWeightedfactors[i] * Integer.parseInt(code.substring(i, i+1));
                    }
                }else {
                    sum = sum + centerWeightedfactors[i] * Integer.parseInt(code.substring(i, i+1));
                }
            }
            if((sum % 97) +1 == Integer.parseInt(code.substring(14))) {
               return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 统一社会信用代码
     * @param code
     * @return
     */
    private static boolean creditCode(String code) {
        if (code.length() == 18 && Pattern.matches("^(1|5|9|Y)(1|2|3|9){1}[0-9]{6}[0-9A-HJ-NP-RT-UW-Y]{10}$", code)) {
            String anCode;
            //每个值权重
            int anCodeValue;
            //第i位加上加权因子的和
            int total = 0;
            for (int i = 0; i < 17; i ++) {
                anCode = code.substring(i, i + 1);
                anCodeValue = creditLegalValue.indexOf(anCode);
                total += anCodeValue * creditWeightedfactors[i];
            }
            //校验码
            int checkCode = 31 - (total % 31);
            if (checkCode > 31 && checkCode < 0 && !creditCheckCodeStr[checkCode].equals(code.substring(17, 18))) {
                return false;
            }
            return true;
        }
        return false;
    }


    /**
     * 组织机构代码
     * @param code
     * @return
     */
    private static boolean organizationCode(String code) {
        if (code.length() == 9 && Pattern.matches("^[0-9A-Z]+$", code)) {
            String anCode;
            //每个值权重
            int anCodeValue;
            //第i位加上加权因子的和
            int total = 0;
            for (int i = 0; i < 8; i++) {
                anCode = code.substring(i, i + 1);
                anCodeValue = organizationLegalValue.indexOf(anCode);
                total += anCodeValue * organizationWeightedfactors[i];
            }
            //校验码
            int checkCode = 11 - (total % 11);
            String checkValue;
            if (checkCode == 10) {
                checkValue = "X";
            } else if (checkCode == 11) {
                checkValue = "0";
            } else {
                checkValue = String.valueOf(checkCode);
            }
            if (code.substring(8, 9).equals(checkValue)) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 军官证
     * @param code
     * @return
     */
    private static boolean soldierCode(String code) {
        if (Pattern.matches("^[\\u4E00-\\u9FA5](字第)([0-9a-zA-Z]{4,8})(号?)$", code)) {
            return true;
        }
        return false;
    }

    /**
     * 其他证件
     * @param code
     * @return
     */
    private static boolean otherCode(String code){
        if (code.length() > 20 || code.length() < 1) {
            return false;
        }
        return true;
    }

    /**
     * tw 身份证
     * @param code
     * @return
     */
    private static boolean twCode(String code) {
        if (code.length() == 10 && Pattern.matches("^[A-KM-QT-XZ][0-9]{9}$", code)) {
            int anCode;
            int total = 0;
            for (int i = 0; i < 10; i++) {
                if (i == 0) {
                    anCode = firstCode[twLegalValue.indexOf(code.substring(0, 1))];
                    total += (anCode / 10) * twWeightedfactors[i];
                    total += (anCode % 10) * twWeightedfactors[i + 1];
                } else {
                    anCode = Integer.parseInt(code.substring(i, i + 1));
                    total += anCode * twWeightedfactors[i + 1];
                }
            }
            if (total % 10 == 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * 大陆身份证
     * @param code
     * @return
     */
    private static boolean chinaCode(String code) {
        //校验长度及规则
        if (code.length() == 18 && Pattern.matches("^\\d{17}([0-9]|X)$", code)) {
            //校验日期
            String year = code.substring(6, 10);
            String month = code.substring(10, 12);
            String day = code.substring(12, 14);
            boolean f = checkDate(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
            if (f) {
                int total = 0;
                for (int i = 0; i < 17; i++) {
                    total += Integer.parseInt(code.substring(i, i + 1)) * chinaWeightedfactors[i];
                }
                int validCode = total % 11;
                if (validCode > 10 || validCode < 0) {
                    return false;
                }

                if (code.substring(17, 18).equals(chinaCodeValue[validCode])) {
                    return true;
                }
                return false;
            }
            return false;
        }

        return false;
    }


    /**
     * 校验身份证日期
     * @param year
     * @param month
     * @param day
     * @return
     */
    private static boolean checkDate(int year, int month, int day) {
        if (year > 2100 || year < 1949) {
            return false;
        }
        if (month > 12 || month < 1) {
            return false;
        }
        if (day > getMaxDay(year, month) || day < 1) {
            return false;
        }
        return true;
    }

    /**
     * 求相应月份的最大日期
     * @param year
     * @param month
     * @return
     */
    private static int getMaxDay(int year, int month) {
        if (month == 4 || month == 6 || month == 9 || month == 11)
            return 30;
        if (month == 2)
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                return 29;
            else
                return 28;
        return 31;
    }

	/**
	 * 校验香港身份证是否正确
	 * @param card
	 * @return
	 */
    private static boolean checkHKCard(String card) {
        if (card.length() == 10 && Pattern.matches("^[A-Z]{1}[0-9]{6}\\(?[0-9A]\\)?$", card)) {
            String firstCode = card.substring(0, 1);
            int firstIndex = hkLegalValue.indexOf(firstCode) + 1;
            int total = 0;
            total = firstIndex * 8;
            for (int i = 0; i < 6; i++) {
                total += Integer.parseInt(card.substring(i + 1, i + 2)) * hkWeightedfactors[i];
            }
            int chekcCode = total % 11;
            String checkValue;
            if (chekcCode == 0) {
                checkValue = "1";
            } else if (chekcCode == 1) {
                checkValue = "A";
            } else {
                checkValue = String.valueOf(11 - chekcCode);
            }
            if (checkValue.equals(card.substring(8, 9))) {
                return true;
            }
            return false;
        }
        return false;
	}

	/**
	 * 校验澳门身份证
	 * @param card
	 * @return
	 */
    private static boolean checkOMCard(String card) {
		Pattern OMCard = Pattern.compile("^[1|5|7][0-9]{6}\\(?[0-9A-Z]\\)?$");
		Matcher matcher = OMCard.matcher(card);
		return matcher.find();
	}
	
	/**
	 * 校验护照
	 * @param card
	 * @return
	 */
    private static boolean checkHZCard(String card) {
    	if(card.length() > 12) {
    		return false;
    	}else {
    		Pattern HZCard = Pattern.compile("^[A-Z]{3}[0-9]{1,9}$");
    		Matcher matcher = HZCard.matcher(card);
    		return matcher.find();
    	}
	}
	
	/**
	 * 校验港澳居民来往内地通行证
	 * @param card
	 * @return
	 */
    private static boolean checkHMCard(String card) {
    	if(card.length() == 9 || card.length() == 11) {
    		Pattern HMCard = Pattern.compile("^[HM]{1}([0-9]{8}|[0-9]{10})$");
    		Matcher matcher = HMCard.matcher(card);
    		return matcher.find();
    	}
    	return false;
		
	}
	
	/**
	 * 校验台湾同胞来往内地通行证
	 * @param card
	 * @return
	 */
    private static boolean checkTWCard(String card) {
    	int len = card.length();
    	if(len == 8 || (len >= 10 && len <= 14)) {
    		String re1 = "^([0-9]{8}|[0-9]{10}[0-9A-Za-z]?[0-9A-Za-z]?)$";
    		String re2 = "^[0-9]{10}\\([A-Za-z]{1}\\)$";
    		String re3 = "^[0-9]{10}\\([0-9]{2}\\)$";
    		Pattern TWCard = Pattern.compile(re1);
    		Pattern TWCard2 = Pattern.compile(re2);
    		Pattern TWCard3 = Pattern.compile(re3);
    		Matcher matcher = TWCard.matcher(card);
    		Matcher matcher2 = TWCard2.matcher(card);
    		Matcher matcher3 = TWCard3.matcher(card);
    		if(matcher.find() || matcher2.find() || matcher3.find()) {
    			return true;
    		}
    	}
		return false;
	}
	
	/**
	 * 校验外国人居留证
	 * @param card
	 * @return
	 */
    private static boolean checkWaiPersonCard(String card) {
    	if(card.length() == 15) {
    		Pattern WaiPersonCard = Pattern.compile("^[A-Z]{3}[0-9]{12}$");
    		Matcher matcher = WaiPersonCard.matcher(card);
    		return matcher.find();
    	}
    	return false;
	}
	
	/**
	 * 校验户口本
	 * @param card
	 * @return
	 */
	private static boolean checkHuKouBen(String card) {
		/*if(card.length() == 9) {
			Pattern HuKouBenCard = Pattern.compile("^[a-zA-Z0-9]{9}$");
			Matcher matcher = HuKouBenCard.matcher(card);
			return matcher.find();
		}
		return false;*/
		return true;
	}

	/**
	 * 校验警官证
	 * @param card
	 * @return
	 */

	private static boolean checkPolice(String card) {
		return true;
	}

    /**
     * 校验证件类型方法
     * @param age1 身份类型 无可传空
     * @param age2 证件类型
     * @param age3 证件号码
     * @return
     */
	public static boolean codeVerification(String age1, String age2, String age3) {
        boolean f = false;
	    if (StringUtils.isNotBlank(age1) && StringUtils.isNotBlank(age2) && StringUtils.isNotBlank(age3)) {
            if (ONE.equals(age1)) {
                f = oneCheck(age2, age3);
            } else if (TWO.equals(age1)) {
                f = twoCheck(age2, age3);
            }
        } else if (StringUtils.isNotBlank(age2) && StringUtils.isNotBlank(age3)) {
	        f = oneCheck(age2, age3);
        }
        return f;
    }

    private static boolean oneCheck(String age2, String age3) {
        boolean result = false;
	    switch (age2) {
            case "1" :
                result = checkHuKouBen(age3);
                break;
            case "2" :
                result = checkHZCard(age3);
                break;
            case "5" :
                result = checkHMCard(age3);
                break;
            case "6" :
                result = checkTWCard(age3);
                break;
            case "8" :
                result = checkWaiPersonCard(age3);
                break;
            case "9" :
                result = checkPolice(age3);
                break;
            case "A" :
                result = checkHKCard(age3);
                break;
            case "B" :
                result = checkOMCard(age3);
                break;
            case "C" :
                result = twCode(age3);
                break;
            case "X" :
                result = otherCode(age3);
                break;
            case "10" :
                result = chinaCode(age3);
                break;
            case "20" :
                result = soldierCode(age3);
                break;
        }
        return result;
    }


    private static boolean twoCheck(String age2, String age3) {
        boolean result = false;
        switch (age2) {
            case "10" :
                result = centerCode(age3);
                break;
            case "20" :
                result = creditCode(age3);
                break;
            case "30" :
                result = organizationCode(age3);
                break;
        }
        return result;
    }

    @Test
    public void checkTest() {
        Assert.assertTrue(codeVerification("2", "30", "673026993"));
    }
}
