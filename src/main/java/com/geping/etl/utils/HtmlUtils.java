package com.geping.etl.utils;

import org.apache.commons.lang.StringUtils;

/**
 *  HTML 工具类
 * @author  liang.xu
 * @date 2021年5月8日
 */
public class HtmlUtils {

    /**
     * 转义特殊字符
     * @param source
     * @return
     */
    public static String htmlEncode(String source) {
        if (StringUtils.isBlank(source)) {
            return "";
        }
        String html = "";
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                case '<':
                    buffer.append("&lt;");
                    break;
                case '>':
                    buffer.append("&gt;");
                    break;
                case '&':
                    buffer.append("&amp;");
                    break;
              default:
                buffer.append(c);
             }
        }
        html = buffer.toString();
        return html;
    }

    public static void main(String[] args) {
        String v="测试---    999";
        System.out.println(HtmlUtils.htmlEncode(v));
    }
}