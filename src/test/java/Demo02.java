import javax.management.StringValueExp;

/**
 * @PACKAGE_NAME: PACKAGE_NAME
 * @USER: tangshuai
 * @DATE: 2021/5/21
 * @TIME: 17:38
 * @描述:
 */
 class Demo02 {
    public static String schoolName = "123" ; // 属于类，只有一份。
    public static void main(String[] args) {
        boolean equals = Demo02User.schoolName.equals("123");
        Demo02User user = new Demo02User();
        user.setName("张三");
        user.setAge("122");
        System.out.println("user = " + user);
    }
}
class Demo02User{
    public static String schoolName = "123" ; // 属于类，只有一份。
    private String name;
    private String age;

    @Override
    public String toString() {
        return "Demo02User{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        if (Integer.valueOf(age)>100||Integer.valueOf(age)<0){
            System.out.println("请输入正确年龄");
        }else{
            this.age = age;
        }
    }
}